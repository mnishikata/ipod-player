//
//  OrigamiTransformLayer.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface OrigamiTransformLayer : CATransformLayer
+ (OrigamiTransformLayer *)transformLayerFromImage:(UIImage *)image Frame:(CGRect)frame Duration:(CGFloat)duration AnchorPiont:(CGPoint)anchorPoint StartAngle:(double)start EndAngle:(double)end;
-(void)setProgress:(double)progress;

@property (nonatomic, readwrite) CGFloat myDuration;
@property (nonatomic, readwrite) double startAngle;
@property (nonatomic, readwrite) double endAngle;
@property (nonatomic, retain) CALayer* myShadowLayer;
@end

