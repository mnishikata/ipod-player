//
//  MNPinchGestureRecognizer.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MNPinchGestureRecognizer.h"

@implementation MNPinchGestureRecognizer
@synthesize distance = currentDistance_;
@synthesize initialDistance = initialDistance_;

- (id)initWithTarget:(UITableView*)target action:(SEL)action; {
	self = [super initWithTarget:(id)target action:(SEL)action];
	if (self) {

		
	}
	return self;
}

-(void)setState: (UIGestureRecognizerState)aState
{
	NSLog(@"%@ %@ %d value = %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__, aState);

	if( aState >= UIGestureRecognizerStateEnded )
	{
		[(UITableView*)self.view setScrollEnabled:YES];

	}
	
	
	[super setState: aState];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	NSLog(@"[touches count] %d  (all touches = %d)",[touches count], event.allTouches.count);
	
	// Ignore 3 or more touchs
	if( event.allTouches.count > 2 || self.state >= UIGestureRecognizerStateBegan )
	{
		for( UITouch* touch in touches )
		{
			[self ignoreTouch: touch forEvent:event];
		}
		
		return;
	}
	
	
	
	
	
	if( event.allTouches.count == 1 )
		self.state = UIGestureRecognizerStatePossible;
	
	if( event.allTouches.count == 2 )
	{
		// Ignore touches outside of this view
		for( UITouch *touch in event.allTouches )
		{
			CGPoint point = [touch locationInView:self.view];
			if( !CGRectContainsPoint(self.view.bounds, point) )
			{
				for( UITouch* touch in touches )
				{
					[self ignoreTouch: touch forEvent:event];
				}
				
				return;
			}
		}
		
		self.state = UIGestureRecognizerStateBegan;

				
		[(UITableView*)self.view setScrollEnabled:NO];
		
		NSArray* allTouches = event.allTouches.allObjects;
		

		initialLocationInViewA_ = [[allTouches objectAtIndex:0] locationInView:self.view];
		lastLocationInViewA_ = initialLocationInViewA_;

		initialLocationInViewB_ = [[allTouches objectAtIndex:1] locationInView:self.view];
		lastLocationInViewB_ = initialLocationInViewB_;

		initialDistance_ = sqrtf( (initialLocationInViewA_.x - initialLocationInViewB_.x)*(initialLocationInViewA_.x - initialLocationInViewB_.x) + (initialLocationInViewA_.y - initialLocationInViewB_.y)*(initialLocationInViewA_.y - initialLocationInViewB_.y));

		currentDistance_ = initialDistance_;
	}
	
	[super touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event];

	
	
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if( self.state <= UIGestureRecognizerStatePossible )
	{
		[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
		return;
	}


	if( event.allTouches.count == 2 )
	{
		NSArray* allTouches = event.allTouches.allObjects;
		
		
		CGPoint locationA_ = [[allTouches objectAtIndex:0] locationInView:self.view];
		
		CGPoint locationB_ = [[allTouches objectAtIndex:1] locationInView:self.view];

		
		currentDistance_ = sqrtf( (locationA_.x - locationB_.x)*(locationA_.x - locationB_.x) + (locationA_.y - locationB_.y)*(locationA_.y - locationB_.y));
		
		//NSLog(@"Distance %.1f (%.0f)",distance, distance/initialDistance_*100);
	}
	
	
	self.state = UIGestureRecognizerStateChanged;	

	[super touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event];
	
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	if( self.state == UIGestureRecognizerStatePossible )
	{
		self.state = UIGestureRecognizerStateFailed;
	}
	
	if( self.state ==  UIGestureRecognizerStateBegan || 
			  self.state == UIGestureRecognizerStateChanged  )
	{
		self.state = UIGestureRecognizerStateEnded;
	}
	
	
	
	[super touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event];
	
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;
{


	if( self.state != UIGestureRecognizerStateFailed &&
	   self.state != UIGestureRecognizerStateEnded ) 
	{
		
		self.state = UIGestureRecognizerStateCancelled;
	}
	
	
	[super touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event];
	
}


- (void)reset
{
	[super reset];

}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
	NSLog(@"can be prevented by Class %@", NSStringFromClass( [preventingGestureRecognizer class] ));
	if( self.state >= UIGestureRecognizerStateBegan ) return NO;
	
	NSLog(@"can be prevented");
	return YES;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
	NSLog(@"can prevent Class %@", NSStringFromClass( [preventingGestureRecognizer class] ));

	
	if( self.state == UIGestureRecognizerStateBegan || self.state == UIGestureRecognizerStateChanged  )
	{

//		
		//２本指以上のタッチは阻止
		if( [preventingGestureRecognizer numberOfTouches] > 1 ) return YES;
		
		CGPoint theirLocation = [preventingGestureRecognizer locationInView:self.view];
		
		if( CGPointEqualToPoint(theirLocation, [self locationInView:self.view] ) ) return YES;
		

		
		NSLog(@"No");
		return NO;
	}
	
	NSLog(@"No");

	return NO;
}

#pragma mark - Accessors

-(CGPoint)initialTopLocation
{
	if( initialLocationInViewA_.y > initialLocationInViewB_.y ) return initialLocationInViewB_;
	else return initialLocationInViewA_;
}

-(CGPoint)initialBottomLocation
{
	if( initialLocationInViewA_.y < initialLocationInViewB_.y ) return initialLocationInViewB_;
	else return initialLocationInViewA_;

}



@end
