//
//  OrigamiLayer.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>


@interface OrigamiLayer : CALayer
{
	NSMutableArray* transformLayers_;
}
@property (nonatomic, readwrite) NSUInteger numberOfFolds;

@end