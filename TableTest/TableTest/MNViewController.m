//
//  MNViewController.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MNViewController.h"
#import "MNPinchGestureRecognizer.h"
#import "OrigamiLayer.h"
#import "OrigamiTransformLayer.h"
#define  ANIMATION_DURATION 2


@interface UITableViewCell (LayerDrawing)
-(void)mn_drawContentsInContext:(CGContextRef)context;
@end


@implementation UITableViewCell (LayerDrawing)

-(void)mn_drawContentsInContext:(CGContextRef)context
{
	
	UIGraphicsPushContext(context);
	[[UIColor blackColor] set];
	[self.textLabel.text drawAtPoint:CGPointMake(10, 10) withFont:[UIFont boldSystemFontOfSize:18]];
	UIGraphicsPopContext();
}

@end


@interface MNViewController ()

@end

@implementation MNViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
		 dataSource_ = [[NSArray alloc] initWithObjects:@"Row1", @"Row2", @"Row3", @"Row4", @"Row5",@"Row6",@"Row7",@"Row8",@"Row9",@"Row10",@"Row11",@"Row12",@"Row13",@"Row14",@"Row15",@"Row16", nil];
		
		 subDataSource_ = [[NSArray alloc] initWithObjects:@"Subrow1", @"Subrow2", @"Subrow3", @"Subrow4",  @"Subrow5",  @"Subrow6",nil];
		 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	
	MNPinchGestureRecognizer* gesture = [[MNPinchGestureRecognizer alloc] initWithTarget:self action:@selector(gesture:)];
	[self.tableView addGestureRecognizer: gesture];
}

-(void)gesture:(MNPinchGestureRecognizer*)gesture
{
	if( gesture.initialDistance - 20 > gesture.distance )
	{
		NSIndexPath* topIndexPath = [self.tableView indexPathForRowAtPoint: gesture.initialTopLocation];
		NSIndexPath* bottomIndexPath = [self.tableView indexPathForRowAtPoint: gesture.initialBottomLocation];

		if( bottomIndexPath.row - topIndexPath.row > 1 )
		{
			CGRect touchedCellRect = [self.tableView rectForRowAtIndexPath: topIndexPath];
			
			NSMutableArray* insertingCell = [NSMutableArray array];
			
			CGFloat pointY = 0;
			for( NSUInteger hoge = topIndexPath.row+1; hoge < bottomIndexPath.row; hoge++ )
			{
				UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];	
				cell.textLabel.text = [dataSource_ objectAtIndex: hoge];
				
				cell.frame = CGRectMake(0, pointY, self.tableView.bounds.size.width, 44);
				[insertingCell addObject: cell];
			}
			
			gesture.state = UIGestureRecognizerStateCancelled;		
			
			[self.tableView hideOrigamiTransitionWithCells:insertingCell duration:ANIMATION_DURATION toY: CGRectGetMaxY(touchedCellRect)  completion:^(BOOL finished) {
				[self.tableView reloadData];
			}];
				
		}
		
	}
	
	NSLog(@"Action!");
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return dataSource_.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
	if( !cell )
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    
	cell.textLabel.text = [dataSource_ objectAtIndex:indexPath.row];

	return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
	
	CGRect touchedCellRect = [tableView rectForRowAtIndexPath: indexPath];
	
	NSMutableArray* insertingCell = [NSMutableArray array];
	
	CGFloat pointY = 0;
	for( NSUInteger hoge = 0; hoge < subDataSource_.count; hoge++ )
	{
		NSString* source = [subDataSource_ objectAtIndex: hoge];
		UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];	
		cell.textLabel.text = source;
		
		cell.frame = CGRectMake(0, pointY, tableView.bounds.size.width, 44);
		[insertingCell addObject: cell];
	}
	

[self.tableView hideOrigamiTransitionWithCells:insertingCell duration:ANIMATION_DURATION toY: CGRectGetMaxY(touchedCellRect)  completion:^(BOOL finished) {
	[tableView reloadData];
}];
 


//	[self.tableView showOrigamiTransitionWithCells: insertingCell 
//													  duration: ANIMATION_DURATION 
//															 fromY: CGRectGetMaxY(touchedCellRect) 
//													completion: ^(BOOL finished){ 
//														[tableView reloadData];
//													}];
	

}


@end



@implementation UITableView (Origami)

KeyframeParametricBlock openFunction = ^double(double time) {
	return sin(time*M_PI_2);
};
KeyframeParametricBlock closeFunction = ^double(double time) {
	return -cos(time*M_PI_2)+1;
};

//- (CATransformLayer *)transformLayerFromImage:(UIImage *)image Frame:(CGRect)frame Duration:(CGFloat)duration AnchorPiont:(CGPoint)anchorPoint StartAngle:(double)start EndAngle:(double)end;
//{
//	
//	CATransformLayer *jointLayer = [CATransformLayer layer];
//	jointLayer.anchorPoint = anchorPoint;
//
//	CGFloat layerHeight;
//
//	layerHeight = image.size.height - frame.origin.y;
//	jointLayer.frame = CGRectMake(0, 0,  frame.size.width, layerHeight );
//	if (frame.origin.y) {
//		jointLayer.position = CGPointMake(frame.size.width/2, frame.size.height);
//	}
//	else {
//		jointLayer.position = CGPointMake(frame.size.width/2, 0);
//	}
//
//	
//	//map image onto transform layer
//	CALayer *imageLayer = [CALayer layer];
//	imageLayer.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
//	imageLayer.anchorPoint = anchorPoint;
//	imageLayer.edgeAntialiasingMask = 0;
//	imageLayer.opaque = YES;
//	imageLayer.position = CGPointMake( frame.size.width/2, layerHeight*anchorPoint.y);
//	[jointLayer addSublayer:imageLayer];
//	
//
//	if( CGRectGetMaxY(frame) > image.size.height )
//	{
//		imageLayer.backgroundColor = [UIColor whiteColor].CGColor;
//		
//	}else {
//		
//		CGImageRef imageCrop = CGImageCreateWithImageInRect(image.CGImage, frame);
//		imageLayer.contents = (__bridge id)imageCrop;
//		imageLayer.backgroundColor = [UIColor clearColor].CGColor;
//
//	}
//		
//	//add shadow
//	NSInteger index = frame.origin.y/frame.size.height;
//	double shadowAniOpacity;
//	CALayer *shadowLayer = [CALayer layer];
//	shadowLayer.frame = imageLayer.bounds;
//	shadowLayer.backgroundColor = [UIColor darkGrayColor].CGColor;
//	shadowLayer.opacity = 0.0;
//	shadowLayer.edgeAntialiasingMask = 0;
//	shadowLayer.backgroundColor = [UIColor blackColor].CGColor;
//	if (index%2) {
////		shadowLayer.startPoint = CGPointMake( 0.5, 0);
////		shadowLayer.endPoint = CGPointMake( 0.5, 1);
//		shadowAniOpacity = (anchorPoint.y)?0.12:0.14;
//	}
//	else {
////		shadowLayer.startPoint = CGPointMake( 0.5, 1);
////		shadowLayer.endPoint = CGPointMake( 0.5, 0);
//		shadowAniOpacity = (anchorPoint.y)?0.28:0.24;
//	}
//	[imageLayer addSublayer:shadowLayer];
//	
//	//animate open/close animation
//	CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
//	[animation setDuration:duration];
//	[animation setFromValue:[NSNumber numberWithDouble:start]];
//	[animation setToValue:[NSNumber numberWithDouble:end]];
//	[animation setRemovedOnCompletion:NO];
//	[jointLayer addAnimation:animation forKey:@"jointAnimation"];
//	
//	//animate shadow opacity
//	animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
//	[animation setDuration:duration];
//	[animation setFromValue:[NSNumber numberWithDouble:(start)?shadowAniOpacity:0]];
//	[animation setToValue:[NSNumber numberWithDouble:(start)?0:shadowAniOpacity]];
//	[animation setRemovedOnCompletion:NO];
//	[shadowLayer addAnimation:animation forKey:nil];
//	
//	return jointLayer;
//}

- (void)showOrigamiTransitionWithCells:(NSArray *)cellArray duration:(CGFloat)duration fromY:(CGFloat)pointY completion:(void (^)(BOOL finished))completion
{
	
	self.userInteractionEnabled = NO;
	
	// Look for pushing cells
	
	NSMutableArray* pushingViews = [NSMutableArray array];
	for( UIView *subview in self.subviews )
	{
		if( [subview isKindOfClass:[UITableViewCell class]] &&
			subview.frame.origin.y >= pointY  )
		{
			[pushingViews addObject: subview];
		}
	}
	
	
	CGSize totalSize = CGSizeZero;
	
	for (UITableViewCell* cell in cellArray )
	{
		totalSize.width = MAX( totalSize.width, cell.frame.size.width);
		totalSize.height += cell.frame.size.height;
	}
	
	CGFloat imageHeight = MIN(self.frame.size.height, totalSize.height);
	
	UIGraphicsBeginImageContextWithOptions(CGSizeMake(totalSize.width, imageHeight), YES, 1.0 );
	
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	
	CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
	CGContextFillRect(context, CGRectMake(0, 0, totalSize.width, imageHeight));
	for( NSUInteger hoge = 0; hoge < cellArray.count; hoge++ )
	{
		UITableViewCell* cell = [cellArray objectAtIndex: hoge];
		if( ![cell isKindOfClass:[UITableViewCell class]] ) continue;
		
		[cell mn_drawContentsInContext:context];
		// これはうまく機能しない
		
		CGAffineTransform t = CGAffineTransformMakeTranslation(0, cell.frame.size.height);
		CGContextConcatCTM(context, t);
		
		CGPoint points[2];
		points[0] = CGPointMake(0, 0.5);
		points[1] = CGPointMake(totalSize.width, 0.5);
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:224.0/255.0 alpha:1].CGColor);
		CGContextStrokeLineSegments(context, points, 2);
	}
	
	UIImage *viewSnapShot = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	
	//set 3D depth
	OrigamiLayer *origamiLayer = [OrigamiLayer layer];
	origamiLayer.frame = CGRectMake(0, pointY, totalSize.width, self.bounds.size.height - (pointY - self.contentOffset.y));

	origamiLayer.contents = viewSnapShot;
	origamiLayer.numberOfFolds = MAX( 1, (int)floorf( cellArray.count /2.0 ) );
	[origamiLayer setNeedsLayout];

	[self.layer addSublayer:origamiLayer];
	
	

	
	// Pushing views
	
	CGFloat pushingLayerHeight = self.bounds.size.height - (pointY - self.contentOffset.y);
	CALayer* pushingLayer = [CALayer layer];
	pushingLayer.frame = CGRectZero;
	pushingLayer.edgeAntialiasingMask = 0;
	pushingLayer.opaque = YES;
	[origamiLayer addSublayer:pushingLayer];
	
	if( pushingLayerHeight > 0 )
	{
		pushingLayer.frame = CGRectMake(0, 0, totalSize.width, pushingLayerHeight);
		
		UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.bounds.size.width, pushingLayerHeight), YES, 1.0 );
		
		
		context = UIGraphicsGetCurrentContext();
		CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
		CGContextFillRect(context, CGRectMake(0, 0, self.bounds.size.width, pushingLayerHeight));
		
		for( UITableViewCell* cell in pushingViews )
		{
			if( ![cell isKindOfClass:[UITableViewCell class]] ) continue;
			
			CGContextSaveGState(context);
			
			CGAffineTransform t = CGAffineTransformMakeTranslation(0, cell.frame.origin.y  - pointY);
			CGContextConcatCTM(context, t);
			
			[cell mn_drawContentsInContext:context];
			
			CGPoint points[2];
			points[0] = CGPointMake(0,  0.5);
			points[1] = CGPointMake(totalSize.width, + 0.5);
			CGContextSetLineWidth(context, 1.0);
			CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:224.0/255.0 alpha:1].CGColor);
			CGContextStrokeLineSegments(context, points, 2);
			
			CGContextRestoreGState(context);
		}
		
		UIImage *pushingViewSnapShot = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		pushingLayer.contents = (__bridge id)pushingViewSnapShot.CGImage;
		
		pushingLayer.anchorPoint = CGPointMake(0.5, 0);
		
	}
	
	[CATransaction begin];
	[CATransaction setCompletionBlock:^{
		
		[origamiLayer removeFromSuperlayer];
		
		
		if (completion)
			completion(YES);
		
		self.userInteractionEnabled = YES;
		
	}];
	//	
	
	
	[CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
	CAAnimation *openAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position.y" function:openFunction fromValue:0 toValue:totalSize.height];
	openAnimation.fillMode = kCAFillModeForwards;
	[openAnimation setRemovedOnCompletion:NO];
	[pushingLayer addAnimation:openAnimation forKey:@"position"];
	
	[CATransaction commit];
}


-(void)hideOrigamiTransitionWithCells:(NSArray*)cellArray duration: (CGFloat)duration toY: (CGFloat)pointY completion:(void (^)(BOOL finished))completion
{
	// Reload before call this method
	
	self.userInteractionEnabled = NO;
	
	// Look for pulling cells
	
	NSMutableArray* pullingViews = [NSMutableArray array];
	for( UIView *subview in self.subviews )
	{
		if( [subview isKindOfClass:[UITableViewCell class]] &&
			subview.frame.origin.y >= pointY  )
		{
			[pullingViews addObject: subview];
		}
	}
	
	
	CGSize totalSize = CGSizeZero;
	
	for (UITableViewCell* cell in cellArray )
	{
		totalSize.width = MAX( totalSize.width, cell.frame.size.width);
		totalSize.height += cell.frame.size.height;
	}
	
	CGFloat imageHeight = MIN(self.frame.size.height, totalSize.height);
	
	UIGraphicsBeginImageContextWithOptions(CGSizeMake(totalSize.width, imageHeight), YES, 1.0 );
	
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	
	CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
	CGContextFillRect(context, CGRectMake(0, 0, totalSize.width, imageHeight));
	for( NSUInteger hoge = 0; hoge < cellArray.count; hoge++ )
	{
		UITableViewCell* cell = [cellArray objectAtIndex: hoge];
		if( ![cell isKindOfClass:[UITableViewCell class]] ) continue;
		
		[cell mn_drawContentsInContext:context];
		// これはうまく機能しない
		
		CGAffineTransform t = CGAffineTransformMakeTranslation(0, cell.frame.size.height);
		CGContextConcatCTM(context, t);
		
		CGPoint points[2];
		points[0] = CGPointMake(0, 0.5);
		points[1] = CGPointMake(totalSize.width, 0.5);
		CGContextSetLineWidth(context, 1.0);
		CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:224.0/255.0 alpha:1].CGColor);
		CGContextStrokeLineSegments(context, points, 2);
	}
	
	UIImage *viewSnapShot = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	
	//set 3D depth
	OrigamiLayer *origamiLayer = [OrigamiLayer layer];
	origamiLayer.frame = CGRectMake(0, pointY, totalSize.width, totalSize.height);

	origamiLayer.contents = viewSnapShot;
	origamiLayer.numberOfFolds = MAX( 1, (int)floorf( cellArray.count /2.0 ) );

	[origamiLayer setNeedsLayout];
	
	
	[self.layer addSublayer:origamiLayer];
	
	
	


	// Pushing views
	
	CGFloat pullingLayerHeight = self.bounds.size.height - (pointY - self.contentOffset.y);
	CALayer* pullingLayer = [CALayer layer];
	pullingLayer.frame = CGRectZero;
	pullingLayer.edgeAntialiasingMask = 0;
	pullingLayer.opaque = YES;
	[origamiLayer addSublayer:pullingLayer];
	
	if( pullingLayerHeight > 0 )
	{
		pullingLayer.frame = CGRectMake(0, 0, totalSize.width, pullingLayerHeight);
		
		UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.bounds.size.width, pullingLayerHeight), YES, 1.0 );
		
		
		context = UIGraphicsGetCurrentContext();
		CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
		CGContextFillRect(context, CGRectMake(0, 0, self.bounds.size.width, pullingLayerHeight));
		
		for( UITableViewCell* cell in pullingViews )
		{
			if( ![cell isKindOfClass:[UITableViewCell class]] ) continue;
			
			CGContextSaveGState(context);
			
			CGAffineTransform t = CGAffineTransformMakeTranslation(0, cell.frame.origin.y  - pointY);
			CGContextConcatCTM(context, t);
			
			[cell mn_drawContentsInContext:context];
			
			CGPoint points[2];
			points[0] = CGPointMake(0,  0.5);
			points[1] = CGPointMake(totalSize.width, + 0.5);
			CGContextSetLineWidth(context, 1.0);
			CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:224.0/255.0 alpha:1].CGColor);
			CGContextStrokeLineSegments(context, points, 2);
			
			CGContextRestoreGState(context);
		}
		
		UIImage *pushingViewSnapShot = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		pullingLayer.contents = (__bridge id)pushingViewSnapShot.CGImage;
		
		pullingLayer.anchorPoint = CGPointMake(0.5, 0);
		
	}
	
	[CATransaction begin];
	[CATransaction setCompletionBlock:^{
		
		[origamiLayer removeFromSuperlayer];
		
		
		if (completion)
			completion(YES);
		
		self.userInteractionEnabled = YES;
		
	}];
	//	
	
	
	[CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
	CAAnimation *openAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position.y" function:closeFunction fromValue:totalSize.height toValue:0];
	openAnimation.fillMode = kCAFillModeForwards;
	[openAnimation setRemovedOnCompletion:NO];
	[pullingLayer addAnimation:openAnimation forKey:@"position"];
	
	[CATransaction commit];
}


@end


@implementation CAKeyframeAnimation (Parametric)

+ (id)animationWithKeyPath:(NSString *)path function:(KeyframeParametricBlock)block fromValue:(double)fromValue toValue:(double)toValue 
{
	// get a keyframe animation to set up
	CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:path];
	// break the time into steps (the more steps, the smoother the animation)
	NSUInteger steps = 100;
	NSMutableArray *values = [NSMutableArray arrayWithCapacity:steps];
	double time = 0.0;
	double timeStep = 1.0 / (double)(steps - 1);
	for(NSUInteger i = 0; i < steps; i++) {
		double value = fromValue + (block(time) * (toValue - fromValue));
		[values addObject:[NSNumber numberWithDouble:value]];
		time += timeStep;
	}
	// we want linear animation between keyframes, with equal time steps
	animation.calculationMode = kCAAnimationLinear;
	// set keyframes and we're done
	[animation setValues:values];
	return(animation);
}

@end