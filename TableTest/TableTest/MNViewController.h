//
//  MNViewController.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface MNViewController : UITableViewController

{
	NSArray* dataSource_;
	NSArray* subDataSource_;

}

@end




@interface UITableView (Origami)
- (CATransformLayer *)transformLayerFromImage:(UIImage *)image Frame:(CGRect)frame Duration:(CGFloat)duration AnchorPiont:(CGPoint)anchorPoint StartAngle:(double)start EndAngle:(double)end;
- (void)showOrigamiTransitionWithCells:(NSArray *)cellArray duration:(CGFloat)duration fromY:(CGFloat)pointY completion:(void (^)(BOOL finished))completion;
-(void)hideOrigamiTransitionWithCells:(NSArray*)cells duration: (CGFloat)duration toY: (CGFloat)pointY completion:(void (^)(BOOL finished))completion;

@end


typedef double (^KeyframeParametricBlock)(double);
@interface CAKeyframeAnimation (Parametric)

+ (id)animationWithKeyPath:(NSString *)path 
                  function:(KeyframeParametricBlock)block
                 fromValue:(double)fromValue
                   toValue:(double)toValue;

@end
