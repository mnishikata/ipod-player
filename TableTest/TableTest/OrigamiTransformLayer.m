//
//  OrigamiTransformLayer.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrigamiTransformLayer.h"

@implementation  OrigamiTransformLayer
@synthesize myDuration, startAngle, endAngle, myShadowLayer;


+ (CATransformLayer *)transformLayerFromImage:(UIImage *)image Frame:(CGRect)frame Duration:(CGFloat)duration AnchorPiont:(CGPoint)anchorPoint StartAngle:(double)start EndAngle:(double)end;
{
	
	OrigamiTransformLayer *jointLayer = [OrigamiTransformLayer layer];
	jointLayer.anchorPoint = anchorPoint;
	
	CGFloat layerHeight;
	
	layerHeight = image.size.height - frame.origin.y;
	jointLayer.frame = CGRectMake(0, 0,  frame.size.width, layerHeight );
	if (frame.origin.y) {
		jointLayer.position = CGPointMake(frame.size.width/2, frame.size.height);
	}
	else {
		jointLayer.position = CGPointMake(frame.size.width/2, 0);
	}
	
	
	//map image onto transform layer
	CALayer *imageLayer = [CALayer layer];
	imageLayer.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
	imageLayer.anchorPoint = anchorPoint;
	imageLayer.edgeAntialiasingMask = 0;
	imageLayer.opaque = YES;
	imageLayer.position = CGPointMake( frame.size.width/2, layerHeight*anchorPoint.y);
	[jointLayer addSublayer:imageLayer];
	
	
	if( CGRectGetMaxY(frame) > image.size.height )
	{
		imageLayer.backgroundColor = [UIColor whiteColor].CGColor;
		
	}else {
		
		CGImageRef imageCrop = CGImageCreateWithImageInRect(image.CGImage, frame);
		imageLayer.contents = (__bridge id)imageCrop;
		imageLayer.backgroundColor = [UIColor clearColor].CGColor;
		
	}
	
	//add shadow
	NSInteger index = frame.origin.y/frame.size.height;
	double shadowAniOpacity;
	CALayer *shadowLayer = [CALayer layer];
	shadowLayer.frame = imageLayer.bounds;
	shadowLayer.backgroundColor = [UIColor darkGrayColor].CGColor;
	shadowLayer.opacity = 0.0;
	shadowLayer.edgeAntialiasingMask = 0;
	shadowLayer.backgroundColor = [UIColor blackColor].CGColor;
	if (index%2) {
		//		shadowLayer.startPoint = CGPointMake( 0.5, 0);
		//		shadowLayer.endPoint = CGPointMake( 0.5, 1);
		shadowAniOpacity = (anchorPoint.y)?0.12:0.14;
	}
	else {
		//		shadowLayer.startPoint = CGPointMake( 0.5, 1);
		//		shadowLayer.endPoint = CGPointMake( 0.5, 0);
		shadowAniOpacity = (anchorPoint.y)?0.28:0.24;
	}
	[imageLayer addSublayer:shadowLayer];
	
	//animate open/close animation
	CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
	[animation setDuration:duration];
	[animation setFromValue:[NSNumber numberWithDouble:start]];
	[animation setToValue:[NSNumber numberWithDouble:end]];
	[animation setRemovedOnCompletion:NO];
	[jointLayer addAnimation:animation forKey:@"jointAnimation"];
	
	//animate shadow opacity
	animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	[animation setDuration:duration];
	[animation setFromValue:[NSNumber numberWithDouble:(start)?shadowAniOpacity:0]];
	[animation setToValue:[NSNumber numberWithDouble:(start)?0:shadowAniOpacity]];
	[animation setRemovedOnCompletion:NO];
	[shadowLayer addAnimation:animation forKey:nil];
	
	return jointLayer;
}

-(void)setProgress:(double)progress
{
	CATransform3D t = CATransform3DIdentity;
	//	t = CATransform3DMakeRotation( endAngle * progress + startAngle * (1-progress), 1, 0, 0);
	
	self.transform = t;
	
	
	//	shadowLayer.opacity = (start)?shadowAniOpacity: 0;
	
}

@end
