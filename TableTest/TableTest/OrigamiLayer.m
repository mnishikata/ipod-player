//
//  OrigamiLayer.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 2/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrigamiLayer.h"
#import "OrigamiTransformLayer.h"

@implementation OrigamiLayer

@synthesize numberOfFolds;

- (id)init
{
	self = [super init];
	if (self) {
		CATransform3D transform = CATransform3DIdentity;
		transform.m34 = +1.0/800.0;
		self.sublayerTransform = transform;
		self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1].CGColor;
		
		transformLayers_ = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)dealloc
{
	transformLayers_ = nil;	
}

- (void)layoutSublayers
{
	for( CALayer* layer in transformLayers_ )
	{
		[layer removeFromSuperlayer];
	}
	
	//setup rotation angle
	double endAngle;
	
	CGFloat frameWidth = self.bounds.size.width;
	CGFloat frameHeight = self.bounds.size.height;
	CGFloat foldHeight = frameHeight/(numberOfFolds*2);
	CALayer *prevLayer = self;
	
	CGPoint anchorPoint = CGPointMake(0.5, 0);
	
	
	for (int b=0; b < numberOfFolds*2; b++) 
	{
		CGRect imageFrame;
		
		if(b == 0)
			endAngle = M_PI_2;
		else {
			if (b%2)
				endAngle = -M_PI;
			else
				endAngle = M_PI;
		}
		imageFrame = CGRectMake(0, b*foldHeight, frameWidth, foldHeight);
		
		OrigamiTransformLayer *transLayer = [OrigamiTransformLayer transformLayerFromImage:self.contents Frame:imageFrame Duration:2 AnchorPiont:anchorPoint StartAngle:0 EndAngle:endAngle];
		[prevLayer addSublayer:transLayer]; //次々にビューを追加している。親、子、孫、、
		prevLayer = transLayer;
		
		[transformLayers_ addObject: transLayer];
		
		[transLayer setProgress: 0.5];
		
	}
	
}

-(void)setProgress:(double)progress
{
	for( OrigamiTransformLayer *layer in transformLayers_ ){
		[layer setProgress: 0.5];
	}
}

@end
