//
//  main.m
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MNAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MNAppDelegate class]));
	}
}
