//
//  MNPinchGestureRecognizer.h
//  TableTest
//
//  Created by Nishikata Masatoshi on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>
#import <UIKit/UIGestureRecognizer.h>
@interface MNPinchGestureRecognizer : UIGestureRecognizer
{
	
	
	CGPoint initialLocationInViewA_;
	CGPoint initialLocationInViewB_;
	CGFloat initialDistance_;
	
	CGFloat currentDistance_;

	CGPoint lastLocationInViewA_;
	CGPoint lastLocationInViewB_;

}

-(CGFloat)distance;

@property (nonatomic, readonly) CGFloat distance;
@property (nonatomic, readonly) CGFloat initialDistance;

@property (nonatomic, readonly) CGPoint initialTopLocation;
@property (nonatomic, readonly) CGPoint initialBottomLocation;

@end


