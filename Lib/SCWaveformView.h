//
//  SCWaveformView.h
//  SCWaveformView
//
//  Created by Simon CORSIN on 24/01/14.
//  Copyright (c) 2014 Simon CORSIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface SCWaveformView : NSObject

typedef struct Bracket {
	Float32 bottom;
	Float32 top;
} Bracket;

+ (void)generateWaveforms:(AVAsset*)asset timeRange:(CMTimeRange)timeRange color:(UIColor*)color size:(CGSize)size antialias:(BOOL)antialiasingEnabled force:(BOOL)force completion:(void (^)(UIImage* theImage))completion ;


@end
