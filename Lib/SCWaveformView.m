//
//  SCWaveformView.m
//  SCWaveformView
//
//  Created by Simon CORSIN on 24/01/14.
//  Copyright (c) 2014 Simon CORSIN. All rights reserved.
//

#import "SCWaveformView.h"

#define absX(x) (x < 0 ? 0 - x : x)
#define minMaxX(x, mn, mx) (x <= mn ? mn : (x >= mx ? mx : x))
#define noiseFloor (-50.0)
#define decibel(amplitude) (20.0 * log10(amplitude) - 90)

#define ENABLE_CANCEL
#define  gIsRunningOnIPad (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)

#define CANCEL_MARGIN 15

#define BRACKETS {32768, 28183, 25118, 22387, 19952, 17782, 15848, 14125, 12589, 11220, 10000, 8912, 7943, 7079, 6309, 5623, 5011, 4466, 3981, 3548, 3162, 2818, 2511, 2238, 1995, 1778, 1584, 1412, 1258, 1122, 1000, 891, 794, 707, 630, 562, 501, 446, 398, 354, 316, 281, 251, 223, 199, 177, 158, 141, 125, 112, 100}


@interface SCWaveformView() {
    UIImageView *_normalImageView;
//    UIImageView *_progressImageView;
    UIView *_cropNormalView;
    UIView *_cropProgressView;
//    BOOL _normalColorDirty;
//    BOOL _progressColorDirty;
}

@end

@implementation SCWaveformView


//- (instancetype)init {
//    self = [super init];
//    
//    if (self) {
//        [self commonInit];
//    }
//    
//    return self;
//}

//
//- (void)commonInit
//{
//    _normalImageView = [[UIImageView alloc] init];
////    _progressImageView = [[UIImageView alloc] init];
//    _cropNormalView = [[UIView alloc] init];
//    _cropProgressView = [[UIView alloc] init];
//    
//    _cropNormalView.clipsToBounds = YES;
//    _cropProgressView.clipsToBounds = YES;
//	
//	_timeRange = CMTimeRangeMake(kCMTimeZero, kCMTimePositiveInfinity);
//    
//    [_cropNormalView addSubview:_normalImageView];
//    [_cropProgressView addSubview:_progressImageView];
//    
//    [self addSubview:_cropNormalView];
//    [self addSubview:_cropProgressView];
//    
//    self.normalColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.4];
////    self.progressColor = [UIColor redColor];
//   
//    _normalColorDirty = NO;
//    _progressColorDirty = NO;
//}
//
void SCRenderPixelWaveformInContext(CGContextRef context, float halfGraphHeight, double sample, float x)
{
    float pixelHeight = halfGraphHeight * (1 - sample / noiseFloor);
    
    if (pixelHeight < 0) {
        pixelHeight = 0;
    }
    
    CGContextMoveToPoint(context, x+0.5, halfGraphHeight - pixelHeight);
    CGContextAddLineToPoint(context, x+0.5, halfGraphHeight + pixelHeight);
    CGContextStrokePath(context);

}

+ (void)renderWaveformInContext:(CGContextRef)context asset:(AVAsset *)asset timeRange:(CMTimeRange)timeRange withColor:(UIColor *)color andSize:(CGSize)size antialiasingEnabled:(BOOL)antialiasingEnabled task:(double)task
{
	
	
	if (asset == nil) {
		return;
	}
	
#ifdef ENABLE_CANCEL
	if( task != 0 && abs( task - currentTask_ ) > CANCEL_MARGIN ) return;
#endif
	
	CGFloat pixelRatio = 1;//[UIScreen mainScreen].scale;
	size.width *= pixelRatio;
	size.height *= pixelRatio;
	
	CGFloat widthInPixels = size.width;
	CGFloat heightInPixels = size.height;
	
	NSError *error = nil;
	AVAssetReader *reader = [[AVAssetReader alloc] initWithAsset:asset error:&error];
   
	reader.timeRange = timeRange;
	
	
	CMTime duration = asset.duration;
	
	Float64 contentRatio = 1.0;
	
	if( !CMTIME_IS_INDEFINITE(timeRange.duration) )
	{
		contentRatio = CMTimeGetSeconds( timeRange.duration ) / CMTimeGetSeconds( duration );
	}
	
	
	NSArray *audioTrackArray = [asset tracksWithMediaType:AVMediaTypeAudio];
	
	if (audioTrackArray.count == 0) {
		return;
	}
	
	
	AVAssetTrack *songTrack = [audioTrackArray objectAtIndex:0];
	
	NSDictionary *outputSettingsDict = [[NSDictionary alloc] initWithObjectsAndKeys:
													[NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
													[NSNumber numberWithInt:16], AVLinearPCMBitDepthKey,
													[NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey,
													[NSNumber numberWithBool:NO], AVLinearPCMIsFloatKey,
													[NSNumber numberWithBool:NO], AVLinearPCMIsNonInterleaved,
													nil];
	AVAssetReaderTrackOutput *output = [[AVAssetReaderTrackOutput alloc] initWithTrack:songTrack outputSettings:outputSettingsDict];
	[reader addOutput:output];
	
	UInt32 channelCount = 2;
	
	NSArray *formatDesc = songTrack.formatDescriptions;

	Float64 sampleRate = 0;
	
	

	for (unsigned int i = 0; i < [formatDesc count]; ++i) {
		CMAudioFormatDescriptionRef item = (__bridge CMAudioFormatDescriptionRef)[formatDesc objectAtIndex:i];
		const AudioStreamBasicDescription* fmtDesc = CMAudioFormatDescriptionGetStreamBasicDescription(item);
		
		
		if (fmtDesc == nil) {
			return;
		}
		
		channelCount = fmtDesc->mChannelsPerFrame;
		sampleRate = fmtDesc->mSampleRate;

	}
	
	
	
	CGContextSetAllowsAntialiasing(context, antialiasingEnabled);
	CGContextSetLineWidth(context, 1.0);
	CGContextSetStrokeColorWithColor(context, color.CGColor);
	CGContextSetFillColorWithColor(context, color.CGColor);
	CGContextSetBlendMode(context, kCGBlendModeNormal);
   
	UInt32 bytesPerInputSample = 2 * channelCount;
	unsigned long int totalSamples;
	NSUInteger samplesPerPixel;
	
	[reader startReading];
	
	float halfGraphHeight = (heightInPixels / 2);
	double bigSample = 0;
	Float32 sampleBrackets[51] = BRACKETS;
	
	NSUInteger bigSampleCount = 0;
//	NSMutableData * data = [NSMutableData dataWithLength:32768];
   
#define BUF_SIZE 32768
	
	void* theBuffer = calloc( BUF_SIZE, sizeof(SInt16));
	
	
	CGFloat currentX = 0;
	
	NSUInteger hoge = 0;
	
	BOOL faster = ( sizeof(CGFloat) == sizeof( float ) );
	
	BOOL firstTime = YES;
	
	while (reader.status == AVAssetReaderStatusReading) {
		
#ifdef ENABLE_CANCEL
		if( task != 0 && abs( task - currentTask_ ) > CANCEL_MARGIN ) break;
#endif
		
		CMSampleBufferRef sampleBufferRef = [output copyNextSampleBuffer];
		
		if( firstTime )
		{
			firstTime = NO;
			CMItemIndex sampleIndex = 0;
			CMSampleTimingInfo info;
			CMSampleBufferGetSampleTimingInfo(sampleBufferRef, sampleIndex, &info);
			CMTime duration = info.duration;

			Float64 dd = CMTimeGetSeconds(duration);
			size_t size = CMSampleBufferGetSampleSize(sampleBufferRef, sampleIndex);
			
			
			totalSamples = CMTimeGetSeconds(asset.duration) * size / dd / bytesPerInputSample;
			
			
			samplesPerPixel = totalSamples / (widthInPixels);
			
			samplesPerPixel *= contentRatio;
			
			samplesPerPixel = samplesPerPixel < 1 ? 1 : samplesPerPixel;
			
			
		}
		
		
		
		if (sampleBufferRef) {
			CMBlockBufferRef blockBufferRef = CMSampleBufferGetDataBuffer(sampleBufferRef);
			size_t bufferLength = CMBlockBufferGetDataLength(blockBufferRef);
			
			if (BUF_SIZE < bufferLength) {
				
				theBuffer = realloc(theBuffer, bufferLength);
				
			}
			
			CMBlockBufferCopyDataBytes(blockBufferRef, 0, bufferLength, theBuffer);
			
			SInt16 *samples = (SInt16 *)theBuffer;
			Float32 prevSample = 0;
			NSUInteger factorCount = 0;
			
#define FACTOR (faster?4096 :1024)

			
			int sampleCount = (int)(bufferLength / bytesPerInputSample);
			for (int i = 0; i < sampleCount; i++)
			{
				
#ifdef ENABLE_CANCEL
				if( task != 0 && abs( task - currentTask_ ) > CANCEL_MARGIN ) break;
#endif
				
				
				Float32 sx = (Float32) *samples++;
				
				Float32 sample = 0;
				
				
				
				

				
				
				
				
				if( factorCount != 0 )
				{
					sample = prevSample;
					
				}else
				{
					sx = absX(sx);
					
					for( NSInteger hoge = 25; hoge > 0 ; hoge-- )
					{
						if( sampleBrackets[hoge*2] > sx )
						{
							sample = -hoge*2;
							break;
						}
					}
					
					prevSample = sample;
				}
				
				
				factorCount++;
				
				if( factorCount > FACTOR )
					factorCount = 0;

				
				//				sample = decibel(sx);
				//				sample = minMaxX(sample, noiseFloor, 0);
				
				
				for (int j = 1; j < channelCount; j++)
					samples++;
				
				
				bigSample += sample;
				bigSampleCount++;
				
				if (bigSampleCount == samplesPerPixel)
				{
					double averageSample = bigSample / (double)bigSampleCount;
					
					
					currentX ++;
					hoge++;
					
					if( faster )
					{
						if( hoge == 3 )
						{
							SCRenderPixelWaveformInContext(context, halfGraphHeight, averageSample, currentX);
							hoge = 0;

						}else
						{
							
						}
						
					}else
					{
						if( hoge != 3 )
						{
							SCRenderPixelWaveformInContext(context, halfGraphHeight, averageSample, currentX);
							
						}
						else
						{
							hoge = 0;
						}
					}
					
					
					
					bigSample = 0;
					bigSampleCount  = 0;
				}
			}
			CMSampleBufferInvalidate(sampleBufferRef);
			CFRelease(sampleBufferRef);
		}
	}
	
	
	if( theBuffer ) free(theBuffer);
	
//	Float64 start = CMTimeGetSeconds(timeRange.start);
//	
//	NSString* str = [NSString stringWithFormat:@"%.0f",start];
//	
//	UIGraphicsPushContext(context);
//	[[UIColor blueColor] set];
//	[str drawAtPoint:CGPointMake(0, size.height/2) withFont:[UIFont systemFontOfSize:20]];
//	UIGraphicsPopContext();
	
	
#ifdef ENABLE_CANCEL
	
	if( task != 0 && abs( task - currentTask_ ) > CANCEL_MARGIN )
	{
		[reader cancelReading];
		reader = nil;
		output = nil;
		return;
	}
#endif
	
	// Rendering the last pixels
	bigSample = bigSampleCount > 0 ? bigSample / (double)bigSampleCount : noiseFloor;
	while (currentX < size.width) {
		SCRenderPixelWaveformInContext(context, halfGraphHeight, bigSample, currentX);
		currentX++;
	}
	
	//
	
	
}





+ (UIImage*)generateWaveformImage:(AVAsset *)asset timeRange:(CMTimeRange)timeRange withColor:(UIColor *)color andSize:(CGSize)size antialiasingEnabled:(BOOL)antialiasingEnabled task:(double)task
{
	 
	int scaledWidth = size.width;
	int scaledHeight = size.height/2;
	
	int bytesPerRowInDest = scaledWidth * 1;
	
	void *_textureBytes = malloc( scaledHeight* bytesPerRowInDest);
	
	// Draw
	
	int bitsPerComponent = 8;
	
	
	CGColorSpaceRef colorSpace;
	colorSpace = CGColorSpaceCreateDeviceRGB();
	
	CGContextRef cgContext = CGBitmapContextCreate(_textureBytes, scaledWidth, scaledHeight, bitsPerComponent, bytesPerRowInDest, nil, kCGImageAlphaOnly );
	
	
	CGContextClearRect(cgContext, CGRectMake(0, 0, scaledWidth, scaledHeight));
	
	
	[SCWaveformView renderWaveformInContext: cgContext
												 asset: asset
											timeRange: timeRange
											withColor: color
											  andSize: CGSizeMake(scaledWidth, scaledHeight)
							  antialiasingEnabled: antialiasingEnabled
												  task: task];
	
#ifdef ENABLE_CANCEL
	if( task != 0 && abs( task - currentTask_ ) > CANCEL_MARGIN )
	{
		CFRelease(cgContext);
		CGColorSpaceRelease(colorSpace);
		
		free(_textureBytes);
		
		return nil;
	}
#endif
	
//	CGContextSetBlendMode(cgContext, kCGBlendModeNormal);
//
//	CGContextSetFillColorWithColor(cgContext, [UIColor redColor].CGColor);
//	CGContextFillEllipseInRect(cgContext, CGRectMake(0, 0, scaledWidth, scaledHeight));
	
	
	CGImageRef cgimage = CGBitmapContextCreateImage(cgContext);
	
	UIImage *image = [[UIImage alloc] initWithCGImage: cgimage];

	
	CFRelease(cgContext);
	CGImageRelease(cgimage);
	CGColorSpaceRelease(colorSpace);
	
	free(_textureBytes);

	
	return image;
}

+ (UIImage*)recolorizeImage:(UIImage*)image withColor:(UIColor*)color
{
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextDrawImage(context, imageRect, image.CGImage);
    [color set];
    UIRectFillUsingBlendMode(imageRect, kCGBlendModeSourceAtop);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

static double currentTask_ = 0;
static dispatch_queue_t waveform_dispatch_queue_ = nil;

+ (void)generateWaveforms:(AVAsset*)asset timeRange:(CMTimeRange)timeRange color:(UIColor*)color size:(CGSize)size antialias:(BOOL)antialiasingEnabled force:(BOOL)force completion:(void (^)(UIImage* theImage))completion 
{
	
	currentTask_ = CMTimeGetSeconds(timeRange.start);
	
	double myTask = currentTask_;
	
	if( force ) myTask = 0;
	
	if( !waveform_dispatch_queue_ )
	{
		waveform_dispatch_queue_ = dispatch_queue_create("waveform_dispatch_queue", DISPATCH_QUEUE_SERIAL);
		dispatch_set_target_queue(waveform_dispatch_queue_, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));

		
	}
	
	dispatch_async(waveform_dispatch_queue_, ^{
		
		
		UIImage* image = [SCWaveformView generateWaveformImage: asset
																	timeRange: timeRange
																	withColor: color
																	  andSize: size
													  antialiasingEnabled: antialiasingEnabled
																		  task: myTask];
		
		dispatch_async(dispatch_get_main_queue(), ^{
		
			completion(image);
		});
		
	});
	

}

//- (void)generateWaveforms
//{
//    CGRect rect = self.bounds;
//   
//    if (self.generatedNormalImage == nil && self.asset) {
//		 self.generatedNormalImage = [SCWaveformView generateWaveformImage:self.asset timeRange:self.timeRange withColor:self.normalColor andSize:CGSizeMake(rect.size.width, rect.size.height) antialiasingEnabled:self.antialiasingEnabled];
//        _normalColorDirty = NO;
//    }
//    
//    if (self.generatedNormalImage != nil) {
//        if (_normalColorDirty) {
//            self.generatedNormalImage = [SCWaveformView recolorizeImage:self.generatedNormalImage withColor:self.normalColor];
//            _normalColorDirty = NO;
//        }
//        
//        if (_progressColorDirty || self.generatedProgressImage == nil) {
//            self.generatedProgressImage = [SCWaveformView recolorizeImage:self.generatedNormalImage withColor:self.progressColor];
//            _progressColorDirty = NO;
//        }
//    }
// 
//}

//- (void)drawRect:(CGRect)rect
//{
////    [self generateWaveforms];
//   
//    [super drawRect:rect];
//}

//- (void)applyProgressToSubviews
//{
//    CGRect bs = self.bounds;
//    CGFloat progressWidth = bs.size.width * _progress;
//    _cropProgressView.frame = CGRectMake(0, 0, progressWidth, bs.size.height);
//    _cropNormalView.frame = CGRectMake(progressWidth, 0, bs.size.width - progressWidth, bs.size.height);
//    _normalImageView.frame = CGRectMake(-progressWidth, 0, bs.size.width, bs.size.height);
//}
//
//- (void)layoutSubviews {
//    [super layoutSubviews];
//    
//    CGRect bs = self.bounds;
//    _normalImageView.frame = bs;
//    _progressImageView.frame = bs;
//    
//    // If the size is now bigger than the generated images
//    if (bs.size.width > self.generatedNormalImage.size.width) {
//        self.generatedNormalImage = nil;
//        self.generatedProgressImage = nil;
//    }
//    
//    [self applyProgressToSubviews];
//}
//
//-(void)generateWaveforms
//{
//	[SCWaveformView generateWaveforms:^(UIImage *theImage) {
//		
//		self.image = theImage;
//	}];
//}
//
//- (void)setNormalColor:(UIColor *)normalColor
//{
//    _normalColor = normalColor;
//    _normalColorDirty = YES;
//    [self setNeedsDisplay];
//}
//
//- (void)setProgressColor:(UIColor *)progressColor
//{
//    _progressColor = progressColor;
//    _progressColorDirty = YES;
//    [self setNeedsDisplay];
//}
//
//- (void)setAsset:(AVAsset *)asset
//{
//    _asset = asset;
//    self.generatedProgressImage = nil;
//    self.generatedNormalImage = nil;
//    [self setNeedsDisplay];
//}
//
//-(void)setTimeRange:(CMTimeRange)timeRange
//{
//	_timeRange = timeRange;
//	self.generatedProgressImage = nil;
//	self.generatedNormalImage = nil;
//
//	[self setNeedsDisplay];
//
//}
//
//- (void)setProgress:(CGFloat)progress
//{
//    _progress = progress;
//    [self applyProgressToSubviews];
//}
//
//- (UIImage*)generatedNormalImage
//{
//    return _normalImageView.image;
//}
//
//- (void)setGeneratedNormalImage:(UIImage *)generatedNormalImage
//{
//    _normalImageView.image = generatedNormalImage;
//}
//
//- (UIImage*)generatedProgressImage
//{
//    return _progressImageView.image;
//}
//
//- (void)setGeneratedProgressImage:(UIImage *)generatedProgressImage
//{
//    _progressImageView.image = generatedProgressImage;
//}
//
//- (void)setAntialiasingEnabled:(BOOL)antialiasingEnabled
//{
//    if (_antialiasingEnabled != antialiasingEnabled) {
//        _antialiasingEnabled = antialiasingEnabled;
//        self.generatedProgressImage = nil;
//        self.generatedNormalImage = nil;
//        [self setNeedsDisplay];        
//    }
//}
//
@end
