/*
 *  AppDefinitions.h
 *  CalendarTest
 *
 *  Created by Masatoshi Nishikata on 09/08/26.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */




extern BOOL isWestern;
extern BOOL isWareki;
extern BOOL isKorean;


#define CAL_FORMAT(key) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:(@"CalendarFormat")]


#define LANG_CAL_FORMAT(key) \
[[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:(@"LangCalendarFormat")]

extern NSString *const ChartGridTypeName;
extern NSString *const EventCellTypeName;
extern NSString *const EventCellSplitTypeName;
extern NSString *const EventCellFontSizeTypeName;
extern NSString *const EventDrawTypeName;
extern NSString *const EventDistributionTypeName;
extern NSString *const EventShadowTypeName;
extern NSString *const EventInvertTypeName;
extern NSString *const ContinuousCalendarNumOfWeeksTypeName;
extern NSString *const EventEditorTitleSizeTypeName;
extern NSString *const EventEditorTitleFontTypeName;
extern NSString *const CalendarDayViewFontTypeName;
extern NSString *const NagaoshiTimeTypeName;
extern NSString *const InitialViewTypeName;
extern NSString *const PlusButtonModeTypeName;
extern NSString *const WeeklyCalendarDistributionTypeName;
extern NSString *const ExpandCellTypeName;
extern NSString *const DirectTouchTypeName;
extern NSString *const RokuyoDisplayTypeName;
extern NSString *const CalendarDoubleTapBehaviourTypeName;
extern NSString *const CalendarSingleTapBehaviourTypeName;
extern NSString *const HolidayTypeName;
extern NSString *const AutomaticSyncTypeName;
extern NSString *const AutomaticSyncOnOpeningTypeName;
extern NSString *const AutomaticSyncOnClosingTypeName;
extern NSString *const SyncResultsReportTypeName;
extern NSString *const SyncImageDestinationTypeName;
extern NSString *const AlarmFireTypeName;

extern NSString *const WeeklyCalendarFontSizeTypeName;
extern NSString *const WeeklyCalendarTimeDisplayTypeName;

extern NSString *const MonthlyCalendarFontSizeTypeName;
extern NSString *const MonthlyCalendarTimeDisplayTypeName;

extern NSString *const ContinuousCalendarFontSizeTypeName;
extern NSString *const ContinuousCalendarTimeDisplayTypeName;

extern NSString *const UseWeekNumberTypeName;
extern NSString *const MinimumDaysInFirstWeekTypeName;

//extern NSString *const EventKitCompatibleTypeName;
extern NSString *const EventKitPriorityTypeName;

extern NSString *const EventEditorDatePickerContainsYearTypeName;
extern NSString *const EventEditorDatePickerInitialButtonTypeName;

extern NSString *const LandscapeTopViewTypeName;
extern NSString *const SyncDurationTypeName;
extern NSString *const SyncPastDurationTypeName;

extern NSString *const TimeZoneDisplayTypeName;
extern NSString *const ChartTextTypeName;

extern NSString *const TaskDirectEditingTypeName;
extern NSString *const TaskDisplayItemTypeName;
extern NSString *const TaskBulletTypeName;
extern NSString *const TaskBulletTypeNameLevel1;
extern NSString *const TaskBulletTypeNameLevel2;
extern NSString *const TaskBulletTypeNameLevel3;

extern NSString *const TaskBulletInheritTypeName;

extern NSString *const TaskAddingLocationTypeName;
extern NSString *const TaskUncheckParentTypeName;

extern NSString *const TaskPanToInsertGestureTypeName;
extern NSString *const TaskApplicationBadgeTypeName;

extern NSString *const TaskListCounterTypeName;
extern NSString *const TaskAddContinuouslyTypeName;

extern NSString *const CCalDevDisableCacheTypeName;

extern NSString *const CCalEventKitWindowTypeName;

extern NSString *const EventEditorDatePickerSwipeGestureTypeName;

extern NSString *const EventEditorAutoSaveTypeName;

extern NSString *const EventEditorInitialComposerTypeName;
extern NSString *const EventEditorInitialWhenTypeName;


// MNItem "Edited" suffix

extern NSString *const kMNItemWasEditedSignature;
// この文字列がETagの末尾に付けられる

typedef enum  {
	TaskSortOrderByDueDate = 0,
	TaskSortOrderByMyOrder
} TaskSortOrder;

typedef enum  {
	EventAccessoryNone = 0,
	EventAccessoryCalendarSwatch,
	EventAccessoryUncompleted,
	EventAccessoryCompleted
} EventAccessoryType;



typedef enum  {
	ChartGridNone = 0,
	ChartGrid5,
	ChartGrid10,
	ChartGrid15,
	ChartGrid30,
	ChartGrid60
} ChartGridType;

typedef enum  {
	EventCellNarrow = 0,
	EventCellBasic,
	EventCellDetailed,
	EventCellDetailedWithDuration,
	EventCellSearchResult
} EventCellType;


typedef enum  {
	EventDrawWithoutDot = 0,
	EventDrawWithDot,
	EventDrawInverted
} EventDrawType;

typedef enum  {
	EventDistributionConnected = 0,
	EventDistributionDiscrete
} EventDistributionType;

typedef enum  {
	EventShadowNone = 0,
	EventShadowNormal
} EventShadowType;

typedef enum  {
	EventInvertTypeRectangle = 0,
	EventInvertTypeSimple,
	EventInvertTypeGradient
} EventInvertType;



typedef enum  {
	ContinuousCalendarNumOfWeeks1 = 0,
	ContinuousCalendarNumOfWeeks2,
	ContinuousCalendarNumOfWeeks3,
	ContinuousCalendarNumOfWeeks4,
	
} ContinuousCalendarNumOfWeeksType;


typedef enum  {
	EventEditorTitleSizeRegular = 0,
	EventEditorTitleSizeLarge,
	EventEditorTitleSizeSmall
} EventEditorTitleSizeType;

typedef enum  {
	EventEditorTitleFontNormal = 0,
	EventEditorTitleFontCustomiszed
} EventEditorTitleFontType;

typedef enum  {
	CalendarDayViewFontNormal = 0,
	CalendarDayViewFontCustomiszed
} CalendarDayViewFontType;

typedef enum  {
	NagaoshiTimeShortest = 0,
	NagaoshiTimeShorter,
	NagaoshiTimeRegular,
	NagaoshiTimeLonger,
	NagaoshiTimeLongest
} NagaoshiTimeType;

typedef enum  {
	InitialViewInherit = 0,
	InitialViewToday,
	InitialViewTodayFullList,
	InitialViewTodayFullChart
} InitialViewType;

typedef enum  {
	PlusButtonModeDefault = 0,
	PlusButtonModeLauncher,
	PlusButtonModeList
} PlusButtonModeType;

typedef enum  {
	WeeklyCalendarDistributionShortenLastTwo = 0,
	WeeklyCalendarDistribution3and4,
	WeeklyCalendarDistribution4and3
} WeeklyCalendarDistributionType;


typedef enum {
	ExpandCellTypeNo = 0,
	ExpandCellTypeYes,
	ExpandCellTypeYesWithTime,
	ExpandCellTypeYesWithTimeAndEnlarged
} ExpandCellType;

typedef enum {
	DirectTouchTypeNo = 0,
	DirectTouchTypeYes
} DirectTouchType;

typedef enum  {
	RokuyoDisplayOff = 0,
	RokuyoDisplayOn
} RokuyoDisplayType;

typedef enum  {
	CalendarDoubleTapBehaviourZoom = 0,
	CalendarDoubleTapBehaviourFullScreen,
	CalendarDoubleTapBehaviourCreateNew
} CalendarDoubleTapBehaviourType;

typedef enum  {
	CalendarSingleTapBehaviourSelect = 0,
	CalendarSingleTapBehaviourSplit,
	CalendarSingleTapBehaviourFullScreen
} CalendarSingleTapBehaviourType;

typedef enum  {
	HolidayNone = 0,
	HolidayNZ,
	HolidayJapan,
	HolidaySouthKorea,
	HolidayAustralia,
	HolidayHongKong
} HolidayType;


typedef enum  {
	AutomaticSyncNone = 0,
	AutomaticSyncAutomaticOnOpening,
	AutomaticSyncAutomaticOnClosing,
	AutomaticSyncAutomaticOnOpeningAndClosing
} AutomaticSyncType;

typedef enum  {
	AutomaticSyncOnOpeningNone = 0,
	AutomaticSyncOnOpeningAlways,
	AutomaticSyncOnOpeningOnceADay
} AutomaticSyncOnOpeningType;

typedef enum  {
	AutomaticSyncOnClosingNone = 0,
	AutomaticSyncOnClosingAlways,
	AutomaticSyncOnClosingOnceADay,
	AutomaticSyncOnClosingWhenEdited
} AutomaticSyncOnClosingType;



typedef enum  {
	SyncResultsReportTypeNone = 0,
	SyncResultsReportTypeAlways,
	SyncResultsReportTypeOnError
} SyncResultsReportType;

typedef enum  {
	SyncImageDestinationTypeNo = 0,
	SyncImageDestinationTypePicasa
} SyncImageDestinationType;

typedef enum  {
	AlarmFireTypeAtDayStart = 0,
	AlarmFireTypeAtDayStartMinusOne,
	AlarmFireTypeAtDayStartMinusTwo,
	AlarmFireTypeAtDayMidnight
} AlarmFireType;


typedef enum  {
	CalendarDayViewFontSizeTypeVerySmall = 0,
	CalendarDayViewFontSizeTypeSmall,
	CalendarDayViewFontSizeTypeMedium,
	CalendarDayViewFontSizeTypeLarge,
	CalendarDayViewFontSizeTypeHuge
} CalendarDayViewFontSizeType;

typedef enum  {
	CalendarDayViewTimeDisplayTypeNone = 0,
	CalendarDayViewTimeDisplayTypeWhenZoomed,
	CalendarDayViewTimeDisplayTypeAlways
} CalendarDayViewTimeDisplayType;

typedef enum  {
	UseWeekNumberTypeNo = 0,
	UseWeekNumberTypeYes
} UseWeekNumberType;


typedef enum  {
	LandscapeTopViewTypeNone = 0,
	LandscapeTopViewTypePerspective
} LandscapeTopViewType;


typedef enum  {
	MinimumDaysInFirstWeekType1 = 0,
	MinimumDaysInFirstWeekType2,
	MinimumDaysInFirstWeekType3,
	MinimumDaysInFirstWeekType4,
	MinimumDaysInFirstWeekType5,
	MinimumDaysInFirstWeekType6,
	MinimumDaysInFirstWeekType7
} MinimumDaysInFirstWeekType;

//typedef enum  {
//	EventKitCompatibleTypeNo = 0,
//	EventKitCompatibleTypeYes
//} EventKitCompatibleType;

typedef enum  {
	EventKitPriorityTypeNo = 0,
	EventKitPriorityTypeYes
} EventKitPriorityType;

typedef enum  {
	EventCellFontSizeTypeSmall = 0,
	EventCellFontSizeTypeRegular
} EventCellFontSizeType;


typedef enum  {
	CalendarDisplayModeMonth = 0,
	CalendarDisplayModeWeek,
	CalendarDisplayModeContinuous,
	CalendarDisplayModePerspective
} CalendarDisplayMode;


typedef enum  {
	DayDisplayModeList = 0,
	DayDisplayModeChart
} DayDisplayMode;


typedef enum  {
	EventEditorDatePickerDoesNotContainYear = 0,
	EventEditorDatePickerContainsYear
} EventEditorDatePickerContainsYearType;

typedef enum  {
	EventEditorDatePickerInitialButtonStartDate = 0,
	EventEditorDatePickerInitialButtonStartTime,
	EventEditorDatePickerInitialButtonTypeUnselected
} EventEditorDatePickerInitialButtonType;


typedef enum  {
	SyncDuration7 = 0,
	SyncDuration30,
	SyncDuration60,
	SyncDuration180,
	SyncDuration500,
	SyncDuration5000
} SyncDurationType;


typedef enum  {
	TimeZoneDisplayTypeNone = 0,
	TimeZoneDisplayTypeAbbreviation,
	TimeZoneDisplayTypeCityName
} TimeZoneDisplayType;

typedef enum  {
	ChartTextTypeTitleOnly = 0,
	ChartTextTypeTitleAndWhereAndNotes
} ChartTextType;



typedef enum{
	MNGDataEventStatusConfirmed = 0,
	MNGDataEventStatusTentative,
	MNGDataEventStatusCanceled	
} MNGDataEventStatusType;

typedef enum  {
	DayChartEventViewStyleNone = 0,
	DayChartEventViewStyleContinuous = 0x11,
	DayChartEventViewStyleBeginsToday = 0x01,
	DayChartEventViewStyleEndsToday = 0x10
} DayChartEventViewStyle;


typedef enum  {
	TaskDirectEditingTypeNo = 0,
	TaskDirectEditingTypeYes
} TaskDirectEditingType;


typedef enum  {
	TaskDisplayItemTypeTitleOnly = 0,
	TaskDisplayItemTypeTitleAndNotes
} TaskDisplayItemType;

typedef enum  {
	TaskBulletTypeNone = 0,
	TaskBulletTypeNumber,
	TaskBulletTypeAlphabet,
	TaskBulletTypeAlphabetSmall
} TaskBulletType;


typedef enum  {
	TaskBulletInheritTypeNo = 0,
	TaskBulletInheritTypeYes
} TaskBulletInheritType;

typedef enum  {
	TaskAddingLocationTypeOnTop = 0,
	TaskAddingLocationTypeOnBottomRoot,
	TaskAddingLocationTypeOnBottomSibling
} TaskAddingLocationType;

typedef enum  {
	TaskUncheckParentTypeNo = 0,
	TaskUncheckParentTypeYes
} TaskUncheckParentType;


typedef enum  {
	TaskPanToInsertGestureTypeNo = 0,
	TaskPanToInsertGestureTypeYes
} TaskPanToInsertGestureType;

typedef enum  {
	TaskApplicationBadgeTypeNone = 0,
	TaskApplicationBadgeTypeNumberOfUncheckedItems,
	TaskApplicationBadgeTypeNumberOfListsContainsUncheckedItem
} TaskApplicationBadgeType;


typedef enum  {
	TaskListCounterTypeNone = 0,
	TaskListCounterTypeUnchecked,
	TaskListCounterTypeChecked
} TaskListCounterType;

typedef enum  {
	TaskAddContinuouslyTypeNo = 0,
	TaskAddContinuouslyTypeYes
} TaskAddContinuouslyType;

typedef enum  {
	CCalDevDisableCacheTypeNo = 0,
	CCalDevDisableCacheTypeYes
} CCalDevDisableCacheType;

typedef enum  {
	CCalEventKitWindowTypeDefault = 0,
	CCalEventKitWindowTypeCCal
} CCalEventKitWindowType;

typedef enum  {
	EventEditorDatePickerSwipeGestureTypeDefault = 0,
	EventEditorDatePickerSwipeGestureTypeNextButton
} EventEditorDatePickerSwipeGestureType;


typedef enum  {
	RecurrenceMonthModeNo = 0,
	RecurrenceMonthModeYes
} RecurrenceMonthMode;


typedef enum  {
	RepeatTableIndexNo = 0,
	RepeatTableIndexDaily,
	RepeatTableIndexWeekly,
	RepeatTableIndexMonthly,
	RepeatTableIndexYearly,
	RepeatTableIndexCustom
} RepeatTableIndexType;


typedef enum  {
	RepeatEndTableIndexNo = 0,
	RepeatEndTableIndexCount,
	RepeatEndTableIndexDate
} RepeatEndTableIndexType;

typedef enum  {
	EventEditorAutoSaveTypeNo = 0,
	EventEditorAutoSaveTypeClear,
	EventEditorAutoSaveTypeSave,
	EventEditorAutoSaveTypeSaveWithNotification
} EventEditorAutoSaveType;


typedef enum  {
	MNDropboxSyncModeFromLocalToRemote = 0,
	MNDropboxSyncModeFromLocalToRemoteMirror,
	MNDropboxSyncModeFromRemoteToLocal,
	MNDropboxSyncModeFromRemoteToLocalMirror,
	MNDropboxSyncModeUpdateBoth
} MNDropboxSyncModeType;

typedef enum  {
	EventEditorInitialComposerTypeNone = 0,
	EventEditorInitialComposerTypeTitle,
	EventEditorInitialComposerTypeTitleBookmark,
	EventEditorInitialComposerTypeWhere,
	EventEditorInitialComposerTypeWhereBookmark,
	EventEditorInitialComposerTypeWhen,
	EventEditorInitialComposerTypeCalendar
} EventEditorInitialComposerType;





typedef enum  {
	EventEditorInitialWhenTypeTime = 0,
	EventEditorInitialWhenTypeAllDay
} EventEditorInitialWhenType;


typedef enum  {
	MNAdNone = 0,
	MNAdIAd,
	MNAdAdMob,
	MNAdAmazon
} MNAdType;


typedef enum  {
	MNAdLocationOnTop = 0,
	MNAdLocationOnBottom
} MNAdLocationType;



enum {
   EventWindowCustomWhere                 = 1 << 0,
   EventWindowCustomURL   = 1 << 1,
   EventWindowCustomRecurrence        = 1 << 2,
   EventWindowCustomAlarm  = 1 << 3,
   EventWindowCustomParticipant    = 1 << 4,
   EventWindowCustomNotes       = 1 << 5,
   EventWindowCustomPhoto = 1 << 6
};
typedef NSUInteger EventWindowCustomMask;


extern NSString * const MNOpenEventNotificationName;
extern NSString * const MNCreateNewEventNotificationName;
extern NSString * const MNTimeZoneUpdatedNotificationName;
extern NSString * const MNReloadCalendarNotificationName;
extern NSString * const MNReloadDayViewNotificationName;
extern NSString * const MNReloadAlarmsNotificationName;
extern NSString * const MNCustomiseDidFinishNotificationName;
extern NSString * const MNSyncIndicatorProgressNotificationName;
extern NSString * const MNSyncIndicatorLabelNotificationName;
extern NSString * const MNCalPhotoUpdatedNotificationName;

extern CGSize gScreenSize;
extern BOOL gIsRunningOnIPad;
extern BOOL gDisableCache;
extern BOOL gIsIOS6;
// Shortcuts

#define APP_DELEGATE (MNAppDelegate*)[[UIApplication sharedApplication] delegate]
#define UNDO_MANAGER [[[CalendarDataManager sharedManager] managedObjectContext] undoManager]
#define MOC [[CalendarDataManager sharedManager] managedObjectContext]


// UD

#define UD_SYNC_DAYS @"UD_SyncDays"
#define UD_SYNC_DAYS_PAST  @"UD_SyncDaysPast"
#define UD_SYNC_DIM_CELL @"UD_SyncDimCell"

//#define UD_SYNC_PICASA @"UD_SyncPicasa"

// Macros

#define FLAT_UI (gIsIOS6?NO:YES)


#define DUMP_RECT(r1,r2) NSLog(@"%@: %@",r1, NSStringFromCGRect(r2));

#ifndef MNLOG
#ifdef DEBUG
#define MNLOG	NSLog
#else
#define MNLOG( fmt, ... )	//
#endif
#endif

#define SHOW_ERROR(n1,n2) 		{UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(n1,@"")\
message:NSLocalizedString(n2,@"")\
delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];\
[alert show];}

#define SHOW_ERROR_ARC(n1,n2) 		{UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(n1,@"")\
message:NSLocalizedString(n2,@"")\
delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];\
[alert show];}

#define RGBA(r,g,b,a) [UIColor colorWithRed: r/255.0f green: g/255.0f \
blue: b/255.0f alpha: a]


#define ONE_WEEK_INTERVAL 7*60*60*24
#define ONE_DAY_INTERVAL 60*60*24

#define DEFAULT_EVENT_DURATION [[NSUserDefaults standardUserDefaults] integerForKey:@"DurationSettingViewController_DefaultDuration"]

#define GOOGLE_USERNAME_KEY @"MNGoogleAccount"
#define GOOGLE_KEYCHAIN_SERVICE_KEY @"com.catalystwo.CCal.Google"

//#define GOOGLE_PASSWORD_KEY @"MNGoogleAccountPassword"


#define ALARM_REFRESH_DAYS 30



// Error Codes

#define CCAL_ERROR_DOMAIN @"com.catalystwo.CCal"
#define SYNC_TIME_OUT_ERROR -100
#define SYNC_INSUFFICIENT_ATTRIBUTE_ERROR 1
#define LOGIN_ERROR 2
#define DATA_RECEIVE_ERROR 3
#define SYNC_CANCELLED_ERROR 4
#define SYNC_UNKNOWN_PUSH_DESTINATION_ERROR 5
#define SYNC_GOOGLE_SENT_WRONG_CLASS_ERROR 6
#define UNKNOWN_FORMAT_ERROR 7
#define PICASA_ACTIVATION_REQUIRED 8

#define IMAGE_TRANSFER_DATA_ERROR 9

#define SYNC_OFFLINE_ERROR 10



// User Default

#define CCAL_COPIED_EVENT_KEY @"UD_CopiedEventKey"
#define CCAL_PUSH_MODE @"UD_PushMode"

#define DB_EXTENSION @"ccal11database"

#define DB_10_EXTENSION @"ccaldatabase"

#define DB_COMPRESSED_SUFFIX @".ccal11database.zip"

#define DB_10_COMPRESSED_SUFFIX @".ccaldatabase.zip"

// File type

#define OUTLINE_EXTENSION @"outline"

// Build Options


//#define LEGACY_SUPPORT
#define LEGACY_CCAL_VERSION @"3"

#define RELESE_4_AND_5_USER @"Release 4 User"



//#define DEPENDENT_SPECIALIZED_EVENT


#ifdef FREE_VERSION
#define APP_STORE_ID @"387570397"
#else
#define APP_STORE_ID @"387569926"
#endif

#define CCAL_11_APP_STORE_ID @"556293500"

#define PAID_VERSION_STORE_ID @"387569926"


#define IN_APP_PURCHAE_ID @"com.catalystwo.LearningPlayer.paid"


// Event displaying

#define EXPAND_INSTANCES_ON_FETCH
#define USE_THREAD
#define HIDE_CANCELLED_EVENTS

#ifdef DEBUG
#undef HIDE_CANCELLED_EVENTS
#endif


// NZSound Option
#define CCAL_USER_ID @"com.catalystwo.nzsound.CCalUser"
#define CCAL_USER_SERVICE  @"com.catalystwo.nzsound.CCalUser.service"
#define KEYCHAIN_ACCESSGROUP @"7HVB3KN35P.com.catalystwo.*"



#ifndef __IPHONE_6_0
#define EKReminder NSClassFromString(@"EKReminder")

typedef enum {
	UIInterfaceOrientationMaskPortrait = (1 << UIInterfaceOrientationPortrait),
	UIInterfaceOrientationMaskLandscapeLeft = (1 << UIInterfaceOrientationLandscapeLeft),
	UIInterfaceOrientationMaskLandscapeRight = (1 << UIInterfaceOrientationLandscapeRight),
	UIInterfaceOrientationMaskPortraitUpsideDown = (1 << UIInterfaceOrientationPortraitUpsideDown),
	UIInterfaceOrientationMaskLandscape = (UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight),
	UIInterfaceOrientationMaskAll = (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskPortraitUpsideDown),
	UIInterfaceOrientationMaskAllButUpsideDown = (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight),
} UIInterfaceOrientationMask;

#endif



// Google ID

#if OUTLINE_BUILD


#define GOOGLE_CLIENT_ID  @"472024094957.apps.googleusercontent.com"
#define GOOGLE_CLIENT_SECRET  @"HutFEYpQBtxopN8JC_yL3JNm"

#else


#define  GOOGLE_CLIENT_ID @"611052824728.apps.googleusercontent.com"
#define  GOOGLE_CLIENT_SECRET  @"pBo2FL4WXRlLvDVDAIdxwRXs"

#define  GOOGLE_CLIENT_ID_PAID_CCAL  @"898733187438.apps.googleusercontent.com"
#define  GOOGLE_CLIENT_SECRET_PAID_CCAL  @"QTLdUEUfhlbawyn4oRkhMVLC"

#endif
