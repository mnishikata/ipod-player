//
//  MNCollectionTableViewController.m
//  iPod Player
//
//  Created by Nishikata Masatoshi on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NowPlayingButton.h"
#import "TrackTableViewCell.h"
#import "AB_Player-Swift.h"

extern dispatch_queue_t enumerationQueue;



@implementation MNSongCollectionTableViewController

@synthesize mediaItems = mediaItems_;
@synthesize soundClips = soundClips_;

+ (UIViewController *) viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
	MNSongCollectionTableViewController* controller = nil;
	
	NSSet* predicates = [coder decodeObjectForKey:@"filterPredicates_"];
	
	controller = [[MNSongCollectionTableViewController alloc] initWithFilterPredicates:predicates];
	
	return controller;
}

 
- (id)initWithFilterPredicates:(NSSet*)predicates
{
	self = [super initWithStyle:UITableViewStylePlain];
	if (self) {
		
		soundClips_ = [[NSMutableArray alloc] init];
		
		filterPredicates_ = predicates;
		
		if( enumerationQueue == nil )
		{
			enumerationQueue = dispatch_queue_create("Browser Enumeration Queue", DISPATCH_QUEUE_SERIAL);
			dispatch_set_target_queue(enumerationQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
		}
		
		shouldReloadData_ = YES;
		
		self.restorationIdentifier = @"MNSongCollectionTableViewControllerID";
		self.restorationClass = [self class];

	}
	return self;
}

-(void)dealloc
{
	soundClips_ = nil;
	mediaItems_ = nil;
	filterPredicates_ = nil;
}

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
	[super encodeRestorableStateWithCoder:coder];
	
	[coder encodeObject:mediaItems_ forKey:@"mediaItems"];
	[coder encodeObject:filterPredicates_ forKey:@"filterPredicates_"];
	[coder encodeObject:self.title forKey:@"title"];

}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
	[super decodeRestorableStateWithCoder:coder];
	
	shouldReloadData_ = YES;
	
	filterPredicates_ = [coder decodeObjectForKey:@"filterPredicates_"];
	mediaItems_ = [coder decodeObjectForKey:@"mediaItems"];
	self.title = [coder decodeObjectForKey:@"title"];
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if( shouldReloadData_ )
	{
		[self reloadData];
	}
	else
	{
		[self fetchSoundClips];
	}
	
	
	// Select currently playing item, if there is no item, deselect
	
	MPMediaItem* mediaItem = [[MNPlayerViewController sharedPlayer] mediaItem];
	
	for( NSUInteger row = 0; row < self.mediaItems.count; row++ )
	{
		MPMediaItem* item = [self.mediaItems objectAtIndex: row];
		if( [item isEqual: mediaItem] )
		{
			NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:1];
			
			[self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
			
			break;
		}
	}
}


#pragma mark - Table view data source

-(void)reloadData
{
	shouldReloadData_ = NO;
	self.mediaItems = nil;
	
	// Read section data first
	dispatch_sync(enumerationQueue, ^(void) {
		// Grab videos from the iPod Library
		
		MPMediaQuery *query = [MPMediaQuery songsQuery];
		query.groupingType = MPMediaGroupingAlbum;
		
		if( filterPredicates_ ) {
			query.filterPredicates = filterPredicates_;
		}
		
		self.mediaItems = [query items];
		
	});
	
	[self fetchSoundClips];
}

-(void)fetchSoundClips
{
	MPMediaItemCollection *collection = [MPMediaItemCollection collectionWithItems: mediaItems_];
	
	NSPredicate* clipPredicate = nil;
	for( MPMediaPropertyPredicate* predicate in filterPredicates_ )
	{
		
		// アルバムで分類されている場合
		
		if( [[predicate property] isEqualToString: MPMediaItemPropertyAlbumPersistentID] )
		{
			clipPredicate = [MNSoundClip fetchSoundClipsInAlbum: @[collection] ];
			break;
		}
		
		// Podcastで分類されている場合
		if( [[predicate property] isEqualToString: MPMediaItemPropertyPodcastPersistentID] )
		{
			clipPredicate = [MNSoundClip fetchSoundClipsInPodcast: @[collection] ];
			break;
		}
	}
	
	
	if( !clipPredicate )
	{
		clipPredicate = [MNSoundClip fetchSoundClipsForSongs: @[collection]];
	}
	
	soundClips_ = nil;
	
	[MNSoundClip fetchWithPredicate:clipPredicate completion:^(NSArray<MNSoundClip *> * _Nonnull clips, NSError * error) {
		
		if( error != nil )  {
			loadingError_ = YES;
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
							  withRowAnimation:UITableViewRowAnimationNone];
			
			return;
		}
		
		self.soundClips = [[NSMutableArray alloc] initWithArray: clips];
		[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
	}];
	
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if( section == 0 ) return  NSLocalizedString(@"Sound Clips", @"");
	
	return NSLocalizedString(@"Tracks", @"");
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if( section == 0) return 1;
	
	return self.mediaItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 0 )
	{
		UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;

		if( !soundClips_ )
		{
			if( loadingError_ ) {
				cell.textLabel.text = NSLocalizedString(@"UnableToLoad", @"");
				cell.textLabel.textColor = [UIColor lightGrayColor];

			}else {
				cell.textLabel.text = NSLocalizedString(@"Loading", @"");
				cell.textLabel.textColor = [UIColor lightGrayColor];

			}
		}
		else
		{
			if( soundClips_.count == 0 )
			{
				cell.textLabel.text = NSLocalizedString(@"0 clip", @"");
				cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell.textLabel.textColor = [UIColor lightGrayColor];
			}
			
			else if( soundClips_.count == 1 )
			{
				cell.textLabel.text = NSLocalizedString(@"1 clip", @"");

			}
			else{
				cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d clips", @""), soundClips_.count];

			}
			
		}
		
		cell.imageView.image = [UIImage imageNamed:@"SoundClipIconFlat"];
		return cell;
	}
	
	
	static NSString *CellIdentifier = @"TrackCell";
	TrackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   if( !cell )
		cell = [[TrackTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	
	
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	
	MPMediaItem* item = [self.mediaItems objectAtIndex: indexPath.row];
	__block NSString* title = nil;
	__block NSNumber *trackNumberNumber = nil;
	
	NSURL* url = [item valueForProperty:MPMediaItemPropertyAssetURL];
	
	dispatch_sync(enumerationQueue, ^{
		
		title = [item valueForProperty:MPMediaItemPropertyTitle];
		
		trackNumberNumber = [item valueForProperty:MPMediaItemPropertyAlbumTrackNumber];
		
		
	});
	
	cell.trackNumber = trackNumberNumber;
	cell.titleLabel.text = title;
	
	cell.imageView.image = nil;
	
	
	if( !url )
	{
		cell.titleLabel.textColor = [UIColor lightGrayColor];
		cell.trackLabel.textColor = [UIColor lightGrayColor];

		cell.selectionStyle = UITableViewCellSelectionStyleNone;

	}
	
	return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Return NO if you do not want the specified item to be editable.
	return NO;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 1 )
	{
		if( indexPath.row != 2*(indexPath.row /2) )
		{
			cell.contentView.backgroundColor = RGBA(240, 240, 240, 0.5);
		}else
		{
			cell.contentView.backgroundColor = [UIColor whiteColor];
			
		}
		
	}
	
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
	if( indexPath.section == 0   )
	{
		if( self.soundClips.count > 0 )
		{
			MNClipCollectionTableViewController * controller = [[MNClipCollectionTableViewController alloc] init];
			
			controller.organizeByTrack = YES;
			controller.soundClips = self.soundClips;
			
			[self.navigationController pushViewController:controller animated:YES];
		}
		
		return;
	}
	
	MPMediaItem* item = [self.mediaItems objectAtIndex: indexPath.row];
	
	NSURL* url = [item valueForProperty:MPMediaItemPropertyAssetURL];

	if( !url )
	{
		return;
	}
	
	MNPlayerViewController* controller = [MNPlayerViewController sharedPlayer];
	controller.mediaItems = self.mediaItems;
	controller.mediaItem = item;
	[self.navigationController pushViewController:controller animated:YES];
	
	[controller playFromBeginning:YES];
	
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	if( self.mediaItems.count < 5 ) return nil;
	
	NSMutableArray* array = [[NSMutableArray alloc] init];
	
	for( NSUInteger hoge = 0; hoge < self.mediaItems.count; hoge++ )
	{
		NSString *title = [NSString stringWithFormat:@"%lu", hoge+1];
		[array addObject: title];
	}
	
	return array;
	
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:1];
	[self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
	
	return NSNotFound;
}

@end


