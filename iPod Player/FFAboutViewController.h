//
//  CCalAboutViewController.h
//  CCal-X
//
//  Created by Masatoshi Nishikata on 31/07/12.
//  Copyright (c) 2012 Catalystwo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FFAboutViewController : UITableViewController
{
	BOOL purchased_;
}

- (id)init;
-(void)viewWillAppear:(BOOL)animated;
- (void)viewDidLoad;
-(void)done;
- (void)didReceiveMemoryWarning;
- (NSUInteger)supportedInterfaceOrientations;
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath;
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)purchaseInfo;
- (void)store:(id)sender ;
- (void)credit:(id)sender ;
- (void)showQR:(id)sender ;
-(void)faq;
-(void)showAbout;
-(void)showGestures;
-(void)showPrivacy;
- (void)appsFromUs;

@end
