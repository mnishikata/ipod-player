//
//  MNSoundClipEditor.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 5/09/12.
//
//

#import <UIKit/UIKit.h>
@class MNSoundClip;


@interface MNSoundClipEditor : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *commentField;
@property (nonatomic, retain) MNSoundClip* clip;
@property (weak, nonatomic) IBOutlet UILabel *transcriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end
