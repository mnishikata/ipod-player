//
//  MNExporterViewController.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 3/09/12.
//
//

#import "MNExporterViewController.h"
#import "SCUI.h"
#import "SCLoginViewController.h"
#import "MNExporter.h"
#import "EvernoteSDK.h"
#import <Twitter/Twitter.h>

@interface MNExporterViewController ()

@end

@implementation MNExporterViewController
@synthesize delegate = delegate_;
@synthesize isMp3 = isMp3_;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		
	}
	return self;
}

-(void)dealloc
{
	[self setMp3Button:nil];
	[self setAacButton:nil];
	[self setMailButton:nil];
	[self setTwitterButton:nil];
	[self setCancelButton:nil];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	
	isMp3_ = [[NSUserDefaults standardUserDefaults] boolForKey:@"MNExporterViewController_isMp3"];
	
	self.mp3Button.selected = isMp3_;
	self.aacButton.selected = !isMp3_;
	
	self.activityIndicator.hidden = YES;
	
	
	self.mailButton.enabled = [MFMailComposeViewController canSendMail];
	self.sendLinkButton.enabled = [MFMailComposeViewController canSendMail];

	
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
	BOOL iPad = ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad );
	if( iPad ) return UIInterfaceOrientationMaskAll;
	
	return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskPortraitUpsideDown;
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	SCAccount *account = [SCSoundCloud account];
	
	[self updateSoundCloudStatus];
	
	
	MNExporter* exporter = [MNExporter sharedExporter];
	
	
	[exporter addObserver: self
				  forKeyPath: @"loadingConnections"
					  options: NSKeyValueObservingOptionNew
					  context: nil];
	
	self.activityIndicator.hidden = YES;
	
	if( account )
	{
		[exporter setAccount:account];
	}
	
	else
	{
		self.activityIndicator.hidden = YES;
	}
	/////
	[self updateEvernoteStatus];
	
	
}

-(void)viewWillDisappear:(BOOL)animated
{
	[[MNExporter sharedExporter] removeObserver:self forKeyPath:@"loadingConnections"];
	
	[[NSUserDefaults standardUserDefaults] setBool:isMp3_ forKey:@"MNExporterViewController_isMp3"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	
	
	BOOL loading = [MNExporter sharedExporter].loadingConnections;
	
	self.activityIndicator.hidden = !loading;
	
	
	[self updateSoundCloudStatus];
	
	
}




- (IBAction)cancel:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidCancel: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidCancel: ) withObject:self];
		}
		
	}];
}

- (IBAction)twtter:(id)sender {
	
	
	[self dismissViewControllerAnimated:YES completion:^{
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectTwitter: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectTwitter: ) withObject:self];
		}
		
	}];
	
}

- (IBAction)sendLink:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectSendLink: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectSendLink: ) withObject:self];
		}
		
	}];
}



- (IBAction)openWithApp:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectApp: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectApp: ) withObject:self];
		}
		
	}];
}

- (IBAction)mail:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectMail: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectMail: ) withObject:self];
		}
		
	}];
}


- (IBAction)sendMore:(id)sender {
	
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectActivity: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectActivity: ) withObject:self];
		}
		
	}];
}


- (IBAction)soundCloud:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectSoundCloud: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectSoundCloud: ) withObject:self];
		}
		
	}];
}

- (IBAction)evernote:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:^{
		
		if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectEvernote: )] )
		{
			[self.delegate performSelector:@selector(exporterViewControllerDidSelectEvernote: ) withObject:self];
		}
		
	}];
}

- (IBAction)mp3:(id)sender {
	
	isMp3_ = YES;
	
	self.mp3Button.selected = isMp3_;
	self.aacButton.selected = !isMp3_;
	
}

- (IBAction)aac:(id)sender {
	isMp3_ = NO;
	
	self.mp3Button.selected = isMp3_;
	self.aacButton.selected = !isMp3_;
	
}

- (IBAction)iCloud:(id)sender {
	
	if( self.delegate && [self.delegate respondsToSelector:@selector(exporterViewControllerDidSelectICloud: )] )
	{
		[self.delegate performSelector:@selector(exporterViewControllerDidSelectICloud: ) withObject:self];
	}
	
	
}

- (IBAction)signInSoundCloud:(id)sender {
	
	SHOW_ERROR_ARC(@"Sound Cloud", @"MNExporterViewControllerSoundCloudMessage");
	
	SCLoginViewControllerCompletionHandler handler = ^(NSError *error) {
		if (SC_CANCELED(error)) {
			NSLog(@"Canceled!");
		} else if (error) {
			NSLog(@"Error: %@", [error localizedDescription]);
		} else {
			NSLog(@"Done!");
			
		}
	};
	
	[SCSoundCloud requestAccessWithPreparedAuthorizationURLHandler:^(NSURL *preparedURL) {
		SCLoginViewController *loginViewController;
		
		loginViewController = [SCLoginViewController loginViewControllerWithPreparedURL:preparedURL
																						  completionHandler:handler];
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
		{
			loginViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
		}
		
		[self presentModalViewController:loginViewController animated:YES];
	}];
}



- (IBAction)signInEvernote:(id)sender
{
	EvernoteSession *session = [EvernoteSession sharedSession];
	
	if (session.isAuthenticated) {
		
		[[EvernoteSession sharedSession] logout];
		[self updateEvernoteStatus];
		
	}else
	{
		[session authenticateWithViewController:self completionHandler:^(NSError *error) {
			if (error || !session.isAuthenticated) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																				message:@"Could not authenticate"
																			  delegate:nil
																  cancelButtonTitle:@"OK"
																  otherButtonTitles:nil];
				[alert show];
			} else {
				NSLog(@"authenticated! noteStoreUrl:%@ webApiUrlPrefix:%@", session.noteStoreUrl, session.webApiUrlPrefix);
				
				[self updateEvernoteStatus];
			}
		}];
		
	}
	
	
	
}


-(void)updateEvernoteStatus
{
	EvernoteSession *session = [EvernoteSession sharedSession];
	self.evernoteButton.enabled = session.isAuthenticated;
	
	self.evernoteLogInButton.selected = session.isAuthenticated;
	self.evernoteButton.enabled = session.isAuthenticated;
	
}


-(void)updateSoundCloudStatus
{
	SCAccount *account = [SCSoundCloud account];
	if (account != nil) {
		self.soundCloudLogInButton.selected = YES;
	}
	
	BOOL isSettingup = [MNExporter sharedExporter].loadingConnections;
	
	BOOL hasLongIn = (account&&!isSettingup)?YES:NO;
	self.twitterButton.enabled = hasLongIn;
	self.sendLinkButton.enabled = hasLongIn && [MFMailComposeViewController canSendMail];
	self.sendMoreButton.enabled = hasLongIn;
	self.soundCloudButton.enabled = hasLongIn;
	
	self.soundCloudLogInButton.selected = hasLongIn;
	
	if( self.twitterButton.enabled && ![TWTweetComposeViewController canSendTweet] )
	{
		self.twitterButton.enabled = NO;
	}
	
}
@end


/*
 
 Upload sample
 
 2012-09-05 18:18:58.221 iPod Player[58602:907] result {
 "artwork_url" = "<null>";
 "attachments_uri" = "https://api.soundcloud.com/tracks/58761570/attachments";
 bpm = "<null>";
 "comment_count" = 0;
 commentable = 1;
 "created_at" = "2012/09/05 06:18:57 +0000";
 "created_with" =     {
 "external_url" = "";
 id = 54479;
 kind = app;
 name = "My App";
 "permalink_url" = "http://soundcloud.com/apps/my-app-5";
 uri = "https://api.soundcloud.com/apps/54479";
 };
 description = "<null>";
 "download_count" = 0;
 "download_url" = "https://api.soundcloud.com/tracks/58761570/download";
 downloadable = 1;
 "downloads_remaining" = 100;
 duration = 0;
 "embeddable_by" = all;
 "favoritings_count" = 0;
 genre = "<null>";
 id = 58761570;
 isrc = "<null>";
 "key_signature" = "<null>";
 kind = track;
 "label_id" = "<null>";
 "label_name" = "<null>";
 license = "all-rights-reserved";
 
 "original_content_size" = "<null>";
 "original_format" = unknown;
 permalink = "funny-sounds";
 "permalink_url" = "http://soundcloud.com/user852782700/funny-sounds";
 "playback_count" = 0;
 "purchase_title" = "<null>";
 "purchase_url" = "<null>";
 release = "<null>";
 "release_day" = "<null>";
 "release_month" = "<null>";
 "release_year" = "<null>";
 "secret_token" = "s-ooD2z";
 "secret_uri" = "https://api.soundcloud.com/tracks/58761570?secret_token=s-ooD2z";
 "shared_to_count" = 0;
 sharing = private;
 state = processing;
 "stream_url" = "https://api.soundcloud.com/tracks/58761570/stream";
 streamable = 1;
 "tag_list" = "\"soundcloud:source=iPod Player\"";
 title = "Funny sounds";
 "track_type" = recording;
 uri = "https://api.soundcloud.com/tracks/58761570";
 user =     {
 "avatar_url" = "https://a1.sndcdn.com/images/default_avatar_large.png?b96a101";
 id = 23519813;
 kind = user;
 permalink = user852782700;
 "permalink_url" = "http://soundcloud.com/user852782700";
 uri = "https://api.soundcloud.com/users/23519813";
 username = user852782700;
 };
 "user_favorite" = 0;
 "user_id" = 23519813;
 "user_playback_count" = 1;
 "video_url" = "<null>";
 "waveform_url" = "https://a1.sndcdn.com/images/player-waveform-medium.png?b96a101";
 }
 
 
 
 */