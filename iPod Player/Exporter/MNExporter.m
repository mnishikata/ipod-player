//
//  MNExporter.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 3/09/12.
//
//

#import "MNExporter.h"
#import "MNMP3Converter.h"
#import "Tui.h"
#import "MNExporterViewController.h"
#import "MBProgressHUD.h"
#import <Twitter/Twitter.h>
//#import "MNSoundClip.h"
#import "AB_Player-Swift.h"

#import "SCUI.h"
#import "JSONKit.h"
#import "SCAccount+Private.h"
#import "EvernoteSDK.h"
#import "MD5.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface EDAMResourceAttributes ()
- (void) unsetSourceURL;
-(void)unsetAltitude;
-(void)unsetLongitude;
-(void)unsetLatitude;
@end


@implementation MNExporter
@synthesize parentViewController = parentViewController_;
@synthesize account = account_;

static MNExporter* sharedExporter_;
static dispatch_queue_t convertQueue = nil;
static MNMP3Converter* mp3converter = nil;


// Sound Cloud
#define YOUR_CLIENT_ID @"c681641e56743a9a3f476d875e4f8c05"
#define YOUR_CLIENT_SECRET @"8de284f363ef92aca35f03aaab9a5704"


// Evernote
#define CONSUMER_KEY @"masa1974-4109"
#define CONSUMER_SECRET @"f5b3fc7da8de4962"


+(MNExporter*)sharedExporter
{
	if( !sharedExporter_ )
		sharedExporter_ = [[MNExporter alloc] init];
	
	return sharedExporter_;
}

- (id)init
{
	self = [super init];
	if (self) {
		[SCSoundCloud setClientID:YOUR_CLIENT_ID
								 secret:YOUR_CLIENT_SECRET
						  redirectURL:[NSURL URLWithString:@"soundcloud-c681641e56743a9a3f476d875e4f8c05://oauth"]];
		
		
		////
		
		
		NSString *EVERNOTE_HOST = @"www.evernote.com";
		
		// Fill in the consumer key and secret with the values that you received from Evernote
		// To get an API key, visit http://dev.evernote.com/documentation/cloud/
		
		// set up Evernote session singleton
		[EvernoteSession setSharedSessionHost:EVERNOTE_HOST
										  consumerKey:CONSUMER_KEY
									  consumerSecret:CONSUMER_SECRET];
		
		
	}
	return self;
}


-(void)dealloc
{
	assetToExport_ = nil;
	progressHUD_ = nil;
}



- (void)exportAsset:(AVURLAsset*)assetToExport soundClip:(MNSoundClip*)clip range:(CMTimeRange)exportRange fromRect:(CGRect)rect inView:(UIView*)view inViewController:(UIViewController*)viewController
{
	
	soundClip_ = clip;
	parentViewController_ = viewController;
	assetToExport_ = assetToExport;
	exportRange_ = exportRange;
	
	MNExporterViewController *controller = [[MNExporterViewController alloc] init];
	
	controller.isMp3 = YES;
	controller.delegate = self;
	
	if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) controller.modalPresentationStyle = UIModalPresentationFormSheet;
	
	[parentViewController_ presentViewController:controller animated:YES completion:nil];
	
}


#pragma mark - Exporter View Controller Delegates

-(void)exporterViewControllerDidSelectICloud:(MNExporterViewController*)viewController
{
	
	__block MNExporter* __weak exporter = self;
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		if( success )
		{
			dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
			dispatch_async(queue, ^{
				
				NSFileManager *fm = [[NSFileManager alloc] init];
				
				NSURL* iCloudURL = [fm URLForUbiquityContainerIdentifier:nil];
				
				
				if( !iCloudURL ) return;
				
				
				NSString* filename = @"20120101";
				
				
				
				NSURL * itemURL = [iCloudURL URLByAppendingPathComponent:filename];
				BOOL success = [fm setUbiquitous:YES itemAtURL:destinationURL destinationURL:itemURL error:nil];
				
				
				
				if( !success ) return;
				
				NSDate* date = nil;
				NSURL* publicURL = [fm URLForPublishingUbiquitousItemAtURL:itemURL expirationDate:&date error:nil];
				
			});
			
			
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
	}];
	
}

-(void)exporterViewControllerDidSelectActivity:(MNExporterViewController*)viewController
{
	
	if( self.loadingConnections )
	{
		NSLog(@"Please wait");
		return;
	}
	
	__block MNExporter* __weak exporter = self;
	
	
	
	if( soundClip_.permalink )
	{
		
		MNBlockAlertView* alert = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"Already Uploaded", @"")
																					 message:NSLocalizedString(@"Do you want to upload to Sound Cloud Again?", @"")];
		
		[alert addTitle:NSLocalizedString(@"No, use existing Link", @"") actionBlock:^{
			
			
			[exporter displayActivityComposerSheet: nil];
			
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Upload again", @"") actionBlock:^{
			
			soundClip_.permalink = nil;
			[self exporterViewControllerDidSelectActivity: viewController];
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
		
		[alert show];
		
		return;
		
	}
	
	
	
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		if( success )
		{
			[exporter displayActivityComposerSheet: destinationURL];
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
	}];
	
	
	
}



-(void)exporterViewControllerDidSelectTwitter:(MNExporterViewController*)viewController
{
	if( self.loadingConnections )
	{
		NSLog(@"Please wait");
		return;
	}
	
	__block MNExporter* __weak exporter = self;
	
	if( soundClip_.permalink )
	{
		
		MNBlockAlertView* alert = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"Already Uploaded", @"")
																					 message:NSLocalizedString(@"Do you want to upload to Sound Cloud Again?", @"")];
		
		[alert addTitle:NSLocalizedString(@"No, use existing Link", @"") actionBlock:^{
			
			[exporter displayTwitterComposerSheet: nil];
			
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Upload again", @"") actionBlock:^{
			
			soundClip_.permalink = nil;
			[self exporterViewControllerDidSelectTwitter: viewController];
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
		
		[alert show];
		
		return;
		
	}
	
	
	
	
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		if( success )
		{
			[exporter displayTwitterComposerSheet: destinationURL];
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
	}];
	
}

-(void)exporterViewControllerDidSelectSendLink:(MNExporterViewController*)viewController
{
	BOOL canSend = [MFMailComposeViewController canSendMail];
	if( !canSend )
	{
		
		SHOW_ERROR_ARC(@"Cannot send email", @"The device is not configured for sending email.");
		return;
	}
	
	
	if( self.loadingConnections )
	{
		NSLog(@"Please wait");
		return;
	}
	
	__block MNExporter* __weak exporter = self;
	
	
	
	if( soundClip_.permalink )
	{
		
		MNBlockAlertView* alert = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"Already Uploaded", @"")
																					 message:NSLocalizedString(@"Do you want to upload to Sound Cloud Again?", @"")];
		
		[alert addTitle:NSLocalizedString(@"No, use existing Link", @"") actionBlock:^{
			
			
			[exporter displayLinkComposerSheet: nil];
			
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Upload again", @"") actionBlock:^{
			
			soundClip_.permalink = nil;
			[self exporterViewControllerDidSelectSendLink: viewController];
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
		
		[alert show];
		
		return;
		
	}
	
	
	
	
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		if( success )
		{
			[exporter displayLinkComposerSheet: destinationURL];
		}
		
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
	}];
	
}


-(void)exporterViewControllerDidSelectMail:(MNExporterViewController*)viewController
{
	BOOL canSend = [MFMailComposeViewController canSendMail];
	if( !canSend )
	{
		
		SHOW_ERROR_ARC(@"Cannot send email", @"The device is not configured for sending email.");
		return;
	}
	
	
	__block MNExporter* __weak exporter = self;
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		
		if( success )
		{
			[exporter displayMailComposerSheet: destinationURL];
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
		
	}];
	
}


-(void)exporterViewControllerDidSelectApp:(MNExporterViewController*)viewController
{

	__block MNExporter* __weak exporter = self;
	
	
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		

		if( success )
		{
			
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
				[exporter displayPickerSheet: destinationURL];
				
			});
		
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
		
	}];
	
}



-(void)exporterViewControllerDidSelectEvernote:(MNExporterViewController*)viewController
{
	__block MNExporter* __weak exporter = self;
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		if( success )
		{
			[exporter posetToEvernote: destinationURL];
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
		
	}];
	
}

-(void)exporterViewControllerDidSelectSoundCloud:(MNExporterViewController*)viewController
{
	__block MNExporter* __weak exporter = self;
	
	[self convertFile: viewController.isMp3 withCompletion:^(BOOL success, NSURL* destinationURL, NSError* error){
		
		if( success )
		{
			[exporter displaySoundCloudSheet: destinationURL];
		}
		else
		{
			NSString *message = [error localizedDescription];
			SHOW_ERROR_ARC( @"Could not convert",message);
		}
		
	}];
	
}

#pragma mark - Functions


-(void)displayMailComposerSheet:(NSURL*)attachmentURL
{
//	BOOL canSend = [MFMailComposeViewController canSendMail];
//	if( !canSend )
//	{
//		
//		SHOW_ERROR(@"Cannot send email", @"The device is not configured for sending email.");
//		return;
//	}
	
	NSString* subject = @"Sound Clip";
	
	if( soundClip_ )
	{
		subject = soundClip_.title;
		
	}
	
	
	NSString* emailBody = [self emailBody];
	
	mailComposer_ = [[MFMailComposeViewController alloc] init];
	mailComposer_.mailComposeDelegate = self;
	
	[mailComposer_ setSubject:subject];
	
	
	
	// Attach an image to the email.
	
	NSString* extension = [attachmentURL pathExtension];
	
	NSData *myData = [NSData dataWithContentsOfURL:attachmentURL];
	
	if( [extension isEqualToString:@"mp3"] )
	{
		[mailComposer_ addAttachmentData:myData mimeType:@"audio/mp3"
										fileName:@"soundclip.mp3"];
	}
	
	if( [extension isEqualToString:@"m4a"] )
	{
		[mailComposer_ addAttachmentData:myData mimeType:@"audio/aac"
										fileName:@"soundclip.m4a"];
	}
	
	
	// Fill out the email body text.
	[mailComposer_ setMessageBody:emailBody isHTML:NO];
	
	// Present the mail composition interface.
	
	
	[parentViewController_ presentViewController:mailComposer_ animated:YES completion:nil];
	
	
	
}


-(void)displayPickerSheet: (NSURL*)URL
{
	
	UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[URL] applicationActivities:nil	];
	
	
	CGRect rect = parentViewController_.view.frame;
//	BOOL flag =
//	[controller presentOpenInMenuFromRect:rect inView:parentViewController_.view animated:YES];
//	
//	if( flag == NO )
//	{
//		SHOW_ERROR_ARC(@"No app found",@"An app that can handle this file is not installed.");
//	}
	[parentViewController_ presentViewController:controller animated:YES completion:nil];
	
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller
  didPickDocumentAtURL:(NSURL *)url {
//	[parentViewController_ dismissViewControllerAnimated:YES completion:nil];
}

-(void)displayOpenWithSheet: (NSURL*)URL
{
	
	UIDocumentInteractionController* controller = [UIDocumentInteractionController interactionControllerWithURL:URL];
	controller.delegate = self;
	
	documentInteractionController_ = controller;
	
	
	CGRect rect = parentViewController_.view.frame;
	BOOL flag =
	[controller presentOpenInMenuFromRect:rect inView:parentViewController_.view animated:YES];
	
	if( flag == NO )
	{
		SHOW_ERROR_ARC(@"No app found",@"An app that can handle this file is not installed.");
	}

	
}

- (void) documentInteractionControllerDidDismissOpenInMenu: (UIDocumentInteractionController *) controller
{
	documentInteractionController_ = nil;
}


-(void)displaySoundCloudSheet: (NSURL*)URL
{
	NSURL *trackURL = URL;
	
	SCShareViewController *shareViewController;
	SCSharingViewControllerComletionHandler handler;
	
	handler = ^(NSDictionary *trackInfo, NSError *error) {
		if (SC_CANCELED(error)) {
			NSLog(@"Canceled!");
		} else if (error) {
			NSLog(@"Error: %@", [error localizedDescription]);
		} else {
			NSLog(@"Uploaded track: %@", trackInfo);
		}
	};
	shareViewController = [SCShareViewController
								  shareViewControllerWithFileURL:trackURL
								  completionHandler:handler];
	[shareViewController setTitle: soundClip_.title];
	[shareViewController setSharingNote: [self emailBody]];
	[shareViewController setPrivate:YES];
	
	
	
	[parentViewController_ presentViewController:shareViewController animated:YES completion:nil];
	
	
	
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	popover_ = nil;
}

-(void)displayActivityComposerSheet:(NSURL*)attachmentURL
{
//	BOOL canSend = [MFMailComposeViewController canSendMail];
//	if( !canSend )
//	{
//		
//		//SHOW_ERROR(@"Cannot send email", @"The device is not configured for sending email.");
//		return;
//	}
	
	
	void(^block)(NSURL* url) = ^(NSURL* url){
		
		UIActivityViewController* controller = [[UIActivityViewController alloc] initWithActivityItems:@[url]
																										 applicationActivities:nil];
		
//		controller.excludedActivityTypes = @[UIActivityTypeMail, UIActivityTypePostToTwitter];
		
//		controller.completionHandler = ^(NSString *activityType, BOOL completed){
//			
//			if( (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) )
//			{
//				[popover_ dismissPopoverAnimated:YES];
//				popover_ = nil;
//			}else
//			{
//			[parentViewController_ dismissViewControllerAnimated:YES completion:nil];
//			}
//		};
		
		
		if( (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) )
		{
			UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController: controller];
			
//			[popover presentPopoverFromRect:(CGRect) inView:<#(UIView *)#> permittedArrowDirections:<#(UIPopoverArrowDirection)#> animated:<#(BOOL)#>
			[popover presentPopoverFromBarButtonItem: [(UINavigationController*)parentViewController_ topViewController].navigationItem.rightBarButtonItem
								 permittedArrowDirections:UIPopoverArrowDirectionAny
													  animated:YES];
			 
			
			
			popover_ = popover;
			popover_.delegate = self;
			
		}else
		{
			[parentViewController_ presentViewController:controller animated:YES completion:nil];
		}
		
		
	};
	
	
	if( !attachmentURL )
	{
		// User permalink
		
		NSURL* url = [NSURL URLWithString:soundClip_.permalink];
		block(url);
	}
	
	else
	{
		[self uploadToSoundClound:attachmentURL completion:^(BOOL success, NSString *permaLink) {
			
			if( success )
			{
				NSURL* url = [NSURL URLWithString:permaLink];
				block(url);
				
			}
			
		}];
		
	}
	
}



-(void)displayLinkComposerSheet:(NSURL*)attachmentURL
{
	BOOL canSend = [MFMailComposeViewController canSendMail];
	if( !canSend )
	{
		
		//SHOW_ERROR(@"Cannot send email", @"The device is not configured for sending email.");
		return;
	}
	
	
	void(^block)(NSString* permalink) = ^(NSString* permalink){
		
		NSString* subject = @"Sound Clip";
		
		if( soundClip_ )
		{
			subject = soundClip_.title;
		}
		
		
		NSString* emailBody = [self emailBody];
		
		emailBody = [NSString stringWithFormat:@"%@\n\n%@", permalink, emailBody];
		
		
		mailComposer_ = [[MFMailComposeViewController alloc] init];
		mailComposer_.mailComposeDelegate = self;
		
		[mailComposer_ setSubject:subject];
		
		
		
		// Fill out the email body text.
		[mailComposer_ setMessageBody:emailBody isHTML:NO];
		
		// Present the mail composition interface.
		[parentViewController_ presentViewController:mailComposer_ animated:YES completion:nil];
		
		
	};
	
	
	
	if( !attachmentURL )
	{
		// User permalink
		block(soundClip_.permalink);
		
	}else
	{
		
		[self uploadToSoundClound:attachmentURL completion:^(BOOL success, NSString *permaLink) {
			
			if( success )
			{
				block(permaLink);
			}
			
		}];
		
	}
	
	
}


-(void)displayTwitterComposerSheet:(NSURL*)attachmentURL
{
	if( ![TWTweetComposeViewController canSendTweet] )
		return;
	
	
	void(^block)(NSURL* url) = ^(NSURL* url){
		
		NSString* transcription = soundClip_.transcription;
		
		if( !transcription ) transcription = @"";
		
		NSString* text = [NSString stringWithFormat:@"#%@\n%@\n", NSLocalizedString(@"AppTag", @""), transcription, nil];
		
		TWTweetComposeViewController* controller = [[TWTweetComposeViewController alloc] init];
		
		[controller addURL:url];
		[controller setInitialText:text];
		
		
		controller.completionHandler = ^(TWTweetComposeViewControllerResult result)
		{
			[parentViewController_ dismissViewControllerAnimated:YES completion:nil];
		};
		
		[parentViewController_ presentViewController:controller animated:YES completion:nil];
		
		
	};
	
	if(! attachmentURL )
	{
		// User permalink
		
		NSURL* url = [NSURL URLWithString:soundClip_.permalink];
		block(url);
		
	}else
	{
		
		[self uploadToSoundClound:attachmentURL completion:^(BOOL success, NSString *permaLink) {
			
			if( success )
			{
				block([NSURL URLWithString: permaLink]);
				
			}
		}];
		
	}
	
	
}


-(void)uploadToSoundClound:(NSURL*)attachmentURL completion:(void (^)(BOOL success, NSString* permaLink))completion
{
	
	
	// set up request
	NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
	
	// track
	[parameters setObject:attachmentURL forKey:@"track[asset_data]"];
	
	
	// metadata
	NSString* title = soundClip_.title;
	[parameters setObject:title forKey:@"track[title]"];
	[parameters setObject:@"public"  forKey: @"track[sharing]"];
	[parameters setObject:@"1" forKey: @"track[downloadable]"];
	[parameters setObject:@"spoken" forKey:@"track[track_type]"];
	
	// sharing
	
	
	
	// DESCRIPTION
	
	
	[parameters setObject:[self emailBody] forKey:@"track[description]"];
	
	
	// artwork
	
	
	// App name
	NSString * appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
	
	
	// tags (location)
	NSMutableArray *tags = [NSMutableArray array];
	[tags addObject:[NSString stringWithFormat:@"\"%@\"", appName]];
	
	
	
	// tags (custom)
	
	[tags addObject:NSLocalizedString(@"AppTag", @"")];
	
	
	[parameters setObject:[tags componentsJoinedByString:@" "] forKey:@"track[tag_list]"];
	
	
	[self showProgressHUDWithMessage:@"Uploading..."];
	
	progressHUD_.dimBackground = YES;
	progressHUD_.graceTime = 0;
	progressHUD_.minShowTime = 0.0;
	
	
	// perform request
	self.uploadRequestHandler = [SCRequest performMethod:SCRequestMethodPOST
															onResource:[NSURL URLWithString:@"https://api.soundcloud.com/tracks.json"]
													 usingParameters:parameters
														  withAccount:self.account
											sendingProgressHandler:^(unsigned long long bytesSend, unsigned long long bytesTotal){
												
												double progress = (double)bytesSend / (double)bytesTotal;
												
												progressHUD_.mode = MBProgressHUDModeDeterminate;
												[progressHUD_ setProgress:(CGFloat)progress];
												
											}
										  
													 responseHandler:^(NSURLResponse *response, NSData *data, NSError *error){
														 
														 [self hideProgressHUD:YES];
														 
														 
														 self.uploadRequestHandler = nil;
														 
														 NSError *jsonError = nil;
														 id result = [data objectFromJSONData]; //[NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
														 
														 
														 if (error || jsonError || result == nil)
														 {
															 
															 // Handle Error
															 
															 if (error)
																 NSLog(@"Upload failed with error: %@", [error localizedDescription]);
															 
															 if (jsonError)
																 NSLog(@"Upload failed with json error: %@", [jsonError localizedDescription]);
															 
															 
															 NSString* message = [error localizedDescription];
															 
															 SHOW_ERROR_ARC(@"Could not upload", message);
															 
															 completion(NO, nil);
															 
														 } else
														 {
															 NSString* link = [result objectForKey:@"permalink_url"];
															 
															 soundClip_.permalink = link;
															 [APP_DELEGATE saveContext];
															 
															 completion(YES, link);
															 
															 
														 }
													 }];
	
}

-(void)posetToEvernote:(NSURL*)fileURL
{
	[self showProgressHUDWithMessage:@"Uploading..."];
	
	
	NSString* path = fileURL.path;
	
	NSString* mime = @"audio/mp3";
	NSString* title = [soundClip_.title stringByAppendingPathExtension:@"mp3"];
	
	
	if( [path.pathExtension isEqualToString:@"m4a"] )
	{
		mime = @"audio/aac";
		title = [soundClip_.title stringByAppendingPathExtension:@"m4a"];
	}
	
	[self postFileAtDispatch:path
							  mime:mime
							 title:title
					creationDate:[NSDate date]
							  tags:nil
						addGeoTag:NO
				completionBlock:^(NSError* error){
					
					[self hideProgressHUD:YES];
					
					
					if( error )
					{
						NSLog(@"With error: %@", error.localizedDescription);
					}
					
					
				}];
	
}

-(void)postFileAtDispatch:(NSString*)path mime:(NSString*)mime title:(NSString*)title creationDate:(NSDate*)creationDate tags:(NSArray*)tags addGeoTag:(BOOL)geoTag completionBlock:(void (^)(NSError* error))completionBlock
{
	EvernoteSession *session = [EvernoteSession sharedSession];
	if( !session.isAuthenticated )
	{
		if( completionBlock )
		{
			NSError *error = [NSError errorWithDomain:@"com.catalystwo.fastfinga3" code:-1 userInfo:nil];
			
			completionBlock(error);
		}
		
		return;
		
	}
	
	
	
	title = [title omitSpacesAtBothEnds];
	
	//
	//
	//	NSString *consumerKey = [[NSString alloc] initWithString: MY_CONSUMER_KEY ] autorelease];
	//	NSString *consumerSecret = [[[NSString alloc] initWithString: MY_CONSUMER_SECRET] autorelease];
	//
	//	NSURL *userStoreUri = [[NSURL alloc] initWithString: @"https://www.evernote.com/edam/user"];
	//	NSString *noteStoreUriBase = [[NSString alloc] initWithString: @"http://www.evernote.com/edam/note/"];
	//
	//
	//
	//	THTTPClient *userStoreHttpClient = [[[THTTPClient alloc] initWithURL:userStoreUri] autorelease];
	//	TBinaryProtocol *userStoreProtocol = [[[TBinaryProtocol alloc] initWithTransport:userStoreHttpClient] autorelease];
	//	EDAMUserStoreClient *userStore = [[[EDAMUserStoreClient alloc] initWithProtocol:userStoreProtocol] autorelease];
	//	EDAMNotebook* defaultNotebook = NULL;
	//
	//	CFBundleRef bundle = CFBundleGetMainBundle();
	//	CFStringRef bundleVersion = CFBundleGetValueForInfoDictionaryKey(bundle, CFSTR("CFBundleVersion"));
	//
	//	CFStringRef bundleName = CFBundleGetValueForInfoDictionaryKey(bundle, CFSTR("CFBundleIdentifier"));
	//
	//
	//	NSString *agentName = [NSString stringWithFormat:@"%@/%@", bundleName, bundleVersion];
	//
	//	MNLOG(@"agentName %@",agentName);
	//
	//	BOOL versionOk = NO;
	//
	//	@try {
	//
	//		versionOk = [userStore checkVersion:agentName :
	//						 [EDAMUserStoreConstants EDAM_VERSION_MAJOR] :
	//						 [EDAMUserStoreConstants EDAM_VERSION_MINOR]];
	//
	//	}
	//	@catch (NSException *exception) {
	//
	//		if( [[exception name] isEqualToString:@"TTransportException"] )
	//		{
	//			NSError *error = [NSError errorWithDomain:@"catalystwo.com" code:0 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Evernote is not responding", NSLocalizedDescriptionKey, nil]];
	//
	//			dispatch_async(dispatch_get_main_queue(), ^{completionBlock(error);});
	//
	//			return;
	//		}
	//
	//	}
	//
	//
	//
	//
	//	if (versionOk == YES)
	//	{
	//		EDAMAuthenticationResult* authResult;
	//		@try {
	//			authResult =
	//			[userStore authenticate:username :password
	//										  :consumerKey :consumerSecret];
	//
	//		}
	//		@catch (NSException * e) {
	//
	//			NSError *error = [NSError errorWithDomain:@"catalystwo.com" code:-1 userInfo:nil];
	//			MNLOG(@"Authentication Error (%@)",[e reason]);
	//
	//			dispatch_async(dispatch_get_main_queue(), ^{completionBlock(error);});
	//
	//			return;
	//		}
	//
	//		EDAMUser *user = [authResult user];
	//		//user.attributes.recognitionLanguage = @"en";
	//
	//		NSString *authToken = [authResult authenticationToken];
	//		MNLOG(@"Authentication was successful for: %@", [user username]);
	//		MNLOG(@"Authentication token: %@", authToken);
	
	//		NSURL *noteStoreUri =  [[[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@",
	//																				 noteStoreUriBase, [user shardId]] ]autorelease];
	//		THTTPClient *noteStoreHttpClient = [[[THTTPClient alloc] initWithURL:noteStoreUri] autorelease];
	//		TBinaryProtocol *noteStoreProtocol = [[[TBinaryProtocol alloc] initWithTransport:noteStoreHttpClient] autorelease];
	//		EDAMNoteStoreClient *noteStore = [[[EDAMNoteStoreClient alloc] initWithProtocol:noteStoreProtocol] autorelease];
	//
	//
	
	
	EvernoteNoteStore *noteStore = [EvernoteNoteStore noteStore];
	
	[noteStore listNotebooksWithSuccess:^(NSArray *notebooks) {
		
		
		EDAMNotebook* defaultNotebook;
		
		MNLOG(@"Found %d notebooks", [notebooks count]);
		for (int i = 0; i < [notebooks count]; i++)
		{
			EDAMNotebook* notebook = (EDAMNotebook*)[notebooks objectAtIndex:i];
			if ([notebook defaultNotebook] == YES)
			{
				defaultNotebook = notebook;
				break;
			}
			MNLOG(@" * %@", [notebook name]);
		}
		
		
		//// Got note book
		
		
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateStyle: NSDateFormatterShortStyle];
		[formatter setTimeStyle: NSDateFormatterShortStyle];
		
		EDAMNote *note = [[EDAMNote alloc] init];
		[note setNotebookGuid:[defaultNotebook guid]];
		NSString* filename = [title safeFilename];
		if( filename == nil || [filename isEqualToString:@""] ) filename = NSLocalizedString(@"Untitled",@"");
		
		[note setTitle:filename];
		
		
		
		
		
		NSMutableString* contentString = [[NSMutableString alloc] init];
		[contentString setString:	@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"];
		[contentString appendString:@"<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml.dtd\">"];
		[contentString appendString:@"<en-note>"];
		
		
		NSMutableString* emailBody = [NSMutableString stringWithString:[self emailBody]];
		[emailBody replaceOccurrencesOfString:@"\n" withString:@"<br/>" options:0 range:NSMakeRange(0, emailBody.length)];
		
		[contentString appendString:emailBody];
		[contentString appendString:@"<br/>"];
		
		
		/// Set image
		NSData *rawImageData = [NSData dataWithContentsOfFile: path];
		
		
		MD5 md5 = MD5WithData(rawImageData);
		
		/////
		////  SOME Windows does not recognize upper-case hash.  SOME do.  Mac recognize it, however once edited in Mac client, attachments disappear.
		/////
		
		NSString* md5string = nil;
		
		//		if( [[NSUserDefaults standardUserDefaults] boolForKey:@"EvernotePolarity"] )
		//		{
		//			md5string = MD5ToString(&md5);
		//		}else
		{
			md5string = [MD5ToString(&md5) lowercaseString];
		}
		
		/////
		/////
		/////
		
		EDAMResource *imageResource = [[EDAMResource alloc] init];
		
		EDAMTimestamp timestamp = (long long)[creationDate timeIntervalSince1970] * 1000;
		EDAMData *imageData = [[EDAMData alloc] initWithBodyHash:[NSData dataWithBytes:&md5 length:sizeof(MD5)] size:[rawImageData length] body:rawImageData];
		[imageResource setData:imageData];
		[imageResource setNoteGuid: [defaultNotebook guid]];
		//[imageResource setRecognition:imageData];
		
		
		
		
		
		EDAMResourceAttributes *attributes = [[EDAMResourceAttributes alloc] initWithSourceURL:nil
																											  timestamp:timestamp
																												latitude:0
																											  longitude:0
																												altitude:0
																											 cameraMake:nil
																											cameraModel:@""
																									  clientWillIndex:NO
																												recoType:nil
																												fileName:filename
																											 attachment:YES
																									  applicationData:nil];
		[imageResource setAttributes: attributes];
		[attributes unsetSourceURL];
		
		
		[attributes unsetAltitude];
		[attributes unsetLongitude];
		[attributes unsetLatitude];
		
		
		
		
		[imageResource setMime:mime];
		
		[note setResources:[NSArray arrayWithObject:imageResource]];
		[note setTagNames:tags];
		////
		
		MNLOG(@"[rawImageData length] %d",[rawImageData length]);
		MNLOG(@"md5string %@",md5string);
		
		
		[contentString appendString:[NSString stringWithFormat: @"<en-media type=\"%@\" hash=\"%@\"/> " , mime, md5string]];
		
		
		
		
		
		[contentString appendString:@"</en-note>"];
		
		
		
		
		[note setContent:contentString];
		
		MNLOG(@"contentString\n %@",[contentString description]);
		
		[note setCreated:timestamp];
		
		
		
		@try {
			
			[noteStore createNote:note success:^(EDAMNote *note){
				
				dispatch_async(dispatch_get_main_queue(), ^{completionBlock(nil);});
				
				
			} failure:^(NSError *error){
				
				dispatch_async(dispatch_get_main_queue(), ^{completionBlock(error);});
				
			}];
			
			
			//
			//			NSString* content = [noteStore getNoteContent:authToken :(EDAMGuid)GUID];
			//			MNLOG(@"content %@",content);
		}
		@catch (NSException * e) {
			
			NSError *error = [NSError errorWithDomain:@"catalystwo.com" code:-2 userInfo:nil];
			MNLOG(@"Creation Error (%@)",[e reason]);
			
			dispatch_async(dispatch_get_main_queue(), ^{completionBlock(error);});
			
			return;
			
			
		}
		
	}
	 
	 //Fetching notebook failed
	 
										 failure:^(NSError *error) {
											 NSLog(@"error %@", error);
											 
											 
										 }];
	
	
}


#pragma mark - Export Utilities


-(void)convertFile:(BOOL)isMp3 withCompletion:(void (^)(BOOL, NSURL*, NSError*))completionHandler
{
	[self showProgressHUDWithMessage:@"Converting..."];
	
	progressHUD_.dimBackground = YES;
	progressHUD_.graceTime = 0;
	progressHUD_.minShowTime = 0.0;
	
	
	[self exportAsset:assetToExport_
		  convertToMP3:isMp3
					range:exportRange_
	  progressHandler:^(BOOL indeterminate, double currentProgress, double maximum){
		  
		  
		  if( indeterminate )
		  {
			  
		  }else
		  {
			  CGFloat progress = currentProgress / maximum;
			  
			  progressHUD_.mode = MBProgressHUDModeDeterminate;
			  [progressHUD_ setProgress:progress];
			  
		  }
		  
		  
	  }
	 
	completionHandler:^(NSURL* destinationURL, NSError* error){
		
		
		[self hideProgressHUD:YES];
		
		
		
		if( error )
		{
			completionHandler(NO, nil, error);
			
			NSLog(@"Error %@", error.localizedDescription);
		}
		
		else
		{
			NSLog(@"convertFile completed");
			
			completionHandler(YES, destinationURL, error);
			
		}
	}];
	
}



-(void)exportAsset:(AVAsset*)assetToExport convertToMP3:(BOOL)convertToMP3 range:(CMTimeRange)exportRange progressHandler:(void (^)(BOOL indeterminate, double currentProgress, double maximum))progressHandler completionHandler:(void (^)(NSURL*, NSError*))completionHandler
{

	
	//mp3を直接convertFileToMP3に渡しても、ExtAudioFileOpenURLが処理してくれない。
	//したがって一旦m4a変換作業は必要。
	
	// mp3 変換速度 duration 1:05 mp3 ->
	
	NSArray* metadata = [assetToExport commonMetadata];
	NSArray* id3metadata = [assetToExport metadataForFormat:@"org.id3"];
	NSArray* iTunesMetadata = [assetToExport metadataForFormat:@"com.apple.itunes"];
	
	metadata = [ metadata arrayByAddingObjectsFromArray: id3metadata];
	metadata = [ metadata arrayByAddingObjectsFromArray: iTunesMetadata];
	
	
	NSString* songTitle = @"clip";
	
	for( AVMetadataItem* item in metadata )
	{
		if( [item.commonKey isEqualToString:@"title"] )
		{
			songTitle = [item stringValue];
			songTitle = [songTitle safeFilename];
			break;
		}
	}
	
	
	
	progressHandler(NO, 0, 0 );
	
	NSArray* presets = [AVAssetExportSession exportPresetsCompatibleWithAsset:assetToExport];
	
	
	if( [presets containsObject:AVAssetExportPresetAppleM4A] )
	{
		AVAssetExportSession* exportSession = [[AVAssetExportSession alloc] initWithAsset: assetToExport
																									  presetName: AVAssetExportPresetAppleM4A];
		
		NSURL* tempURL = [NSURL fileURLWithPath: NSTemporaryDirectory() ];
		
		tempURL = [tempURL URLByAppendingPathComponent:@"com.catalystwo"];
		
		NSFileManager *fm = [NSFileManager defaultManager];
		
		if( ![fm fileExistsAtPath:tempURL.path] )
		{
			[fm createDirectoryAtURL:tempURL withIntermediateDirectories:YES attributes:nil error:nil];
		}
		
		NSString* filename = [NSString stringWithFormat:@"%@.m4a", songTitle];
		
		tempURL = [tempURL URLByAppendingPathComponent:filename];
		
		if( [fm fileExistsAtPath:tempURL.path] )
		{
			[fm removeItemAtURL:tempURL error:nil];
		}
		
		
		exportSession.outputURL = tempURL;
		exportSession.outputFileType = AVFileTypeAppleM4A;
		exportSession.timeRange = exportRange;
		
		// Start exporting
		[exportSession exportAsynchronouslyWithCompletionHandler:^{
			
			
			dispatch_async(dispatch_get_main_queue(), ^{
				progressHandler(NO, 0, 0 );
			});
			
			
			if( exportSession.status == AVAssetExportSessionStatusFailed|| exportSession.status == AVAssetExportSessionStatusCancelled )
			{
				
				dispatch_async(dispatch_get_main_queue(), ^{
					completionHandler( nil, [exportSession error] );
				});
				
			}
			
			if( exportSession.status == AVAssetExportSessionStatusCompleted )
			{
				if( !convertToMP3 )
				{
					
					dispatch_async(dispatch_get_main_queue(), ^{
						completionHandler(tempURL, nil);
					});
					
					return;
				}
				
				
				
				// Write to mp3 form m4a
				
				if( convertQueue == nil )
					convertQueue = dispatch_queue_create("Converter Queue", DISPATCH_QUEUE_SERIAL);
				
				if( mp3converter == nil )
					mp3converter = [[MNMP3Converter alloc] init];
				
				dispatch_async(convertQueue, ^{
					
					NSFileManager *fm = [[NSFileManager alloc] init];
					
					NSString* filename = [NSString stringWithFormat:@"%@.mp3", songTitle];

					NSURL* destinationURL = [[NSURL fileURLWithPath: NSTemporaryDirectory() ] URLByAppendingPathComponent:filename];
					
					if( [fm fileExistsAtPath:destinationURL.path] )
					{
						[fm removeItemAtURL:destinationURL error:nil];
					}
					
					
					[mp3converter convertFileToMP3: tempURL
										 destinationURL: destinationURL
												 metadata: metadata
										progressHandler: progressHandler
									 completionHandler: completionHandler ];
					
					
					[fm removeItemAtURL:tempURL error:nil];
					
				});
				
				
				
			}else {
				// Cleaning
				
				NSFileManager *fm = [[NSFileManager alloc] init];
				
				[fm removeItemAtURL:tempURL error:nil];
			}
			
		}];
	}
	
	else
	{
		NSString* string = [NSString stringWithFormat:@"Could not convert... This device only converts %@", [presets description]];
		
		
		NSDictionary* dict = [NSDictionary dictionaryWithObject:string forKey: NSLocalizedDescriptionKey];
		
		NSError* error = [NSError errorWithDomain:@"com.catalystwo" code:-1 userInfo:dict];

		completionHandler(nil, error);
	}
}


-(NSString*)emailBody
{
	NSString* emailBody = @"";
	
	
	if( soundClip_ )
	{
		
		NSString* comment = soundClip_.comment;
		NSString* albumTitle = soundClip_.albumTitle;
		NSString* title = soundClip_.title;
		NSString* transcription = soundClip_.transcription;
		
		double startTime = CMTimeGetSeconds(soundClip_.timeRange.start);
		double duration = CMTimeGetSeconds(soundClip_.timeRange.duration);
		double endTime = startTime + duration;


		double startTimeMin = floor( startTime /60 );
		double startTimeSec = startTime - startTimeMin*60;

		double durationMin = floor( duration /60 );
		double durationSec = duration - durationMin*60;

		double endTimeMin = floor( endTime /60 );
		double endTimeSec = endTime - endTimeMin*60;

		
		NSString* timeString = [NSString stringWithFormat:@"%.0f:%02.0f - %.0f:%02.0f (Duration %.0f:%02.0f)",startTimeMin, startTimeSec,  endTimeMin, endTimeSec, durationMin, durationSec];
		
		
		if( !comment ) comment = @"";
		if( !albumTitle ) albumTitle = @"";
		if( !title ) title = @"";
		if( !transcription ) transcription = @"";
		
		emailBody = [NSString stringWithFormat:@"[Transcript]\n%@\n\n[Comment]\n%@\n\n\n[Track]\n%d. %@\n\n[Time]\n%@\n\n[Disk]\n%@", transcription, comment, soundClip_.trackNumber, title, timeString, albumTitle];
	}
	
	return emailBody;
}



// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
			 didFinishWithResult:(MFMailComposeResult)result
								error:(NSError *)error
{
	[parentViewController_ dismissViewControllerAnimated:YES completion:^{ mailComposer_ = nil; }];
	
}



- (MBProgressHUD *)progressHUD {
	if (!progressHUD_) {
		progressHUD_ = [[MBProgressHUD alloc] initWithView:parentViewController_.view];
		progressHUD_.minSize = CGSizeMake(120, 120);
		progressHUD_.minShowTime = 0;
		// The sample image is based on the
		// work by: http://www.pixelpressicons.com
		// licence: http://creativecommons.org/licenses/by/2.5/ca/
		self.progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark.png"]];
		[parentViewController_.view addSubview:progressHUD_];
	}
	return progressHUD_;
}

- (void)showProgressHUDWithMessage:(NSString *)message {
	self.progressHUD.labelText = message;
	self.progressHUD.mode = MBProgressHUDModeIndeterminate;
	[self.progressHUD show:YES];
}

- (void)hideProgressHUD:(BOOL)animated {
	[self.progressHUD hide:animated];
}

- (void)showProgressHUDCompleteMessage:(NSString *)message {
	if (message) {
		if (self.progressHUD.isHidden) [self.progressHUD show:YES];
		self.progressHUD.labelText = message;
		self.progressHUD.mode = MBProgressHUDModeCustomView;
		[self.progressHUD hide:YES afterDelay:1.5];
	} else {
		[self.progressHUD hide:YES];
	}
}

#pragma mark - ==========================================
#pragma mark	Sound Cloud
#pragma mark   ==========================================

- (void)setAccount:(SCAccount *)anAccount;
{
	if (account_ != anAccount) {
		account_ = anAccount;
		
		if (self.account) {
			
			self.loadingConnections = YES;
			
			//			[self.tableView reloadData];
			
			[SCRequest performMethod:SCRequestMethodGET
							  onResource:[NSURL URLWithString:@"https://api.soundcloud.com/me/connections.json"]
						usingParameters:nil
							 withAccount:anAccount
			  sendingProgressHandler:nil
						responseHandler:^(NSURLResponse *response, NSData *data, NSError *error){
							
							self.loadingConnections = NO;
							
							if (data) {
								NSError *jsonError = nil;
								NSArray *result = [data objectFromJSONData];
								if (result) {
									
									
									
									
									//									[self setAvailableConnections:result];
								} else {
									NSLog(@"%s json error: %@", __FUNCTION__, [jsonError localizedDescription]);
								}
							} else {
								NSLog(@"%s error: %@", __FUNCTION__, [error localizedDescription]);
							}
						}];
			
			[SCRequest performMethod:SCRequestMethodGET
							  onResource:[NSURL URLWithString:@"https://api.soundcloud.com/me.json"]
						usingParameters:nil
							 withAccount:anAccount
			  sendingProgressHandler:nil
						responseHandler:^(NSURLResponse *response, NSData *data, NSError *error){
							if (data) {
								NSError *jsonError = nil;
								id result = [data objectFromJSONData];
								if (result) {
									
									anAccount.userInfo = result;
									
									NSURL *avatarURL = [NSURL URLWithString:[result objectForKey:@"avatar_url"]];
									NSData *avatarData = [NSData dataWithContentsOfURL:avatarURL];
									//									[self.headerView setAvatarImage:[UIImage imageWithData:avatarData]];
									//									[self.headerView setUserName:[result objectForKey:@"username"]];
									
								} else {
									NSLog(@"%s json error: %@", __FUNCTION__, [jsonError localizedDescription]);
								}
							} else {
								NSLog(@"%s error: %@", __FUNCTION__, [error localizedDescription]);
							}
						}];
		} else {
			//			[self.headerView setAvatarImage:nil];
			//			[self.headerView setUserName:nil];
		}
	}
}




@end
