//
//  EmbossButton.m
//  NZSound
//
//  Created by Masatoshi Nishikata on 7/11/10.
//  Copyright 2010 Catalystwo Ltd. All rights reserved.
//

#import "ExportCancelButton.h"
#import "DrawUtils.h"

@implementation ExportCancelButton


#define RGBA(r,g,b,a) [UIColor colorWithRed: r/255.0f green: g/255.0f \
blue: b/255.0f alpha: a]

#define BG_RGBA(r,g,b,a) CGContextSetRGBFillColor(context, r/255.0f, \
g/255.0f, b/255.0f, a)



#define kPriceBGColorT RGBA(255, 255, 255, 0.8)
#define kPriceBGColorB RGBA(220, 220, 220, 0.8)
#define kPriceBGColorDisabled RGBA(220, 220, 220, 0.4)

#define kSelectBGColorT RGBA(109, 116, 130, 1)
#define kSelectBGColorB RGBA(78, 85, 99, 1)

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		 [self setup];

    }
    return self;
}

-(void)awakeFromNib
{
	[self setup];
}

-(void)setup
{
	
	UIImage* image = [UIImage imageNamed:@"ExportCancelButton"];
	image = [image stretchableImageWithLeftCapWidth:60 topCapHeight:24];
	[self setBackgroundImage:image forState:UIControlStateNormal];

	
	image = [UIImage imageNamed:@"ExportCancelButtonHighlighted"];
	image = [image stretchableImageWithLeftCapWidth:60 topCapHeight:24];
	[self setBackgroundImage:image forState:UIControlStateHighlighted];
	
	
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];

	self.titleLabel.font = [UIFont boldSystemFontOfSize:20];
}




@end
