//
//  MNMP3Converter.h
//  iPod Player
//
//  Created by Nishikata Masatoshi on 25/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <semaphore.h>
#import <libmp3lame/lame.h>


@interface MNMP3Converter : NSObject
{
	unsigned int lameBufferCounter;
	sem_t *lame_semaphore;
//	NSString *filename;
	BOOL lameWrite;
	lame_global_flags *lgf;
}
-(void)convertFileToMP3:(NSURL*) sourceURL  destinationURL:(NSURL*) destinationURL metadata:(NSArray*)metadataArray progressHandler:(void (^)(BOOL indeterminate, double currentProgress, double maximum))progressHandler completionHandler:(void (^)(NSURL*, NSError*))completionHandler;

@end
