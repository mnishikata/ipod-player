//
//  MNExporterViewController.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 3/09/12.
//
//

#import <UIKit/UIKit.h>

@interface MNExporterViewController : UIViewController
{
	BOOL isMp3_;

	id __weak delegate_;
	
}
@property (weak, nonatomic) id delegate;
@property (readwrite, nonatomic) BOOL isMp3;

@property (weak, nonatomic) IBOutlet UIButton *soundCloudLogInButton;
@property (weak, nonatomic) IBOutlet UIButton *evernoteLogInButton;

@property (weak, nonatomic) IBOutlet UIButton *mp3Button;
@property (weak, nonatomic) IBOutlet UIButton *aacButton;
@property (weak, nonatomic) IBOutlet UIButton *mailButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *sendLinkButton;
@property (weak, nonatomic) IBOutlet UIButton *soundCloudButton;
@property (weak, nonatomic) IBOutlet UIButton *sendMoreButton;
@property (weak, nonatomic) IBOutlet UIButton *evernoteButton;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)evernote:(id)sender;

- (IBAction)cancel:(id)sender;
- (IBAction)twtter:(id)sender;
- (IBAction)mail:(id)sender;
- (IBAction)mp3:(id)sender;
- (IBAction)aac:(id)sender;
- (IBAction)signInSoundCloud:(id)sender;
- (IBAction)soundCloud:(id)sender;
- (IBAction)sendLink:(id)sender;
- (IBAction)sendMore:(id)sender;
- (IBAction)signInEvernote:(id)sender;
- (IBAction)openWithApp:(id)sender;

@end
