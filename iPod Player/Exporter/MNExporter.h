//
//  MNExporter.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 3/09/12.
//
//

#import <Foundation/Foundation.h>
#include <MessageUI/MessageUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@class MBProgressHUD, MNSoundClip, SCAccount;

@interface MNExporter : NSObject <MFMailComposeViewControllerDelegate, UIPopoverControllerDelegate>
{
	MFMailComposeViewController *mailComposer_;

	UIDocumentInteractionController* documentInteractionController_;
	
	UIViewController * __weak parentViewController_;
	AVURLAsset* assetToExport_;
	MNSoundClip* soundClip_;
	CMTimeRange exportRange_;
	
	// IB
	UIPopoverController* popover_;
	MBProgressHUD* progressHUD_;

	//
	SCAccount *account_;
}
+(MNExporter*)sharedExporter;
- (void)exportAsset:(AVURLAsset*)assetToExport soundClip:(MNSoundClip*)clip range:(CMTimeRange)exportRange fromRect:(CGRect)rect inView:(UIView*)view inViewController:(UIViewController*)viewController;
-(void)exportAsset:(AVAsset*)assetToExport convertToMP3:(BOOL)convertToMP3 range:(CMTimeRange)exportRange progressHandler:(void (^)(BOOL indeterminate, double currentProgress, double maximum))progressHandler completionHandler:(void (^)(NSURL*, NSError*))completionHandler;
-(void)displayComposerSheet:(NSURL*)attachmentURL;
- (void)mailComposeController:(MFMailComposeViewController *)controller;
-(void)convertFile:(BOOL)isMp3 withCompletion:(void (^)(BOOL, NSURL*, NSError*))completionHandler;
-(void)uploadToSoundClound:(NSURL*)attachmentURL completion:(void (^)(BOOL success, NSString* permaLink))completion;
-(void)displayOpenWithSheet: (NSURL*)URL;

@property (weak, nonatomic) UIViewController * parentViewController;

@property (nonatomic, readwrite, retain) SCAccount *account;
@property (nonatomic, readwrite) BOOL loadingConnections;
@property (nonatomic, retain) id uploadRequestHandler;

@end
