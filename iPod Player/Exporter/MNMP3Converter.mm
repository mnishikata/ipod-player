//
//  MNMP3Converter.m
//  iPod Player
//
//  Created by Nishikata Masatoshi on 25/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MNMP3Converter.h"
#import <AudioToolbox/AudioConverter.h>
#include <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#include "CAXException.h"
#include "CAStreamBasicDescription.h"


#import <unistd.h>
#import <mach/mach.h>
#import <mach/mach_error.h>
#include <pthread.h>

#import <libmp3lame/lame.h>
#import <id3/tag.h>


#define SAMPLE_RATE 44100
#define BIT_RATE 128
#define OUT_SAMPLE_RATE 0 
#define CONSTANT 4

@implementation MNMP3Converter


- (id)init
{
	if ((self = [super init])) {

		lgf = lame_init();
		
		lame_set_num_channels(lgf,2);
		lame_set_in_samplerate(lgf,SAMPLE_RATE);
		lame_set_out_samplerate(lgf,OUT_SAMPLE_RATE);
		lame_set_brate(lgf, BIT_RATE);
		lame_set_scale(lgf, 0); /// volume
		
		
		id3tag_init(lgf);
		
	}
	return self;
}

- (void)setThreadPolicy
{
	kern_return_t error;
	thread_extended_policy_data_t extendedPolicy;
	thread_precedence_policy_data_t precedencePolicy;
	
	extendedPolicy.timeshare = 0;
	error = thread_policy_set(mach_thread_self(), THREAD_EXTENDED_POLICY,  (thread_policy_t)&extendedPolicy, THREAD_EXTENDED_POLICY_COUNT);
	if (error != KERN_SUCCESS)
		mach_error("Couldn't set feeder thread's extended policy", error);
	
	precedencePolicy.importance = 5;
	error = thread_policy_set(mach_thread_self(), THREAD_PRECEDENCE_POLICY, (thread_policy_t)&precedencePolicy, THREAD_PRECEDENCE_POLICY_COUNT);
	if (error != KERN_SUCCESS)
		mach_error("Couldn't set feeder thread's precedence policy", error);
}

static pthread_mutex_t  sStateLock;         // protects sState
static pthread_cond_t   sStateChanged;      // signals when interruption thread unblocks conversion thread
enum ThreadStates {
kStateRunning,
kStatePaused,
kStateDone
};
static ThreadStates sState;

// initialize the thread state
void ThreadStateInitalize()
{
	int rc;
	
	assert([NSThread isMainThread]);
	
	rc = pthread_mutex_init(&sStateLock, NULL);
	assert(rc == 0);
	
	rc = pthread_cond_init(&sStateChanged, NULL);
	assert(rc == 0);
	
	sState = kStateDone;
}

// handle begin interruption - transition to kStatePaused
void ThreadStateBeginInterruption()
{
	int rc;
	
	assert([NSThread isMainThread]);
	
	rc = pthread_mutex_lock(&sStateLock);
	assert(rc == 0);
	
	if (sState == kStateRunning) {
		sState = kStatePaused;
	}
	
	rc = pthread_mutex_unlock(&sStateLock);
	assert(rc == 0);
}

// handle end interruption - transition to kStateRunning
void ThreadStateEndInterruption()
{
	int rc;
	
	assert([NSThread isMainThread]);
	
	rc = pthread_mutex_lock(&sStateLock);
	assert(rc == 0);
	
	if (sState == kStatePaused) {
		sState = kStateRunning;
		
		rc = pthread_cond_signal(&sStateChanged);
		assert(rc == 0);
	}
	
	rc = pthread_mutex_unlock(&sStateLock);
	assert(rc == 0);                
}

// set state to kStateRunning
void ThreadStateSetRunning()
{
	int rc = pthread_mutex_lock(&sStateLock);
	assert(rc == 0);
	
	assert(sState == kStateDone);
	sState = kStateRunning;
	
	rc = pthread_mutex_unlock(&sStateLock);
	assert(rc == 0);
}

// block for state change to kStateRunning
Boolean ThreadStatePausedCheck()
{
	Boolean wasInterrupted = false;
	
	int rc = pthread_mutex_lock(&sStateLock);
	assert(rc == 0);
	
	assert(sState != kStateDone);
	
	while (sState == kStatePaused) {
		rc = pthread_cond_wait(&sStateChanged, &sStateLock);
		assert(rc == 0);
		wasInterrupted = true;
	}
	
	// we must be running or something bad has happened
	assert(sState == kStateRunning);
	
	rc = pthread_mutex_unlock(&sStateLock);
	assert(rc == 0);
	
	return wasInterrupted;
}

void ThreadStateSetDone()
{
	int rc = pthread_mutex_lock(&sStateLock);
	assert(rc == 0);
	
	assert(sState != kStateDone);
	sState = kStateDone;
	
	rc = pthread_mutex_unlock(&sStateLock);
	assert(rc == 0);
}

// ***********************
#pragma mark- Convert
/* The main Audio Conversion function using ExtAudioFile APIs */

// our own error code when we cannot continue from an interruption
enum {
	kMyAudioConverterErr_CannotResumeFromInterruptionError = 'CANT'
};


-(void)convertFileToMP3:(NSURL*) sourceURL  destinationURL:(NSURL*) destinationURL metadata:(NSArray*)metadataArray progressHandler:(void (^)(BOOL indeterminate, double currentProgress, double maximum))progressHandler completionHandler:(void (^)(NSURL*, NSError*))completionHandler
{
	
	ExtAudioFileRef sourceFile = 0;
	Boolean         canResumeFromInterruption = true; // we can continue unless told otherwise
	OSStatus        error = noErr;
	
	
	// Start from empty data
	[[NSData data] writeToURL:destinationURL atomically:NO];
	
	
	
	NSFileHandle *fh = [NSFileHandle fileHandleForWritingToURL:destinationURL error:nil];
	
	// open the source file
	XThrowIfError(ExtAudioFileOpenURL( (__bridge_retained CFURLRef)sourceURL, &sourceFile), "ExtAudioFileOpenURL failed");
	
	// Input buffer
	AudioStreamBasicDescription fileAudioFormat = {0};
	UInt32 formatPropertySize = sizeof (fileAudioFormat);
	
	
	OSStatus result =    ExtAudioFileGetProperty (
																 sourceFile,
																 kExtAudioFileProperty_FileDataFormat,
																 &formatPropertySize,
																 &fileAudioFormat
																 );
	UInt32 channelCount = fileAudioFormat.mChannelsPerFrame;
	
	
	AudioBufferList *fillBufList = (AudioBufferList*) malloc (
																				 sizeof (AudioBufferList) + sizeof (AudioBuffer) * (channelCount - 1)
																				 );
	
	
	// Output data
	unsigned char mp3Data[32768];
	int mp3DataSize;
	
	
	// in this sample we should never be on the main thread here
	assert(![NSThread isMainThread]);
	
	// transition thread state to kStateRunning before continuing
	//	ThreadStateSetRunning();
	
	
	try {
		CAStreamBasicDescription srcFormat, dstFormat;
		
		
		
		// get the source data format
		UInt32 size = sizeof(srcFormat);
		XThrowIfError(ExtAudioFileGetProperty(sourceFile, kExtAudioFileProperty_FileDataFormat, &size, &srcFormat), "couldn't get source data format");
		
		printf("Source file format: "); srcFormat.Print();
		
		// set the client format - The format must be linear PCM (kAudioFormatLinearPCM)
		// You must set this in order to encode or decode a non-PCM file data format
		// You may set this on PCM files to specify the data format used in your calls to read/write
		CAStreamBasicDescription clientFormat;
		//		if (outputFormat == kAudioFormatLinearPCM) {
		//			clientFormat = dstFormat;
		//		} else 
		{
			
			clientFormat.SetCanonical(channelCount, true);
			
			clientFormat.mSampleRate = srcFormat.mSampleRate;
			
			lame_set_num_channels(lgf,channelCount);
			lame_set_in_samplerate(lgf,srcFormat.mSampleRate);
			
			lame_init_params(lgf);
			
		}
		
		size = sizeof(clientFormat);
		XThrowIfError(ExtAudioFileSetProperty(sourceFile, kExtAudioFileProperty_ClientDataFormat, size, &clientFormat), "couldn't set source client format");
		
		size = sizeof(clientFormat);
		
		
		// Get max size
		double maximumFrames;
		SInt64 maxFramesInt;
		UInt32 maxFramesIntSize = sizeof( maxFramesInt );
		ExtAudioFileGetProperty(sourceFile, kExtAudioFileProperty_FileLengthFrames, &maxFramesIntSize, &maxFramesInt);
		
		NSLog(@"maxFramesInt %lld",maxFramesInt);
		maximumFrames = (double)maxFramesInt;
		
		// set up buffers
		UInt32 bufferByteSize = 32768;
		char srcBuffer[bufferByteSize];
		
		
		// keep track of the source file offset so we know where to reset the source for
		// reading if interrupted and input was not consumed by the audio converter
		SInt64 sourceFrameOffset = 0;
		
		//***** do the read and write - the conversion is done on and by the write call *****//
		printf("Converting...\n");
		while (1) {
			
			@autoreleasepool {
				
				
				fillBufList->mNumberBuffers = 1;
				fillBufList->mBuffers[0].mNumberChannels = 1; // Non interleaved
				fillBufList->mBuffers[0].mDataByteSize = bufferByteSize;
				fillBufList->mBuffers[0].mData = srcBuffer;
				
				
				// client format is always linear PCM - so here we determine how many frames of lpcm
				// we can read/write given our buffer size
				UInt32 numFrames = clientFormat.BytesToFrames(bufferByteSize); // (bufferByteSize / clientFormat.mBytesPerFrame);
				XThrowIfError(ExtAudioFileRead(sourceFile, &numFrames, fillBufList), "ExtAudioFileRead failed!");	
				if (!numFrames) {
					// this is our termination condition
					error = noErr;
					break;
				}
				
				
				sourceFrameOffset += numFrames;
				
				
				
				
				// this will block if we're interrupted
				//	Boolean wasInterrupted = ThreadStatePausedCheck();
				
				if ((error /*|| wasInterrupted*/) && (false == canResumeFromInterruption)) {
					// this is our interruption termination condition
					// an interruption has occured but the audio converter cannot continue
					error = kMyAudioConverterErr_CannotResumeFromInterruptionError;
					break;
				}
				
				
				if( channelCount == 1 )
				{
					mp3DataSize = lame_encode_buffer(lgf, 
																(short int *)fillBufList->mBuffers[0].mData,
																nil,
																(const int)numFrames,
																mp3Data,
																sizeof(mp3Data));
					
				}
				
				else
				{
					mp3DataSize = lame_encode_buffer_interleaved(lgf, 
																				(short int *)fillBufList->mBuffers[0].mData,
																				(const int)numFrames,
																				mp3Data,
																				sizeof(mp3Data));
					
				}
				
				[fh writeData: [NSData dataWithBytesNoCopy:mp3Data length:mp3DataSize freeWhenDone:NO]];
				
				

				
				dispatch_async(dispatch_get_main_queue(), ^{
					
					progressHandler(NO, (double)sourceFrameOffset, maximumFrames);
					
				});
	
				
				
				
			}//autorelease pool
		} // while
	}
	
	catch (CAXException e) {
		char buf[256];
		fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
		error = e.mError;
	}
	
	
	mp3DataSize = lame_encode_flush(lgf, mp3Data, sizeof(mp3Data));
	// fwrite(mp3Data, mp3DataSize, 1, lameFile);
	[fh writeData: [NSData dataWithBytesNoCopy:mp3Data length:mp3DataSize freeWhenDone:NO]];
	
	[fh closeFile];
	free (fillBufList);
	
	
	// close
	if (sourceFile) ExtAudioFileDispose(sourceFile);
	
	
	// transition thread state to kStateDone before continuing
	//	ThreadStateSetDone();
	
	
	// Update metadata
	
	if( error != noErr )
	{
		NSError* nserror = nil;
		nserror = [NSError errorWithDomain:@"Audio Error" code:error userInfo:nil];
		
		dispatch_async(dispatch_get_main_queue(), ^{
		
			completionHandler(nil, nserror);

		});
		
		return;
	}
	
	
	// Update tags
	
	ID3_Tag tag;
	
	// Write title tag
	tag.Link([destinationURL.path UTF8String]);
	tag.Strip(ID3TT_ALL);
	tag.Clear();
	
	
	
	for( AVMetadataItem *metadataItem in metadataArray )
	{
		NSString* commonKey = metadataItem.commonKey;
		NSString* key = (NSString*) metadataItem.key;
		
		
		// ID3の場合、数字
		if( [key isKindOfClass:[NSNumber class]] )
		{
			
			NSUInteger akey = [(NSNumber *)key unsignedIntegerValue]; 
			NSString *keyString = [NSString stringWithFormat:@"%.4s",&akey];
			
			//Swap ... NSSwapIntはうまく動作しない
			
			NSMutableString* mstr = [NSMutableString string];
			for( NSUInteger hoge = 0; hoge < keyString.length; hoge++ )
			{
				[mstr insertString:[keyString substringWithRange:NSMakeRange(hoge, 1)]
							  atIndex:0];
				
			}
			
			key = mstr;
		}
		
				
		
		// メタデータの設定
		
		if( [AVMetadataCommonKeyDescription isEqualToString:commonKey]  )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_COMMENT);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
			
		}
		
		
		if( [AVMetadataCommonKeyPublisher isEqualToString:commonKey] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_PUBLISHER);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
			
		}
		
		
		if( [AVMetadataCommonKeyCopyrights isEqualToString:commonKey] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_COPYRIGHT);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
			
		}
		
		
		
		if( [AVMetadataCommonKeyTitle isEqualToString:commonKey] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_TITLE);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
			
			
		}
		
		if( [AVMetadataCommonKeyAlbumName isEqualToString:commonKey] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_ALBUM);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
			
		}
		
		
		if( [AVMetadataCommonKeyArtist isEqualToString:commonKey] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_LEADARTIST);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
			
		}
		
		
		
		if( [@"COM" isEqualToString:key]  )
		{
			
			NSDictionary* dictionary = (NSDictionary*) [metadataItem value];
			
			if( [dictionary isKindOfClass:[NSDictionary class]] )
			{
				//コメントの場合はidentifierがエンプティ。その他の場合は、iTunes_CDDB_1など
				if( [[dictionary objectForKey:@"identifier"] length] == 0 )
				{
					ID3_Frame frame;
					
					frame.SetID(ID3FID_COMMENT);
					frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
					NSString *newTitle = [dictionary objectForKey:@"text"];
					
					
					frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
					
					tag.AddFrame(frame);
					
				}
			}
		}
		
		
		if( [@"TRK" isEqualToString:key] )
		{
			
			ID3_Frame frame;
			
			frame.SetID(ID3FID_TRACKNUM);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
		}
		
		
		if( [@"TCO" isEqualToString:key] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_CONTENTTYPE);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
		}
		
		if( [@"ULT" isEqualToString:key] )
		{
			NSDictionary* dictionary = (NSDictionary*) [metadataItem value];
			
			if( [dictionary isKindOfClass:[NSDictionary class]] )
			{
				//歌詞の場合はidentifierがエンプティ。
				if( [[dictionary objectForKey:@"identifier"] length] == 0 )
				{
					ID3_Frame frame;
					
					frame.SetID(ID3FID_UNSYNCEDLYRICS);
					frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
					NSString *newTitle = [dictionary objectForKey:@"text"];
					
					frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
					
					tag.AddFrame(frame);
					
				}
			}
			
			
		}
		
		if( [@"©gen" isEqualToString:key] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_CONTENTTYPE);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
		}
		
		
		if( [@"©cmt" isEqualToString:key] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_COMMENT);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
		}
		
		if( [@"©lyr" isEqualToString:key] )
		{
			ID3_Frame frame;
			
			frame.SetID(ID3FID_UNSYNCEDLYRICS);
			frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
			NSString *newTitle = [metadataItem stringValue];
			
			frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
			
			tag.AddFrame(frame);
		}
		
	}
	
	//		ID3_Frame frame;
	//		
	//		frame.SetID(ID3FID_ENCODEDBY);
	//		frame.GetField(ID3FN_TEXTENC)->Set(ID3TE_UNICODE);
	//		NSString *newTitle = @"Catalystwo Limited";
	//		
	//		frame.GetField(ID3FN_TEXT)->Set((unicode_t *) [newTitle cStringUsingEncoding:NSUTF16StringEncoding]);
	//		
	//		tag.AddFrame(frame);
	//		
	//		
	tag.SetPadding(false);
	tag.SetUnsync(false);
	tag.Update(ID3TT_ID3V2);
	
	
	
	dispatch_async(dispatch_get_main_queue(), ^{
		
		completionHandler(destinationURL, nil);
		
	});
	
	
}


@end



