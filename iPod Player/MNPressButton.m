//
//  MNPressButton.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 10/09/12.
//
//

#import "MNPressButton.h"

@implementation MNPressButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		 self.backgroundColor = [UIColor clearColor];
		 self.opaque = NO;
    }
    return self;
}


-(void)awakeFromNib
{
	self.backgroundColor = [UIColor clearColor];
	self.opaque = NO;
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	if( self.delegate && [self.delegate respondsToSelector:@selector(pressButtonDidBegin:)] )
	{
		[self.delegate pressButtonDidBegin: self];
	}
	
	self.highlighted = YES;
	[self setNeedsDisplay];
	
	return YES;
}


- (void)cancelTrackingWithEvent:(UIEvent *)event
{
	if( self.delegate && [self.delegate respondsToSelector:@selector(pressButtonDidEnd:)] )
	{
		[self.delegate pressButtonDidEnd: self];
	}
	
	self.highlighted = NO;
	[self setNeedsDisplay];

}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	if( self.delegate && [self.delegate respondsToSelector:@selector(pressButtonDidEnd:)] )
	{
		[self.delegate pressButtonDidEnd: self];
	}
	
	self.highlighted = NO;
	[self setNeedsDisplay];
	
}

-(void)drawRect:(CGRect)rect
{
	
	UIFont *font = [UIFont systemFontOfSize:13];
	CGPoint point = rect.origin;
	point.y += 10;
	point.x += 5;
	[self.tintColor set];
	if( self.highlighted )
	{
		[[UIColor grayColor] set];
		
		NSString* title = NSLocalizedString(@"Hold to Select", @"");
		[title drawAtPoint:point withFont:font];
	}
	else if( self.selected )
	{
		NSString* title = NSLocalizedString(@"Select All", @"");
		[title drawAtPoint:point withFont:font];
	}
	else
	{
		NSString* title = NSLocalizedString(@"Hold to Select", @"");
		[title drawAtPoint:point withFont:font];
		
	}
	
	
}

@end
