//
//  MNFontLabelCell.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 8/01/11.
//  Copyright 2011 Catalystwo Ltd. All rights reserved.
//

#import "TrackTableViewCell.h"
#import "MNCTFontUtils.h"
#import "DrawUtils.h"
#import <QuartzCore/QuartzCore.h>
#import "MNMenuView.h"
#import "NSString+MNPath.h"
#import "Tui.h"


#define SUB_TEXT_FONT [UIFont systemFontOfSize:14]


#define STANDARD_RIGHT_INDENTATION 10
#define DISCLOSURE_RIGHT_INDENTATION 30
#define DETAILED_DISCLOSURE_RIGHT_INDENTATION 40
#define DELETING_RIGHT_INDENTATION 60
#define REORDER_RIGHT_INDENTATION 40

#define EDITING_LEFT_INDENTATIO 35
#define  gIsRunningOnIPad (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define   ICON_WIDTH  (gIsRunningOnIPad?60:44)

@implementation TrackTableViewCell

@synthesize trackNumber = trackNumber_;
@synthesize titleLabel = titleLabel_;
@synthesize trackLabel = trackLabel_;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code.
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		
		
		
		CGFloat trackWidth = 30;
		CGRect bounds = self.contentView.bounds;
		
		UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(bounds.origin.x , 0, trackWidth, bounds.size.height)];
		trackLabel_ = label;
		trackLabel_.textAlignment = UITextAlignmentRight;
		trackLabel_.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
		trackLabel_.font = [UIFont systemFontOfSize:17];
		trackLabel_.backgroundColor = [UIColor clearColor];
		trackLabel_.textColor = [UIColor grayColor];
		trackLabel_.opaque = NO;
		[self.contentView addSubview: trackLabel_];
		
		label = [[UILabel alloc] initWithFrame:CGRectMake(bounds.origin.x + trackWidth + 15, 0, bounds.size.width - (trackWidth + 15), bounds.size.height)];
		titleLabel_ = label;
		titleLabel_.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		titleLabel_.font = [UIFont systemFontOfSize:15];
		titleLabel_.minimumFontSize = 12;
		titleLabel_.adjustsFontSizeToFitWidth = YES;
		titleLabel_.backgroundColor = [UIColor clearColor];
		titleLabel_.opaque = NO;
		titleLabel_.numberOfLines = 0;

		[self.contentView addSubview: titleLabel_];
		

	}
	return self;
}

-(void)layoutSubviews {
	[super layoutSubviews];
	
	if( [self respondsToSelector:@selector(readableContentGuide)] ) {
		
		CGRect layoutFrame = self.readableContentGuide.layoutFrame;
		CGRect rect = trackLabel_.frame;
		rect.origin.x = layoutFrame.origin.x;
		trackLabel_.frame = rect;
		
		if( trackLabel_.text == nil ) {
			rect = titleLabel_.frame;
			rect.origin.x = layoutFrame.origin.x;
			rect.size.width = layoutFrame.size.width;
			titleLabel_.frame = rect;
		}else {
			rect = titleLabel_.frame;
			rect.origin.x = CGRectGetMaxX(trackLabel_.frame) + 7;
			rect.size.width = layoutFrame.size.width - (trackLabel_.frame.size.width + 7);
			titleLabel_.frame = rect;
		}
	}
}

-(void)setTrackNumber:(NSNumber *)num
{
	trackNumber_ = num;
	
	if( !trackNumber_ )
	{
		trackLabel_.text = nil;
	}
	else
	{
		NSInteger trackNumber = num.integerValue;
		
		if( trackNumber == 0 )
		{
			trackLabel_.text = nil;
			
		}else
		{
			
			trackLabel_.text = [num stringValue];
		}
	}
	
	[self setNeedsLayout];
}



- (void)prepareForReuse
{
	[super prepareForReuse];
	
	[self setUserInteractionEnabled: YES];


	trackNumber_ = nil;
	titleLabel_.text = nil;
	trackLabel_.text = nil;
	titleLabel_.textColor = [UIColor blackColor];
	trackLabel_.textColor = [UIColor grayColor];
	self.backgroundColor = [UIColor whiteColor];
	
	self.indentationLevel = 0;
	


}


- (void)dealloc {

	trackNumber_ = nil;
	titleLabel_.text = nil;
	trackLabel_.text = nil;

}


@end
