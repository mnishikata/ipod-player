//
//  HelpViewController.m
//  BigFingerGL
//
//  Created by Masatoshi Nishikata on 10/01/05.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import "HelpViewController.h"
#import "Reachability.h"

@implementation HelpViewController
@synthesize helpFile, title, webView = webView_;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close",@"")
																				  style:UIBarButtonItemStyleBordered
																				 target:self action:@selector(close)];
	[self.navigationItem setRightBarButtonItem:button];
	
}

-(void)close
{
	[self.navigationController dismissModalViewControllerAnimated:YES];
	
}


//
//// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
	if( self.title == nil ) self.title = @"Help";
	self.navigationItem.title = NSLocalizedString(self.title,@"");
	
	[self loadHelp];
	
	if( [helpFile hasPrefix:@"http://"] )
	{
		Reachability *internetReach = [Reachability reachabilityForInternetConnection];
		NetworkStatus netStatus = [internetReach currentReachabilityStatus];
		if( netStatus == NotReachable )
		{
			SHOW_ERROR_ARC(@"Oops", @"ERR_SyncFailedOfflineMessage");
		}
		
	}
}



-(void)loadHelp
{
	NSURL *url = nil;
	if( !helpFile ) helpFile = @"Help.gif";
	
	if( [helpFile hasPrefix:@"http://"] )
	{
		url = [NSURL URLWithString: helpFile];
	}else {
		
		NSString *path = [[NSBundle mainBundle] pathForResource:helpFile ofType:nil];
		url = [NSURL fileURLWithPath:path];
	}
	
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	[webView_ loadRequest:request];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	webView_.scalesPageToFit = YES;
}




-(void)viewWillDisappear:(BOOL)animated
{
	
	[webView_ stopLoading];	// in case the web view is still loading its content
	webView_.delegate = nil;	// disconnect the delegate as the webview is hidden
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];
	
	[webView_.scrollView flashScrollIndicators];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)dealloc {
	
	webView_.delegate = nil;
	webView_ = nil;
	self.helpFile = nil;
	
}




//#pragma mark UIWebViewDelegate
//
- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview
	NSString* errorString = [NSString stringWithFormat:
									 @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
									 error.localizedDescription];
	[webView loadHTMLString:errorString baseURL:nil];
}

@end
