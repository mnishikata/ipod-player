//
//  CCalStoreViewController.h
//  CCal-X
//
//  Created by Nishikata Masatoshi on 28/07/12.
//  Copyright (c) 2012 Catalystwo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface SPStoreViewController : UIViewController <SKProductsRequestDelegate, UITableViewDelegate, UITableViewDataSource>
{
	UITableView* tableView_;
	
		
	NSArray *myProducts_;
	BOOL offline_;
	
	SKProductsRequest *productsRequest_;
}
- (id)init;
-(void)dealloc;
- (void)viewDidLoad;
-(void)viewWillAppear:(BOOL)animated;
-(void)viewWillDisappear:(BOOL)animated;
- (void)viewDidUnload;
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
-(NSArray*)toolbarItems;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView ;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section ;
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath ;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath ;
-(void)close;
-(void)showFAQ;
-(void)clear;
-(void)reload;
- (void) requestProductData;
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response;

@property (nonatomic, retain) NSArray *products;
@property (nonatomic, retain) UITableView* tableView;
@end
