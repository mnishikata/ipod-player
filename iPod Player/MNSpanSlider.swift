//
//  MNSpanSlider.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 19/02/16.
//
//

import Foundation
import UIKit

let KNOB_WIDTH:CGFloat = 30
let KNOB_HEAD_HEIGHT:CGFloat = 30
let KNOB_SIZE:CGFloat = 27
let BODY_HEIGHT:CGFloat = 40

func RGBA(r:CGFloat,_ g:CGFloat,_ b:CGFloat,_ a:CGFloat) -> UIColor  {
	return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}

extension CGFloat {
	func limitValue(lowerLimit:CGFloat, _ upperLimit:CGFloat) -> CGFloat {
		if lowerLimit > upperLimit { return self }
		if self < lowerLimit { return lowerLimit }
		if self > upperLimit { return upperLimit }
		return self
	}
}

// MARK:- Private abstruct class for elements

// Abstruct class for elements
private class MNSpanSliderElement : UIControl {
	
	private weak var slider:MNSpanSlider? = nil
	private var touchedPoint:CGPoint = CGPointZero
	private var originalX:CGFloat = 0
	private var currentRate:CGFloat = 0

	override var highlighted:Bool {
		didSet {
			setNeedsDisplay()
		}
	}
	
	private override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
		currentRate = 1.0
		touchedPoint = touch.locationInView(superview)
		originalX = frame.origin.x
		highlighted = true
		slider?.hideElements(true, exceptForElements:[self])
		slider?.notify(self, visible:true)
		return true
	}
	
	private func draggingDistance(touch: UITouch) -> CGFloat {
		guard let slider = self.slider else { return 1.0 }
		
		let locationX = touch.locationInView(superview).x
		var dx = locationX - touchedPoint.x
		let dy = fabs(touch.locationInView(superview).y - touchedPoint.y)
		let factor = min(1.0, 90.0 / slider.maximumValue )
		let rate: CGFloat = {
			if dy < 50.0 {
				return 1.0
				
			} else if dy >= 50.0 && dy < 100.0 {
				return 0.5 * factor
				
			} else if dy < 200.0 {
				return 0.25 * factor
				
			} else  {
				return 0.1 * factor
			}
		}()
		
		if rate != currentRate {
			currentRate = rate
			touchedPoint.x = locationX
			originalX = frame.origin.x
			dx = locationX - touchedPoint.x
		}

		return  dx * rate
	}
	
	private override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
		guard let slider = self.slider else { return false }
		
		moveWithDraggingDistance( draggingDistance(touch) )
		slider.notify(self, visible:true)
		
		return true
	}
	
	private func moveWithDraggingDistance(distance:CGFloat) {
		// DO SOMETHING HERE USING distance
	}
	
	private override func cancelTrackingWithEvent(event: UIEvent?) {
		super.cancelTrackingWithEvent(event)
		
		highlighted = false
		slider?.hideElements(false)
		slider?.notify(self, visible:false)
	}
	
	private override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
		
		highlighted = false
		slider?.hideElements(false)
		slider?.notify(self, visible:false)
	}
	
}

//MARK:- Private MNSpanSliderKnob
private class MNSpanSliderKnob : MNSpanSliderElement {
	
	private var onTop:Bool = false

	convenience init(onTop:Bool, slider:MNSpanSlider) {
		self.init(frame:CGRectMake(0, 0, 10, 10))
		self.onTop = onTop
		self.slider = slider
		self.opaque = false
		self.backgroundColor = UIColor.clearColor()
	}

	private override func drawRect(rect: CGRect) {
		
		let context = UIGraphicsGetCurrentContext()
		CGContextSetFillColorWithColor(context, tintColor.CGColor)
		CGContextSetStrokeColorWithColor(context, tintColor.CGColor)
		var points: [CGPoint] = [CGPointZero, CGPointZero]
		CGContextSetLineWidth(context, 0.5)
		if onTop {
			let rect = CGRectMake(KNOB_WIDTH/2 - 4, 18, 8, 8)
			CGContextFillEllipseInRect(context, rect)
			points[0] = CGPointMake(KNOB_WIDTH/2 + 0.25, 26)
			points[1] = CGPointMake(KNOB_WIDTH/2 + 0.25, CGRectGetMaxY(bounds) )
			CGContextStrokeLineSegments(context, points, 2)
		} else {
			let rect = CGRectMake(KNOB_WIDTH/2 - 4, CGRectGetMaxY(bounds) - 26, 8, 8)
			CGContextFillEllipseInRect(context, rect)
			points[0] = CGPointMake(KNOB_WIDTH/2 - 0.25, 0)
			points[1] = CGPointMake(KNOB_WIDTH/2 - 0.25, CGRectGetMaxY(bounds) - 18)
			CGContextStrokeLineSegments(context, points, 2)
		}
	}
	
	private override func moveWithDraggingDistance(distance:CGFloat) {
		
		let location = ( originalX + distance + KNOB_WIDTH/2 - slider!.bodyFrame.origin.x ) / slider!.bodyFrame.size.width * slider!.maximumValue
		if onTop {
			slider!.startLocation = location
		}
		else {
			slider!.endLocation = location
		}
	}
}

// MARK:- Private MNSpanSliderHandle

private class MNSpanSliderHandle : MNSpanSliderElement {
	
	private var handleMovedTimer:NSTimer? = nil
	
	convenience init( slider:MNSpanSlider) {
		self.init(frame:CGRectMake(0, 0, 30, 30))
		self.slider = slider
		self.opaque = false
		self.backgroundColor = UIColor.clearColor()
	}
	
	override func drawRect(frame: CGRect) {
		
		let context = UIGraphicsGetCurrentContext()
		tintColor.set()
		let lineRect: CGRect = CGRectMake(round(frame.size.width / 2) - 1, frame.origin.y, 2, frame.size.height + 2)
		CGContextFillRect(context, lineRect)
	}
	
	private override func moveWithDraggingDistance(distance:CGFloat) {
		
		let currentLocation = ( originalX + distance - slider!.bodyFrame.origin.x + KNOB_SIZE / 2) / slider!.bodyFrame.size.width * slider!.maximumValue
		slider!.currentLocation = currentLocation
		
		// Re-setting update timer
		handleMovedTimer?.invalidate()
		handleMovedTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: Selector("handleMovedWithTimer"), userInfo: nil, repeats: false)
	}
	
	@objc func handleMovedWithTimer() {
		slider?.handleMoved()
		handleMovedTimer = nil
	}
	
	private override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
		
		handleMovedTimer?.fire()
		super.endTrackingWithTouch(touch, withEvent: event)
	}
}

// MARK:- Private MNSpanSliderBody

private class MNSpanSliderBody : UIControl {
	
	private weak var slider:MNSpanSlider? = nil
	private var backgroundImage:UIImage? = nil
	private var selectedFrame:CGRect = CGRectZero
	
	convenience init( slider:MNSpanSlider) {
		self.init(frame:CGRectMake(0, 0, 30, 30))
		self.slider = slider
		self.opaque = false
		self.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		guard let slider = self.slider else { return }
		
		let value = touches.first!.locationInView(self).x
		let location = value / bounds.size.width * slider.maximumValue
		slider.currentLocation = location
		slider.handleMoved()
	}
	
	override func drawRect(var rect: CGRect) {
		let context: CGContextRef = UIGraphicsGetCurrentContext()!
		CGContextClearRect(context, rect)

		if let backgroundImage = self.backgroundImage {
			backgroundImage.drawInRect(rect)
			CGContextSetFillColorWithColor(context, UIColor.lightGrayColor().CGColor)
			CGContextSetBlendMode(context, .SourceAtop)
			CGContextFillRect(context, rect)
			
		}else {
			rect.origin.y = CGRectGetMidY(rect) - 2
			rect.size.height = 4
			CGContextSetFillColorWithColor(context, UIColor.lightGrayColor().CGColor)
			CGContextSetBlendMode(context, .Normal)
			CGContextFillRect(context, rect)
		}
		
		var toColor: UIColor = RGBA(1, 95, 231, 1)
		if respondsToSelector("tintColor") {
			toColor = self.tintColor
		}
		var clipFrame = self.selectedFrame
		clipFrame.origin.y += 2
		clipFrame.size.height -= 4
		CGContextClipToRect(context, clipFrame)
		CGContextSetFillColorWithColor(context, toColor.CGColor)
		CGContextSetAlpha(context, 0.3)
		CGContextSetBlendMode(context, .SourceAtop)
		CGContextFillRect(context, clipFrame)
		CGContextSetFillColorWithColor(context, toColor.CGColor)
		CGContextSetBlendMode(context, .Normal)
		CGContextSetAlpha(context, 0.2);
		CGContextFillRect(context, clipFrame);
	}
	
}


// MARK:- MNSpanSlider
public class MNSpanSlider : UIView {
	
	weak public var delegate:MNSpanSliderDelegate? = nil
	
	override public var frame:CGRect {
		didSet {
			setNeedsLayout()
		}
	}
	
	public var maximumValue:CGFloat = 1.0

	var trackingCurrentLocation:Bool {
		get {
			return handle.tracking
		}
	}
	var currentLocation:CGFloat = 0.0 {
		didSet {
			currentLocation = currentLocation.limitValue(0, maximumValue )
			
			// Update view frame
			handle.frame = handleFrame
		}
	}
	
	var backgroundImage:UIImage? {
		didSet {
			sliderBody.backgroundImage = backgroundImage;
			sliderBody.setNeedsDisplay()
		}
	}
	
	var startLocation:CGFloat = 0.0 {
		didSet {
			if oldValue == startLocation { return }
			
			// Validate value
			startLocation = startLocation.limitValue(0, maximumValue )
			startLocation = startLocation.limitValue(0, endLocation )
			
			// Update view frame
			sliderTopView.frame = topKnobFrame

			updateSelectedFrame()
			selectionUpdated()
		}
	}
	
	var endLocation:CGFloat = 0.0 {
		didSet {
			if oldValue == endLocation { return }
			
			// Validate value
			endLocation = endLocation.limitValue(0, maximumValue )
			startLocation = startLocation.limitValue(0, endLocation )
			
			// Update view frame
			sliderBottomView.frame = bottomKnobFrame

			updateSelectedFrame()
			selectionUpdated()
		}
	}
	
	//MARK:- Private properties
	
	lazy private var sliderTopView:MNSpanSliderKnob = {
		[unowned self] in
		let element = MNSpanSliderKnob(onTop:true, slider:self)
		self.addSubview(element)
		return element
		}()
	
	lazy private var sliderBottomView:MNSpanSliderKnob = {
		[unowned self] in
		let element = MNSpanSliderKnob(onTop:false, slider:self)
		self.addSubview(element)
		return element
		
		}()
	
	lazy private var handle:MNSpanSliderHandle = {
		[unowned self] in
		let element = MNSpanSliderHandle(slider:self)
		self.addSubview(element)
		return element
		
		}()
	
	lazy private var sliderBody:MNSpanSliderBody = {
		[unowned self] in
		let element = MNSpanSliderBody(slider:self)
		self.addSubview(element)
		return element
		
		}()
	
	//MARK:- Calculated frames
	
	var bodyFrame:CGRect {
		get {
			var bodyFrame = CGRectInset(bounds, 20, 0)
			bodyFrame.origin.x += 20
			bodyFrame.size.width -= 40
			bodyFrame.size.height = BODY_HEIGHT
			bodyFrame.origin.y = round((bounds.size.height - BODY_HEIGHT) / 2)
			return bodyFrame
		}
	}
	
	var topKnobFrame:CGRect {
		get {
			let x = bodyFrame.origin.x + startLocation / maximumValue * bodyFrame.size.width - KNOB_WIDTH/2
			let y = bodyFrame.origin.y - KNOB_HEAD_HEIGHT
			let size = CGSizeMake(KNOB_WIDTH, bodyFrame.size.height + KNOB_HEAD_HEIGHT)
			
			return CGRectMake(x, y, size.width, size.height)
		}
	}
	
	var bottomKnobFrame:CGRect {
		get {
			let x = bodyFrame.origin.x + endLocation / maximumValue * bodyFrame.size.width - KNOB_WIDTH/2
			let y = bodyFrame.origin.y
			let size = CGSizeMake(KNOB_WIDTH, bodyFrame.size.height + KNOB_HEAD_HEIGHT)
			
			return CGRectMake(x, y, size.width, size.height)
		}
	}
	
	var handleFrame:CGRect {
		get {
			let knobX = bodyFrame.size.width * currentLocation / maximumValue - KNOB_SIZE / 2
			return CGRectMake(knobX + bodyFrame.origin.x, bodyFrame.origin.y, KNOB_SIZE, bodyFrame.size.height)
		}
	}

	//MARK:-
	
	deinit {
		sliderTopView.removeFromSuperview()
		sliderBottomView.removeFromSuperview()
		handle.removeFromSuperview()
		sliderBody.removeFromSuperview()
		delegate = nil
	}
	
	func updateSelectedFrame() {
		
		let sx = topKnobFrame.origin.x + KNOB_WIDTH / 2
		let ex = bottomKnobFrame.origin.x + KNOB_WIDTH / 2
		var selectionFrame: CGRect = CGRectMake(sx, bodyFrame.origin.y, fmax(1, ex - sx), bodyFrame.size.height)
		selectionFrame.origin.x -= bodyFrame.origin.x
		selectionFrame.origin.y -= bodyFrame.origin.y
		sliderBody.selectedFrame = selectionFrame
		sliderBody.setNeedsDisplay()
	}
	
	override public func layoutSubviews() {
		super.layoutSubviews()
		
		sliderBody.frame = bodyFrame
		sliderTopView.frame = topKnobFrame
		sliderBottomView.frame = bottomKnobFrame
		handle.frame = handleFrame
		updateSelectedFrame()
	}
	
	func hideElements(hide:Bool, exceptForElements elements: [UIControl] = []) {

		if hide {
			UIView.beginAnimations(nil, context: nil)
			UIView.setAnimationDuration(0.1)
			if elements.contains( sliderTopView ) == false { sliderTopView.alpha = 0 }
			if elements.contains( sliderBottomView ) == false { sliderBottomView.alpha = 0 }
			//if elements.contains( handle ) == false { handle.alpha = 0 }
			UIView.commitAnimations()
		}else {
			UIView.beginAnimations(nil, context: nil)
			UIView.setAnimationDuration(0.2)
			sliderTopView.alpha = 1
			sliderBottomView.alpha = 1
			handle.alpha = 1
			UIView.commitAnimations()
		}
	}
	
	func notify(sender:UIControl, visible:Bool) {
		
		var locationMovedTo:CGFloat = 0
		
		switch sender {
		case sliderTopView:
			locationMovedTo = startLocation
		case sliderBottomView:
			locationMovedTo = endLocation
		case handle:
			locationMovedTo = currentLocation
		default:
			break
		}
		
		delegate?.spanSliderSelectionDraggingElement?(self, visible:visible, toLocation:locationMovedTo)
	}
	
	func selectionUpdated() {
		delegate?.spanSliderSelectionUpdated?(self)
	}
	
	func handleMoved() {
		delegate?.spanSliderValueChanged?(currentLocation)
	}
	
	// MARK:- Unused
	
	func isOnSlider(pointInView:CGPoint) -> Bool {
		if nil == sliderBody.window { return false }
		return 	CGRectContainsPoint(sliderBottomView.frame, pointInView) ||
			CGRectContainsPoint(sliderTopView.frame, pointInView)
	}
	
	func isOnBoddy(pointInView:CGPoint) -> Bool {
		if nil == sliderBody.window { return false }
		if isOnSlider(pointInView) { return false }
		return CGRectContainsPoint(sliderBody.frame, pointInView)
	}
}
