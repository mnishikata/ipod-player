//
//  MNSoundClipEditor.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 5/09/12.
//
//

#import "MNSoundClipEditor.h"
//#import "MNSoundClip.h"
#import "MNExporter.h"
#import "Tui.h"
#import "AB_Player-Swift.h"

@implementation MNSoundClipEditor
@synthesize clip;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		 
		 self.title = NSLocalizedString(@"Sound Clip", @"");
    }
    return self;
}

-(UIRectEdge)edgesForExtendedLayout
{
	return UIRectEdgeNone;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	
	if( [self.navigationController.viewControllers objectAtIndex:0] == self )
	{
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
	}
	
	UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(export:)];
//	UIBarButtonItem* deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(delete:)];
	
	UIBarButtonItem* organizeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(organize:)];


	self.navigationItem.rightBarButtonItems = @[button, organizeButton];
			
	self.transcriptionLabel.text = NSLocalizedString(@"Transcription", @"");
	self.commentLabel.text = NSLocalizedString(@"Comment", @"");
		
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.textView.text = self.clip.transcription;
	self.commentField.text = self.clip.comment;
}

-(void)viewWillDisappear:(BOOL)animated
{
	[self.textView resignFirstResponder];
	[self.commentField resignFirstResponder];
	
	NSString* comment = self.commentField.text;
	
	if( !comment ) comment = @"";
	
	clip.comment = comment;
	
	
	NSString* transcription = self.textView.text;
	
	if( !transcription ) transcription = @"";
	
	clip.transcription = transcription;
	
	
	
	[APP_DELEGATE saveContext];
}



-(void)close
{
	if( self.navigationController.topViewController == self )
	{
		[self.navigationController dismissViewControllerAnimated:YES completion:nil];

	}
	else
	{
		[self.navigationController popViewControllerAnimated:YES];

	}
}

-(void)organize:(UIBarButtonItem*)sender
{
	ClipPlaylistsPickerViewController* controller = [[ClipPlaylistsPickerViewController alloc] init];
	controller.clip = clip;
	UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:controller];
	[self presentViewController:nav animated:YES completion:nil];
}

-(void)export:(UIBarButtonItem*)sender
{
	
	GCNetworkReachability *internetReach = [GCNetworkReachability reachabilityForInternetConnection];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	if( netStatus == NotReachable )
	{
		SHOW_ERROR_ARC(@"Oops", @"The device needs to be online.");
		return;
	}
	
	[self.textView resignFirstResponder];
	[self.commentField resignFirstResponder];

	
	unsigned long long ullvalue = strtoull([self.clip.persistentID UTF8String], NULL, 0);

	NSNumber *num = [NSNumber numberWithUnsignedLongLong: ullvalue];
	
	MPMediaPropertyPredicate *predicate = [MPMediaPropertyPredicate predicateWithValue:num forProperty:MPMediaItemPropertyPersistentID];
	
	
	MPMediaQuery *query = [[MPMediaQuery alloc] initWithFilterPredicates: [NSSet setWithObject:predicate]];
	
	NSArray* obj = [query items];
	
	
	
	if( obj && obj.count > 0 )
	{
		MPMediaItem* mediaItem = [obj lastObject];

	
		
		NSURL* url = [mediaItem valueForProperty:MPMediaItemPropertyAssetURL];
		
		
		AVURLAsset* asset = [[AVURLAsset alloc] initWithURL:url options:nil];
		
		if( !asset )
		{
			SHOW_ERROR_ARC(@"",@"Media Not Found");
			return;
		}
		
		
		CGRect fromRect = self.navigationController.navigationBar.frame;
		fromRect.origin.x = CGRectGetMaxX(fromRect)-20;
		fromRect.size.width = 20;
		
		[[MNExporter sharedExporter] exportAsset:asset
												  soundClip:self.clip
													  range:clip.timeRange
												  fromRect:fromRect
													 inView:self.view
										inViewController:self.navigationController];
	
	}else
	{
		if( clip.binaryData )
		{
			
			
			NSFileManager *fm = [NSFileManager defaultManager];
			
			NSURL* tempURL = [NSURL fileURLWithPath: NSTemporaryDirectory() ];
			
			tempURL = [tempURL URLByAppendingPathComponent:@"com.catalystwo"];

			
			if( ![fm fileExistsAtPath:tempURL.path] )
			{
				[fm createDirectoryAtURL:tempURL withIntermediateDirectories:YES attributes:nil error:nil];
			}
			
			tempURL = [tempURL URLByAppendingPathComponent:@"exportingBinary.m4a"];
			
			if( [fm fileExistsAtPath:tempURL.path] )
			{
				[fm removeItemAtURL:tempURL error:nil];
			}
			
			BOOL success = [clip.binaryData writeToURL:tempURL atomically:NO];
			
			if( !success )
			{
				NSLog(@"Failed");
				return;
			}
			
			
			AVURLAsset* asset = [[AVURLAsset alloc] initWithURL:tempURL options:nil];
			
			if( !asset )
			{
				SHOW_ERROR_ARC(@"",@"Media Not Found");
				return;
			}
			
			
			CGRect fromRect = self.navigationController.navigationBar.frame;
			fromRect.origin.x = CGRectGetMaxX(fromRect)-20;
			fromRect.size.width = 20;
			CMTime start, duration;
			start = CMTimeMakeWithSeconds(0, asset.duration.timescale);
			duration = asset.duration;
			CMTimeRange timeRange = CMTimeRangeMake(start, duration);
			
			[[MNExporter sharedExporter] exportAsset:asset
													 soundClip:self.clip
														  range:timeRange
													  fromRect:fromRect
														 inView:self.view
											inViewController:self.navigationController];
		}
		else
		{
			
			SHOW_ERROR_ARC(@"", @"NotInThisDeviceMessage");

		}
		
	}

}

- (void)viewDidUnload {
	[self setCommentField:nil];
	[super viewDidUnload];
}
@end
