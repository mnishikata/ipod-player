//
//  ClipInfoButton.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 9/09/12.
//
//

#import <UIKit/UIKit.h>

@interface ClipInfoButton : UIButton

@property (retain, nonatomic) id representedObject;
@end
