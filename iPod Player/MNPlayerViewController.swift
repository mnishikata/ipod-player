//
//  MNPlayerViewController.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 25/02/16.
//
//

import Foundation
import MediaPlayer
import CoreData

let MAX_SPEED:CGFloat = 2.0
let MIN_SPEED:CGFloat = 0.5
func APP_DELEGATE() -> MNAppDelegate { return UIApplication.sharedApplication().delegate as! MNAppDelegate }
let MAX_RECENTS = 100


func WLoc( title:String ) -> String {
	return NSLocalizedString(title, comment: "" )
}



enum SPRepeatType : Int {
	case None = 0
	case All
	case Track
	
	func next() -> SPRepeatType {
		if self == .Track { return SPRepeatType.None }
		return SPRepeatType(rawValue: self.rawValue + 1)!
	}
}

class MNPlayerViewController : UIViewController, WaveformSource, MNSpanSliderDelegate, MNPressButtonDelegate  {
	
	@IBOutlet weak var trackView: MNTrackCollectionView!
	@IBOutlet weak var sliderView: MNPlayerSliderView!
	@IBOutlet weak var fastfaward: UIButton!
	@IBOutlet weak var rewind: UIButton!
	@IBOutlet weak var skipForwardButton: UIButton!
	@IBOutlet weak var skipBackwardButton: UIButton!
	@IBOutlet weak var speedKnob: UIImageView!
	@IBOutlet weak var repeatButton: UIButton!
	@IBOutlet weak var play: UIButton!
	@IBOutlet weak var pressButton: MNPressButton!
	@IBOutlet weak var addClipButton: UIButton!
	@IBOutlet weak var clipContainerView: UIView!
	@IBOutlet weak var fasterButton: UIButton!
	@IBOutlet weak var slowerButton: UIButton!
	@IBOutlet weak var controlPanel: UIView!
	@IBOutlet weak var contentView: UIView!
	weak var systemVolumeSlider:MPVolumeView? = nil

	func assertMainThread() {
		assert( NSThread.isMainThread() == true, "*** Call from main thread ***")
	}
	
	var mediaItems:[MPMediaItem] = [] {
		didSet {
			assertMainThread()
			
			let fetchQueue: dispatch_queue_t = APP_DELEGATE().fetchQueue()
			dispatch_async(fetchQueue, { [unowned self] () -> Void in
			 self.fetchSoundClips()
				})
			trackView.mediaItems = mediaItems
			if mediaItem != nil {
				trackView.selectMedia(mediaItem!, animated: true)
			}
		}
	}
	var mediaItem:MPMediaItem? = nil {
		didSet {
			if (mediaItem == nil) {
				return
			}
			assertMainThread()
			
			let ud = NSUserDefaults.standardUserDefaults()
			if let url = mediaItem!.valueForProperty(MPMediaItemPropertyAssetURL) as! NSURL? {
				setAssetAt(url)
			}
			view.backgroundColor = RGBA(250, 250, 250, 1)
			
			updateUI()
			
			// Update history
			let albumID: NSNumber? = mediaItem!.valueForProperty( MPMediaItemPropertyAlbumPersistentID) as! NSNumber?
			var history: [NSNumber] = ud.objectForKey("MNPlayerRecentAlbums") as! [NSNumber]? ?? []

			if albumID != nil {
				if history.contains(albumID!) {
					history.removeAtIndex(history.indexOf(albumID!)!)
				}
				history.insert(albumID!, atIndex: 0)
			}
			if history.count > MAX_RECENTS {
				history = Array(history[ 0..<MAX_RECENTS ])
			}
			
			ud.setObject(history, forKey: "MNPlayerRecentAlbums")
		}
	}
	
	var currentTrackNumber:Int? {
		get {
			if mediaItem == nil { return nil }
			if let index = mediaItems.indexOf(mediaItem!) { return index }
			
			let myPersistentID = mediaItem!.valueForProperty( MPMediaItemPropertyPersistentID ) as! NSNumber?
			if myPersistentID == nil { return nil }
			
			for var track = 0; track < mediaItems.count; track++ {
				let item = mediaItems[track]
				let persistentID = item.valueForProperty(MPMediaItemPropertyPersistentID) as! NSNumber?
				if persistentID?.isEqualToNumber(myPersistentID!) == true {
					return track
				}
			}
			return nil
		}
	}

	var stepperRate:CGFloat = {
		var stepperRate:CGFloat = 1.0
		let ud = NSUserDefaults.standardUserDefaults()
		if ud.objectForKey("MNPlayerViewControllerStepperRate") != nil {
			stepperRate = CGFloat( ud.floatForKey("MNPlayerViewControllerStepperRate") )
			if stepperRate < MIN_SPEED || stepperRate > MAX_SPEED {
				stepperRate = 1.0
			}
		}
		return stepperRate
	}()

	var repeatMode:SPRepeatType = {
		let ud = NSUserDefaults.standardUserDefaults()
		var repeatMode:SPRepeatType = .None
		if ud.objectForKey("MNPlayerViewControllerRepeatMode") != nil  {
			repeatMode = SPRepeatType(rawValue: ud.integerForKey("MNPlayerViewControllerRepeatMode") ) ?? .None
		}
		return repeatMode
	}()
	var playingClipCollection:Bool = false
	var currentTime:CMTime = CMTimeMakeWithSeconds(0, 1)
	var selectedRange:CMTimeRange = CMTimeRangeMake(CMTimeMakeWithSeconds(0, 1), CMTimeMakeWithSeconds(0, 1)) {
		didSet {
			assertMainThread()

			if endObserver != nil {
				player.removeTimeObserver(endObserver!)
				endObserver = nil
			}
			let currentTime = CMTimeGetSeconds(player.currentTime())
			if currentTime < CMTimeGetSeconds(selectedRange.start) {
				seekToTime(selectedRange.start)
				sliderView.updateTimeDisplay()
			}
			let selectedAll = timeSubstantiallyEqual(selectedRange.duration, asset.duration)
			var endTimeSeconds = CMTimeGetSeconds(selectedRange.start) + CMTimeGetSeconds(selectedRange.duration)

			if selectedAll {
				endTimeSeconds = CMTimeGetSeconds(asset.duration) - kEndTimeCutOff
			}
			
			pressButton.selected = !selectedAll
			let cmtime = CMTimeMakeWithSeconds(endTimeSeconds, asset.duration.timescale)
			let times = [NSValue(CMTime: cmtime)]
			endObserver = player.addBoundaryTimeObserverForTimes(times, queue: dispatch_get_main_queue()) {
				[unowned self] in
				// 終了時間の発見がこける
				self.endBoundary()
			}
			updateUI()
		}
	}
	
	var clipCollectionViewController:MNClipCollectionTableViewController = {
		let controller = MNClipCollectionTableViewController()
		controller.organizeType = .ByTrack
		controller.enableToolbar = false
		controller.soundClips = []

		return controller
	}()
	
	var player = AVPlayer()
	var asset:AVURLAsset!
	var recentlyAddedSoundClip:MNSoundClip? = nil
	
	var waveform:UIImage? = nil
	var waveformTimer:NSTimer? = nil
	var waveformTimerInitiatedInterval:NSTimeInterval = 0
	var periodicObserver:AnyObject? = nil
	var endObserver:AnyObject? = nil
	var playerObserverTimer:NSTimer? = nil
	var activeSelectMode:Bool = false
	var songTitle:String {
		get {
			if asset == nil { return "" }
			let metadataArray = asset!.commonMetadata
			for item: AVMetadataItem in metadataArray {
				if (item.commonKey == "title") {
					return item.stringValue!
				}
			}
			return asset!.URL.lastPathComponent!
		}
	}

	static let sharedPlayer:MNPlayerViewController = {
		var obj = MNPlayerViewController()
		WaveformMeganeView.sharedWaveformMeganeView.waveformSouce = obj
		return obj
	}()
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		coder: NSCoder) -> UIViewController? {
			return MNPlayerViewController.sharedPlayer
	}
	
	class func canPlay() -> Bool {
		return MNPlayerViewController.sharedPlayer.mediaItem != nil
	}
	
	//MARK:- BODY
	
	init() {
		super.init(nibName: "MNPlayerViewControllerFlat", bundle: nil)
		
		if let _ = self.view {
			//Instantiate
		}
		
		self.hidesBottomBarWhenPushed = true
		
		let center = NSNotificationCenter.defaultCenter()
//		center.addObserver(self, selector: "didEnterBackground:", name: UIApplicationDidEnterBackgroundNotification, object: nil)
//		center.addObserver(self, selector: "willEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)
//		center.addObserver(self, selector: "destroyADBannerView", name: "MNStoreObserverDidChangeNotificationName", object: nil)
		self.restorationIdentifier = "MNPlayerViewControllerID"
		self.restorationClass = self.dynamicType
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
		mediaItems = []
		mediaItem = nil
		asset = nil
	}
	
	func saveToUserDefaults() {
		let ud = NSUserDefaults.standardUserDefaults()
		ud.setFloat(Float(stepperRate), forKey: "MNPlayerViewControllerStepperRate")
		ud.setInteger(repeatMode.rawValue, forKey: "MNPlayerViewControllerRepeatMode")
		ud.synchronize()
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		
		let selectedRangeDict = CMTimeRangeCopyAsDictionary(selectedRange, nil)
		coder.encodeObject(selectedRangeDict, forKey: "selectedRange_")
		coder.encodeObject(mediaItems, forKey: "mediaItems_")
		coder.encodeObject(mediaItem, forKey: "mediaItem")
		coder.encodeObject(asset.URL, forKey: "assetURL")
		coder.encodeDouble(CMTimeGetSeconds(currentTime), forKey: "currentTime")
		
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		
		if let url = coder.decodeObjectForKey("assetURL") as! NSURL? {
			asset = AVURLAsset(URL: url)
		}
		
		if let newMediaItem = coder.decodeObjectForKey("mediaItem") as! MPMediaItem? {
			if let newMediaItems = coder.decodeObjectForKey("mediaItems_") as! [MPMediaItem]? {
				mediaItems = newMediaItems
			}
			mediaItem = newMediaItem
		}
		
		if let selectedRangeDict = coder.decodeObjectForKey("selectedRange_") as! CFDictionary? {
			selectedRange = CMTimeRangeMakeFromDictionary(selectedRangeDict)
		}
		
		if coder.containsValueForKey("currentTime") {
			if let currentTimeSeconds = coder.decodeDoubleForKey("currentTime") as Double? {
				
				if asset != nil {
					currentTime = CMTimeMakeWithSeconds(currentTimeSeconds, asset!.duration.timescale)
					player.seekToTime(currentTime)
				}
			}
		}
	}
	
	// The output below is limited by 1 KB.
	// Please Sign Up (Free!) to remove this limitation.
	
	override func viewDidLoad() {

		super.viewDidLoad()

		// Addtional layout
		let constraint = NSLayoutConstraint(item:contentView, attribute: .Top, relatedBy: .Equal, toItem: topLayoutGuide, attribute: .Bottom, multiplier: 1.0, constant: 0.0)
		self.view.addConstraint(constraint)
		self.view.layoutSubviews()
		
		// Setup initial view size
		let applicationFrame = UIScreen.mainScreen().applicationFrame
		self.view.frame = applicationFrame
		// Setting up user interface
		let title = NSAttributedString(string: WLoc("Clip Selection"), attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13), NSForegroundColorAttributeName: self.view.tintColor])
		
		addClipButton.setTitle(WLoc("Clip Selection"), forState: .Normal)
		
		var image: UIImage
		image = UIImage(named: "SpeedSlowEnabledFlat.png")!
		slowerButton.setImage(UIImage.colorizedImage(image, withTintColor: UIColor.lightGrayColor(), alpha: 1, glow: false), forState: .Normal)
		slowerButton.setImage(UIImage.colorizedImage(image, withTintColor: self.view.tintColor, alpha: 1, glow: false), forState: .Selected)
		image = UIImage(named: "SpeedFastEnabledFlat.png")!
		fasterButton.setImage(UIImage.colorizedImage(image, withTintColor: UIColor.lightGrayColor(), alpha: 1, glow: false), forState: .Normal)
		fasterButton.setImage(UIImage.colorizedImage(image, withTintColor: self.view.tintColor, alpha: 1, glow: false), forState: .Selected)

		// Setting up volume slider
		systemVolumeSlider = MPVolumeView(frame: CGRectMake(10, CGRectGetMaxY(controlPanel.bounds) - 32, controlPanel.bounds.size.width - 20, 60))
		let size = systemVolumeSlider!.sizeThatFits(CGSizeMake(controlPanel.bounds.size.width - 20, 60))
		var frame = systemVolumeSlider!.frame
		frame.size = size
		systemVolumeSlider!.frame = frame
		systemVolumeSlider!.autoresizingMask = [.FlexibleWidth, .FlexibleTopMargin]
		controlPanel.addSubview(systemVolumeSlider!)
		
		// Setting up slider and so on
		sliderView.spanSliderDelegate = self
		sliderView.player = player
		sliderView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		trackView.trackDelegate = self
		trackView.mediaItems = mediaItems
		pressButton.delegate = self
		
		let view = clipCollectionViewController.view!
		view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		view.frame = clipContainerView.bounds
		clipContainerView.addSubview(view)

		
		// set Initial location
		player.seekToTime(currentTime)
		
		let item = UIBarButtonItem(image: UIImage(named: "DictionaryButton.png"), style: .Plain, target: self, action: "showDictionary")
		navigationItem.rightBarButtonItem = item
		
		updateUI()
		udpateWaveform()
	}
	
	override func viewDidAppear(animated: Bool) {
		
		let purchased_ = APP_DELEGATE().storeObserver.purchased("com.catalystwo.LearningPlayer.paid")
		if( purchased_ == false ) {
			clipCollectionViewController.admobView()
		}else {
			clipCollectionViewController.destroyADBannerView()
		}
		
	}
	override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
		WaveformMeganeView.sharedWaveformMeganeView.clearCache()
	}
	
	
	func fetchSoundClips() {
		var persistentIDs: [String] = []
		for entity: MPMediaItem in mediaItems {
			if let num = entity.valueForProperty( MPMediaItemPropertyPersistentID ) as! NSNumber? {
				persistentIDs.append(String(num.unsignedLongLongValue))
			}
		}
		let predicate = NSPredicate(format: "persistentID IN %@", persistentIDs)
		MNSoundClip.fetchWithPredicate(predicate) { (clips, error) -> Void in
			self.clipCollectionViewController.soundClips = clips
			self.clipCollectionViewController.tableView.reloadData()
			if self.recentlyAddedSoundClip != nil {
				self.clipCollectionViewController.flashSoundClip(self.recentlyAddedSoundClip!)
				self.recentlyAddedSoundClip = nil
			}
		}
	}
	
	func setAssetAt(assetURL: NSURL) {
		
		asset = AVURLAsset(URL: assetURL, options: nil)
		waveform = nil
		sliderView.backgroundImage = nil
		WaveformMeganeView.sharedWaveformMeganeView.clearCache()
		//	NSURL* muteURL = [[NSBundle mainBundle] URLForResource:@"mute" withExtension:@"m4a"];
		//
		//	AVURLAsset *dumAsset = [[AVURLAsset alloc] initWithURL:muteURL options:nil];
		let playerItem = AVPlayerItem(asset:asset!)
		player.replaceCurrentItemWithPlayerItem(playerItem)
		currentTime = CMTimeMakeWithSeconds(0, asset!.duration.timescale)
		selectedRange = CMTimeRangeMake(currentTime, asset!.duration)
		updateUI()
		udpateWaveform()
	}
	
	let kEndTimeCutOff:Double = 0

	func udpateWaveform() {
		// Waveform
		if waveform == nil && asset != nil {
			let endTimeSeconds: Double = CMTimeGetSeconds(asset!.duration) - kEndTimeCutOff
			let allRange: CMTimeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(endTimeSeconds, asset!.duration.timescale))
			SCWaveformView.generateWaveforms(asset!, timeRange: allRange, color: UIColor.grayColor(),
				size: sliderView.slider.bodyFrame.size, antialias: false, force: true) { [unowned self] (image: UIImage?) -> Void in
				if image != nil {
					self.sliderView.backgroundImage = image
				}
				self.waveform = image
			}
		}
		else {
			sliderView.backgroundImage = waveform
		}
	}
	
	func updateUI() {
		let playing = (player.rate != 0)
		play.selected = playing
		
		if mediaItem != nil {
			trackView.selectMedia(mediaItem!, animated: true)
		}
		
		if asset != nil && 0 != asset?.duration.timescale {
			let second: Float64 = CMTimeGetSeconds(asset!.duration)
			sliderView.maximumValue = CGFloat(second)
			sliderView.selectedRange = selectedRange
		}
		
		sliderView.updateTimeDisplay()
		title = songTitle
		updateSpeedometor()
		
		var image: UIImage
		if repeatMode == .None {
			image = UIImage(named: "RepeatNoneFlat")!
			repeatButton.setImage(UIImage.colorizedImage(image, withTintColor: UIColor.lightGrayColor(), alpha: 1, glow: false), forState: .Normal)
		}
		if repeatMode == .All {
			image = UIImage(named: "RepeatNoneFlat")!
			repeatButton.setImage(UIImage.colorizedImage(image, withTintColor: view.tintColor, alpha: 1, glow: false), forState: .Normal)
		}
		if repeatMode == .Track {
			image = UIImage(named: "RepeatTrackFlat")!
			repeatButton.setImage(UIImage.colorizedImage(image, withTintColor: view.tintColor, alpha: 1, glow: false), forState: .Normal)
		}

		var selectedAll = false
		if asset != nil && CMTimeGetSeconds(selectedRange.duration) == CMTimeGetSeconds(asset!.duration) {
			selectedAll = true
		}
		if playing {
			pressButton.selected = false
		}
		else {
			pressButton.selected = !selectedAll
		}
		var activateClipButton: Bool = false
		if !selectedAll {
			let second: Float64 = CMTimeGetSeconds(selectedRange.duration)
			if second < 3.0 * 60 {
				activateClipButton = true
			}
		}
		addClipButton.enabled = activateClipButton

	}
	
	func updateSpeedometor() {
		fasterButton.selected = false
		slowerButton.selected = false
		speedKnob.hidden = true
		if stepperRate > 1.01 {
			fasterButton.selected = true
			speedKnob.hidden = false
		}
		if stepperRate < 0.99 {
			slowerButton.selected = true
			speedKnob.hidden = false
		}
		var center: CGPoint = CGPointZero
		center.y = CGRectGetMidY(slowerButton.frame) + 15
		let midX: CGFloat = (slowerButton.center.x + fasterButton.center.x) / 2
		if stepperRate < 0.99 {
			let step: CGFloat = (midX - slowerButton.center.x) / (1.0 - MIN_SPEED)
			center.x = midX - step * (1.0 - stepperRate)
		}
		if stepperRate > 1.01 {
			let step: CGFloat = (fasterButton.center.x - midX) / (MAX_SPEED - 1.0)
			center.x = midX + step * (stepperRate - 1.0)
		}
		speedKnob.center = center
	}
	
	
	//MARK:- Actions
	
	@IBAction func play(sender: AnyObject) {
		if play.selected {
			stop()
			return
		}
		let currentTime: Double = CMTimeGetSeconds(player.currentTime())
		if fabs(currentTime - (CMTimeGetSeconds(selectedRange.start) + CMTimeGetSeconds(selectedRange.duration))) < 0.1 {
			playFromBeginning(true)
		}
		else {
			playFromBeginning(false)
		}
		updateUI()
		play.selected = true
	}
	
	func stop() {
		playerObserverTimer?.invalidate()
		playerObserverTimer = nil
		if periodicObserver != nil {
			player.removeTimeObserver(periodicObserver!)
		}
		periodicObserver = nil
		player.pause()
		play.selected = false
		let center = NSNotificationCenter.defaultCenter()
		center.postNotificationName("MNPlayerViewControllerDidFinishPlaying", object: self, userInfo: nil)
		updateUI()
	}
	
	var waitingForPlayerReadyLazilyCount:Int = 0

	func playFromBeginning(fromBeginning: Bool, withRange range: CMTimeRange) {
		if player.status != .ReadyToPlay {
			// LAZY...
			if waitingForPlayerReadyLazilyCount < 10 {
				
				let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
				dispatch_after(delayTime, dispatch_get_main_queue()) {
					self.playFromBeginning(fromBeginning, withRange: range)
					self.waitingForPlayerReadyLazilyCount++
				}
			}
			print("** not reaady **")
			return
		}
		selectedRange = range
		playFromBeginning(fromBeginning)
	}
	
	
	
	func playFromBeginning(fromBeginning: Bool) {
		if self.player.status != .ReadyToPlay {
			// LAZY...
			if waitingForPlayerReadyLazilyCount < 10 {

				let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
				dispatch_after(delayTime, dispatch_get_main_queue()) {
					
					self.playFromBeginning(fromBeginning)
					self.waitingForPlayerReadyLazilyCount++
				}
			}
			print("** not reaady **")
			return
		}
		
		if fromBeginning {
			let currentTime: Double = CMTimeGetSeconds(player.currentTime())
			if fabs(currentTime - CMTimeGetSeconds(selectedRange.start)) > 0.1 {
				rewind(nil)
			}
		}
	
		player.rate = Float(stepperRate)
		play.selected = self.player.rate == 0 ? false : true
		pressButton.selected = false
		//	[self.player play];
		playerObserverTimer?.invalidate()
		playerObserverTimer = nil
		playerObserverTimer = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: "playerObserverTimerFire", userInfo: nil, repeats: true)

		if 0 != self.asset.duration.timescale && periodicObserver == nil {
			var cmtime: CMTime
			cmtime = CMTimeMakeWithSeconds(0.25, self.asset.duration.timescale)

			periodicObserver = player.addPeriodicTimeObserverForInterval(cmtime, queue: dispatch_get_main_queue())  { [unowned self] (time: CMTime) -> Void in
				if self.sliderView.draggingCurrentLocation == true { return }
				let endTimeSeconds: Double = CMTimeGetSeconds(self.asset.duration) - self.kEndTimeCutOff
				if CMTimeGetSeconds(time) >= endTimeSeconds {
					self.endBoundary()
					print("*** Over the boundary ***")
					return
				}
				self.currentTime = time
				if self.activeSelectMode {
					var selectedRange = self.selectedRange
					let duration: Double = CMTimeGetSeconds(time) - CMTimeGetSeconds(selectedRange.start)
					selectedRange.duration = CMTimeMakeWithSeconds(duration, selectedRange.duration.timescale)
					self.selectedRange = selectedRange
				}
				self.sliderView.updateTimeDisplay()
				
				
				if !self.play.selected {
					self.play.selected = true
				}
				let timeDictionary = CMTimeCopyAsDictionary(time, nil)
				let dict = [ "time" : timeDictionary! ]
				
				let center = NSNotificationCenter.defaultCenter()
				center.postNotificationName("MNPlayerViewControllerPlaying", object: self, userInfo: dict)
				
			}
		}
	}
	
	
	func endBoundary() {
		let moved = currentTrackIsEndingMoveToNextTrackIfNecessary()
		if moved == false {
			playerObserverTimer?.invalidate()
			playerObserverTimer = nil
			if periodicObserver != nil {
				player.removeTimeObserver(periodicObserver!)
			}
			periodicObserver = nil
			player.pause()
			play.selected = false
		NSNotificationCenter.defaultCenter().postNotificationName("MNPlayerViewControllerDidFinishPlaying", object: self, userInfo: nil)
			updateUI()
		}
	}
	
	func currentTrackIsEndingMoveToNextTrackIfNecessary() -> Bool {
		assertMainThread()
		if playingClipCollection == true { return false }
		if repeatMode == .None { return false }
		
		if mediaItems.count == 0 {
			if repeatMode == .Track || repeatMode == .All {
				playFromBeginning(true)
				return true
			}
			else {
				return false
			}
		}
		if repeatMode == .All && timeSubstantiallyEqual(selectedRange.duration, asset.duration) {
			nextTrack()
			return true
		}
		if repeatMode == .Track || repeatMode == .All {
			seekToTime(selectedRange.start)
			sliderView.updateTimeDisplay()
			playFromBeginning(false)
			updateUI()
			return true
		}
		return false
	}
	
	func seekToTime(time: CMTime) {
		currentTime = time
		player.seekToTime(time) { [unowned self] finished in
			self.sliderView.updateTimeDisplay()
		}
	}
	
	func timeSubstantiallyEqual(t1: CMTime, _ t2: CMTime) -> Bool {
		let d1: Double = CMTimeGetSeconds(t1)
		let d2: Double = CMTimeGetSeconds(t2)
		return (fabs(d1 - d2) < 0.1)
	}
	
	func previousTrack() {
		if var index = currentTrackNumber {
			if index == 0 {
				seekToTime(selectedRange.start)
				sliderView.updateTimeDisplay()
				playFromBeginning(false)
				updateUI()
				return
			}
			index--
			if index >= mediaItems.count {
				return
			}
			mediaItem = mediaItems[index]
			playFromBeginning(true)
		}
	}
	
	func nextTrack() {
		if var index = currentTrackNumber {
			
			index++
			if index >= mediaItems.count {
				index = 0
			}
			mediaItem = mediaItems[index]
			playFromBeginning(true)
		}
	}
	
	@objc func playerObserverTimerFire() {
		let isSelected = play.selected
		if !isSelected {
			return
		}
		let isPlaying: Bool = (player.rate > 0)
		if isSelected != isPlaying {
			if sliderView.draggingCurrentLocation == false {
				player.rate = Float(stepperRate)
			}
			print("** Player is not working correctly **")
		}

	}

	@IBAction func faster(sender: AnyObject) {
		if stepperRate >= MAX_SPEED {
			return
		}
		stepperRate += 0.2
		// If playing, change rate now
		if player.rate != 0 {
			player.rate = Float(stepperRate)
		}
		if stepperRate > 0.99 && stepperRate < 1.01 {
			stepperRate = 1.0
		}
		updateUI()
		saveToUserDefaults()
	}
	
	@IBAction func slower(sender: AnyObject) {
		if stepperRate <= MIN_SPEED {
			return
		}
		stepperRate -= 0.2
		// If playing, change rate now
		if player.rate != 0 {
			player.rate = Float(stepperRate)
		}
		if stepperRate > 0.99 && stepperRate < 1.01 {
			stepperRate = 1.0
		}
		updateUI()
		saveToUserDefaults()
	}

	@IBAction func repeatButton(sender: AnyObject) {
		repeatMode = repeatMode.next()
		updateUI()
		saveToUserDefaults()
	}
	
	@IBAction func rewind(sender: AnyObject?) {
		let start: Float64 = CMTimeGetSeconds(currentTime)
		if start < 2.0 {
			previousTrack()
			return
		}
		seekToTime(selectedRange.start)
		sliderView.updateTimeDisplay()
		playFromBeginning(false)
		updateUI()
	}
	
	@IBAction func fastfaward(sender: AnyObject) {
		nextTrack()
	}
	
	@IBAction func tenSecondsRewind(sender: AnyObject) {
		var currentTime: Double = CMTimeGetSeconds(player.currentTime())
		currentTime -= 10
		currentTime = max(0, currentTime)
		let cmtime = CMTimeMakeWithSeconds(currentTime, player.currentTime().timescale)
		seekToTime(cmtime)
	}
	
	@IBAction func twoSecondsRewind(sender: AnyObject) {
		var currentTime: Double = CMTimeGetSeconds(player.currentTime())
		currentTime -= 2
		currentTime = max(0, currentTime)
		let cmtime = CMTimeMakeWithSeconds(currentTime, player.currentTime().timescale)
		seekToTime(cmtime)
	}
	
	@IBAction func thirtySecondsFoward(sender: AnyObject) {
		var currentTime: Double = CMTimeGetSeconds(player.currentTime())
		currentTime += 30
		currentTime = min(CMTimeGetSeconds(asset.duration), currentTime)
		let cmtime = CMTimeMakeWithSeconds(currentTime, player.currentTime().timescale)
		seekToTime(cmtime)
	}
	
	func showDictionary() {
		let controller = JishoViewController()
		let nav: MNBaseNavigationController = MNBaseNavigationController(rootViewController: controller)
		nav.restorationIdentifier = "ModalNav"
		APP_DELEGATE().window.rootViewController!.presentViewController(nav, animated: true, completion: nil)
	}
	
	@IBAction func getInfo(sender: AnyObject) {
	
		let alertController = UIAlertController(title: WLoc("SelectionRange"), message: "", preferredStyle: .Alert)
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .Cancel) { (UIAlertAction action) -> Void in }
		let action1 = UIAlertAction(title: WLoc("Select"), style: .Default) { [unowned self] (UIAlertAction action) -> Void in
			let fields = alertController.textFields

			let start:Float64 = Float64(fields![0].text!)!
			let end:Float64 = Float64(fields![1].text!)!
			
		self.selectedRange =	CMTimeRangeMake(CMTimeMakeWithSeconds( start, self.asset.duration.timescale), CMTimeMakeWithSeconds( end - start, self.asset.duration.timescale))

		}

		alertController.addAction(action1)
		alertController.addAction(cancel)
		
		alertController.addTextFieldWithConfigurationHandler { (field:UITextField) -> Void in
			field.text = String(CMTimeGetSeconds(self.selectedRange.start))
			field.keyboardType = .DecimalPad
		}
		
		alertController.addTextFieldWithConfigurationHandler { (field:UITextField) -> Void in
			
			field.keyboardType = .DecimalPad
			field.text = String(CMTimeGetSeconds(self.selectedRange.end))

		}
		presentViewController(alertController, animated: true, completion: nil)

	}
	
	func export(sender: UIButton) {
		let fromRect = sender.superview!.convertRect(sender.frame, toView: self.view!)
		MNExporter.sharedExporter().exportAsset(asset, soundClip: nil, range: selectedRange, fromRect: fromRect,
			inView: self.view!, inViewController: navigationController!)
	}
	
	//MARK: - Delegate Methods
	
	func trackView(trackView: MNTrackCollectionView, didSelectMedia item: MPMediaItem) {
		clipCollectionViewController.unselect()
		mediaItem = item
		playFromBeginning(true)
	}
	
	func spanSliderValueChanged(newCurrentLocation: CGFloat) {

		let secondNumber = NSNumber(float:Float(newCurrentLocation))
		let secondDecimalValue = NSDecimalNumber(decimal: secondNumber.decimalValue)
		
		let durationTimescaleDecimal = NSNumber(longLong: Int64(asset.duration.timescale)).decimalValue
		let durationTiemscaleValue = NSDecimalNumber(decimal: durationTimescaleDecimal)
		let valueDecimal:NSDecimalNumber = durationTiemscaleValue.decimalNumberByMultiplyingBy(secondDecimalValue)
		
		let cmtime = CMTime(value: valueDecimal.longLongValue, timescale: asset.duration.timescale, flags: .Valid, epoch: 0)
		let rate: Float = player.rate
		player.rate = 0
		player.seekToTime(cmtime, completionHandler: { [unowned self] (finished: Bool) -> Void in
			if finished {
				self.sliderView.updateTimeDisplay()
				self.player.rate = rate
			}
		})
	}
	
	func spanSliderSelectionDraggingElement(sender: MNSpanSlider, visible: Bool, toLocation location: CGFloat) {
		let meganeView = WaveformMeganeView.sharedWaveformMeganeView
		if visible == false {
			meganeView.hidden = true
			return
		}
		let y = sliderView.superview!.convertRect(sliderView.frame, toView: self.view!).origin.y
		meganeView.frame = CGRectMake(0, y - 50, self.view.bounds.size.width, 50)
		self.view.addSubview(meganeView)
		let time = CMTimeMakeWithSeconds(Float64(location), asset.duration.timescale)
		let displaySecs: Float64 = 5
		let timeRange = CMTimeRangeMake(time, CMTimeMakeWithSeconds(displaySecs, asset.duration.timescale))
		let contentRect = sliderView.slider.superview!.convertRect(sliderView.slider.bodyFrame, toView: self.view)
		let x = contentRect.origin.x + contentRect.size.width * ( location / CGFloat(CMTimeGetSeconds(asset.duration)))

		meganeView.selectedRange = selectedRange
		meganeView.offset = x
		meganeView.time = timeRange.start
		meganeView.hidden = false
	}
	
	func spanSliderSelectionUpdated(sender: MNSpanSlider) {
		//	[[WaveformMeganeView sharedWaveformMeganeView] setWaveformImage: nil];
		selectedRange = CMTimeRangeMake(CMTimeMakeWithSeconds(Float64(sender.startLocation), asset.duration.timescale),
			CMTimeMakeWithSeconds(Float64(sender.endLocation - sender.startLocation), asset.duration.timescale))
		let location: CGFloat = sender.currentLocation
		waveformTimer?.invalidate()
		waveformTimer = NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: "spanSliderSelectionUpdateWithTimer:", userInfo: ["location": Float64(location)], repeats: false)
		if NSDate.timeIntervalSinceReferenceDate() - waveformTimerInitiatedInterval > 0.2 {
			waveformTimer?.invalidate()
			waveformTimer = nil
			waveformTimer = NSTimer.scheduledTimerWithTimeInterval(0.0, target: self, selector: "spanSliderSelectionUpdateWithTimer:", userInfo: ["location":Float64(location), "force":true], repeats: false)
			
			waveformTimer!.fire()
			waveformTimer = nil
		}
	}

	
	@objc func spanSliderSelectionUpdateWithTimer(timer: NSTimer) {
		var location: Float64 = timer.userInfo?["location"] as! Float64!
		if location < 0 {
			location = 0
		}
		let duration = CMTimeGetSeconds(asset.duration)
		if location > duration {
			location = duration
		}
		let _ = timer.userInfo?["force"] as! Bool? ?? false // Unused
		let displaySecs: Float64 = 5
		let time = CMTimeMakeWithSeconds(location, asset.duration.timescale)
		let timeRange = CMTimeRangeMake(time, CMTimeMakeWithSeconds(displaySecs, asset.duration.timescale))
		waveformTimer = nil
		let contentRect = sliderView.slider.superview!.convertRect(sliderView.slider.bodyFrame, toView: self.view!)
		let x = contentRect.origin.x + contentRect.size.width * CGFloat(location / CMTimeGetSeconds(asset.duration))

		let meganeView = WaveformMeganeView.sharedWaveformMeganeView
		meganeView.selectedRange = selectedRange
		meganeView.offset = x
		meganeView.time = timeRange.start
	}
	
	func waveformAtIndex(index: Int, size: CGSize, forSectionRange range: CMTimeRange, completion completionHandler: (image: UIImage?) -> Void) {
		SCWaveformView.generateWaveforms(asset, timeRange: range, color: UIColor.grayColor(), size: size, antialias: false, force: false) { (image: UIImage?) -> Void in
			completionHandler(image: image)
		}
	}
	
	func pressButtonDidBegin(button: MNPressButton) {
		if player.rate == 0 {
			return
		}
		activeSelectMode = true
		if endObserver != nil {
			player.removeTimeObserver(endObserver!)
			endObserver = nil
		}
		selectedRange.start = currentTime
		selectedRange.duration = CMTimeMakeWithSeconds(0, selectedRange.duration.timescale)
		sliderView.updateTimeDisplay()
	}
	
	func pressButtonDidEnd(button: MNPressButton) {
		if player.rate == 0 {
			if CMTimeGetSeconds(selectedRange.duration) == CMTimeGetSeconds(asset.duration) {
				ShowAlertMessage(WLoc("HowToUseSelectButton"), self)
				return
			}
			let range = CMTimeRange(start: CMTimeMakeWithSeconds(0, asset.duration.timescale), duration: asset.duration)
			selectedRange = range
			return
		}
		activeSelectMode = false
		stop()
		rewind(nil)
	}

	//MAR:- DB
	
	func addData(sender: AnyObject) {
		guard let mediaItem = self.mediaItem else { return }
		
		let context = APP_DELEGATE().managedObjectContext
		let entity = NSEntityDescription.entityForName("MNSoundClip", inManagedObjectContext: context)
		let soundClip = MNSoundClip(entity: entity!, insertIntoManagedObjectContext: context)
		recentlyAddedSoundClip = soundClip
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyPersistentID ) as! NSNumber? {
			soundClip.persistentID = String(num.unsignedLongLongValue)
		}
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyAlbumPersistentID) as! NSNumber?  {
			soundClip.albumPersistentID = String(num.unsignedLongLongValue)
		}
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyArtistPersistentID) as! NSNumber? {
			soundClip.artistPersistentID = String(num.unsignedLongLongValue)
		}
		
		soundClip.artist = mediaItem.valueForProperty(MPMediaItemPropertyArtist) as! String?
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyGenrePersistentID) as! NSNumber? {
			soundClip.genrePersistentID = String(num.unsignedLongLongValue)
		}
		
		soundClip.genre = mediaItem.valueForProperty(MPMediaItemPropertyGenre) as! String?
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyPodcastPersistentID) as! NSNumber? {
			soundClip.podcastPersistentID = String(num.unsignedLongLongValue)
		}
		
		soundClip.albumTitle = mediaItem.valueForProperty(MPMediaItemPropertyAlbumTitle) as! String?
		soundClip.title = mediaItem.valueForProperty(MPMediaItemPropertyTitle) as! String?
		
		soundClip.timeRange = selectedRange
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyAlbumTrackNumber) as! NSNumber? {
			soundClip.trackNumber = Int16(num.integerValue)
		}
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyMediaType) as! NSNumber? {
			soundClip.mediaType = Int32(num.integerValue)
		}
		
		let uuid = CFUUIDCreate(nil)
		let uuidString = CFUUIDCreateString(nil, uuid)
		soundClip.uuid = uuidString as String
		
		if MPMediaType.Podcast.rawValue == UInt(soundClip.mediaType) {
			let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A)!
			var tempURL = NSURL.fileURLWithPath(NSTemporaryDirectory())
			tempURL = tempURL.URLByAppendingPathComponent("com.catalystwo")
			let fm = NSFileManager.defaultManager()
			if !fm.fileExistsAtPath(tempURL.path!) {
				do {
					try fm.createDirectoryAtURL(tempURL, withIntermediateDirectories: true, attributes: nil)
				} catch {
					
				}
			}
			
			let uuid = CFUUIDCreate(nil)
			let string = CFUUIDCreateString(nil, uuid) as String
			let filename = "\(string).m4a"
			tempURL = tempURL.URLByAppendingPathComponent(filename)
			if fm.fileExistsAtPath(tempURL.path!) {
				do {
					try fm.removeItemAtURL(tempURL)
				} catch {
					
				}
			}
			exportSession.outputURL = tempURL
			exportSession.outputFileType = AVFileTypeAppleM4A
			exportSession.timeRange = selectedRange
			exportSession.metadata = asset.commonMetadata
			
			// Start exporting
			exportSession.exportAsynchronouslyWithCompletionHandler { [weak exportSession] in
				if exportSession!.status == .Completed {
					dispatch_async(dispatch_get_main_queue()) {
						soundClip.binaryData = NSData(contentsOfURL: tempURL)
						APP_DELEGATE().saveContext()
						do {
							try NSFileManager().removeItemAtURL(tempURL)
						}catch {
						}
						dispatch_async(APP_DELEGATE().fetchQueue()) {
							self.fetchSoundClips()
						}
					}
				}
				else {
					print("failed")
				}
			}
			
		}
		
		
		//albumPlaybackDuration
		var albumPlaybackDuration: Double = 0
		for item: MPMediaItem in mediaItems {
			if let num = item.valueForProperty(MPMediaItemPropertyPlaybackDuration) as! NSNumber? {
				albumPlaybackDuration += num.doubleValue
			}
		}
		soundClip.albumPlaybackDuration = albumPlaybackDuration
		
		if let num = mediaItem.valueForProperty(MPMediaItemPropertyPlaybackDuration) as! NSNumber? {
			soundClip.playbackDuration = num.doubleValue
		}
		APP_DELEGATE().saveContext()
		
		dispatch_async(APP_DELEGATE().fetchQueue()) {
			self.fetchSoundClips()
		}
	}
	

}