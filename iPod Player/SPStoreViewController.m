//
//  CCalStoreViewController.m
//  CCal-X
//
//  Created by Nishikata Masatoshi on 28/07/12.
//  Copyright (c) 2012 Catalystwo. All rights reserved.
//

#import "SPStoreViewController.h"
#import "HelpViewController.h"
#import "MyStoreObserver.h"
#import "Tui.h"
#import "GCNetworkReachability.h"

#define kMyFeatureIdentifier IN_APP_PURCHAE_ID


@interface SPStoreViewController ()

@end

@implementation SPStoreViewController

@synthesize products = myProducts_;
@synthesize tableView = tableView_;

- (id)init
{
	self = [super initWithNibName:nil bundle:nil];
	if (self) {
		// Custom initialization
		[self requestProductData];

	}
	return self;
}



-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	productsRequest_.delegate = nil;
	productsRequest_ = nil;
	
	self.products = nil;	
	
	self.tableView.delegate = nil;
	self.tableView.dataSource = nil;
	self.tableView = nil;
	
}




- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
	
	UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
	tableView.delegate = self;
	tableView.dataSource = self;
	tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
	self.tableView = tableView;
	
	[self.view addSubview: tableView];
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"Close", @"")
																									  style: UIBarButtonItemStyleDone
																									 target: self
																									 action: @selector(close)];
	
	if ([SKPaymentQueue canMakePayments]) {
		// Display a store to the user.
		
		
	} else {
		// Warn the user that purchases are disabled.
		
		
	}
	
	
	self.navigationItem.title = NSLocalizedString(@"FFStoreViewControllerTitle", @"");
	
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear: animated];
	
	[[NSNotificationCenter defaultCenter] addObserver: self
														  selector: @selector(reload)
																name: @"MNStoreObserverDidChangeNotificationName"
															 object: nil];
	
	GCNetworkReachability *internetReach = [GCNetworkReachability reachabilityForInternetConnection];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	if( netStatus == NotReachable )
	{
		offline_ = YES;
	}
	
	
	self.navigationController.toolbarHidden = NO;
	
	
}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
//
-(NSArray*)toolbarItems
{
	
	
	UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"FAQ", @"")
																				style: UIBarButtonItemStyleBordered
																			  target: self
																			  action: @selector(showFAQ)];
	
	UIBarButtonItem* separator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
																										target: nil
																										action: nil];
	
	
	UIBarButtonItem* clear = nil;
	
#if DEBUG
	clear = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(clear)];
#endif
	//

	return [NSArray arrayWithObjects: item, separator,clear, nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	
	
	// Return the number of sections.
	if( self.products )
	{
		return MAX(1, self.products.count) + 1;
		
	}else {
		
		return 1;
	}
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
		
	
	if( self.products && self.products.count > 0 )
	{
		if( section == self.products.count ) return 1;
		
		
		return 2;
		
	}else {
		
		return 1;
	}
	
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{	
	
	if( !self.products || self.products.count == 0 ) return nil;
	if( self.products && self.products.count > 0 )
	{
		if( section == self.products.count ) return NSLocalizedString(@"Tap restore button to restore purchased history", @"");

	}
	
	SKProduct* product = [self.products objectAtIndex: section];
	return product.localizedDescription;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
	UITableViewCell *cell = nil;
	

	
	if( self.products )
	{
		if( self.products.count == 0 )
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
			
			cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerServerDown", @"");
			
			cell.userInteractionEnabled = NO;
			cell.textLabel.textColor = [UIColor grayColor];
			cell.textLabel.textAlignment = UITextAlignmentCenter;
			
			
			return cell;
		}
		
		if( indexPath.section == self.products.count )
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
			cell.textLabel.textAlignment = UITextAlignmentCenter;
			cell.textLabel.text = NSLocalizedString(@"Restore", @"");
			return cell;
		}

		
		SKProduct* product = [self.products objectAtIndex: indexPath.section];
		
		if( indexPath.row == 0 )
		{
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
			
			//cell.imageView.image = [UIImage imageNamed:@"CCalIcon72.png"];
			cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
			cell.textLabel.text = product.localizedTitle;
			cell.textLabel.font = [UIFont systemFontOfSize:16];
			NSString* currencyCode = [[product priceLocale] objectForKey:NSLocaleCurrencyCode];
			NSString* currencySymbol = [[product priceLocale] objectForKey:NSLocaleCurrencySymbol];
			
			if( !currencyCode ) currencyCode = @"";
			if( !currencySymbol ) currencySymbol = @"";
			
			NSString* priceString = [NSString stringWithFormat:@"%@%@ (%@)", currencySymbol ,product.price, currencyCode];
			cell.detailTextLabel.text = priceString;
			cell.userInteractionEnabled = NO;
		}
		
		
		if( indexPath.row == 1 )
		{
			BOOL purchased = [[APP_DELEGATE storeObserver] purchased:kMyFeatureIdentifier];
			
			if( !purchased )
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
				
				cell.textLabel.textAlignment = UITextAlignmentCenter;
				cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerPurchase", @"");
				
				cell.userInteractionEnabled = YES;
			}
			
			if( purchased )
			{
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
				
				cell.textLabel.textAlignment = UITextAlignmentCenter;
				cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerAlreadyPurchased", @"");
				//cell.textLabel.textColor = [UIColor grayColor];
				cell.userInteractionEnabled = YES;
			}
			
		}
		
		
	}else {
		
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		cell.userInteractionEnabled = NO;
		
		if( ![SKPaymentQueue canMakePayments] )
		{
			cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerUnable", @"");
			
		}
		
		else if( offline_ )
		{
			
			cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerOffline", @"");
		}
		
		else {
			
			cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerRetrieving", @"");
			
		}
	}
	
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
	if( indexPath.section == self.products.count )
	{
		[self restore];
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
		
		return;
	}
	
	
	BOOL purchased = [[APP_DELEGATE storeObserver] purchased:kMyFeatureIdentifier];
	
	
	
	if( purchased )
	{
		MNBlockAlertView * alert = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"CCalStoreViewControllerAlreadyPurchased", @"") message:NSLocalizedString(@"CCalStoreViewControllerAlreadyPurchasedMessage", @"")];
		
		[alert addTitle:NSLocalizedString(@"OK", @"") actionBlock:^{
			
			SKProduct *selectedProduct = [self.products objectAtIndex: indexPath.section];
			SKPayment *payment = [SKPayment paymentWithProduct:selectedProduct];
			[[SKPaymentQueue defaultQueue] addPayment:payment];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
		
		[alert show];
		
	}else
	{
		SKProduct *selectedProduct = [self.products objectAtIndex: indexPath.section];
		SKPayment *payment = [SKPayment paymentWithProduct:selectedProduct];
		[[SKPaymentQueue defaultQueue] addPayment:payment];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
}

-(void)close
{
	[self dismissModalViewControllerAnimated:YES];
}

-(void)showFAQ
{
	HelpViewController* vc = [[HelpViewController alloc] init];
	vc.helpFile = NSLocalizedString(@"FFStoreViewControllerFAQURL", @"");
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
	vc.title = NSLocalizedString(@"FAQ", @"");
	[self presentModalViewController:nav animated:YES];
	
}


-(void)clear
{
	// Clear product
	for( SKProduct* product in self.products )
	{
		[[APP_DELEGATE storeObserver] clear:product.productIdentifier];
	}
	
	
	[[APP_DELEGATE storeObserver] sendNotification];
	[self reload];
}

-(void)restore
{
	MNBlockAlertView * alert = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"CCalStoreViewControllerRestore", @"") message:NSLocalizedString(@"CCalStoreViewControllerAlreadyPurchasedMessage", @"")];
	
	[alert addTitle:NSLocalizedString(@"OK", @"") actionBlock:^{
		
		//			SKProduct *selectedProduct = [self.products objectAtIndex: indexPath.section];
		//			SKPayment *payment = [SKPayment paymentWithProduct:selectedProduct];
		//			[[SKPaymentQueue defaultQueue] addPayment:payment];
		
		[self clear];
		
		[APP_DELEGATE storeObserver].restoring = YES;
		[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
		
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[self reload];
		
		
	} cancel:NO];
	
	[alert addTitle:NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
	
	[alert show];
	
}


-(void)reload
{
	[self.tableView reloadData];
	[self requestProductData];
}

- (void) requestProductData
{
	if( productsRequest_ )
	{
		productsRequest_.delegate = nil;
		productsRequest_ = nil;
	}
	
	
	
	productsRequest_ = [[SKProductsRequest alloc] initWithProductIdentifiers:
							  [NSSet setWithObject: kMyFeatureIdentifier]];
	productsRequest_.delegate = self;
	[productsRequest_ start];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
	
	self.products = response.products;
	// Populate your UI from the products list.
	// Save a reference to the products list.
	
	productsRequest_.delegate = nil;
	productsRequest_ = nil;
	
	[self.tableView reloadData];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
}



@end
