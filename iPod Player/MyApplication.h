//
//  MyApplication.h
//  Speak
//
//  Created by Masatoshi Nishikata on 27/08/13.
//  Copyright (c) 2013 Catalystwo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyApplication : UIApplication

-(void)setOverrideFontSize:(NSInteger)size;
-(NSInteger)overrideFontSize;

@end
