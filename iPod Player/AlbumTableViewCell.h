//
//  MNFontLabelCell.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 8/01/11.
//  Copyright 2011 Catalystwo Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AlbumTableViewCell : UITableViewCell  {
	
	
	UIImageView * __weak artworkView_;

//	__block OAuthTwitterUtils *twitterUtils_;

}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier ;
-(void)setTrackNumber:(NSNumber *)num;
- (void)prepareForReuse;
- (void)dealloc ;


@property (nonatomic, weak, readonly) UIImageView* artworkView;

@end