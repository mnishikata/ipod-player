//
//  SoundClipTableViewCell.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 22/04/14.
//
//

#import <UIKit/UIKit.h>

@class SoundClipTableViewCell, CERoundProgressView;

@interface SoundClipTableViewCellContentView : UIView

@property (nonatomic, weak) SoundClipTableViewCell* cell;

@end

@interface SoundClipTableViewCell : UITableViewCell
{
	CGFloat cellHeight_;
	SoundClipTableViewCellContentView __weak * cellContentView_;
	UIImageView __weak * iconView_;
	
	CERoundProgressView *progressView_;

}
-(void)updateHeightForWidth:(CGFloat)width;
+(NSDictionary* )attributes;
+(NSDictionary* )disabledAttributes;
+(NSDictionary* )excerptAttributes;

@property (nonatomic, strong) NSAttributedString* attributedString;
@property (nonatomic, readonly) CGFloat cellHeight;
@property (nonatomic, readwrite) CGFloat width;
@property (nonatomic, readwrite) BOOL copiedClip;
@property (nonatomic, readwrite) CGFloat progress;

@end
