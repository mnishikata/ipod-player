//
//  MNFontLabelCell.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 8/01/11.
//  Copyright 2011 Catalystwo Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "MNAttributedTextLabel.h"

extern void MNRunDraw(CTRunRef run,
				 CGContextRef ctx,
				 CFRange range,
				  BOOL printingResolution,
					  BOOL ignoreScaling);



@interface HistoryTableViewCell : UITableViewCell  {
	
	
	UIImageView* iconView;
	MNAttributedTextLabel* contentView;
	MNAttributedTextLabel* contentDetailView;
	MNAttributedTextLabel* subtitleView;
	MNAttributedTextLabel* secondSubtitleView;

	CGFloat rightIndentationWidth;
	CGFloat leftIndentationWidth;

	CGRect iconFrame;
	CGRect contentViewFrame;
	CGRect contentDetailViewFrame;


	UITableViewCellStateMask currentState;
	CALayer *backgroundLayer;

	

//	__block OAuthTwitterUtils *twitterUtils_;

}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier ;
-(void)setImage:(UIImage *)anImage;
-(void)setFullFrameImage:(UIImage *)anImage;
-(void)setFrame:(CGRect)frame;
-(void)setUserInteractionEnabled:(BOOL)flag;
- (void)prepareForReuse;
- (void)dealloc ;
-(void)setAttributedString:(NSAttributedString*)attributedString;
-(NSAttributedString*)attributedString;
-(void)setDetailedAttributedString:(NSAttributedString*)detailedAttributedString;
-(NSAttributedString*)detailedAttributedString;
-(void)setDetailedAttributedStringInverted:(BOOL)flag;
-(void)setSubtitleAttributedString:(NSAttributedString*)attributedString;
-(NSAttributedString*)subtitleAttributedString ;
-(void)setSecondSubtitleAttributedString:(NSAttributedString*)attributedString;
-(NSAttributedString*)secondSubtitleAttributedString;
- (void)setHighlighted:(BOOL)selected animated:(BOOL)animated;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated;
-(void)setNeedsDisplay;
- (void)willTransitionToState:(UITableViewCellStateMask)state;
-(void)willTransitionToStateMain;
-(void)layoutSubviews;

@property (nonatomic, assign) NSAttributedString * attributedString;
@property (nonatomic, assign) NSAttributedString * detailedAttributedString;
@property (nonatomic, assign) NSAttributedString * subtitleAttributedString;
@property (nonatomic, assign) NSAttributedString * secondSubtitleAttributedString;

@property (nonatomic, assign) UIImage* image;

@end