//
//  MNSoundClip.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 25/02/16.
//
//

import Foundation
import CoreData

let debugFlagOnlyID = false
let debugFlagForceLookForName = false

extension MNSoundClip {
	
	@NSManaged var albumPersistentID: String?
	@NSManaged var albumPlaybackDuration: Double
	@NSManaged var albumTitle: String?
	@NSManaged var artist: String?
	@NSManaged var artistPersistentID: String?
	@NSManaged var binaryData: NSData?
	@NSManaged var comment: String?
	@NSManaged var creationDate: NSTimeInterval
	@NSManaged var genre: String?
	@NSManaged var genrePersistentID: String?
	@NSManaged var mediaType: Int32
	@NSManaged var permalink: String?
	@NSManaged var persistentID: String?
	@NSManaged var playbackDuration: Double
	@NSManaged var podcastPersistentID: String?
	@NSManaged var timeRangePropertyList: NSData?
	@NSManaged var title: String?
	@NSManaged var trackNumber: Int16
	@NSManaged var transcription: String?
	@NSManaged var uuid: String?
	
	var timeRange: CMTimeRange {
		set {
			let dict = CMTimeRangeCopyAsDictionary(newValue, nil)
			let propertyListData = NSPropertyListSerialization.dataFromPropertyList(dict!, format: .BinaryFormat_v1_0, errorDescription: nil)
			setValue(propertyListData, forKey:"timeRangePropertyList")
			
		}
		get {
			let data = valueForKey("timeRangePropertyList")
			if data == nil {
				return CMTimeRangeMake(CMTimeMakeWithSeconds(0, 1), CMTimeMakeWithSeconds(0, 1))
			}
			let dict = NSPropertyListSerialization.propertyListFromData(data! as! NSData, mutabilityOption: .Immutable, format: nil, errorDescription: nil)
			if nil == dict {
				return CMTimeRangeMake(CMTimeMakeWithSeconds(0, 1), CMTimeMakeWithSeconds(0, 1))
			}
			return CMTimeRangeMakeFromDictionary((dict as! CFDictionaryRef))
			
		}
	}
	
}



@objc(MNSoundClip)
class MNSoundClip: NSManagedObject {


	class func fetchSoundClipsInGenre(collections: [MPMediaItemCollection]) -> NSPredicate {
		// Genre... genre, genre ID
		var predicateByID: NSPredicate
		var predicateByName: NSPredicate
		var entries: [String] = []
		// Genre
		for entity in collections {
			if let num = entity.representativeItem?.valueForProperty(MPMediaItemPropertyGenrePersistentID) as! NSNumber? {
			entries.append(String(num.unsignedLongLongValue))
			}
		}
		predicateByID = NSPredicate(format: "genrePersistentID IN %@", entries)
		// Name
		entries = []
		for entity in collections {
			let name = entity.representativeItem?.valueForProperty( MPMediaItemPropertyGenre) as! String?
			if name?.characters.count > 0 {
				entries.append(name!)
			}
		}
		
		predicateByName = NSPredicate(format: "genre IN %@", entries)

		if( debugFlagOnlyID ) { return predicateByID }
		if( debugFlagForceLookForName ) { return predicateByName }
		
		return NSCompoundPredicate( orPredicateWithSubpredicates:[predicateByID, predicateByName])
	}
	
	
	class func fetchSoundClipsInAlbum(collections: [MPMediaItemCollection]) -> NSPredicate {
		// Genre... genre, genre ID
		var predicateByID: NSPredicate
		var predicateByName: NSPredicate
		var entries: [String] = []
		// Genre
		for entity in collections {
			if let num = entity.representativeItem?.valueForProperty(MPMediaItemPropertyAlbumPersistentID) as! NSNumber? {
			entries.append(String(num.unsignedLongLongValue))
			}
		}
		predicateByID = NSPredicate(format: "albumPersistentID IN %@", entries)
		// Name
		entries = []
		for entity: MPMediaItemCollection in collections {
			let name = entity.representativeItem?.valueForProperty(MPMediaItemPropertyAlbumTitle) as! String?
			if name?.characters.count > 0 {
				entries.append(name!)
			}
		}
		predicateByName = NSPredicate(format: "albumTitle IN %@", entries)

		if debugFlagOnlyID {
			return predicateByID
		}
		if debugFlagForceLookForName {
			return predicateByName
		}
		return NSCompoundPredicate(orPredicateWithSubpredicates:[predicateByID, predicateByName])

	}
	
	
	class func fetchSoundClipsInArtist(collections: [MPMediaItemCollection]) -> NSPredicate {
		// Genre... genre, genre ID
		var predicateByID: NSPredicate
		var predicateByName: NSPredicate
		var entries: [String] = []
		// Genre
		for entity in collections {
			if let num = entity.representativeItem?.valueForProperty( MPMediaItemPropertyArtistPersistentID) as! NSNumber? {
			entries.append(String(num.unsignedLongLongValue))
			}
		}
		predicateByID = NSPredicate(format: "artistPersistentID IN %@", entries)
		// Name
		entries = []
		for entity: MPMediaItemCollection in collections {
			let name = entity.representativeItem?.valueForProperty(MPMediaItemPropertyArtist) as! String?
			if name?.characters.count > 0 {
				entries.append(name!)
			}
		}
		predicateByName = NSPredicate(format: "artist IN %@", entries)
		if debugFlagOnlyID {
			return predicateByID
		}
		if debugFlagForceLookForName {
			return predicateByName
		}
		return NSCompoundPredicate(orPredicateWithSubpredicates:[predicateByID, predicateByName])

	}
	
	class func fetchSoundClipsInPodcast(collections: [MPMediaItemCollection]) -> NSPredicate {
		// Genre... genre, genre ID
		var predicateByID: NSPredicate
		var entries: [String] = []
		// Genre
		for entity in collections {
			if let num = entity.representativeItem?.valueForProperty(MPMediaItemPropertyPodcastPersistentID) as! NSNumber? {
				entries.append(String(num.unsignedLongLongValue))
			}
		}
		predicateByID = NSPredicate(format: "podcastPersistentID IN %@", entries)
		return NSCompoundPredicate(orPredicateWithSubpredicates:[predicateByID])
	}
	
	
	// THIS CAUSES CORE DATA ERROR
	
	class func fetchSoundClipsForSongs(collections: [MPMediaItemCollection]) -> NSPredicate {
		// Genre... genre, genre ID
		var predicateByID: NSPredicate? = nil
		var orPredicates: [NSPredicate] = []
		var entries: [String] = []
		// Genre
		for entity: MPMediaItemCollection in collections {
			let songs: [MPMediaItem] = entity.items
			for song in songs {
				if let num = song.valueForProperty(MPMediaItemPropertyPersistentID) as! NSNumber? {
				entries.append(String(num.unsignedLongLongValue))
				}
			}
		}
		predicateByID = NSPredicate(format: "persistentID IN %@", entries)
		if debugFlagOnlyID {
			return predicateByID!
		}
		orPredicates.append(predicateByID!)
		if debugFlagForceLookForName {
			orPredicates = []
		}
		// Album title and property title
		
		entries = []
		for entity in collections {
			let songs: [MPMediaItem] = entity.items
			for song in songs {
				let title = song.valueForProperty( MPMediaItemPropertyTitle) as! String?
				let albumTitle = song.valueForProperty( MPMediaItemPropertyAlbumTitle) as! String?
				let artistTitle = song.valueForProperty( MPMediaItemPropertyArtist) as! String?
				var predicateArray: [NSPredicate] = []
				if title?.characters.count > 0 {
					let predicate = NSPredicate(format: "(title LIKE %@)", title!)
					predicateArray.append(predicate)
				}
				if albumTitle?.characters.count > 0 {
					let predicate = NSPredicate(format: "(albumTitle LIKE %@)", albumTitle!)
					predicateArray.append(predicate)
				}
				if artistTitle?.characters.count > 0 {
					let predicate = NSPredicate(format: "(artist LIKE %@)", artistTitle!)
					predicateArray.append(predicate)
				}
				let predicate = NSCompoundPredicate(andPredicateWithSubpredicates:predicateArray)
				orPredicates.append(predicate)

			}
		}
	
		return NSCompoundPredicate(orPredicateWithSubpredicates:orPredicates)
	}
	
	class func lookForMedia(clip: MNSoundClip) -> MPMediaItem? {
		let ullvalue: UInt64 = strtoull( (clip.persistentID! as NSString).UTF8String, nil, 0)
		let num = NSNumber(unsignedLongLong: ullvalue)
		let predicate = MPMediaPropertyPredicate(value: num, forProperty: MPMediaItemPropertyPersistentID)
		let query = MPMediaQuery(filterPredicates: [predicate])
		var obj: [MPMediaItem]? = query.items
		if obj?.count > 0 {
			return obj!.last
		}
		if debugFlagOnlyID {
			return nil
		}
		
		// Album title, property title, and artist name
		
		var predicateSet:Set<MPMediaPropertyPredicate> = Set()
		if clip.title?.characters.count > 0 {
			let predicate1 = MPMediaPropertyPredicate(value: clip.title, forProperty: MPMediaItemPropertyTitle)
			predicateSet.insert(predicate1)
		}
		if clip.albumTitle?.characters.count > 0 {
			let predicate2 = MPMediaPropertyPredicate(value: clip.albumTitle, forProperty: MPMediaItemPropertyAlbumTitle)
			predicateSet.insert(predicate2)
		}
		if clip.albumTitle?.characters.count > 0 {
			let predicate3 = MPMediaPropertyPredicate(value: clip.artist, forProperty: MPMediaItemPropertyArtist)
			predicateSet.insert(predicate3)
		}
		let propertyQuery = MPMediaQuery(filterPredicates: predicateSet)
		obj = propertyQuery.items
		if obj?.count > 0 {
			return obj!.last
		}
		
		return nil
	}
	
	class func fetchWithPredicate(predicate: NSPredicate?, completion handler: (clips: [MNSoundClip], error:NSError?) -> Void) {
		let fetchQueue = APP_DELEGATE().fetchQueue()
		let fetchMOC = APP_DELEGATE().fetchManagedObjectContext
		
		dispatch_async(fetchQueue) {
			let request = NSFetchRequest(entityName:"MNSoundClip")
			request.resultType = .ManagedObjectIDResultType
			if predicate != nil {
				request.predicate = predicate
			}
			let descriptor = NSSortDescriptor(key: "title", ascending: true)
			request.sortDescriptors = [descriptor]
			var results: [NSManagedObjectID]
			var error:NSError? = nil
			do {
				results = try fetchMOC.executeFetchRequest(request) as! [NSManagedObjectID]
			}catch let error1 as NSError  {
				results = []
				error = error1
			}
			
			// Convert object id to object
			dispatch_async(dispatch_get_main_queue()) {
				var array: [MNSoundClip] = []
				let context = APP_DELEGATE().managedObjectContext
				for objectID: NSManagedObjectID in results {
					do {
						let clip: MNSoundClip = try context.objectWithID(objectID) as! MNSoundClip
						array.append(clip)
					}catch {
					}
				}
				// DONE
				handler(clips: array, error:error)
			}
		}
	}
	


}
