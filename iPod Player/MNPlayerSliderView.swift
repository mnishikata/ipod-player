//
//  MNPlayerSliderView.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 19/02/16.
//
//

import Foundation
import UIKit
import MediaPlayer

@objc public protocol MNSpanSliderDelegate : NSObjectProtocol {

	optional func spanSliderValueChanged(newCurrentLocation: CGFloat)
	optional func spanSliderSelectionUpdated(sender: MNSpanSlider)
	optional func spanSliderSelectionDraggingElement(sender: MNSpanSlider, visible:Bool, toLocation:CGFloat)

}

class MNPlayerSliderView : UIView {
	
	lazy var slider: MNSpanSlider = {
		[unowned self] in
		let slider = MNSpanSlider(frame: self.bounds)
		slider.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
		self.addSubview(slider)
		return slider
		}()
	
	lazy var remaining: UILabel = {
		[unowned self] in

		// Remaining Label
		let label = UILabel(frame: CGRectMake(self.bounds.size.width - 50, 10, 42, 21))
		label.textColor = UIColor.darkGrayColor()
		label.font = UIFont.systemFontOfSize(13)
		label.autoresizingMask = [.FlexibleLeftMargin, .FlexibleBottomMargin]
		label.backgroundColor = UIColor.clearColor()
		self.addSubview(label)

		return label
		
		}()
	
	lazy var elapsed:UILabel = {
		[unowned self] in

		let label = UILabel(frame: CGRectMake(10, self.bounds.size.height - 20, 42, 21))
		label.textColor = UIColor.darkGrayColor()
		label.font = UIFont.systemFontOfSize(13)
		label.autoresizingMask = [.FlexibleRightMargin, .FlexibleTopMargin]
		label.backgroundColor = UIColor.clearColor()
		self.addSubview(label)

		return label
		
	}()
	
	var spanSliderDelegate:MNSpanSliderDelegate? {
		set {
			slider.delegate = newValue
		}
		get {
			return slider.delegate
		}
	}
	
	var backgroundImage:UIImage? {
		set {
			slider.backgroundImage = newValue
			updateTimeDisplay()
		}
		get {
			return slider.backgroundImage
		}
	}
	
	var maximumValue:CGFloat {
		set {
			slider.maximumValue = newValue
		}
		get {
			return slider.maximumValue
		}
	}
	
	var selectedRange:CMTimeRange {
		set {
			slider.startLocation = CGFloat( CMTimeGetSeconds(newValue.start) )
			slider.endLocation = CGFloat( CMTimeGetSeconds(newValue.start) + CMTimeGetSeconds(newValue.duration) )
		}
		get {
			let start = slider.startLocation
			let end = slider.endLocation
			
			var timeScale:CMTimeScale? = player?.currentItem?.duration.timescale

			if timeScale == nil {
				timeScale = Int32(1.0)
			}
			return CMTimeRangeMake(CMTimeMakeWithSeconds(Double(start), timeScale!), CMTimeMakeWithSeconds( Double(end-start), timeScale!))
		}
	}
	
	var draggingCurrentLocation:Bool {
		return slider.trackingCurrentLocation
	}
	
	var currentLocation:CGFloat {
		set {
			slider.currentLocation = newValue
		}
		get {
			return slider.currentLocation
		}
	}
	
	// Optional property
	weak var player: AVPlayer? = nil
	
	//MARK:-
	
	override init(frame:CGRect) {
		super.init(frame:frame)
		backgroundColor = UIColor.clearColor()
		autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder : aDecoder)
		backgroundColor = UIColor.clearColor()
	}

	deinit {
		slider.removeFromSuperview()
		remaining.removeFromSuperview()
		elapsed.removeFromSuperview()
	}
	
	func updateTimeDisplay() {
		
		guard let _ = player?.currentItem else {
			elapsed.text = "--:--"
			remaining.text = "--:--"
			return
		}
		
		let elapsedTime = CMTimeGetSeconds(player!.currentTime())
		let remainingTime = CMTimeGetSeconds(player!.currentItem!.duration) - elapsedTime
		let elapsedTimeMin = floor(elapsedTime / 60)
		let elapsedTimeSec = elapsedTime - elapsedTimeMin * 60
		elapsed.text = String(format: "%.0f:%02.0f", elapsedTimeMin, elapsedTimeSec)
		
		let remainingTimeMin = floor(remainingTime / 60)
		let remainingTimeSec = remainingTime - remainingTimeMin * 60
		remaining.text = String(format: "-%.0f:%02.0f", remainingTimeMin, remainingTimeSec)
		
		slider.currentLocation = CGFloat( elapsedTime )

	}
}