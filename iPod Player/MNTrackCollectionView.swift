//
//  MNTrackCollectionView.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 22/02/16.
//
//

import Foundation
import UIKit
import MediaPlayer

class MNTrackCollectionView : UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	let CellIdentifier = "Cell"

	var mediaItems:[MPMediaItem] = [] {
		didSet {
			reloadData()
		}
	}
	
	var selectedMedia:MPMediaItem? = nil
	var trackDelegate:NSObjectProtocol? = nil
	
	override init(frame:CGRect, collectionViewLayout layout: UICollectionViewLayout) {
		super.init(frame:frame, collectionViewLayout: layout)
		self.registerClass(PlayerTrackCell.self, forCellWithReuseIdentifier: CellIdentifier)
		self.dataSource = self
		self.delegate = self
		self.contentInset = UIEdgeInsetsMake(0, 20,0,20);
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder:aDecoder)
		self.registerClass(PlayerTrackCell.self, forCellWithReuseIdentifier: CellIdentifier)
		self.dataSource = self
		self.delegate = self
		self.contentInset = UIEdgeInsetsMake(0, 20,0,20);

	}
	
	// MARK: - UICollectionViewDataSource protocol
	
	// tell the collection view how many cells to make
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return mediaItems.count
	}
	
	// make a cell for each cell index path
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		// get a reference to our storyboard cell
		let cell = dequeueReusableCellWithReuseIdentifier(CellIdentifier, forIndexPath: indexPath) as! PlayerTrackCell
		
		// Use the outlet in our custom class to get a reference to the UILabel in the cell
		cell.trackNumber = indexPath.row
		let item = mediaItems[indexPath.row]
		if let num = item.valueForProperty(MPMediaItemPropertyAlbumTrackNumber) {
			cell.trackNumber = (num as? NSNumber)?.integerValue
			
			if cell.trackNumber == 0 {
				cell.trackNumber = indexPath.row + 1

			}
		}else {
			cell.trackNumber = indexPath.row + 1
		}
		
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

		if ((trackDelegate?.respondsToSelector("trackView:didSelectMedia:")) != nil) {
			let item = mediaItems[indexPath.row]
			trackDelegate?.performSelector("trackView:didSelectMedia:", withObject: self, withObject: item)
		}
	}
	
	override func reloadData() {
		super.reloadData()
		if selectedMedia != nil {
			selectMedia(selectedMedia!, animated: false)
		}
	}
	
	func selectMedia(item: MPMediaItem, animated: Bool) {
		guard let index = mediaItems.indexOf(item) else { return }
		
		self.selectedMedia = item
		let indexPath = NSIndexPath(forRow: index, inSection: 0)
		
		let selectedItems = indexPathsForSelectedItems()
		
		selectedItems?.forEach{ indexPath in
			deselectItemAtIndexPath(indexPath, animated: false)
		}
		
		selectItemAtIndexPath(indexPath, animated: animated, scrollPosition: .CenteredHorizontally)
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		return CGSizeMake(30,30)
	}
}

class PlayerTrackCell : UICollectionViewCell {
	
	var trackNumber:Int? = nil {
		didSet {
			if trackNumber == nil {
				trackLabel.text = "•"
			}else {
				if trackNumber! == 0 {
					trackLabel.text = "•"

				}else if trackNumber! < 0 {
					trackLabel.text = "•"

				}else {
					trackLabel.text = String(trackNumber!)
				}
			}
		}
	}
	
	lazy var trackLabel:UILabel = {
		[unowned self] in
		let size = self.bounds.size
		let trackLabel = UILabel(frame: CGRectMake(0, 0, size.width, size.height))
		trackLabel.textAlignment = .Center
		trackLabel.font = UIFont.systemFontOfSize(14)
		trackLabel.backgroundColor = UIColor.clearColor()
		trackLabel.opaque = false
		self.addSubview(trackLabel)
		return trackLabel
	}()
	weak var imageView:UIImageView? = nil
	override var backgroundColor:UIColor? {
		get {
			return UIColor.clearColor()
		}
		set {
		}
	}
	override var selected:Bool {
		didSet {
			if selected && imageView == nil {
				let view = UIImageView(frame: CGRectMake(0, CGRectGetMaxY(bounds) - 3, bounds.width, 3))
					imageView = view
					view.backgroundColor = self.tintColor
					self.addSubview(view)
				}
				
			trackLabel.textColor = UIColor.blackColor()
			imageView?.hidden = !selected
		}
	}
	
	deinit {
		trackLabel.removeFromSuperview()
	}
	
	override func prepareForReuse() {
		super.prepareForReuse()
		userInteractionEnabled = true
		trackNumber = nil
		trackLabel.text = ""
		trackLabel.textColor = UIColor.blackColor()
		backgroundColor = UIColor.whiteColor()
		imageView?.removeFromSuperview()
		imageView = nil
	}
}
