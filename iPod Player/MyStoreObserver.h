//
//  MyStoreObserver.h
//  CCal-X
//
//  Created by Nishikata Masatoshi on 28/07/12.
//  Copyright (c) 2012 Catalystwo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface MyStoreObserver : NSObject <SKPaymentTransactionObserver>
{
	
	
}
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;
- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;
-(NSDictionary*)verifyWithTransaction:(SKPaymentTransaction *)transaction;
-(void)sendNotification;
-(BOOL)purchased:(NSString*)productID;
-(void)clear:(NSString*)productID;
-(BOOL)purchased:(NSString*)productID;

@property (nonatomic, readwrite) BOOL restoring;

@end

