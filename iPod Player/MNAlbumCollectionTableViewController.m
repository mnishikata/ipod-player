//
//  MNCollectionTableViewController.m
//  iPod Player
//
//  Created by Nishikata Masatoshi on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//#import "MNPlayerViewController.h"
#import "NowPlayingButton.h"
#import "FFAboutViewController.h"
#import "MNBaseNavigationController.h"
#import "AlbumTableViewCell.h"
//#import "MNSoundClip.h"
#import "AB_Player-Swift.h"


@implementation MNAlbumCollectionTableViewController

@synthesize sections = sections_;
@synthesize collections = collections_;
@synthesize soundClips = soundClips_;
@synthesize hideSoundClips = hideSoundClips_;
@synthesize showArtistName = showArtistName_;

+ (UIViewController *) viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
	MNAlbumCollectionTableViewController* controller = nil;
	
	MPMediaGrouping groupingType = MPMediaGroupingTitle;
	NSSet* predicates = nil;
	
	
	groupingType = [coder decodeIntegerForKey:@"groupingType_"];
	predicates = [coder decodeObjectForKey:@"filterPredicates_"];

	
	controller = [[MNAlbumCollectionTableViewController alloc] initWithGroupingType:groupingType
																						filterPredicates:predicates];
	
	return controller;
}


- (id)initWithGroupingType:(MPMediaGrouping)groupingType filterPredicates:(NSSet*)predicates
{
	self = [super initWithStyle:UITableViewStylePlain];
	if (self) {
		
		filterPredicates_ = predicates;
		groupingType_ = groupingType;
		shouldReloadData_ = YES;
		
		self.restorationIdentifier = @"MNAlbumCollectionTableViewControllerID";
		self.restorationClass = [self class];
	}
	return self;
}

-(void)dealloc
{
	collections_ = nil;
	sections_ = nil;
	filterPredicates_ = nil;
	soundClips_ = nil;
	
}

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
	[super encodeRestorableStateWithCoder:coder];

	[coder encodeInteger:groupingType_ forKey:@"groupingType_"];
	[coder encodeObject:filterPredicates_ forKey:@"filterPredicates_"];
	[coder encodeObject:sections_ forKey:@"sections_"];
	[coder encodeObject:collections_ forKey:@"collections_"];	
	[coder encodeBool:hideSoundClips_ forKey:@"hideSoundClips_"];
	[coder encodeBool:showArtistName_ forKey:@"showArtistName_"];

}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
	[super decodeRestorableStateWithCoder:coder];

	shouldReloadData_ = YES;
	
	groupingType_ = [coder decodeIntegerForKey:@"groupingType_"];
	filterPredicates_ = [coder decodeObjectForKey:@"filterPredicates_"];
	sections_ = [coder decodeObjectForKey:@"sections_"];
	collections_ = [coder decodeObjectForKey:@"collections_"];
	hideSoundClips_ = [coder decodeBoolForKey:@"hideSoundClips_"];
	showArtistName_ = [coder decodeBoolForKey:@"showArtistName_"];

}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	

	if( shouldReloadData_ )
	{
		[self reloadData];
	}
	else
	{
		[self fetchSoundClips];
	}
	
	
	if( MPMediaGroupingAlbum == groupingType_ )
		self.navigationItem.title = NSLocalizedString(@"TabAlbums", @"");
	
	else if( MPMediaGroupingArtist == groupingType_ )
		self.navigationItem.title = NSLocalizedString(@"TabArtists", @"");
	
	else if( MPMediaGroupingGenre == groupingType_ )
		self.navigationItem.title = NSLocalizedString(@"TabGenre", @"");
	
	else if( MPMediaGroupingPodcastTitle == groupingType_ )
		self.navigationItem.title = NSLocalizedString(@"TabPodcasts", @"");
	
	else if( MPMediaGroupingPlaylist == groupingType_ )
		self.navigationItem.title = NSLocalizedString(@"TabPlaylists", @"");
	
}


-(void)reloadData
{
	shouldReloadData_ = NO;
	self.collections = nil;
	
	
	// Read section data
	dispatch_sync(enumerationQueue, ^(void) {
		// Grab videos from the iPod Library
		
		MPMediaQuery *query = nil;
		
		
		if( MPMediaGroupingAlbum == groupingType_ )
			query = [MPMediaQuery albumsQuery];
		
		else if( MPMediaGroupingArtist == groupingType_ )
			query = [MPMediaQuery artistsQuery];
		
		else if( MPMediaGroupingGenre == groupingType_ )
			query = [MPMediaQuery genresQuery];
		
		else if( MPMediaGroupingPodcastTitle == groupingType_ )
			query = [MPMediaQuery podcastsQuery];
		
		else if( MPMediaGroupingPlaylist == groupingType_ )
			query = [MPMediaQuery playlistsQuery];
		
		
		
		query.groupingType = groupingType_;
		
		if( filterPredicates_ )
		{
			query.filterPredicates = filterPredicates_;
		}
		
		NSArray* sections = [query collectionSections];
		
		if( sections == nil ) sections = [NSArray array];
		
		self.sections = sections;
		
	});
	
	
	// Read album collections
	dispatch_async(enumerationQueue, ^(void) {
		
		
		MPMediaQuery *query = nil;
		
		
		if( MPMediaGroupingAlbum == groupingType_ )
			query = [MPMediaQuery albumsQuery];
		
		else if( MPMediaGroupingArtist == groupingType_ )
			query = [MPMediaQuery artistsQuery];
		
		else if( MPMediaGroupingGenre == groupingType_ )
			query = [MPMediaQuery genresQuery];
		
		else if( MPMediaGroupingPodcastTitle == groupingType_ )
			query = [MPMediaQuery podcastsQuery];
		
		else if( MPMediaGroupingPlaylist == groupingType_ )
			query = [MPMediaQuery playlistsQuery];
		
		
		query.groupingType = groupingType_;
		
		if( filterPredicates_ )
		{
			query.filterPredicates = filterPredicates_;
		}
		
		NSMutableArray *items = [NSMutableArray arrayWithCapacity:0];
		
		
		NSArray* collections = [query collections];
		
		[items addObjectsFromArray: collections];
		
		dispatch_async(dispatch_get_main_queue(), ^(void) {
			
			self.collections = items;
			[self.tableView reloadData];
			[self fetchSoundClips];
		});
	});
	
	
}

-(void)fetchSoundClips
{
	if( hideSoundClips_ ) return;
	
	
	// Sound Cliip	// Instantiate main context first!!!
	[APP_DELEGATE managedObjectContext];
	
	
	soundClips_ = nil;
	
	
	NSPredicate *predicate = nil;
	
	
	if( MPMediaGroupingAlbum == groupingType_ )
	{
		predicate = [MNSoundClip fetchSoundClipsInAlbum: self.collections];
		
		//			for( MPMediaItemCollection * entity in self.collections )
		//			{
		//				num = [entity.representativeItem valueForProperty:MPMediaItemPropertyAlbumPersistentID];
		//
		//				ullNum = [num unsignedLongLongValue];
		//
		//				[persistentIDs addObject: [NSString stringWithFormat:@"%llu",ullNum]];
		//			}
		//
		//			predicate = [NSPredicate predicateWithFormat:@"albumPersistentID IN %@", persistentIDs];
	}
	
	
	if( MPMediaGroupingGenre == groupingType_ )
	{
		predicate = [MNSoundClip fetchSoundClipsInGenre: self.collections];
		
		//			for( MPMediaItemCollection * entity in self.collections )
		//			{
		//				num = [entity.representativeItem valueForProperty:MPMediaItemPropertyGenrePersistentID];
		//
		//				ullNum = [num unsignedLongLongValue];
		//
		//				[persistentIDs addObject: [NSString stringWithFormat:@"%llu",ullNum]];
		//			}
		//
		//			predicate = [NSPredicate predicateWithFormat:@"genrePersistentID IN %@", persistentIDs];
	}
	
	
	if( MPMediaGroupingArtist == groupingType_ )
	{
		predicate = [MNSoundClip fetchSoundClipsInArtist: self.collections];
		
		//			for( MPMediaItemCollection * entity in self.collections )
		//			{
		//				num = [entity.representativeItem valueForProperty:MPMediaItemPropertyArtistPersistentID];
		//
		//				ullNum = [num unsignedLongLongValue];
		//
		//				[persistentIDs addObject: [NSString stringWithFormat:@"%llu",ullNum]];
		//			}
		//
		//			predicate = [NSPredicate predicateWithFormat:@"artistPersistentID IN %@", persistentIDs];
	}
	
	
	
	
	if( MPMediaGroupingPodcastTitle == groupingType_ )
	{
		predicate = [MNSoundClip fetchSoundClipsInPodcast: self.collections];
		
		//			for( MPMediaItemCollection * entity in self.collections )
		//			{
		//				num = [entity.representativeItem valueForProperty:MPMediaItemPropertyPodcastPersistentID];
		//
		//				ullNum = [num unsignedLongLongValue];
		//
		//				[persistentIDs addObject: [NSString stringWithFormat:@"%llu",ullNum]];
		//			}
		//
		//			predicate = [NSPredicate predicateWithFormat:@"podcastPersistentID IN %@", persistentIDs];
	}
	
	
	if( MPMediaGroupingPlaylist == groupingType_ )
	{
		predicate = [MNSoundClip fetchSoundClipsForSongs: self.collections];
		
		//			for( MPMediaItemCollection * entity in self.collections )
		//			{
		//				NSArray *songs = [entity items];
		//				for (MPMediaItem *song in songs) {
		//
		//					num = [song valueForProperty:MPMediaItemPropertyPersistentID];
		//
		//					ullNum = [num unsignedLongLongValue];
		//
		//					[persistentIDs addObject: [NSString stringWithFormat:@"%llu",ullNum]];
		//				}
		//
		//			}
		//
		//			predicate = [NSPredicate predicateWithFormat:@"persistentID IN %@", persistentIDs];
	}
	

	
	[MNSoundClip fetchWithPredicate:predicate completion:^(NSArray<MNSoundClip *> * _Nonnull clips, NSError * error) {
		
			self.soundClips = [[NSMutableArray alloc] initWithArray: clips];
			[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
		}];
	
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return self.sections.count + 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if( hideSoundClips_ &&  section == 0  ) return nil;
	
	if( section == 0 )
		return NSLocalizedString(@"Sound Clips", @"");
	
	if( groupingType_ == MPMediaGroupingPlaylist) return nil;
	
	
	MPMediaQuerySection* mediaSection = [self.sections objectAtIndex:section - 1];
	
	return mediaSection.title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	
	if( section == 0 && hideSoundClips_ ) return 0;
	if( section == 0 ) return 1;
	
	MPMediaQuerySection* mediaSection = [self.sections objectAtIndex:section - 1];
	
	return mediaSection.range.length;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 0 )
	{
		UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		cell.selectionStyle = UITableViewCellSelectionStyleBlue;

		if( !soundClips_ )
		{
			cell.textLabel.text = NSLocalizedString(@"Loading", @"");
			cell.textLabel.textColor = [UIColor lightGrayColor];
			
		}
		else
		{
			if( soundClips_.count == 0 )
			{
				cell.textLabel.text = NSLocalizedString(@"0 clip", @"");
				cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell.textLabel.textColor = [UIColor lightGrayColor];

			}
			
			else if( soundClips_.count == 1 )
			{
				cell.textLabel.text = NSLocalizedString(@"1 clip", @"");
				
			}
			else{
				cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d clips", @""), soundClips_.count];
				
			}
		}
		
		cell.imageView.image = [UIImage imageNamed:@"SoundClipIconFlat"];

		return cell;
	}
	
	AlbumTableViewCell *cell = nil;
	
	
	if( self.showArtistName )
	{
		cell = [tableView dequeueReusableCellWithIdentifier:@"CellWithArtist"];
		if( !cell )
			cell = [[AlbumTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"CellWithArtist"];

	}else
	{
		cell = [tableView dequeueReusableCellWithIdentifier:@"CellWithoutArtist"];
		if( !cell )
			cell = [[AlbumTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellWithoutArtist"];

	}
	
	
	if( !self.collections ){
		
		cell.textLabel.text = NSLocalizedString(@"Loading", @"");
		cell.textLabel.textColor = [UIColor lightGrayColor];
		
		return cell;
	}
	
	
	MPMediaQuerySection *mediaSection = [self.sections objectAtIndex:indexPath.section - 1];
	
	NSArray* subcollection = [self.collections subarrayWithRange: mediaSection.range];
	
	
	MPMediaItemCollection* item = [subcollection objectAtIndex: indexPath.row];
	__block NSString* title = nil;
	__block NSString* artistName = nil;

	dispatch_sync(enumerationQueue, ^{
		
		if( MPMediaGroupingAlbum == groupingType_ )
			title = [item.representativeItem valueForProperty:MPMediaItemPropertyAlbumTitle];
		
		if( MPMediaGroupingArtist == groupingType_ )
			title = [item.representativeItem valueForProperty:MPMediaItemPropertyArtist];
		
		if( MPMediaGroupingGenre == groupingType_ )
			title = [item.representativeItem valueForProperty:MPMediaItemPropertyGenre];
		
		if( MPMediaGroupingPodcastTitle == groupingType_ )
			title = [item.representativeItem valueForProperty:MPMediaItemPropertyPodcastTitle];
		
		if( MPMediaGroupingPlaylist == groupingType_ )
			title = [item valueForProperty:MPMediaPlaylistPropertyName];
		
		artistName = [item.representativeItem valueForProperty:MPMediaItemPropertyArtist];
		
	});
	
	
	
	cell.textLabel.text = title;
	
	if( self.showArtistName )
	{
		cell.detailTextLabel.text = artistName;
	}
	
	
	if( [item isKindOfClass:[MPMediaItem class]] )
	{
		cell.artworkView.image = nil;
		return cell;
	}
	
	
	dispatch_async(enumerationQueue, ^{
		
		MPMediaItemArtwork* artwork = [item.representativeItem valueForProperty:MPMediaItemPropertyArtwork];
		
		CGFloat imageSize = 44;
		
		if( self.showArtistName ) imageSize = 55;
		
		UIImage* image = [artwork imageWithSize:CGSizeMake(imageSize, imageSize)];
		
		if( image )
		{
			
			dispatch_async(dispatch_get_main_queue(), ^{
				
				cell.artworkView.image = image;
				
			});
		}
	});
	
	return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Return NO if you do not want the specified item to be editable.
	return NO;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	if( self.sections.count < 2 ) return nil;
	
	NSMutableArray* array = [[NSMutableArray alloc] init];
	
	for( NSUInteger hoge = 0; hoge < self.sections.count; hoge++ )
	{
	MPMediaQuerySection* mediaSection = [self.sections objectAtIndex:hoge];
		[array addObject: mediaSection.title];
		
	}
	
	return array;

}


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

	
	
	[tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
	
	if( indexPath.section == 0   )
	{
		if( self.soundClips.count > 0 )
		{
			MNClipCollectionTableViewController * controller = [[MNClipCollectionTableViewController alloc] init];
			
			controller.soundClips = self.soundClips;
			controller.organizeByAlbumTitle = YES;
			
			[self.navigationController pushViewController:controller animated:YES];
		}
		return;
	}
	
	
	MPMediaQuerySection *mediaSection = [self.sections objectAtIndex:indexPath.section -1];
	
	NSArray* subcollection = [self.collections subarrayWithRange: mediaSection.range];
	
	MPMediaItemCollection* item = [subcollection objectAtIndex: indexPath.row];
	
	
	
	
	
	if( MPMediaGroupingAlbum == groupingType_ )
	{
		id albumID = [item valueForKey:MPMediaItemPropertyPersistentID];
		MPMediaPropertyPredicate *predicate =
		[MPMediaPropertyPredicate predicateWithValue:albumID
													forProperty:MPMediaItemPropertyAlbumPersistentID];
		
		
		
		
		NSMutableSet* predicates = [NSMutableSet setWithObject: predicate];
		[predicates addObjectsFromArray: filterPredicates_.allObjects];
		
		MNSongCollectionTableViewController * songsController =
		[[MNSongCollectionTableViewController alloc] initWithFilterPredicates:predicates];
		
		
		songsController.title = [item.representativeItem valueForKey:MPMediaItemPropertyAlbumTitle];
		
		[self.navigationController pushViewController:songsController animated:YES];
		
	}
	
	else if( MPMediaGroupingArtist == groupingType_ )
	{
		id artistID = [item valueForKey:MPMediaItemPropertyPersistentID];
		MPMediaPropertyPredicate *predicate =
		[MPMediaPropertyPredicate predicateWithValue:artistID
													forProperty:MPMediaItemPropertyArtistPersistentID];
		
		
		
		NSMutableSet* predicates = [NSMutableSet setWithObject: predicate];
		[predicates addObjectsFromArray: filterPredicates_.allObjects];
		
		MNAlbumCollectionTableViewController * songsController =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType:MPMediaGroupingAlbum filterPredicates:predicates];
		
		songsController.showArtistName = YES;
		songsController.title = [item.representativeItem valueForKey:MPMediaItemPropertyArtist];
		
		[self.navigationController pushViewController:songsController animated:YES];
	}
	
	
	else if( MPMediaGroupingGenre == groupingType_ )
	{
		id genreID = [item valueForKey:MPMediaItemPropertyPersistentID];
		MPMediaPropertyPredicate *predicate =
		[MPMediaPropertyPredicate predicateWithValue:genreID
													forProperty:MPMediaItemPropertyGenrePersistentID];
		
		
		NSMutableSet* predicates = [NSMutableSet setWithObject: predicate];
		[predicates addObjectsFromArray: filterPredicates_.allObjects];
		
		MNAlbumCollectionTableViewController * songsController =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType:MPMediaGroupingArtist filterPredicates:predicates];
		
		songsController.showArtistName = NO;
		songsController.title = [item.representativeItem valueForKey:MPMediaItemPropertyGenre];
		
		[self.navigationController pushViewController:songsController animated:YES];
	}
	
	else if( MPMediaGroupingPodcastTitle == groupingType_ )
	{
		id podcastID = [item valueForKey:MPMediaItemPropertyPersistentID];
		MPMediaPropertyPredicate *predicate =
		[MPMediaPropertyPredicate predicateWithValue:podcastID
													forProperty:MPMediaItemPropertyPodcastPersistentID];
		
		
		
		NSMutableSet* predicates = [NSMutableSet setWithObject: predicate];
		[predicates addObjectsFromArray: filterPredicates_.allObjects];
		
		MNSongCollectionTableViewController * songsController =
		[[MNSongCollectionTableViewController alloc] initWithFilterPredicates:predicates];
		
		
		songsController.title = [item.representativeItem valueForKey:MPMediaItemPropertyPodcastTitle];
		
		[self.navigationController pushViewController:songsController animated:YES];
		
	}
	
	else if( MPMediaGroupingPlaylist == groupingType_ )
	{
		id podcastID = [item valueForKey:MPMediaItemPropertyPersistentID];
		MPMediaPropertyPredicate *predicate =
		[MPMediaPropertyPredicate predicateWithValue:podcastID
													forProperty:MPMediaPlaylistPropertyPersistentID];
		
		
		
		NSMutableSet* predicates = [NSMutableSet setWithObject: predicate];
		[predicates addObjectsFromArray: filterPredicates_.allObjects];
		
		MNSongCollectionTableViewController * songsController =
		[[MNSongCollectionTableViewController alloc] initWithFilterPredicates:predicates];
		
		
		songsController.title = [item valueForKey:MPMediaPlaylistPropertyName];
		
		[self.navigationController pushViewController:songsController animated:YES];
		
	}
	
	
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( self.showArtistName && indexPath.section != 0 ) return 55;
	return 44;
	 
}

@end


