//
//  CCalAboutViewController.m
//  CCal-X
//
//  Created by Masatoshi Nishikata on 31/07/12.
//  Copyright (c) 2012 Catalystwo. All rights reserved.
//

#import "FFAboutViewController.h"
#import "HelpViewController.h"
//#import "QRCodeViewController.h"
#import "C2Manager.h"
#import <QuartzCore/QuartzCore.h>
#import "SPStoreViewController.h"
#import "MyStoreObserver.h"


#define kMyFeatureIdentifier IN_APP_PURCHAE_ID


@interface FFAboutViewController ()

@end

@implementation FFAboutViewController

- (id)init
{
	self = [super initWithStyle:UITableViewStyleGrouped];
	if (self) {
		// Custom initialization
	}
	return self;
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	purchased_ = [[APP_DELEGATE storeObserver] purchased:kMyFeatureIdentifier];

	
	[self.tableView reloadData];
}


- (void)viewDidLoad
{
	[super viewDidLoad];
	
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
	self.navigationItem.leftBarButtonItem = doneButton;
	
	self.navigationItem.title = NSLocalizedString(@"About This App",@"");
	
}

-(void)done
{
	[self.navigationController dismissViewControllerAnimated:YES completion:nil];
	
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#if __IPHONE_6_0
- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}
#endif

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.

	if( section == 0 )
	{
		return 3;


	}
	
	if( section == 1 )
	{
		return 1;
	}
	
	return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	if( section == 0 )
	{
		C2Manager *manager = [[[C2Manager alloc] init] autorelease];
		
		NSString* caption = [NSString stringWithFormat:@"%@ %@", [manager displayName], [manager versionString]];

		return caption;
	}
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 0 && indexPath.row == 0 )
	{
//		C2Manager *manager = [[[C2Manager alloc] init] autorelease];
//		
//		UIImage* image = [manager appIcon];

		return 44;

	}
	
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
												reuseIdentifier:CellIdentifier] autorelease];
	}
	

	
	
	
	if( indexPath.section == 0 )
	{

		
		if( indexPath.row == 0 )
		{
			cell.textLabel.text = NSLocalizedString(@"App Store Page", @"");
			cell.accessoryType = UITableViewCellAccessoryNone;
			
			
			C2Manager *manager = [[[C2Manager alloc] init] autorelease];
			
			
			cell.imageView.image = [manager appIcon];
			
			BOOL iPad = ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad );

			cell.imageView.layer.cornerRadius = iPad?12:10;
			cell.imageView.layer.masksToBounds = YES;

			
		}
		
//		if( indexPath.row == 1 )
//		{
//			cell.textLabel.text = NSLocalizedString(@"Show QR Code Link", @"");
//			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//			
//		}
		
		if( indexPath.row == 1 )
		{
			cell.textLabel.text = NSLocalizedString(@"Catalystwo Apps", @"");
			cell.accessoryType = UITableViewCellAccessoryNone;
			
		}
		
		
			
		if( indexPath.row == 2 )
		{
			if( purchased_ )
			{
			cell.textLabel.text = NSLocalizedString(@"Purchase Info", @"");
			}else{
				cell.textLabel.text = NSLocalizedString(@"CCalStoreViewControllerPurchase", @"");

			}
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			
		}


		
	}
	
	
	if( indexPath.section == 1 && indexPath.row == 0 )
	{
		cell.textLabel.text = NSLocalizedString(@"Credits", @"");
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

	}
	
	
	return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Navigation logic may go here. Create and push another view controller.

	
	if( indexPath.section == 0 )
	{
		
		
		if( indexPath.row == 0 )
		{
			[self store:nil];
		}
		
//		if( indexPath.row == 1 )
//		{
//			[self showQR:nil];
//		}
		
		if( indexPath.row == 1 )
		{
			[self appsFromUs];
		}
		
		
		if( indexPath.row == 2 )
		{
			[self purchaseInfo];
		}
		
		
		[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
		
	}
	
	
	if( indexPath.section == 1 )
	{
		if( indexPath.row == 0 )
		{
			[self credit:nil];
		}
	}
	
	
}

-(void)purchaseInfo
{

	SPStoreViewController* controller = [[[SPStoreViewController alloc] init] autorelease];
	[self.navigationController pushViewController:controller animated:YES];

}


- (void)store:(id)sender {
	
	C2Manager *manager = [[[C2Manager alloc] init] autorelease];

	[[UIApplication sharedApplication] openURL:[manager storeLinkURL]];

}


- (void)appsFromUs 
{
	
	C2Manager *manager = [[[C2Manager alloc] init] autorelease];
	
	[[UIApplication sharedApplication] openURL:[manager appsFromUs]];
	
}

- (void)credit:(id)sender {
	
	HelpViewController *legal = [[[HelpViewController alloc] init] autorelease];
	legal.helpFile = @"Credit.docx";
	legal.title = @"Credits";
	[self.navigationController pushViewController:legal animated:YES];
	
}

//- (void)showQR:(id)sender {
//	
//	
//	C2Manager *manager = [[[C2Manager alloc] init] autorelease];
//	
//	QRCodeViewController *controller = [[[QRCodeViewController alloc] init] autorelease];
//	
//	NSString* caption = [NSString stringWithFormat:@"%@ %@", [manager displayName], [manager versionString]];
//																				 
//	controller.caption = caption;
//	controller.image = [manager qrCode];
//
//	[self.navigationController pushViewController:controller animated:YES];
//	
//}



@end
