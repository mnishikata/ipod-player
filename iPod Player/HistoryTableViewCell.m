//
//  MNFontLabelCell.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 8/01/11.
//  Copyright 2011 Catalystwo Ltd. All rights reserved.
//

#import "HistoryTableViewCell.h"
#import "MNCTFontUtils.h"
#import "DrawUtils.h"
#import <QuartzCore/QuartzCore.h>
#import "MNMenuView.h"
#import "NSString+MNPath.h"
#import "Tui.h"
#define IOS7_CELL_HIGHLIGHT_COLOR RGBA(41, 132, 254, 0.5)


#define SUB_TEXT_FONT [UIFont systemFontOfSize:14]


#define STANDARD_RIGHT_INDENTATION 10
#define DISCLOSURE_RIGHT_INDENTATION 30
#define DETAILED_DISCLOSURE_RIGHT_INDENTATION 40
#define DELETING_RIGHT_INDENTATION 60
#define REORDER_RIGHT_INDENTATION 40

#define EDITING_LEFT_INDENTATIO 35
#define  gIsRunningOnIPad (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define   ICON_WIDTH  (gIsRunningOnIPad?60:44)

@implementation HistoryTableViewCell

@synthesize image;
@synthesize subtitleAttributedString = subtitleAttributedString_;
@synthesize secondSubtitleAttributedString = secondSubtitleAttributedString_;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		
		rightIndentationWidth = STANDARD_RIGHT_INDENTATION;
		leftIndentationWidth = self.indentationLevel * self.indentationWidth;
		
		
		
		///
		
		iconFrame = CGRectMake(0, 0, ICON_WIDTH, ICON_WIDTH);
		iconView = [[UIImageView alloc] initWithFrame: iconFrame];
		iconView.contentMode = UIViewContentModeScaleAspectFit;
		[self addSubview:iconView];
		[self setImage : nil];
		
		////
		
		contentViewFrame = self.bounds;
		contentViewFrame.size.width -= rightIndentationWidth + leftIndentationWidth;
		contentViewFrame.origin.x = leftIndentationWidth;
		
		contentView = [[MNAttributedTextLabel alloc] initWithFrame:contentViewFrame];
		contentView.autoresizingMask =UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
		contentView.backgroundColor = [UIColor clearColor];
		contentView.opaque = NO;	
		contentView.userInteractionEnabled = NO;
		contentView.contentWidth = contentViewFrame.size.width;
		
		[self addSubview: contentView  ]; 
		contentView.hidden = YES;
		

		///
		
		
		contentDetailViewFrame = contentViewFrame;
		
		contentDetailView = [[MNAttributedTextLabel alloc] initWithFrame:self.bounds];
		contentView.autoresizingMask =UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
		contentDetailView.backgroundColor = [UIColor clearColor];
		contentDetailView.opaque = NO;
		contentDetailView.onRight = YES;
		contentDetailView.userInteractionEnabled = NO;
		contentDetailView.contentWidth = contentDetailViewFrame.size.width;

		[self addSubview: contentDetailView  ]; 
		contentDetailView.hidden = YES;
		
		
		///
		
		
		subtitleView = [[MNAttributedTextLabel alloc] initWithFrame:self.bounds];
		subtitleView.autoresizingMask =UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
		subtitleView.backgroundColor = [UIColor clearColor];
		subtitleView.opaque = NO;
		subtitleView.onRight = YES;
		subtitleView.userInteractionEnabled = NO;
		subtitleView.contentWidth = self.bounds.size.width;
		
		[self addSubview: subtitleView  ]; 
		subtitleView.hidden = YES;
		
		///
		
		
		secondSubtitleView = [[MNAttributedTextLabel alloc] initWithFrame:self.bounds];
		secondSubtitleView.autoresizingMask =UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
		secondSubtitleView.backgroundColor = [UIColor clearColor];
		secondSubtitleView.opaque = NO;
		secondSubtitleView.onRight = YES;
		secondSubtitleView.userInteractionEnabled = NO;
		secondSubtitleView.contentWidth = self.bounds.size.width;
		
		[self addSubview: secondSubtitleView  ]; 
		secondSubtitleView.hidden = YES;
		
		
		
		//// Selection 
		
		
		backgroundLayer = [CALayer layer];
		backgroundLayer.frame = self.bounds;
		[self.layer addSublayer:backgroundLayer];
		backgroundLayer.backgroundColor = IOS7_CELL_HIGHLIGHT_COLOR.CGColor;
		backgroundLayer.opaque = NO;
		backgroundLayer.opacity = 0;
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		
		
		////
		
		currentState = 0;
		[self willTransitionToStateMain];
		[self setNeedsLayout];

    }
    return self;
}

-(void)setImage:(UIImage *)anImage
{
	iconView.contentMode = UIViewContentModeScaleAspectFit;

	if( anImage )
	{  
		iconView.image = anImage;
		iconFrame = CGRectMake(2, 0, ICON_WIDTH,ICON_WIDTH);
		iconFrame = CGRectInset(iconFrame, 2, 4);

	}else
	{
		iconFrame = CGRectZero;
	}
	
	
	contentViewFrame = self.bounds;
	
	contentViewFrame.size.width -= rightIndentationWidth + leftIndentationWidth + iconFrame.size.width;
	contentViewFrame.origin.x = leftIndentationWidth + iconFrame.size.width;
	contentDetailViewFrame = self.bounds;
	
	
	[self willTransitionToStateMain];
	[self setNeedsLayout];

}

-(void)setFullFrameImage:(UIImage *)anImage
{
	iconView.contentMode = UIViewContentModeScaleAspectFill;
	iconView.clipsToBounds = YES;
	if( anImage )
	{
		iconView.image = anImage;
		iconFrame = CGRectMake(0, 0, ICON_WIDTH-2, ICON_WIDTH);
		
	}else
	{
		iconFrame = CGRectZero;
	}
	
	
	contentViewFrame = self.bounds;
	
	contentViewFrame.size.width -= rightIndentationWidth + leftIndentationWidth + iconFrame.size.width;
	contentViewFrame.origin.x = leftIndentationWidth + iconFrame.size.width;
	contentDetailViewFrame = self.bounds;
	
	
	[self willTransitionToStateMain];
	[self setNeedsLayout];
	
}


-(void)setFrame:(CGRect)frame
{
	[super setFrame:frame];
	
	contentViewFrame = self.bounds;

	contentViewFrame.size.width -=  leftIndentationWidth + iconFrame.size.width;
	contentViewFrame.origin.x = leftIndentationWidth + iconFrame.size.width;
	contentDetailViewFrame = self.bounds;

	
	
	[self willTransitionToStateMain];
	[self setNeedsLayout];

}


-(void)setUserInteractionEnabled:(BOOL)flag
{
	[super setUserInteractionEnabled:flag];
	contentView.alpha = flag? 1.0 : 0.4;
	contentDetailView.alpha = flag? 1.0 : 0.4;
	iconView.alpha = flag? 1.0 : 0.4;

}


- (void)prepareForReuse
{
	[super prepareForReuse];
	
	[self setUserInteractionEnabled: YES];

	currentState = 0;
	contentView.attributedString = nil;
	contentDetailView.attributedString = nil;
	subtitleView.attributedString = nil;
	secondSubtitleView.attributedString = nil;

	[self setDetailedAttributedStringInverted: NO];
	self.indentationLevel = 0;
	
	[self setImage : nil];
	


}


- (void)dealloc {

	[iconView removeFromSuperview];
	iconView = nil;
	
	[contentView removeFromSuperview];
	contentView = nil;
	
	[contentDetailView removeFromSuperview];
	contentDetailView = nil;

}



-(void)setAttributedString:(NSAttributedString*)attributedString
{
	if( attributedString ) contentView.hidden = NO;
	else contentView.hidden = YES;
	
	BOOL hasChange = [contentView.attributedString.string isEqualToString: attributedString.string];

	contentView.attributedString = attributedString;
	[self willTransitionToStateMain];
	
	if( hasChange )
	[self setNeedsLayout];

}

-(NSAttributedString*)attributedString
{
	return contentView.attributedString;
}

-(void)setDetailedAttributedString:(NSAttributedString*)detailedAttributedString
{
	if( detailedAttributedString ) contentDetailView.hidden = NO;
	else contentDetailView.hidden = YES;
	
	BOOL hasChange = [contentDetailView.attributedString.string isEqualToString: detailedAttributedString.string];
	
	contentDetailView.attributedString = detailedAttributedString;
	[self willTransitionToStateMain];
	
	if( hasChange )
		[self setNeedsLayout];
}

-(NSAttributedString*)detailedAttributedString
{
	return contentDetailView.attributedString;
}

-(void)setDetailedAttributedStringInverted:(BOOL)flag
{
	contentDetailView.inverted = flag;
	[self willTransitionToStateMain];
	[self setNeedsLayout];

}

-(void)setSubtitleAttributedString:(NSAttributedString*)attributedString
{
	if( attributedString ) subtitleView.hidden = NO;
	else subtitleView.hidden = YES;
	
	subtitleView.attributedString = attributedString;
	[self willTransitionToStateMain];
	[self setNeedsLayout];
	
}

-(NSAttributedString*)subtitleAttributedString 
{
	return subtitleView.attributedString;
}

-(void)setSecondSubtitleAttributedString:(NSAttributedString*)attributedString
{
	if( attributedString ) secondSubtitleView.hidden = NO;
	else secondSubtitleView.hidden = YES;
	
	secondSubtitleView.attributedString = attributedString;
	[self willTransitionToStateMain];
	[self setNeedsLayout];
	
}

-(NSAttributedString*)secondSubtitleAttributedString
{
	return secondSubtitleView.attributedString;
}


- (void)setHighlighted:(BOOL)selected animated:(BOOL)animated
{
	
	if( self.selectionStyle != UITableViewCellSelectionStyleBlue )
	{
		backgroundLayer.opacity = selected?1.0:0.0;
		[backgroundLayer removeAllAnimations];
	}
	
	contentView.highlighted = selected;
	contentDetailView.highlighted = selected;
	
	
	if( self.selectionStyle == UITableViewCellSelectionStyleBlue )
		[super setHighlighted:(BOOL)selected animated:(BOOL)animated ];	
	
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{

	if( self.selectionStyle != UITableViewCellSelectionStyleBlue )
	{
		backgroundLayer.opacity = selected?1.0:0.0;
		
		
		if( selected || !animated )
		{
			[backgroundLayer removeAllAnimations];
		}
	}
	
	[super setSelected:(BOOL)selected animated:(BOOL)animated ];	
	
}



-(void)setNeedsDisplay
{
	[contentView setNeedsDisplay];
	[contentDetailView setNeedsDisplay];
	
	[super setNeedsDisplay];
}



- (void)willTransitionToState:(UITableViewCellStateMask)state
{
	[super willTransitionToState:state];
	currentState = state;
	
	[self willTransitionToStateMain];
}

-(void)willTransitionToStateMain
{
	

	//rightIndentationWidth = self.bounds.size.width - [self contentView].frame.size.width;
	
	leftIndentationWidth = 8 + self.indentationLevel * self.indentationWidth;
	
	rightIndentationWidth = STANDARD_RIGHT_INDENTATION;
	if( currentState & 0x02  )
	{

		rightIndentationWidth = DELETING_RIGHT_INDENTATION;
		
	}else if( currentState & 0x01)
	{
		
		// editing mode
		
		if( self.editingAccessoryType == UITableViewCellAccessoryDisclosureIndicator || self.editingAccessoryType == UITableViewCellAccessoryCheckmark )
		{
			rightIndentationWidth = DISCLOSURE_RIGHT_INDENTATION;
		}
		
		
		if( self.showsReorderControl ) 
			rightIndentationWidth += REORDER_RIGHT_INDENTATION;

	}else if( self.accessoryType == UITableViewCellAccessoryDisclosureIndicator || self.accessoryType == UITableViewCellAccessoryCheckmark )
	{
		if(currentState & 0x01)
			rightIndentationWidth = STANDARD_RIGHT_INDENTATION;
		
		else
			rightIndentationWidth = DISCLOSURE_RIGHT_INDENTATION;
		
	}else if( self.accessoryType == UITableViewCellAccessoryDetailDisclosureButton )
	{

		rightIndentationWidth = DETAILED_DISCLOSURE_RIGHT_INDENTATION;
	}
	
	
	contentViewFrame.origin.x = leftIndentationWidth + iconFrame.size.width;
	iconFrame.origin.x = leftIndentationWidth - 8;
	
	if( currentState & 0x01 )
	{
		contentViewFrame.origin.x = roundf( leftIndentationWidth  + iconFrame.size.width + EDITING_LEFT_INDENTATIO );
		
		iconFrame.origin.x = leftIndentationWidth - 8 + EDITING_LEFT_INDENTATIO;
		

	}
	
	
}

-(void)layoutSubviews
{

	[super layoutSubviews];
	
	
	// Adjust height
	if( self.subtitleAttributedString && self.secondSubtitleAttributedString )
	{
		contentViewFrame.size.height = self.bounds.size.height - 38;
		contentDetailViewFrame.size.height = self.bounds.size.height - 38;
		
		subtitleView.frame = CGRectMake(contentViewFrame.origin.x, CGRectGetMaxY(contentViewFrame), self.bounds.size.width - contentViewFrame.origin.x - 14, 17 );
		
		secondSubtitleView.frame =  CGRectMake(contentViewFrame.origin.x, CGRectGetMaxY(contentViewFrame) + 15, self.bounds.size.width - contentViewFrame.origin.x - 14, 17 );
		
		
		subtitleView.hidden = NO;
		secondSubtitleView.hidden = NO;
		
	}else if( self.subtitleAttributedString || self.secondSubtitleAttributedString )
	{
		contentViewFrame.size.height = self.bounds.size.height - 19;
		contentDetailViewFrame.size.height = self.bounds.size.height - 19;
		
		subtitleView.frame = CGRectMake(contentViewFrame.origin.x, CGRectGetMaxY(contentViewFrame), self.bounds.size.width - contentViewFrame.origin.x - 14, 17 );
		
		subtitleView.hidden = NO;
		secondSubtitleView.hidden = YES;
		
	}else
	{
		contentViewFrame.size.height = self.bounds.size.height;
		contentDetailViewFrame.size.height = self.bounds.size.height;
		subtitleView.hidden = YES;
		secondSubtitleView.hidden = YES;
	}
	
	rightIndentationWidth = self.bounds.size.width - CGRectGetMaxX([self contentView].frame);
	contentDetailViewFrame.origin.x =   - rightIndentationWidth -10;

	contentView.contentWidth =  roundf( CGRectGetMaxX(contentDetailViewFrame)  - [contentDetailView width] - CGRectGetMinX(contentViewFrame) );
	
	CGRect frame = subtitleView.frame;
	frame.origin.x -= rightIndentationWidth;
	subtitleView.frame = frame;
	subtitleView.contentWidth = subtitleView.frame.size.width;

	frame = secondSubtitleView.frame;
	frame.origin.x -= rightIndentationWidth;
	secondSubtitleView.frame = frame;
	secondSubtitleView.contentWidth = secondSubtitleView.frame.size.width;
	

	
	[contentView setNeedsDisplay];
	[subtitleView setNeedsDisplay];
	[secondSubtitleView setNeedsDisplay];

	
	contentView.frame = CGRectIntegral( contentViewFrame );	
	contentDetailView.frame = CGRectIntegral( contentDetailViewFrame );
	contentDetailView.contentWidth = contentDetailView.frame.size.width;
	iconView.frame = CGRectIntegral( iconFrame );

//	NSAttributedString *test = [[[NSAttributedString alloc] initWithString:@"x"] autorelease];
//	MNLOG(@"contentDetailView.attributedString length %d", contentDetailView.attributedString.length);
	//contentDetailView.attributedString = test;
	
	[contentDetailView setNeedsDisplay];

	
	backgroundLayer.frame = self.bounds;
	
	//[self setNeedsDisplay];
}

//-(void)drawRect:(CGRect)rect
//{
//	/// Draw background
//	
//	CGContextRef ctx = UIGraphicsGetCurrentContext();
//	UIColor* paperTextureColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"PaperTexture.png"]];
//	CGContextSetAlpha(ctx, 0.2);
//	CGContextSetBlendMode(ctx, kCGBlendModePlusDarker);
//	CGContextSetFillColorWithColor(ctx, paperTextureColor.CGColor);
//	CGContextFillRect(ctx, rect);
//	
//	CGContextSetAlpha(ctx, 1.0);
//	CGContextSetBlendMode(ctx, kCGBlendModeNormal);	
//}
//-(void)drawRect:(CGRect)rect
//{
//
//	
//	/// ==================================
//	MNLOG(@"%@ %@",NSStringFromClass([self class]), NSStringFromSelector( _cmd ));
///// ==================================
//
//	CGContextRef ctx = UIGraphicsGetCurrentContext();
//	
//	
//	if( self.highlighted )
//	{
//	//	UIColor *selectedFromColor  = [UIColor colorWithRed:65.0/255.0 green:133.0/255.0 blue:244.0/255.0 alpha:1];
////		UIColor *selectedToColor  = [UIColor colorWithRed:25.0/255.0 green:79.0/255.0 blue:220.0/255.0 alpha:1];
////		UIColor *selectedHighlightColor = [UIColor colorWithRed:108.0/255.0 green:172.0/255.0 blue:249.0/255.0 alpha:1];
////		
////		
////		[DrawUtils drawGradientInRect:rect fromColor:selectedFromColor  toColor:selectedToColor highlightColor:selectedHighlightColor];
//		
//		
//		[RGBA(0,0,0,0.2) set];
//		CGContextFillRect(ctx, rect);
//
//		
//	}else {
//		[[self backgroundColor] set];
//		CGContextFillRect(ctx, rect);
//	}
//	
//
//	
//	
//}

#pragma mark - Longpress

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//	if( ![MNMenuView sharedMenuView].hidden )
//		[MNMenuView sharedMenuView].hidden = YES;
//	
//	[timer_ invalidate];
//	[timer_ release];
//	timer_ = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(longpress) userInfo:nil repeats:NO] retain];
//	
//	
//	[super touchesBegan:touches withEvent:event];
//
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//	[timer_ invalidate];
//	[timer_ release];
//	timer_ = nil;
//	
//	[super touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event];	
//	
//	if( ![MNMenuView sharedMenuView].hidden )
//		[self setHighlighted:NO animated:YES];
//}
//
//- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
//{
//	[timer_ invalidate];
//	[timer_ release];
//	timer_ = nil;
//	
//	[super touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event];	
//}
//
//-(void)longpress
//{
//
//	if( showPathForLongPress_ )
//	{
//		NSString* path = [self.iconObject.path stringByDeletingLastPathComponent];
//		//path - FF_DOCUMENT_PATH
//		
//		if( [path hasPrefix: FF_DOCUMENTS_FOLDER] )
//		{
//			path = [path substringFromIndex: [DOCUMENT_FOLDER length] +1];
//		}
//		
//		MNMenuView* menu = [MNMenuView sharedMenuView];
//		MNMenuItem* item;
//		item = [[[MNMenuItem alloc] init] autorelease];
//		item.title = [NSString stringWithFormat:NSLocalizedString(@"%@", @""),path];
//		
//		item.action = ^{};
//		item.disabled = YES;
//		
//		menu.targetView = self.superview;
//		menu.targetRect = [self convertRect:[self bounds] toView:self.window];
//
//		menu.items = [NSArray arrayWithObject:item];
//		menu.cancelAction = ^{ [viewController_.tableView deselectRowAtIndexPath:[viewController_.tableView indexPathForSelectedRow] animated:YES]; };
//		menu.hidden = NO;
//
//
//	}else
//	{
//		[self showMenu];
//	}
//}
//
//
//-(void)showMenu
//{
//	[APP_DELEGATE instantiateDropboxSession];
//
//
//	
//    BOOL canEdit = YES;
//    
//	MNMenuView* menu = [MNMenuView sharedMenuView];
//	
//	//menu.interfaceOrientation = viewController.interfaceOrientation;
//	
//	menu.targetView = self.superview;
//	menu.targetRect = [self convertRect:[self bounds] toView:self.window];
//	
//	NSMutableArray *items = [NSMutableArray array];
//	MNMenuItem* item;
//	
//    
//	//FileInfoViewController
//	
//	
//	item = [[[MNMenuItem alloc] init] autorelease];
//	item.title = @"Info";
//	item.image = [UIImage imageNamed:@"FFInfo.png"];
//	item.action = ^{  
//            FileInfoViewController* controller = [[[FileInfoViewController alloc] init] autorelease];
//            controller.path = self.iconObject.path;
//            UINavigationController* nav = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
//            
//            [viewController_ presentModalViewController:nav animated:YES];
//
//	};
//	[items addObject: item];
//	
//    // iCloud
//	
//	if( [self.iconObject.path hasPrefix:TRASH_FOLDER] )
//		canEdit = NO;
//
//    
//    if( ICLOUD_ENABLED )
//	{
//        if( [self.iconObject.path isEqualToString:[IconObject iCloudPath]] )
//            canEdit = NO;
//        
////       NSArray* allList = [NSFileCoordinator filePresenters];
////        
////        if( [allList containsObject: self.iconObject] )
////        {
////            item = [[[MNMenuItem alloc] init] autorelease];
////            item.title = @"** Registered **";
////            item.action = ^{   };
////        }else
////        {
////            item = [[[MNMenuItem alloc] init] autorelease];
////            item.title = @"Unregistered";
////            item.action = ^{   };
////
////        }
////		
////
////		[items addObject: item];
////        
////        /// Versions
////        item = [[[MNMenuItem alloc] init] autorelease];
////        item.title = @"Versions";
////        item.action = ^{ 
////        
////            MNVersionListViewController* controller = [[[MNVersionListViewController alloc] initWithStyle: UITableViewStylePlain] autorelease];
////            controller.url = [NSURL fileURLWithPath: self.iconObject.path];
////            UINavigationController* nav = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
////            
////            [viewController_ presentModalViewController:nav animated:YES];
////            
////            
////        };
////		[items addObject: item];
//
//        
//        
//        ///
//        
//        
//        if( self.iconObject.isReallyInCloud )
//            canEdit = NO;
//        
//        ///
//        
////        if( canEdit && self.iconObject.isUbiquitous && !self.iconObject.isFolder )
////        {
////            item = [[[MNMenuItem alloc] init] autorelease];
////            item.title = @"Evict";
////            item.action = ^{ 
////				
////				void(^block)(IconObject*) = ^(IconObject* anIconObject){
////					
////					NSError* error = nil;
////					NSFileManager *fm = [[[NSFileManager alloc] init] autorelease];
////					
////					
////					if( ! anIconObject.isReallyInCloud && !anIconObject.isFolder )
////					{
////						if(  [fm evictUbiquitousItemAtURL:[anIconObject URL] error:&error] )
////						{
////							[self.iconObject.supericon updateSubicons];
////							//[viewController_.tableView reloadData];
////							
////							MNLOG(@"Eviction Started");
////						}else
////						{
////							NSString* message = [error localizedDescription];
////							SHOW_ERROR(@"Error", message);
////						}
////					}
////					
////					
////				};
////				
////				if( self.iconObject.isFolder )
////				{
////					[self.iconObject makeIconsPerformBlock:block];
////
////				}else
////				{
////					block(self.iconObject);
////				}
////				
////            };
//// 
////            [items addObject: item];
////
////        }
//        
//	}
//    
//    
//	
//	//iconObject
//	
//    if( canEdit )
//	{
//		
//		item = [[[MNMenuItem alloc] init] autorelease];
//		item.image = [UIImage imageNamed:@"SKPOrganize.png"];
//		item.action = ^{ 
//			
//			
//			[self setHighlighted:NO animated:YES];
//			
//			
//			if( [self.iconObject containsCloudItem] )
//			{
//				SHOW_ERROR(@"Sorry", @"NSFileCoordinatorCannotMoveUbiquitousItem");			
//				return;
//			}
//			 
//			
//
//			SimpleOrganizeViewController* controller = [[[SimpleOrganizeViewController alloc] init] autorelease];
//			controller.targetObject = self.iconObject;
//			
//			
//			
//			controller.action = ^(NSString* newPath){ 
//				
//				[viewController_.tableView reloadData];
//				
//			};
//			
//
//			if( gIsRunningOnIPad )
//			{
//				controller.modalPresentationStyle = UIModalPresentationFormSheet;	
//			}
//			
//			[viewController_ presentModalViewController:controller animated:YES];
//			
//			
//		};
//		[items addObject: item];
//	}
//	
////	
////	item = [[[MNMenuItem alloc] init] autorelease];
////	item.title = @"Duplicate";
////	item.action = ^{ 
////		
////		[self setHighlighted:NO animated:YES];
////		
////
////		// Action here
////		
////		NSError *error = nil;
////		NSString* destPath = [[NSString stringWithFormat:@"%@ (Copy).%@", [[self.iconObject.path lastPathComponent] stringByDeletingPathExtension], [self.iconObject.path pathExtension]] uniquePathForFolder];
////		
////		if( [[NSFileManager defaultManager] copyItemAtPath:self.iconObject.path toPath:destPath error:&error] )
////		{
////			[self.iconObject updateSubicons];
////			[self.iconObject setUserInfoCache:nil];
////			[viewController_.tableView reloadData];
////
////			
////		}else
////		{
////			NSString* message = [error localizedDescription];
////			SHOW_ERROR(@"Error", message);	
////		}
////		
////	};
////	[items addObject: item];
////	
//	
//	
//	
//	/// For any file
//	
//#ifdef SKETCH_PAD_BUILD
//	
//	if( canEdit && ![[self.iconObject.path pathExtension] isEqualToString:SKP_DOCUMENT_EXTENSION] )
//		
//#else
//		if( canEdit )
//			
//#endif
//	{
//		item = [[[MNMenuItem alloc] init] autorelease];
//		item.title = NSLocalizedString(@"Rename",@"");
//		item.action = ^{ 
//			
//			[self setHighlighted:NO animated:YES];
//
//			FolderNameViewController* controller = [[[FolderNameViewController alloc] init] autorelease];
//			controller.delegate = self;
//			controller.hasTip = NO;
//
//			if( [[self.iconObject.path pathExtension] isEqualToString:FF_DOCUMENT_EXTENSION] )
//			{
//				NSString* metadataPath = [self.iconObject.path stringByAppendingPathComponent:@"metadata.plist"];
//				if( [[NSFileManager defaultManager] fileExistsAtPath:metadataPath] )
//				{
//					NSDictionary* metadata = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:metadataPath]
//																			  mutabilityOption:NSPropertyListImmutable format:nil
//																			  errorDescription:nil];
//					
//					NSString* titleString = [metadata objectForKey:@"title"];
//					controller.name = titleString;
//					
//				}
//			}else
//			{
//				controller.name = [[self.iconObject.path lastPathComponent] stringByDeletingPathExtension];
//				
//			}
//			
//			controller.navigationItem.title = NSLocalizedString(@"Rename",@"");
//			
//			UINavigationController* nav = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
//			
//			if( gIsRunningOnIPad ) nav.modalPresentationStyle = UIModalPresentationFormSheet;
//			
//			[viewController_ presentModalViewController:nav animated:YES];
//			
//		};
//		[items addObject: item];
//		
//	}
//	
////	if( !self.iconObject.isFolder && ![self.iconObject isText] )
////	{
////		item = [[[MNMenuItem alloc] init] autorelease];
////		item.title = @"Tag";
////		item.action = ^{ 
////			
////			
////		};
////		[items addObject: item];
////		
////	}
//	
//
//
//	
//	BOOL isDir;
//	if( [[NSFileManager defaultManager] fileExistsAtPath:self.iconObject.path isDirectory:&isDir] && !isDir )
//	{
//		
//		
//		if( [self.iconObject isText] )
//		{
//			/// Export
//			
//			item = [[[MNMenuItem alloc] init] autorelease];
//			item.image = [UIImage imageNamed:@"SKPExportW.png"];
//			item.title = @"Export";
//			item.action = ^{ 
//				
//				
//				FFUIDocument *note = [[[FFUIDocument alloc] initWithFileURL:  [self.iconObject URL]] autorelease];
//				
//				__block BOOL readFromSuccess = NO;
//				[self.iconObject readWithBlock:^(NSURL* newURL){
//
//					readFromSuccess = [note readFromURL:newURL error:nil];
//
//					
//				} onCompletion:^(BOOL success){
//
//					if( success && readFromSuccess)
//					{
//						ExportViewController *controller = [[[ExportViewController alloc] init] autorelease];
//						controller.note = note;
//						controller.textStorage = note.textStorage;
//						controller.willStartExporting = ^(MNExportDestinationType destination){	};
//						
//						[viewController_ presentModalViewController:controller animated:YES];
//					}
//				}];			
//
//
//				
//				
//			};
//			
//			[items addObject: item];
//			
//		}else
//		{
//			
//		/// Export
//		item = [[[MNMenuItem alloc] init] autorelease];
//		item.image = [UIImage imageNamed:@"SKPExportW.png"];
//		item.title = @"Export";
//		item.action = ^{
//			
//			[self setHighlighted:NO animated:YES];
//
//			// Exporting menu
//			
//			MNBlockActionSheet* sheet = [[[MNBlockActionSheet alloc] initWithTitle: NSLocalizedString(@"Export to", @"")] autorelease];
//			
//			if( [MFMailComposeViewController canSendMail] )
//			{
//				[sheet addTitle: NSLocalizedString(@"Email", @"") actionBlock:^{
//					
//					
//					[mailViewController release];
//					mailViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
//					mailViewController.mailComposeDelegate = self;
//					
//					[mailViewController addAttachmentData:[NSData dataWithContentsOfFile:self.iconObject.path] mimeType:nil fileName:[self.iconObject.path lastPathComponent]];
//					
//					
//					if( gIsRunningOnIPad )
//						mailViewController.modalPresentationStyle = UIModalPresentationPageSheet;
//					
//					NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//					
//					NSString* toString = [ud objectForKey:@"FavoriteEmailTo"];
//					NSString* ccString = [ud objectForKey:@"FavoriteEmailCc"];
//					NSString* bccString = [ud objectForKey:@"FavoriteEmailBcc"];
//					
//					NSArray* toRecipients = [toString componentsSeparatedByString:@","];
//					NSArray* ccRecipients = [ccString componentsSeparatedByString:@","];
//					NSArray* bccRecipients = [bccString componentsSeparatedByString:@","];
//					
//					if( toString.length > 0 && toRecipients && [toRecipients count] )
//						[mailViewController setToRecipients:toRecipients];
//					
//					if( ccString.length > 0 && ccRecipients && [ccRecipients count] )
//						[mailViewController setCcRecipients:ccRecipients];
//					
//					if( bccString.length > 0 && bccRecipients && [bccRecipients count] )
//						[mailViewController setBccRecipients:bccRecipients];
//					
//					
//					[viewController_ presentModalViewController:mailViewController animated:YES];
//					
//				}
//				 
//					destructive:NO cancel:NO];
//				
//				
//			}
//			
//			///
//			
//			[sheet addTitle: NSLocalizedString(@"App", @"") actionBlock:^{
//				
//				
//				UIDocumentInteractionController* controller = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:self.iconObject.path]];
//				[controller setDelegate : self];
//				
//				CGRect cellFrame = [self.superview convertRect:self.frame toView:viewController_.view];
//				BOOL flag =
//				[controller presentOpenInMenuFromRect:cellFrame inView:viewController_.view animated:YES];
//				
//				if( flag == NO )
//				{
//					SHOW_ERROR(@"No app found",@"An app that can handle this file is not installed (such as iBooks for PDF file)");	
//				}
//				else {
//					[controller retain];
//				}
//				
//				
//			}destructive:NO cancel:NO];
//			
//			
//			///
//			
//			if( self.iconObject.isMovie || self.iconObject.isPhoto )
//			{
//				
//				[sheet addTitle: NSLocalizedString(@"Photo Library", @"") actionBlock:^{
//					
//					MNLOG(@"self.iconObject.path %@",self.iconObject.path);
//					
//					if( self.iconObject.isPhoto )
//					{
//						
//						UIImageWriteToSavedPhotosAlbum([UIImage imageWithContentsOfFile:self.iconObject.path], self, @selector(saveFinished:didFinishSavingWithError:contextInfo:), nil);
//						
//						
//					}
//					
//					if( self.iconObject.isMovie )
//					{
//						
//						UISaveVideoAtPathToSavedPhotosAlbum(self.iconObject.path, self,  @selector(saveFinished:didFinishSavingWithError:contextInfo:), nil);
//						
//					}
//					
//					
//				}destructive:NO cancel:NO];
//				
//			}
//
//			
//			
//			if( self.iconObject.isPhoto )
//			{
//				//saveToPicasa
//				
//				if( [PasswordViewController hasAccountForKey:GOOGLE_KEYCHAIN_KEY ] )
//				{
//					[sheet addTitle: NSLocalizedString(@"Picasa Web", @"") actionBlock:^{
//						
//						[self saveToPicasa];
//						
//					}destructive:NO cancel:NO];
//				}
//
//				
//				// Twitter
//				NSString* extension = [[iconObject_.path pathExtension] lowercaseString];
//				if( [TWTweetComposeViewController canSendTweet] && ([extension isEqualToString:@"jpg"] || [extension isEqualToString:@"jpeg"]) ) 
//				{
//					[sheet addTitle: NSLocalizedString(@"Twitter", @"") actionBlock:^{
//						
//						[self saveToTwitter];
//						
//					}destructive:NO cancel:NO];
//					
//				}
//			}
//			
//			/// Save to google doc
//			if( [PasswordViewController hasAccountForKey:GOOGLE_KEYCHAIN_KEY ] )
//			{
//				[sheet addTitle: NSLocalizedString(@"Google Doc", @"") actionBlock:^{
//					
//					[self saveToGoogleDoc];
//					
//				}destructive:NO cancel:NO];
//			}
//			
//			
//			//saveToEvernote
//			
//			
//			if( [PasswordViewController hasAccountForKey:EVERNOTE_KEYCHAIN_KEY ] )
//			{
//				[sheet addTitle: NSLocalizedString(@"Evernote", @"") actionBlock:^{
//					
//					[self saveToEvernote];
//					
//				}destructive:NO cancel:NO];
//			}
//			
//			///
//			
//			if( [[DBSession sharedSession] isLinked] )
//			{
//				[sheet addTitle: NSLocalizedString(@"Dropbox", @"") actionBlock:^{
//					[self saveToDropbox];
//				} destructive:NO cancel:NO];
//				
//			}
//			
//			
//			///
//			CGRect cellFrame = [self.superview convertRect:self.frame toView:viewController_.view];
//			
//			[sheet addTitle:NSLocalizedString(@"Cancel",@"") actionBlock:^{} destructive:NO cancel:YES];
//			
//			[sheet showFromRect:cellFrame inView:viewController_.view animated:YES];
//		};
//		[items addObject: item];
//		
//		
//		}
//		
//	}
//
//	
//	if( canEdit && ([[self.iconObject.path pathExtension] isEqualToString:SKP_DOCUMENT_EXTENSION] ||
//	   [[self.iconObject.path pathExtension] isEqualToString:FF_DOCUMENT_EXTENSION] ) )
//	{
//		if( ![APP_DELEGATE photoBuild] )
//		{
//			// Read Cache
//			NSDictionary* userInfo = [self.iconObject userInfoCache];
//			
//			BOOL flagged = [[userInfo objectForKey:@"Flagged"] boolValue]; 
//			NSTimeInterval alarm = [[userInfo objectForKey:@"AlarmDate"] doubleValue];
//			
//			if( !userInfo )
//			{
//				
//				// Read metadata
//				NSString* metadataPath = [self.iconObject.path stringByAppendingPathComponent:@"metadata.plist"];
//				if( [[NSFileManager defaultManager] fileExistsAtPath:metadataPath] )
//				{
//					NSDictionary* metadata = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:metadataPath]
//																			  mutabilityOption:NSPropertyListImmutable format:nil
//																			  errorDescription:nil];
//					MNLOG(@"metadata %@",[metadata description]);
//					flagged = [[metadata objectForKey:@"Flagged"] boolValue];
//					alarm = [[metadata objectForKey:@"AlarmDate"] doubleValue];
//					MNLOG(@"reload alarm %f",alarm);
//					
//				}
//			}
//			
//			
//			
//			
//			if( !flagged )
//			{
//				
//				item = [[[MNMenuItem alloc] init] autorelease];
//				item.title = @"Flag me";
//				item.action = ^{ 
//					
//					[self setHighlighted:NO animated:YES];
//					
//					NSError *error = nil;
//					NSString* path = [self.iconObject path];
//					NSString* metadataPath = [path stringByAppendingPathComponent:@"metadata.plist"];
//					NSMutableDictionary* metadata = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:metadataPath]
//																					 mutabilityOption:NSPropertyListMutableContainers format:nil
//																					 errorDescription:nil];
//					
//					[metadata setObject:[NSNumber numberWithBool:YES] forKey:@"Flagged"];
//					
//					
//					NSData* serializedData = [NSPropertyListSerialization dataWithPropertyList:metadata format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
//					
//					[serializedData writeToFile:metadataPath atomically:YES];
//					
//					
//					
//					// Flag file
//					
//					NSURL* flagPath = [[NSURL fileURLWithPath:path] URLByAppendingPathComponent:@"flag"];
//					if( 1 )
//					{
//						NSData* data = [@"1" dataUsingEncoding:NSUTF8StringEncoding];
//						[data writeToURL:flagPath options:0 error:nil];
//						
//					}else
//					{
//						[[NSData data] writeToURL:flagPath options:0 error:nil];
//					}
//					
//					
//					
//					[self.iconObject setUserInfoCache:nil];
//					[viewController_.tableView reloadData];
//				};
//				
//			}else {
//				
//				item = [[[MNMenuItem alloc] init] autorelease];
//				item.title = @"Unflag";
//				item.action = ^{ 
//					
//					[self setHighlighted:NO animated:YES];
//					
//					NSError *error = nil;
//					NSString* path = [self.iconObject path];
//					NSString* metadataPath = [path stringByAppendingPathComponent:@"metadata.plist"];
//					NSMutableDictionary* metadata = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:metadataPath]
//																					 mutabilityOption:NSPropertyListMutableContainers format:nil
//																					 errorDescription:nil];
//					
//					[metadata setObject:[NSNumber numberWithBool:NO] forKey:@"Flagged"];
//					
//					
//					NSData* serializedData = [NSPropertyListSerialization dataWithPropertyList:metadata format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
//					
//					[serializedData writeToFile:metadataPath atomically:YES];
//					
//					
//					// Flag file
//					
//					NSURL* flagPath = [[NSURL fileURLWithPath:path] URLByAppendingPathComponent:@"flag"];
//					if( 0 )
//					{
//						NSData* data = [@"1" dataUsingEncoding:NSUTF8StringEncoding];
//						[data writeToURL:flagPath options:0 error:nil];
//						
//					}else
//					{
//						[[NSData data] writeToURL:flagPath options:0 error:nil];
//					}
//					
//					
//					[self.iconObject setUserInfoCache:nil];
//					[viewController_.tableView reloadData];
//					
//				};
//			}
//			
//			
//			
//			[items addObject: item];
//			
//			//
//			
//			void (^setDateBlock)(NSDate*) = ^(NSDate *date){
//				
//				/// Set alarm date here
//				
//				NSError *error = nil;
//				NSString* path = [self.iconObject path];
//				NSString* metadataPath = [path stringByAppendingPathComponent:@"metadata.plist"];
//				NSMutableDictionary* metadata = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:metadataPath]
//																				 mutabilityOption:NSPropertyListMutableContainers format:nil
//																				 errorDescription:nil];
//				
//				
//				// Cancel exisiting local notification if available
//				
//				for( UILocalNotification *notif in [[UIApplication sharedApplication] scheduledLocalNotifications] )
//				{
//					NSString* thisPath = [notif.userInfo objectForKey:LOCAL_NOTIFICATION_DOCUMENT_PATH];
//					
//					if( [thisPath isEqualToString:path] )
//					{
//						[[UIApplication sharedApplication] cancelLocalNotification: notif];
//					}
//				}
//				
//				
//				
//				
//				// Check date
//				
//				if( [(NSDate*)[NSDate date] compare: (NSDate*)date] == NSOrderedAscending )
//				{
//					
//					
//					
//					UILocalNotification *localNotification = [[[UILocalNotification alloc] init] autorelease];	
//					localNotification.userInfo = [NSDictionary dictionaryWithObjectsAndKeys: path, LOCAL_NOTIFICATION_DOCUMENT_PATH, nil];
//					localNotification.soundName =  UILocalNotificationDefaultSoundName;
//					localNotification.alertBody = NSLocalizedString(@"The document is calling you!",@"");
//					localNotification.alertAction = NSLocalizedString(@"Show", nil);
//					
//					localNotification.fireDate = date;
//					
//					//			NSString* mainBundlePath = [[NSBundle mainBundle] bundlePath];
//					//			NSString* iconPath = [iconObject iconPathForOrientation:pageManager.interfaceOrientation];
//					//			
//					//			MNLOG(@"mainBundlePath %@",mainBundlePath);
//					//			
//					//			
//					//			// This does not happen
//					//			if( [iconPath hasPrefix: mainBundlePath] )
//					//			{
//					//				NSString *subpath = [iconPath substringFromIndex: mainBundlePath.length];
//					//
//					//				MNLOG(@"subpath %@",subpath);
//					//				localNotification.alertLaunchImage = subpath;
//					//
//					//			}
//					
//					
//					
//					
//					// Register local notification
//					
//					[[UIApplication sharedApplication] scheduleLocalNotification: localNotification];
//					
//					[metadata setObject:[NSNumber numberWithDouble:[date timeIntervalSinceReferenceDate]] forKey:@"AlarmDate"];
//					
//				}else
//				{
//					[metadata removeObjectForKey: @"AlarmDate"];
//				}
//				
//				
//				NSData* serializedData = [NSPropertyListSerialization dataWithPropertyList:metadata format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
//				
//				[serializedData writeToFile:metadataPath atomically:YES];
//				
//				
//				
//				[self.iconObject setUserInfoCache: nil];
//				
//				[self setNeedsDisplay];
//				
//				
//				
//				
//				if( gIsRunningOnIPad )
//				{
//					[popover_ dismissPopoverAnimated:YES];
//					[popover_ release];
//					popover_ = nil;
//				}
//			};
//			
//			
//			if( alarm > 0 )
//			{
//				NSDate* myDate = [NSDate dateWithTimeIntervalSinceReferenceDate: alarm];
//				
//				NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
//				[formatter setTimeStyle: NSDateFormatterShortStyle];
//				NSString* timeString = [formatter stringFromDate: myDate];
//				
//				NSString* dateFormat = [NSDateFormatter dateFormatFromTemplate:@"YYYYMMMMdEEE" options:0 locale:[NSLocale currentLocale]];
//				[formatter setDateFormat:dateFormat];
//				NSString* dateString = [formatter stringFromDate: myDate];
//				
//				item = [[[MNMenuItem alloc] init] autorelease];
//				item.title = [NSString stringWithFormat:@"%@, %@", dateString, timeString];
//				item.action = ^{ 
//					
//					CalendarDatePickerController *controller = [[[CalendarDatePickerController alloc] initWithNibName:@"CalendarDatePickerController" bundle:nil] autorelease];
//					controller.action = setDateBlock;
//					
//					
//					
//					if( gIsRunningOnIPad )
//					{
//						
//						popover_ = [[UIPopoverController alloc] initWithContentViewController: controller];
//						popover_.delegate = self;
//						popover_.popoverContentSize = CGSizeMake(320, 500);
//						controller.popover = popover_;
//						[popover_ presentPopoverFromRect:[self frame]
//												  inView:self.superview 
//								permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//						
//						controller.cancelAction = ^{
//							
//							[popover_ dismissPopoverAnimated:YES];
//							[popover_ release];
//							popover_ = nil;
//							
//							
//						};
//					}else {
//						
//						
//						[viewController_ presentModalViewController:controller animated:YES];
//						
//					}
//					
//					
//					[controller setOriginalDate: myDate];
//					
//				};
//				
//				[items addObject: item];
//				
//			}else
//			{
//				
//				item = [[[MNMenuItem alloc] init] autorelease];
//				item.title = @"Set Alarm";
//				item.action = ^{ 
//					
//					[self setHighlighted:NO animated:YES];
//					
//					CalendarDatePickerController *controller = [[[CalendarDatePickerController alloc] initWithNibName:@"CalendarDatePickerController" bundle:nil] autorelease];
//					controller.action = setDateBlock;
//					
//					
//					
//					if( gIsRunningOnIPad )
//					{
//						
//						popover_ = [[UIPopoverController alloc] initWithContentViewController: controller];
//						popover_.delegate = self;
//						popover_.popoverContentSize = CGSizeMake(320, 500);
//						controller.popover = popover_;
//						[popover_ presentPopoverFromRect:[self frame]
//												  inView:self.superview
//								permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//						
//						controller.cancelAction = ^{
//							
//							[popover_ dismissPopoverAnimated:YES];
//							[popover_ release];
//							popover_ = nil;
//							
//							
//						};
//					}else {
//						
//						
//						[viewController_ presentModalViewController:controller animated:YES];
//						
//					}
//					
//					
//					
//				};
//				[items addObject: item];
//				
//			}
//			
//		}
//		
//		// Recover
//		
//		NSString* prevContentsPath = [self.iconObject.path stringByAppendingPathComponent:@"previous contents"];
//		
//		if( [[NSFileManager defaultManager] fileExistsAtPath:prevContentsPath] )
//		{
//			
//			item = [[[MNMenuItem alloc] init] autorelease];
//			item.title = @"Recover...";
//			item.action = ^{ 
//				
//				[self setHighlighted:NO animated:YES];
//
//				MNBlockActionSheet *sheet = [[[MNBlockActionSheet alloc] initWithTitle:NSLocalizedString(@"RecoverMessage",@"") ] autorelease];
//				
//				[sheet addTitle:NSLocalizedString(@"Recover",@"") actionBlock:^{
//					
//					
//					[viewController_ recover:self.iconObject.path];
//					
//					
//				} destructive:NO cancel:NO];
//				
//				[sheet addTitle:NSLocalizedString(@"Cancel",@"") actionBlock:^{} destructive:NO cancel:YES];
//				
//				if( gIsRunningOnIPad )
//					[sheet showFromRect:[self frame] inView:self.superview animated:YES];
//				else
//					[sheet showInView: self];
//				
//			};
//			
//			[items addObject: item];
//			
//		}
//		
//		
//		
//		/// Export
//		
//		item = [[[MNMenuItem alloc] init] autorelease];
//		item.image = [UIImage imageNamed:@"SKPExportW.png"];
//		item.title = @"Export";
//		item.action = ^{ 
//			
//			if( [[self.iconObject.path pathExtension] isEqualToString:SKP_DOCUMENT_EXTENSION] )
//			{
//				SKPUIDocument *map = [[[SKPUIDocument alloc] initWithFileURL: [self.iconObject URL]] autorelease];
//				
//				__block BOOL readFromSuccess = NO;
//				
//				[self.iconObject readWithBlock:^(NSURL* newURL){
//					
//					readFromSuccess = [map readFromURL:newURL error:nil];
//					
//					
//				} onCompletion:^(BOOL success){
//					
//					if( success && readFromSuccess )
//					{
//						SKPExportViewController *controller = [[[SKPExportViewController alloc] init] autorelease];
//						controller.map = map;
//						controller.willStartExporting = ^(MNExportDestinationType destination){	};
//						
//						[viewController_ presentModalViewController:controller animated:YES];
//					}
//				}];			
//
//	
//			}
//			
//			if( [[self.iconObject.path pathExtension] isEqualToString:FF_DOCUMENT_EXTENSION] )
//			{
//				FFUIDocument *note = [[[FFUIDocument alloc] initWithFileURL: [self.iconObject URL]] autorelease];
//
//				__block BOOL readFromSuccess = NO;
//
//				[self.iconObject readWithBlock:^(NSURL* newURL){
//					
//					readFromSuccess = [note readFromURL:newURL error:nil];
//					
//					
//				} onCompletion:^(BOOL success){
//					
//					if( success && readFromSuccess )
//					{
//						ExportViewController *controller = [[[ExportViewController alloc] init] autorelease];
//						controller.note = note;
//						controller.textStorage = note.textStorage;
//						controller.willStartExporting = ^(MNExportDestinationType destination){	};
//						
//						[viewController_ presentModalViewController:controller animated:YES];
//					}
//				}];			
//
//
//			}
//			
//		};
//		
//		[items addObject: item];
//
//	}
//	
//	
//	
//#ifndef QUICK_ADD_BUILD
//	if( AVIARY_ENABLED )
//	{
//	if( self.iconObject.isPhoto )
//	{
//		/// Aviary
//		item = [[[MNMenuItem alloc] init] autorelease];
//		item.title = NSLocalizedString(@"Edit",@""); //Aviary
//		item.action = ^{
//		
////			NSArray *tools = [NSArray arrayWithObjects:	kAFEnhance, kAFEffects, kAFOrientation, kAFCrop,  kAFBrightness, kAFContrast, kAFSaturation, kAFColors, kAFRedeye, kAFStickers, kAFWhiten, kAFBlemish, kAFSharpen, kAFBlur,	nil, nil];
//
//			
//			UIImage *imageToEdit = [UIImage imageWithContentsOfFile: self.iconObject.path];
////			AFFeatherController *featherController = [[[AFFeatherController alloc] initWithImage:imageToEdit andTools:tools] autorelease];
////			[featherController setDelegate:self];
//			
//			AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imageToEdit];
//			[editorController setDelegate:self];
//			
//
//			[viewController_ presentModalViewController:editorController animated:YES];
//
//		};
//		[items addObject: item];
//	}
//	}
//	
//#endif
//	
//	menu.items = items;
//	menu.cancelAction = ^{ [viewController_.tableView deselectRowAtIndexPath:[viewController_.tableView indexPathForSelectedRow] animated:YES]; };
//	menu.hidden = NO;
//	
//	
//	
//}
//
//-(void)saveFinished:(UIImage*)image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo;
//{
//	if( !error )
//	{
//		SHOW_ERROR(@"Saved To Photo Library",@"");
//		
//	}
//	else {
//		NSString *message = [error localizedDescription];
//		SHOW_ERROR(@"Error", message);
//		
//	}
//}
//
//
//-(void)folderNameWillChangeTo:(NSString*)name
//{
//    NSError* error = nil;
//    
//	if( self.iconObject.isFolder )
//	{
//		if( name.length == 0 ) return;
//		
//		NSString* dstPath = [[[self.iconObject.path stringByDeletingLastPathComponent] stringByAppendingPathComponent:name] uniquePathForFolder];
//
//		BOOL flag = [self.iconObject moveToPath: dstPath error: &error];
//		if( !flag ) return;
//		
//		
//        
//		viewController_.selectedIcon = [IconObject iconObjectForPath: dstPath];
//		[self.viewController.tableView reloadData];
//
//
//		
//	}else if( [[self.iconObject.path pathExtension] isEqualToString:FF_DOCUMENT_EXTENSION] )
//	{
//		if( name.length == 0 ) name = @"";
//		
//		NSError* error = nil;
//		NSString* path = [self.iconObject path];
//		NSString* metadataPath = [path stringByAppendingPathComponent:@"metadata.plist"];
//		NSMutableDictionary* metadata = [NSPropertyListSerialization propertyListFromData:[NSData dataWithContentsOfFile:metadataPath]
//																		 mutabilityOption:NSPropertyListMutableContainers format:nil
//																		 errorDescription:nil];
//		
//		[metadata setObject:name forKey:@"title"];
//		
//		NSData* serializedData = [NSPropertyListSerialization dataWithPropertyList:metadata format:NSPropertyListXMLFormat_v1_0 options:0 error:&error];
//		
//		[serializedData writeToFile:metadataPath atomically:YES];
//		
//		[self.iconObject setUserInfoCache:nil];
//		[self.viewController.currentIcon updateSubicons];
//		
//		viewController_.selectedIcon = self.iconObject;
//
//		[self.viewController.tableView reloadData];
//		
//	}else
//	{
//		NSString* originalExtension = [[self.iconObject.path lastPathComponent] pathExtension];
//		
//		if( name.length == 0 ) return;
//		
//		
//		if( ![[[name pathExtension] lowercaseString] isEqualToString: [originalExtension lowercaseString]] )
//		{
//			name = [name stringByAppendingPathExtension:originalExtension];
//		}
//		
//		NSString* dstPath = [[[self.iconObject.path stringByDeletingLastPathComponent] stringByAppendingPathComponent:name] uniquePathForFolder];
//		
//        BOOL flag = [self.iconObject moveToPath: dstPath error: &error];
//        if( !flag ) return;
//		
//        
//		viewController_.selectedIcon = [IconObject iconObjectForPath: dstPath];
//		[self.viewController.tableView reloadData];
//
//	}
//	
//}
//
//
//
//- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
//{
//	
//	/// ==================================
//	MNLOG(@"%@ %@",NSStringFromClass([self class]), NSStringFromSelector( _cmd ));
//	/// ==================================
//	
//	NSString* message = @""; 
//	NSString* messageTitle = @""; 
//	
//	switch (result) {
//		case MFMailComposeResultCancelled:
//			messageTitle = NSLocalizedString(@"Mail Cancelled",@"");
//			break;
//		case MFMailComposeResultSaved:
//			messageTitle = NSLocalizedString(@"Mail Saved",@"");
//			break;
//		case MFMailComposeResultSent:
//			messageTitle = NSLocalizedString(@"Mail Sent",@"");
//			message = NSLocalizedString(@"Mail Sent Message",@"");
//			break;
//		case MFMailComposeResultFailed:
//			messageTitle = NSLocalizedString(@"Mail Failed",@"");
//			break;
//		default:
//			messageTitle = NSLocalizedString(@"Mail Failed",@"");
//			break;
//	}
//	
//	
//	
//	
//	
//	if( MFMailComposeResultSent == result )
//	{
//		[controller.presentingViewController dismissModalViewControllerAnimated:YES];
//		dispatch_after(DISPATCH_TIME_NOW, dispatch_get_main_queue(), ^{ SHOW_ERROR(messageTitle, message); });
//		
//	}
//	else
//	{
//		[controller.presentingViewController dismissModalViewControllerAnimated:YES];
//		
//		if( MFMailComposeResultCancelled != result )
//			dispatch_after(DISPATCH_TIME_NOW, dispatch_get_main_queue(), ^{ SHOW_ERROR(messageTitle, message); });
//	}
//	
//	
//	//baseNavigationController.view.frame = CGRectMake(0, 0, 300, 480);
//	[mailViewController release];
//	mailViewController = nil;
//	
//	
//	
//}
//
//
//- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
//{
//	[controller autorelease];	
//}
//
//- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application;
//{
//	
//}
//
//-(void)saveToDropbox
//{
//	Reachability *internetReach = [Reachability reachabilityForInternetConnection];
//	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
//	if( netStatus == NotReachable )
//	{
//		SHOW_ERROR(@"Oops", @"The device needs to be online.");
//		return;
//	}
//	
//	
//	if( [DropboxIconObject inProcessJob] )
//	{
//		SHOW_ERROR(@"Busy", @"Currently the app is synchronizing with Dropbox.  Try again later after the job finished.");
//		return;
//		
//	}
//	
//	
//	DropboxDirectoryBrowser* controller = [[[DropboxDirectoryBrowser alloc] init] autorelease];
//	UINavigationController *nav = [[[UINavigationController alloc] init] autorelease];
//	
//	
//	NSString* destinationPath = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExportViewControllerDropboxPath"];
//	
//	if( !destinationPath ) destinationPath = @"/";
//	
//	
//	controller.endAction = ^(DropboxDirectoryBrowser* browser, BOOL alreadyExists){
//		
//		[[NSUserDefaults standardUserDefaults] setObject:browser.directoryPath forKey:@"ExportViewControllerDropboxPath"];
//		
//		NSString* path = self.iconObject.path;
//		
//		
//		
//		
//		[[ActivityIndicator sharedIndicator] showIndicatorWithBlock:^{}
//													  cancelAction:nil
//													  hasIndicator:NO];
//		
//		[DropboxIconObject setProcessEndAction:^(BOOL finished, NSError *error) {
//			
//			[[ActivityIndicator sharedIndicator] close];
//			
//			if( finished )
//			{
//				SHOW_ERROR(@"Done", @"");
//
//			}else
//			{
//				[DropboxIconObject displayError: error];
//			}
//			
//		}];
//		
//		
//		DropboxIconObject* obj = [[[DropboxIconObject alloc] init] autorelease];
//		
//		obj.path = path;
//		obj.dropboxPath = [browser.directoryPath stringByAppendingPathComponent: browser.filename];
//		
//		MNLOG(@"obj.path %@",obj.path);
//		MNLOG(@"obj.dropboxPath %@",obj.dropboxPath);
//		
//		if( alreadyExists )
//		{
//			[obj deleteDropboxItem];	
//		}
//		
//		
//		[obj uploadToDropbox];
//		
//		[DropboxIconObject startProcess];
//		
//		
//	};
//	
//	
//	controller.doneButtonTitle = NSLocalizedString(@"Save",@"");
//	controller.filename = [[self.iconObject.path lastPathComponent] stringByDeletingPathExtension];
//	controller.extension = [self.iconObject.path pathExtension];
//	
//	
//	NSArray* array = [controller viewControllersForPath: destinationPath];
//	
//	[nav setViewControllers:array];
//	
//	if( gIsRunningOnIPad )
//		nav.modalPresentationStyle = UIModalPresentationFormSheet;
//	[viewController_ presentModalViewController: nav animated:YES];
//	
//}
//
//-(void)saveToGoogleDoc
//{
//#ifndef QUICK_ADD_BUILD
//
//	Reachability *internetReach = [Reachability reachabilityForInternetConnection];
//	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
//	if( netStatus == NotReachable )
//	{
//		SHOW_ERROR(@"Oops", @"The device needs to be online.");
//		return;
//	}
//	
//	[[GoogleUtils sharedUtility] uploadFileAtPath:self.iconObject.path];
//	
//#endif
//}
//
//
//-(void)saveToTwitter
//{
//	
//	if( ![TWTweetComposeViewController canSendTweet] ) 
//	{
//		SHOW_ERROR(@"Needs setup",@"Needs twitter account setting");
//		return;
//	}
//	
//	Reachability *internetReach = [Reachability reachabilityForInternetConnection];
//	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
//	if( netStatus == NotReachable )
//	{
//		SHOW_ERROR(@"Oops", @"The device needs to be online.");
//		return;
//	}
//	
//	
//	
////	
////	twitterUtils_ = [[OAuthTwitterUtils alloc] init];
////	twitterUtils_.delegate = self;
////	
////	
//	
//	
//	
//	void (^block)(void) = ^{
//		
//		
//		
//		
//		if( [[NSData dataWithContentsOfMappedFile: self.iconObject.path] length] > 4000000 ) 
//		{
//			[[ActivityIndicator sharedIndicator] close];
//			SHOW_ERROR(@"Sorry",@"The image is too big to upload");
//			return;
//		}
//		
//		
//		// POST TO TWITPIC HERE
//		
//		
//		//NSString* picurl = [twitterUtils_  sendImage: image isPNG: NO ];
//		
//		
//		[[ActivityIndicator sharedIndicator] close];
//		
////		if( picurl == nil )
////		{
////			SHOW_ERROR(@"Failed to send",@"Failed to send to twitter");	
////		}
////		else {
////			
////			// SHOW TWEET WINDOW
////			TweetViewController * controller = [[[TweetViewController alloc] initWithNibName:@"TweetViewController" bundle:nil] autorelease];
////			controller.picurl = picurl;
////			controller.engine = twitterUtils_.engine;
////			
////			controller.cancelAction = ^{ 
////				
////				[viewController_ dismissModalViewControllerAnimated:YES];
////			};
////			
////			UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
////			
////			if( gIsRunningOnIPad )
////			{
////				nav.modalPresentationStyle	 = UIModalPresentationFormSheet;
////			}
////			
////			[viewController_ presentModalViewController: nav animated:YES];
////			
////		}
//		
//		
//		TWTweetComposeViewController *controller = [[[TWTweetComposeViewController alloc] init] autorelease];
//		[controller addImage: image];
//		
//		
//		controller.completionHandler = ^(TWTweetComposeViewControllerResult result){ 
//			
//			if( result ==  TWTweetComposeViewControllerResultDone )
//				SHOW_ERROR(@"TwitterDone", @"");
//			if( result ==  TWTweetComposeViewControllerResultCancelled )
//				SHOW_ERROR(@"Cancelled", @"");
//			
//			
//			[viewController_ dismissModalViewControllerAnimated:YES]; };
//		
//		// UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:controller] autorelease];
//		
//
//		
//		[viewController_ presentModalViewController: controller animated:YES];
//	};
//	
//	
//	[[ActivityIndicator sharedIndicator] showIndicatorWithBlock:block
//												  cancelAction:nil
//												  hasIndicator:NO];
//	[ActivityIndicator sharedIndicator].titleLabel.text = NSLocalizedString(@"Preparing Tweet", @"");
//	
//}
//
//-(void)twitterSucceeded:(NSString*)identifier
//{
//	SHOW_ERROR(@"Posted to twitter",@"Posted to twitter Message");
//	
//	[[ActivityIndicator sharedIndicator] close];
//	
//	[viewController_ dismissModalViewControllerAnimated:YES];
//}
//
//
//-(void)twitterFailed:(NSError*)error
//{
//	NSString* message = [NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"Posting error", @""), [error localizedDescription]];
//	SHOW_ERROR(@"Error",message);
//	
//	[[ActivityIndicator sharedIndicator] close];
//	
//}
//
//-(void)saveToPicasa
//{
//#ifndef QUICK_ADD_BUILD
//
//	Reachability *internetReach = [Reachability reachabilityForInternetConnection];
//	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
//	if( netStatus == NotReachable )
//	{
//		SHOW_ERROR(@"Oops", @"The device needs to be online.");
//		return;
//	}
//	
//	
//	NSString* mimeType = [NSString stringWithFormat:@"image/%@", [self.iconObject.path pathExtension]];
//	
//	
//	
//	[[GoogleUtils sharedUtility] uploadImageAtPath:self.iconObject.path
//										  withName:[self.iconObject.path lastPathComponent]
//										  mimeType:mimeType
//								   completionBlock: ^(NSError* error, NSString* GPhotoID){ 
//									   
//									   if( error )
//									   {
//										   NSString* errorMessage = [error localizedDescription];
//										   SHOW_ERROR(@"Error",errorMessage);
//									   }else
//									   {
//										   SHOW_ERROR(@"Done",@"");
//										   
//									   }
//								   }];
//	
//#endif
//	
//}
//
//-(void)saveToEvernote
//{
//	Reachability *internetReach = [Reachability reachabilityForInternetConnection];
//	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
//	if( netStatus == NotReachable )
//	{
//		SHOW_ERROR(@"Oops", @"The device needs to be online.");
//		return;
//	}
//	
//	
//	NSString* mimeType = @"application/data";
//	
//	if( self.iconObject.isPhoto )
//	{
//		NSString* extension = [[self.iconObject.path pathExtension] lowercaseString];
//		
//		if( [extension isEqualToString:@"jpg"] ) extension = @"jpeg";
//		
//		mimeType = [NSString stringWithFormat:@"image/%@", extension];
//	}
//	
//	if( self.iconObject.isMp3 )
//		mimeType = @"audio/mpeg";
//	
//	NSString* filename = [self.iconObject.path lastPathComponent];
//	
//	[EvernoteUtils postFileAt:self.iconObject.path mime:mimeType title:filename creationDate:[NSDate date] tags:nil addGeoTag:YES ];
//	
//	
//}

//#pragma mark - Aviary
//
//#ifndef QUICK_ADD_BUILD
//
//- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)anImage
//{
//	
//	NSData* data = UIImageJPEGRepresentation(anImage, 1.0);
//
//	void(^block)(NSURL*) = ^(NSURL* newParentURL){
//		
//		MNLOG(@"%@ %@ %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__);
//		
//		
//		//		NSString* name = [self.iconObject.path lastPathComponent];
//		//		
//		//		if( ![[self.iconObject.path lastPathComponent] hasPrefix:@"Edited "] )
//		//		{
//		//			name = [NSString stringWithFormat:@"Edited %@", name];
//		//			
//		//		}
//		//		
//		//		NSString* newPath = [[[self.iconObject.path stringByDeletingLastPathComponent] stringByAppendingPathComponent: name] uniquePathForFolder];
//		
//		NSURL* urlToWrite = [newParentURL URLByAppendingPathComponent: [self.iconObject.path lastPathComponent]];
//		if( [data writeToFile:urlToWrite.path atomically:NO] )
//		{
//			
//			// Completed, update spotlight data after 0.2 on main thread
//			double delayInSeconds = 0.2;
//			dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//			dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//				
//				MNFileObject *document = [[[MNFileObject alloc] initWithFileURL:urlToWrite] autorelease];
//				
//				[self.iconObject setUserInfoCache:nil];
//				[self.iconObject.supericon updateSubicons];
//
//				[document updateSpotlightMetadata];
//				[document updateLocalNotifications];
//				
//				[[MNSpotlightServer sharedServer] saveContext];
//				[self.viewController.tableView reloadData];
//				
//				[editor dismissModalViewControllerAnimated:YES];
//
//			});
//		}else
//		{
//			[editor dismissModalViewControllerAnimated:YES];
//		}
//		
//	};
//	
//	
//	[self.iconObject.supericon addingSubItemWithBlock: block onCompletion: ^(BOOL finished){}];
//	
//
//}
//
//- (void)photoEditorCanceled:(AFPhotoEditorController *)editor
//{
//	MNLOG(@"%@ %@ %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__);
//	[editor dismissModalViewControllerAnimated:YES];
//
//}
//
//#endif


@end
