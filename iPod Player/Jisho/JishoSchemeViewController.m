//
//  JishoSchemeViewController.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 4/09/12.
//
//

#import "JishoSchemeViewController.h"
#import "JishoSchemeEditorViewController.h"

@interface JishoSchemeViewController ()

@end

@implementation JishoSchemeViewController

- (id)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
		 NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];

		 NSArray* schemes = [ud objectForKey:@"DictionarySchemes"];
		 
		 schemes_ = [[NSMutableArray alloc] init];
		 
		 if( schemes )
			 [schemes_ addObjectsFromArray: schemes];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	
	self.title = @"Schemes";
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)];
	
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	if( editing )
	{
		self.navigationItem.rightBarButtonItem = nil;
	}else
	{
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)];

		
	}
	
	[super setEditing:editing animated:animated];
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

-(void)close
{
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];

	[ud setObject:schemes_ forKey:@"DictionarySchemes"];
	
	if( [self.delegate respondsToSelector:@selector(jishoSchemeViewControllerDidEnd:)] )
	{
		[self.delegate performSelector:@selector(jishoSchemeViewControllerDidEnd:)
								  withObject:nil];
	}
	
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	if( section == 1 )
	{
	return NSLocalizedString(@"JishoSchemeFooter", @"");
	}
	
	return nil;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	
	
	return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	
	// Do not display on startup
	
	if( 1 == section ) return 1;
	
	return schemes_.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	if( indexPath.section == 1 )
	{
		UITableViewCell *	cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		
		cell.textLabel.textAlignment = UITextAlignmentCenter;
		cell.textLabel.text = @"Add New";
		
		
		return cell;
	}
	
	
	static NSString *CellIdentifier = @"SchemeCell";
	
	UITableViewCell *	cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	
	NSDictionary* dict = [schemes_ objectAtIndex: indexPath.row];
	

		cell.textLabel.text = [dict objectForKey:@"title"];
		
	NSString* schemeString = [dict objectForKey:@"scheme"];
	
		cell.detailTextLabel.text = schemeString;

	
	return cell;
	
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 1 )
	{
		NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:@"http://www.google.com/search?q=<#word#>", @"scheme", @"Search Google", @"title", [NSNumber numberWithBool:YES], @"encode", nil];
		
		[schemes_ addObject: dict];
		
		JishoSchemeEditorViewController* controller = [[JishoSchemeEditorViewController alloc] init];
		
		controller.sourceDictionary = dict;
		controller.delegate = self;
		
		[self.navigationController pushViewController:controller animated:YES];
		
		[self.tableView reloadData];
	}
	
	
	if( indexPath.section == 0 )
	{
		JishoSchemeEditorViewController* controller = [[JishoSchemeEditorViewController alloc] init];
		
		controller.sourceDictionary = [schemes_ objectAtIndex: indexPath.row];
		controller.delegate = self;
		
		[self.navigationController pushViewController:controller animated:YES];
	}
	
}

-(void)jishoSchemeEditorViewControllerDidEnd:(JishoSchemeEditorViewController*)controller titleAndScheme:(NSDictionary*)dictionary
{
	NSString* title = [dictionary objectForKey:@"title"];
	NSString* scheme = [dictionary objectForKey:@"scheme"];
	NSNumber* encode = [dictionary objectForKey:@"encode"];
	
	if( scheme == nil ) scheme = @"";
	if( title == nil ) title = @"";
	if( encode == nil ) encode = [NSNumber numberWithBool:NO];
	
	NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:scheme, @"scheme", title, @"title", encode, @"encode", nil];
	
	NSUInteger index = [schemes_ indexOfObject: controller.sourceDictionary];
	[schemes_ replaceObjectAtIndex:index withObject:dict];
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
	[self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
	
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 0 ) return YES;
	
	return NO;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( indexPath.section == 0 ) return YES;
	
	return NO;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	NSDictionary *dict = [schemes_ objectAtIndex:fromIndexPath.row];
	[schemes_ removeObjectAtIndex:fromIndexPath.row];
	[schemes_ insertObject:dict atIndex:toIndexPath.row];
	

}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if( UITableViewCellEditingStyleDelete == editingStyle )
	{
		[schemes_ removeObjectAtIndex: indexPath.row];
		[self.tableView deleteRowsAtIndexPaths:@[indexPath]
									 withRowAnimation:UITableViewRowAnimationAutomatic];
	}
	
}

@end
