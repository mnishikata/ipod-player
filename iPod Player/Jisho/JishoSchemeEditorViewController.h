//
//  JishoSchemeEditorViewController.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 4/09/12.
//
//

#import <UIKit/UIKit.h>

@interface JishoSchemeEditorViewController : UIViewController


- (IBAction)schemeButton:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextField *scheme;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *insertButton;

@property (weak, nonatomic) id delegate;
@property (weak, nonatomic) NSDictionary* sourceDictionary;
@end
