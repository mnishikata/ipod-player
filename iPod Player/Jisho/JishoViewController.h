//  MNViewController.h
//  Jisho
//  Copyright (c) 2011 Catalystwo Limited, New Zealand. All rights reserved.


#import <UIKit/UIKit.h>
#import <UIKit/UITextChecker.h>

@class JishoSchemeViewController;

@interface JishoViewController : UIViewController  <UIApplicationDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIViewControllerRestoration>
{
	UITextChecker* 	textChecker_;
	
	NSArray* gusses_;
	BOOL guessing_;
	BOOL hasDefinition_;
	
	NSMutableArray* schemes_; // Dictionary array where dictionary {title, scheme, open in Safari}
	
	NSIndexPath *tappedIndexPath_;
	
	BOOL viewAppeared_;
	
	double dispatch_signature_;
	
	CGFloat scale_;
	NSTimer *scaleTimer_;
	BOOL pinching_;
	
	// Flags
	BOOL startingUp_;
	BOOL fistLaunch_;
	
	
	BOOL showHistoryCount_;
	
	// User interface
	
	UIBarButtonItem* exportBarButtonItem_;
	
	
	NSString *sortOrder_;
	
	NSInteger pinchStartVal_;

}

+ (UIViewController *) viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder;
- (id)init;
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder;
- (void)decodeRestorableStateWithCoder:(NSCoder *)coder;
-(NSString*)title;
- (void)close;
-(void)saveUD;
-(void)dealloc;
-(void)viewDidLoad;
-(void)reloadView;
-(void)loadView;
- (void)viewWillAppear:(BOOL)animated;
- (void)viewDidAppear:(BOOL)animated;
-(void)viewWillDisappear:(BOOL)animated;
- (void)viewDidDisappear:(BOOL)animated;
- (NSUInteger)supportedInterfaceOrientations;
- (void)keyboardWasShown:(NSNotification*)aNotification;
- (void)keyboardWillBeHidden:(NSNotification*)aNotification;
-(NSArray*)toolbarItems;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView ;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section ;
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath ;
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath ;
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath ;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)jishoSchemeViewControllerDidEnd:(JishoSchemeViewController*)controller;
-(void)presentReferenceLibrary:(NSString*)term addToHistory:(BOOL)addToHistory;
-(void)scaleTimer:(NSTimer*)timer;
-(void)swipe:(UISwipeGestureRecognizer*)gesture;
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(NSDictionary*)folderDetailedAttributes;
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)aSearchText;
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
- (void)searchBar:(UISearchBar *)aSearchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope;
- (void)searchBarBookmarkButtonClicked:(UIBarButtonItem *)aSearchBar;
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
-(void)pinch:(UIPinchGestureRecognizer*)gesture;
- (NSUInteger)supportedInterfaceOrientations;


@property (weak, nonatomic) UISearchBar *searchBar;
@property (weak, nonatomic) UITableView *tableView;

@property (nonatomic, strong) NSString* searchText;



@end
