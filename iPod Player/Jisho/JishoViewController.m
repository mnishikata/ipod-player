//
//  MNViewController.m
//  Jisho
//
//  Created by Nishikata Masatoshi on 15/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "JishoViewController.h"
#import "MNTextRenderer.h"
#import "MNCTFontUtils.h"
#import "MNCoreTextDefinitions.h"
#import "MNCheckboxAttachmentCell.h"
#import "NSAttributedString+Serialization.h"
#import "KeychainItemWrapper.h"
#import "NSDate+Extension.h"
#import <CoreText/CoreText.h>
#import "ActivityIndicator.h"
#import "HistoryTableViewCell.h"
#import "Tui.h"
#import "JishoSchemeViewController.h"
#import "MNBaseNavigationController.h"
#import "MyApplication.h"

/*
 sceheme
 
 http://www.google.com/search?q=%@
 
 */

#define IOS7_HIGHLIGHT_COLOR RGBA(41, 132, 254, 1)

CGFloat gDocumentScaling = 1.0;


static dispatch_queue_t search_dispatch_queue_ = nil;

BOOL gEnglishMode = NO; // DUMMY.... DOES NOT WORK ON iOS6


@implementation JishoViewController
@synthesize searchBar;
@synthesize tableView;
@synthesize searchText;



//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Release any cached data, images, etc that aren't in use.
//}

#define MAX_SCALE  (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad?10.0:2.0)

#pragma mark - View lifecycle

extern BOOL gEnglishMode;

static NSString* SearchTextKey = @"SearchText";

static NSString* fontNameKey = @"FontName";
static NSString* EnglishModeKey = @"EnglishMode";
//static NSString* InternationalModeKey = @"InternationalMode";
static NSString* HideScopeBarKey = @"HideScopeBar";


+ (UIViewController *) viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
	JishoViewController* controller = nil;
	
	
	controller = [[JishoViewController alloc] init];
	
	return controller;
}




- (id)init
{
	
	
	self = [super initWithNibName:nil bundle:nil];
	if (self) {
		// Test; reset evernote account
		//[EvernoteUtils setAccount:@"" password:@""];
		
		NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
		// When first launch
		if( ![ud objectForKey:EnglishModeKey] )
		{
			if( [NSLocalizedString(@"MyLocale", @"") isEqualToString:@"ja"] )
			{
				[ud setBool:NO forKey:HideScopeBarKey];
				[ud setBool:NO forKey:EnglishModeKey];
				[ud setObject:@"HiraMinProN-W3" forKey:@"FontName"];
				
			}
			else
			{
				[ud setBool:YES forKey:HideScopeBarKey];
				[ud setBool:YES forKey:EnglishModeKey];
			}
			
		}
		
		[[NSUserDefaults standardUserDefaults] synchronize];
		
		
		
//		NSString* aSearchText = [ud objectForKey:SearchTextKey];
//		
//		self.searchText = aSearchText;
		
		self.searchText = nil;
		
		
		NSArray* schemes = [ud objectForKey:@"DictionarySchemes"];
		
			schemes_ = [[NSMutableArray alloc] init];
		
		if( schemes )
			[schemes_ addObjectsFromArray: schemes];
		
		
		
		scale_ = [ud floatForKey:@"DictionaryScale"];
		
		if( scale_ <= 0 ) scale_ = 1.0;
		if( scale_ > MAX_SCALE ) scale_= 1.0;
		
		
		
		[self reloadView];
		
		self.restorationIdentifier = @"JishoViewControllerID";
		self.restorationClass = [self class];

	}
	return self;
}

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
	
	[super encodeRestorableStateWithCoder:coder];
	
	[coder encodeObject:self.searchText forKey:@"searchText"];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{

	[super decodeRestorableStateWithCoder:coder];
	
	self.searchText = [coder decodeObjectForKey:@"searchText"];
	[self reloadView];
	
	[APP_DELEGATE setShouldShowJisho:self];
	
		
}



-(NSString*)title
{
	return NSLocalizedString(@"Dictionary", @"");
}


//- (void)applicationWillResignActive:(UIApplication *)application
//{
//	/*
//	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//	 */
//}

- (void)close
{
	
	[self saveUD];
	
	[self dismissViewControllerAnimated:YES completion:nil];
}



-(void)saveUD
{
	
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	
	[ud setObject:searchText forKey:SearchTextKey];
	//	[ud setObject:history forKey:HistoryKey];
	[ud setBool:gEnglishMode forKey:EnglishModeKey];
	[ud setBool: !searchBar.showsScopeBar forKey:HideScopeBarKey];
	[ud setFloat:scale_ forKey:@"DictionaryScale"];
	[ud synchronize];
}




-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NSPersistentStoreDidImportUbiquitousContentChangesNotification object:nil];
	
	[self setTableView:nil];
	[self setSearchBar:nil];
	
}

-(void)viewDidLoad
{
	[super viewDidLoad];

	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)];
	
	
//	UIBarButtonItem* bookmark = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(searchBarBookmarkButtonClicked:)];
//	
//	self.navigationItem.leftBarButtonItem = bookmark;
	
	
//	UIButton* button = [UIButton buttonWithType:UIButtonTypeInfoLight];
//	[button addTarget:self action:@selector(languageInformation) forControlEvents:UIControlEventTouchUpInside];
//	
//	UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:button];
//	self.navigationItem.leftBarButtonItem = item;
}


-(void)languageInformation
{
	SHOW_ERROR_ARC(@"", @"JishoLanguageInformation");
}

-(void)reloadView
{

	
	CGRect frame = self.view.bounds;
	
	[self.tableView removeFromSuperview];
	
	
	UITableView* aTableView = [[UITableView alloc] initWithFrame:frame];
	aTableView.delegate = (id <UITableViewDelegate> )self;
	aTableView.dataSource = (id <UITableViewDataSource> )self;
	aTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
	
	
	
	UISearchBar *bar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	
	NSString* fontname = [[NSUserDefaults standardUserDefaults] objectForKey:fontNameKey];
	

	
	showHistoryCount_ = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShowWordViewCount"];
	
	
	bar.autocorrectionType = UITextAutocorrectionTypeNo;
	bar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	bar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	bar.delegate = (id <UISearchBarDelegate> )self;
	
	
	if( 0 )
		//if( [NSLocalizedString(@"MyLocale", @"") isEqualToString:@"ja"] )
	{
		gEnglishMode = [[NSUserDefaults standardUserDefaults] boolForKey:EnglishModeKey];
		
		bar.scopeButtonTitles = [NSArray arrayWithObjects: NSLocalizedString(@"Japanese Mode", @""), NSLocalizedString(@"English Mode", @""), nil];
		[bar setSelectedScopeButtonIndex: gEnglishMode?1:0 ];
		
		BOOL hideScopeBar = [[NSUserDefaults standardUserDefaults] boolForKey:HideScopeBarKey];
		
		////
		///
		////
		///
		
		/// iOS6 Dictionary choosing does not work.
		hideScopeBar = YES;
		
		////
		///
		////
		///
		
		bar.showsScopeBar = !hideScopeBar;
//		bar.showsBookmarkButton = YES;
		
		[bar sizeToFit];
		[aTableView setNeedsLayout];
		
		
		//		bar.showsBookmarkButton = YES;
		//		[bar setImage:[UIImage imageNamed:@"Gear.png"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
		//		[bar setPositionAdjustment:UIOffsetMake(-3, -1) forSearchBarIcon:UISearchBarIconBookmark];
		
		
		
	}else
	{
		gEnglishMode = NO;
		
		bar.showsScopeBar = NO;
		bar.showsBookmarkButton = NO;
		
	}
	
	
	[self.view addSubview: aTableView];
	aTableView.tableHeaderView = bar;
	
	
	self.searchBar = bar;
	self.tableView = aTableView;
	
	searchBar.text = self.searchText;
	[self searchBar:searchBar textDidChange:searchBar.text];
	
	
	
	for(int i =0; i<[bar.subviews count]; i++) {
		
		UIView* subview = [bar.subviews objectAtIndex:i];
		
		
		if([subview isKindOfClass:[UITextField class]])
		{
			
			
			
			UIFont *font = [UIFont fontWithName:@"Baskerville" size:22];
			
			
			[(UITextField*)subview setFont:font];
		}
	}
	
	
}

-(void)loadView
{
	
	UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 320, 460)];
	view.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
	
	self.view = view;
	
	[self reloadView];
	

	
	
}




//-(void)check
//{
//	UIView* view = [self.window hitTest:CGPointMake(160, 240) withEvent:nil];
//	UIWebView* webView = view.superview.superview;
//
//BOOL flag = [webView isLoading];
//	NSLog(@"is loading %d",flag);
//}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	viewAppeared_ = YES;
	
	
	if( scaleTimer_ )
	{
		[scaleTimer_ invalidate];
		scaleTimer_ = nil;
	}
	
	if( [[NSUserDefaults standardUserDefaults] boolForKey:@"ClearWhenDone"] )
	{
		//searchBar.text =  nil;
		
	}else
	{
		searchBar.text = self.searchText;
	}

	[self.tableView reloadData];
	
	if( tappedIndexPath_ )
		[self.tableView selectRowAtIndexPath:tappedIndexPath_ animated:NO scrollPosition:UITableViewScrollPositionMiddle];
	
	
	//[self registerForKeyboardNotifications];
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	
	[nc addObserver:self
			 selector:@selector(keyboardWasShown:)
				  name:UIKeyboardDidShowNotification object:nil];
	
	[nc addObserver:self
			 selector:@selector(keyboardWillBeHidden:)
				  name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	
	if( tappedIndexPath_ )
	{
		[self.tableView deselectRowAtIndexPath:tappedIndexPath_ animated:YES];
		tappedIndexPath_ = nil;
	}
	if( searchBar.text.length == 0 )
		[self.searchBar becomeFirstResponder];
	
	[self performSelector:@selector(scaleTimer:) withObject:nil afterDelay:0.0];

}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	viewAppeared_ = NO;
	
}


- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
	
	
	
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

-(BOOL)automaticallyAdjustsScrollViewInsets
{
	return YES;
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
	NSDictionary* info = [aNotification userInfo];
	CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
	
	UIEdgeInsets contentInsets;
	BOOL iOS7 = ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"6"];
	
	if( iOS7 )
	{
		contentInsets = tableView.contentInset;
		contentInsets = UIEdgeInsetsMake(contentInsets.top, 0.0, kbSize.height, 0.0);
		
	}else
	{
		contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
	}
	
	tableView.contentInset = contentInsets;
	tableView.scrollIndicatorInsets = contentInsets;
	
	
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	UIEdgeInsets contentInsets;
	BOOL iOS7 = ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"6"];
	
	if( iOS7 )
	{
		contentInsets = tableView.contentInset;
		contentInsets = UIEdgeInsetsMake(contentInsets.top, 0.0, 0, 0.0);
		
	}else
	{
		contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0, 0.0);
	}
	
	tableView.contentInset = contentInsets;
	tableView.scrollIndicatorInsets = contentInsets;
}

//-(NSArray*)toolbarItems
//{
//	
//	
//	UIBarButtonItem* bookmark = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(searchBarBookmarkButtonClicked:)];
//	
//	exportBarButtonItem_ = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(export:)];
//	
//	UIBarButtonItem* flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//	
//	return [NSArray arrayWithObjects: bookmark, flexible, exportBarButtonItem_, nil];
//}
//
//


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	
	
	return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	
	// Do not display on startup
	
	if( section == 0 ) return hasDefinition_?1:0;
	if( section == 1 ) return (guessing_?1:[gusses_ count]);
	if( section == 2 ) return (self.searchText.length > 0 )? schemes_.count +1 : 0;
	return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if( hasDefinition_ && section == 0 )
	{
		if( UIInterfaceOrientationIsLandscape(self.interfaceOrientation) &&
			( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ) ) return nil;
		
		return NSLocalizedString(@"Match",@"");
	}
	
	
	if( ([gusses_ count] > 0 || guessing_ ) && section == 1 ) return NSLocalizedString(@"Did you mean?",@"");

	
	if( section == 2 ) return (self.searchText.length > 0 )?@"Search other sources":nil;
	
	
	return nil;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
	// The table view should not be re-orderable.
	return NO;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	// The table view should not be re-orderable.

	
	return NO;
}




static CTFontRef ctfont_ = nil;


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	static NSString *HistoryCellIdentifier = @"HistoryCell";
	static NSString *FontName = @"";
	
	NSString* fontname = [[NSUserDefaults standardUserDefaults] objectForKey:fontNameKey];
	
	if( !fontname ) fontname = @"HiraMinProN-W3";
	
	
	if( ![fontname isEqualToString: FontName] )
	{
		FontName = fontname;
		
		if( ctfont_ ) CFRelease(ctfont_);
		
		UIFont* font = [UIFont fontWithName:fontname size:24];
		
		if( !font ) font = [UIFont fontWithName:NSLocalizedString(@"FontName",@"") size:24];
		
		if( [fontname isEqualToString:@"Helvetica-Bold"] ) font = [UIFont fontWithName:fontname size:16];		
				 
		
		ctfont_ = [font createCTFont];
		
	}
	
		
	
	
	UIColor *textColor = [UIColor blackColor];
	
	NSString* text = @"";
	
	HistoryTableViewCell *cell = nil;
	
	if( indexPath.section == 2 )
		cell = [tableView dequeueReusableCellWithIdentifier:HistoryCellIdentifier];
	else
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	
	
	if (cell == nil)
	{
		cell = [[HistoryTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleGray;
	}
	
	
	// Configure the cell...
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.userInteractionEnabled = YES;
	
	if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
	{
		cell.indentationLevel = 1;
		cell.indentationWidth = 7;
	}
	
	if( indexPath.section == 0 )
	{
		textColor = IOS7_HIGHLIGHT_COLOR;
		text = self.searchText;
	}
	
	
	if( indexPath.section == 1 )
	{
		if( guessing_ )
		{
			textColor = [UIColor lightGrayColor];
			text = NSLocalizedString(@"Guessing...", @"");
			cell.userInteractionEnabled = NO;
			cell.accessoryType = UITableViewCellAccessoryNone;
			
		}else if( [gusses_ count] > 0 )
		{
			textColor = [UIColor blackColor];
			text = [gusses_ objectAtIndex:indexPath.row];
		}
	}
	
	
	if( indexPath.section == 2 )
	{
		UITableViewCell *	cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		
		cell.textLabel.textAlignment = UITextAlignmentCenter;
		cell.accessoryType = UITableViewCellAccessoryNone;
		
		
		if( indexPath.row == schemes_.count )
		{
			cell.textLabel.text = @"Edit Schemes";
			cell.textLabel.font = [UIFont systemFontOfSize:18];
			cell.textLabel.textColor = IOS7_HIGHLIGHT_COLOR;
			
		}else
		{
			
			textColor = [UIColor blackColor];
			
			NSDictionary* dict = [schemes_ objectAtIndex: indexPath.row];
			
			cell.textLabel.text = [dict objectForKey:@"title"];
		}
		
		return cell;
	}
	
	
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys: (__bridge id)ctfont_, MNCTFontAttributeName, textColor.CGColor, MNCTForegroundColorAttributeName, nil];
	
	
	if( text == nil ) text = @"";
	
	NSAttributedString* attr = [[NSAttributedString alloc] initWithString:text attributes: attributes];
	
	[cell setAttributedString: attr];
	
	return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

	
	tappedIndexPath_ = indexPath;
	
	if( indexPath.section == 0 )
	{
		BOOL hasDef = [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:self.searchText];
		
		if( !hasDef )
		{
			tappedIndexPath_ = nil;
		}
		
		[self presentReferenceLibrary:self.searchText addToHistory:YES];
	}
	
	
	if( indexPath.section == 1 )
	{
		[self presentReferenceLibrary:[gusses_ objectAtIndex:indexPath.row] addToHistory:YES];
	}
	
	
	if( indexPath.section == 2 )
	{
		if( indexPath.row == schemes_.count )
		{
			JishoSchemeViewController* controller = [[JishoSchemeViewController alloc] init];
			controller.delegate = self;
			MNBaseNavigationController* nav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
			nav.supportedInterfaceOrientations = UIInterfaceOrientationMaskAll;
			
			[self presentViewController:nav animated:YES completion:nil];
			
		}else{
			
			NSDictionary* dict = [schemes_ objectAtIndex: indexPath.row];
			
			NSString* scheme = [dict objectForKey:@"scheme"];
			
			NSMutableString* mstr = [NSMutableString stringWithString: scheme];
			
			[mstr replaceOccurrencesOfString:@"<#word#>" withString:@"%@" options:0 range:NSMakeRange(0, mstr.length)];
			
			NSString* encodedSearchString = [self.searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			
			
			NSString* string = [NSString stringWithFormat:mstr, encodedSearchString];
			
			
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
			
			[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
		}
	}
	
}

-(void)jishoSchemeViewControllerDidEnd:(JishoSchemeViewController*)controller
{
	NSArray* schemes = [[NSUserDefaults standardUserDefaults]  objectForKey:@"DictionarySchemes"];
	
	schemes_ = [[NSMutableArray alloc] init];
	
	if( schemes )
		[schemes_ addObjectsFromArray: schemes];
	
	tappedIndexPath_ = nil;
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//	return 20;
//}

-(void)presentReferenceLibrary:(NSString*)term addToHistory:(BOOL)addToHistory
{
	
//#define PREF_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"../../Library/Preferences/"]
//	NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:[PREF_FOLDER stringByAppendingPathComponent:@"com.apple.Preferences.plist"]];
//
//	NSLog(@"before dict %@",dict);
//
//	[dict setObject:@"ja_JP-Romaji@sw=QWERTY-Japanese;hw=US" forKey:@"KeyboardsCurrentAndNext"];
//	[dict writeToFile:[PREF_FOLDER stringByAppendingPathComponent:@"com.apple.Preferences.plist"] atomically:YES];
//
//	NSLog(@"after dict %@",dict);

	/*
	 "ja_JP-Kana@sw=Kana;hw=US",
	 "en_US@hw=US;sw=QWERTY"
	 
	 
	 */
	// "en_US@hw=US;sw=QWERTY"
	
	
	
	UIReferenceLibraryViewController *controller = nil;
	
	@try {
		controller = [[UIReferenceLibraryViewController alloc] initWithTerm:term];
		
	}
	@catch (NSException *exception) {
		controller = [[UIReferenceLibraryViewController alloc] initWithTerm:term];
		
	}
	
	
	
	[self presentViewController:controller animated:YES completion:^{
	
		// Add history after presenting modal view
		
		
		UIPinchGestureRecognizer* pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
		
		[controller.view addGestureRecognizer: pinchGesture];
		
		
		UISwipeGestureRecognizer* gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
		gesture.direction = UISwipeGestureRecognizerDirectionRight;
		
		[controller.view addGestureRecognizer:gesture];
		
		
		[self performSelector:@selector(scaleTimer:) withObject:nil afterDelay:0.05];
		
		if( scaleTimer_ )
		{
			[scaleTimer_ invalidate];
			scaleTimer_ = nil;
		}
		
		scaleTimer_ = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(scaleTimer:) userInfo:nil repeats:YES];
		
	}];
	
	

	
}

-(void)scaleTimer:(NSTimer*)timer
{
	
	if( [[UIApplication sharedApplication] applicationState] != UIApplicationStateActive )
		return;
	
	
	if( pinching_ ) return;
	
	UIView* view = [[APP_DELEGATE window] hitTest:CGPointMake(160, 240) withEvent:nil];
	UIView* webView = view.superview.superview;
	
	
	NSString* js = [NSString stringWithFormat:@"document.body.style.fontSize = \"%.0f%%\"", scale_*100];
	
	

	
	if( [webView respondsToSelector:@selector(stringByEvaluatingJavaScriptFromString:)] )
		[webView performSelector:@selector(stringByEvaluatingJavaScriptFromString:) withObject:js ];
	
	
}

-(void)swipe:(UISwipeGestureRecognizer*)gesture
{
	[self dismissViewControllerAnimated:YES completion:nil];
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString* typeString = [[NSUserDefaults standardUserDefaults] objectForKey:@"ShowHistoryDate"];
	
	
	
	if( indexPath.section != 2 )
	{
		return 44;
	}
	
	if( [typeString isEqualToString:@"CreationAndViewedDate"] )
	{
		return 70;
		
	}
	
	else if( [typeString isEqualToString:@"ViewedDate"] || [typeString isEqualToString:@"CreationDate"] )
		
	{
		
		return 60;
		
	}
	
	return  44;
}


#pragma mark -
#pragma mark Fetched results controller delegate


-(NSDictionary*)folderDetailedAttributes
{
	static NSDictionary *FolderDetaileAttributes = nil;
	if( FolderDetaileAttributes ) return FolderDetaileAttributes;
	
	NSMutableDictionary* attributes = [NSMutableDictionary dictionary];
	[attributes setObject:(id)[UIColor whiteColor].CGColor forKey:MNCTForegroundColorAttributeName];
	
	UIFont *font = [UIFont boldSystemFontOfSize:16];
	CTFontRef ctfont = [font createCTFont];
	[attributes setObject:(__bridge id)ctfont forKey:MNCTFontAttributeName];
	
	CFRelease(ctfont);
	
	FolderDetaileAttributes = attributes;
	
	return attributes;
}






#pragma mark -
#pragma mark Search bar delegate



- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)aSearchText
{
	if( [aSearchText isEqualToString:@""] ) aSearchText = nil;
	
	self.searchText = aSearchText;
	
	if( aSearchText == nil  )
	{
		gusses_ = nil;
		hasDefinition_ = NO;
		
		if( viewAppeared_ )
			[self.tableView reloadData];
		return;
	}
	
	
	guessing_ = YES;
	hasDefinition_ = NO;
	[self.tableView reloadData];
	
	//[self.tableView reloadData];
	
	if( search_dispatch_queue_ == nil )
		search_dispatch_queue_ = dispatch_queue_create("com.catalystwo.JishoPlus", DISPATCH_QUEUE_SERIAL);
	
	
	
	dispatch_async(search_dispatch_queue_, ^{
		
		
		dispatch_signature_ = [NSDate timeIntervalSinceReferenceDate];
						
		double mySignature = dispatch_signature_;
		
		
		BOOL hasDef = NO;
		
		@try {
			hasDef = [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:aSearchText];
			
		}
		@catch (NSException *exception) {
			hasDef = [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:aSearchText];
			
		}
		
		if( viewAppeared_ && hasDef )
		{
			hasDefinition_ = hasDef;
			[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
		}
		
				
		
		if( textChecker_ == nil )
		{
			textChecker_ = [[UITextChecker alloc] init];
		}
		
		NSString* language = [[UITextChecker availableLanguages] objectAtIndex:0];
		if( !language ) language = @"en_US";
		
		
		
		NSArray* guessedArray = nil;
		NSArray *possibleCompletions = nil;
		
		if( self.searchText.length > 1 )
		{
			@autoreleasepool {
				
				@synchronized(textChecker_)
				{
					guessedArray = [textChecker_ guessesForWordRange:NSMakeRange(0, self.searchText.length) inString:self.searchText language:language];
					
					possibleCompletions = [textChecker_ completionsForPartialWordRange:NSMakeRange(0, self.searchText.length)
																								 inString:self.searchText language:language];
					
					
#if TARGET_IPHONE_SIMULATOR
					usleep(1000000);
#endif
				}
			}
		}
		
		
		
		NSMutableArray *array = [[NSMutableArray alloc] init];
		
		
		
		for( NSString* str in guessedArray )
		{
			if( mySignature != dispatch_signature_ )
				break;
			
				
			if( aSearchText != self.searchText ) break;
			
			if( ![str isEqualToString:self.searchText] )
//				if( [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:str] )
				{
					[array addObject: str];
				}
			
		}
		
		
		
		for( NSString* str in possibleCompletions )
		{
			if( mySignature != dispatch_signature_ )
				break;
			
			if( aSearchText != self.searchText  ) break;
			
			if( ![str isEqualToString:self.searchText] )
//				if( [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:str] )
				{
					[array addObject: str];
				}
		}
		
		
		if( aSearchText == self.searchText  && mySignature == dispatch_signature_ )
		{
			@autoreleasepool {
				
				@synchronized(self)
				{
					gusses_ = array;
					guessing_ = NO;
					hasDefinition_ = hasDef;
					
					if( viewAppeared_ )
						[self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
				}
			}
		}
		
	});
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	
	
	BOOL hasDef = [UIReferenceLibraryViewController dictionaryHasDefinitionForTerm:self.searchText];
	
	if( hasDef )
	{
		[self presentReferenceLibrary:self.searchText addToHistory:YES];
		
	}else
	{
		//1 Wait for search_dispatch_queue_
		
		dispatch_sync(search_dispatch_queue_, ^{});
		
		//2 Choose from Guess
		if( [gusses_ count] > 0 )
			[self presentReferenceLibrary:[gusses_ objectAtIndex:0] addToHistory:YES];
		
	}
	
	
}

- (void)searchBar:(UISearchBar *)aSearchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
	if( selectedScope == 0 ) gEnglishMode = NO;
	else gEnglishMode = YES;
	
	[self searchBar: searchBar textDidChange:searchBar.text];
}


- (void)searchBarBookmarkButtonClicked:(UIBarButtonItem *)aSearchBar
{
	if( [MNBlockActionSheet sheetIsVisible] ) return;
	
	
	MNBlockAlertView *sheet = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"Choose Dictionary",@"") message:@""];
	
	NSString* japaneseTitle = NSLocalizedString(@"Japanese Mode" ,@"");
	
	if( !gEnglishMode ) japaneseTitle = [japaneseTitle stringByAppendingString: NSLocalizedString(@"Check Mark", @"")];
	
	[sheet addTitle: japaneseTitle actionBlock:^{
		
		gEnglishMode = NO;
		
		[self searchBar: searchBar textDidChange:searchBar.text];
		
		[searchBar setSelectedScopeButtonIndex:0];
		
	} cancel:NO];
	
	NSString* englishTitle = NSLocalizedString(@"English Mode" ,@"");
	
	if( gEnglishMode ) englishTitle = [englishTitle stringByAppendingString: NSLocalizedString(@"Check Mark", @"")];
	[sheet addTitle: englishTitle actionBlock:^{
		
		gEnglishMode = YES;
		
		[self searchBar: searchBar textDidChange:searchBar.text];
		
		[searchBar setSelectedScopeButtonIndex:1];
		
	} cancel:NO];
	
	if( searchBar.showsScopeBar )
	{
		
		[sheet addTitle: NSLocalizedString(@"Turn Off Scope Bar",@"") actionBlock:^{
			
			searchBar.showsScopeBar = NO;
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:HideScopeBarKey];
			[searchBar sizeToFit];
			
			self.tableView.tableHeaderView = searchBar;
			
		} cancel:NO];
	}else
	{
		[sheet addTitle: NSLocalizedString(@"Toggle Scope Bar",@"") actionBlock:^{
			
			searchBar.showsScopeBar = YES;
			[[NSUserDefaults standardUserDefaults] setBool:!NO forKey:HideScopeBarKey];
			[searchBar sizeToFit];
			
			self.tableView.tableHeaderView = searchBar;
			
			
		} cancel:NO];
	}
	
	[sheet addTitle: NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
	
	//	CGRect rect = [searchBar.superview convertRect:searchBar.frame toView:self.view];
	
	[sheet show];
	
}




- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[searchBar resignFirstResponder];
}


#pragma mark - Actions


-(void)pinch:(UIPinchGestureRecognizer*)gesture
{
	pinching_ = YES;
	
	UIView* view = [gesture.view.window hitTest:CGPointMake(160, 240) withEvent:nil];
	UIView* webView = view.superview.superview;
	
	
	CGFloat currentScale = scale_ * gesture.scale;
	if( currentScale > MAX_SCALE ) currentScale = MAX_SCALE;
	if( currentScale < 0.5 ) currentScale = 0.5;
	
	NSString* js = [NSString stringWithFormat:@"document.body.style.fontSize = \"%.0f%%\"", currentScale * 100];
	

	
	if( [webView respondsToSelector:@selector(stringByEvaluatingJavaScriptFromString:)] )
	{
		[webView performSelector:@selector(stringByEvaluatingJavaScriptFromString:) withObject:js];
	}
	else
	{
		// iOS 7
		
		MyApplication* myApp = (MyApplication*)[UIApplication sharedApplication];
		
		NSInteger size = [myApp overrideFontSize];
		
		
		if( gesture.state == UIGestureRecognizerStateBegan )
		{
			pinchStartVal_ = size;
		}
		
		
		
		
		size = (NSInteger) ( (CGFloat)(pinchStartVal_+1) * gesture.scale ) - 1;
		
		
		[myApp setOverrideFontSize: size];
	
		
		
	}
	
	if( gesture.state == UIGestureRecognizerStateEnded )
	{
		scale_ = currentScale;
		pinching_ = NO;
	}
	
	
}





@end




@interface UIReferenceLibraryViewController (extension)

@end

@implementation UIReferenceLibraryViewController (extension)

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

@end

//@interface UIWebView (Extension)
//@end
//
//@implementation UIWebView (Extension)
//- (void)loadHTMLString:(NSString *)string baseURL:(NSURL *)baseURL
//{
//	NSLog(@"string %@",string);
//
//	NSData* data = [string dataUsingEncoding:NSUTF8StringEncoding];
//	[self loadData:data MIMEType:@"text/html" textEncodingName:@"utf-8" baseURL:baseURL];
//}
//@end
//
