//
//  JishoSchemeEditorViewController.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 4/09/12.
//
//

#import "JishoSchemeEditorViewController.h"

@interface JishoSchemeEditorViewController ()

@end

@implementation JishoSchemeEditorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
	self.navigationItem.title = @"Edit";
	self.insertButton.enabled = NO;
	
	
	self.textView.text = NSLocalizedString(@"JishoSchemeDescription", @"");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
	[self setTitleField:nil];
	[self setScheme:nil];
	[self setTextView:nil];
	[self setInsertButton:nil];
	[super viewDidUnload];
}
- (IBAction)schemeButton:(id)sender {
	
	[self.scheme insertText:@"<#word#>"];
	
}


- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}


-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.titleField.text = [self.sourceDictionary objectForKey:@"title"];
	
	self.scheme.text = [self.sourceDictionary objectForKey:@"scheme"];

}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	if( [self.delegate respondsToSelector:@selector(jishoSchemeEditorViewControllerDidEnd:titleAndScheme:)] )
	{
		[self.titleField resignFirstResponder];
		[self.scheme resignFirstResponder];
		
		NSString *title = self.titleField.text;
		NSString* scheme = self.scheme.text;
		
//		[self.sourceDictionary setObject:title forKey:@"title"];
//		[self.sourceDictionary setObject:scheme forKey:@"scheme"];

		
		NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:scheme, @"scheme", title, @"title", nil];

		[self.delegate performSelector:@selector(jishoSchemeEditorViewControllerDidEnd:titleAndScheme:) withObject:self withObject:dict];
		
		
	}
	
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
	self.insertButton.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	self.insertButton.enabled = NO;
}

@end
