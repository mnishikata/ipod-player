//
//  MyApplication.m
//  Speak
//
//  Created by Masatoshi Nishikata on 27/08/13.
//  Copyright (c) 2013 Catalystwo. All rights reserved.
//

#import "MyApplication.h"

@implementation MyApplication
/*
 NSString *const UIContentSizeCategoryExtraSmall;
 NSString *const UIContentSizeCategorySmall;
 NSString *const UIContentSizeCategoryMedium;
 NSString *const UIContentSizeCategoryLarge;
 NSString *const UIContentSizeCategoryExtraLarge;
 NSString *const UIContentSizeCategoryExtraExtraLarge;
 NSString *const UIContentSizeCategoryExtraExtraExtraLarge;

 NSString *const UIContentSizeCategoryAccessibilityMedium;
 NSString *const UIContentSizeCategoryAccessibilityLarge;
 NSString *const UIContentSizeCategoryAccessibilityExtraLarge;
 NSString *const UIContentSizeCategoryAccessibilityExtraExtraLarge;
 NSString *const UIContentSizeCategoryAccessibilityExtraExtraExtraLarge;
 
 */

static NSInteger sCurrentFontSize = -1;

#define values @[ UIContentSizeCategoryExtraSmall, UIContentSizeCategorySmall, UIContentSizeCategoryMedium, UIContentSizeCategoryLarge, UIContentSizeCategoryExtraLarge, UIContentSizeCategoryExtraExtraLarge, UIContentSizeCategoryExtraExtraExtraLarge, UIContentSizeCategoryAccessibilityMedium, UIContentSizeCategoryAccessibilityLarge, UIContentSizeCategoryAccessibilityExtraLarge, UIContentSizeCategoryAccessibilityExtraExtraLarge, UIContentSizeCategoryAccessibilityExtraExtraExtraLarge]

-(NSInteger)overrideFontSize
{
	if( sCurrentFontSize == -1 )
	{
		NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:@"MyApp-sCurrentFontSize"];
		
		if( num )
			sCurrentFontSize = num.integerValue;
		else
			sCurrentFontSize = 2;
		
		sCurrentFontSize = MAX( sCurrentFontSize, 0 );
		sCurrentFontSize = MIN( sCurrentFontSize, 11);
		
	}
	
	return sCurrentFontSize;
}

-(void)setOverrideFontSize:(NSInteger)size
{
	
	sCurrentFontSize = size;
	
	sCurrentFontSize = MAX( sCurrentFontSize, 0 );
	sCurrentFontSize = MIN( sCurrentFontSize, 11);
	
	
	NSString* val = UIContentSizeCategoryMedium;
	
	val = values[sCurrentFontSize];
	
	
	NSDictionary* dict = @{ UIContentSizeCategoryNewValueKey : val, @"UIContentSizeCategoryTextLegibilityEnabledKey" : @0 };
	
	[[NSNotificationCenter defaultCenter] postNotificationName:UIContentSizeCategoryDidChangeNotification object:self userInfo:dict];
	
	[[NSUserDefaults standardUserDefaults] setInteger:sCurrentFontSize forKey:@"MyApp-sCurrentFontSize"];
}

-(NSString*)preferredContentSizeCategory
{
	if( sCurrentFontSize == -1 )
	{
		NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:@"MyApp-sCurrentFontSize"];
		
		if( num )
			sCurrentFontSize = num.integerValue;
		else
			sCurrentFontSize = 2;
		
		sCurrentFontSize = MAX( sCurrentFontSize, 0 );
		sCurrentFontSize = MIN( sCurrentFontSize, 11);

	}
	
	//　辞書のときしか呼ばれない？
	
	NSString* val = UIContentSizeCategoryMedium;
	val = values[sCurrentFontSize];

	return val;
}

@end
