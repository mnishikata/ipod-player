//
//  HelpViewController.h
//  BigFingerGL
//
//  Created by Masatoshi Nishikata on 10/01/05.
//  Copyright 2010 Masatoshi Nishikata. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HelpViewController : UIViewController <UIWebViewDelegate> {
	
	
	//	NSString* title;
	NSString* helpFile;
	UIWebView* __weak webView_;
}


-(void)loadHelp;

@property (nonatomic, weak) IBOutlet UIWebView* webView;
@property (nonatomic, retain) NSString* helpFile;
//@property (nonatomic, retain) NSString* title;

@end
