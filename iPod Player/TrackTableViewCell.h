//
//  MNFontLabelCell.h
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 8/01/11.
//  Copyright 2011 Catalystwo Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TrackTableViewCell : UITableViewCell  {
	
	
	NSNumber* trackNumber_;
	UILabel* __weak titleLabel_;
	UILabel* __weak trackLabel_;

//	__block OAuthTwitterUtils *twitterUtils_;

}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier ;
-(void)setTrackNumber:(NSNumber *)num;
- (void)prepareForReuse;
- (void)dealloc ;


@property (nonatomic, retain) NSNumber* trackNumber;
@property (nonatomic, weak, readonly) UILabel* titleLabel;
@property (nonatomic, weak, readonly) UILabel* trackLabel;

@end