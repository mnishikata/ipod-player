//
//  MNPressButton.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 10/09/12.
//
//

#import <UIKit/UIKit.h>

@class MNPressButton;

@protocol MNPressButtonDelegate <NSObject>
@optional

-(void)pressButtonDidBegin:(MNPressButton*)button;
-(void)pressButtonDidEnd:(MNPressButton*)button;

@end


@interface MNPressButton : UIControl

@property (nonatomic, weak) id <MNPressButtonDelegate> delegate;

@end


