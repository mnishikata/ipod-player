//
//  MyStoreObserver.m
//  CCal-X
//
//  Created by Nishikata Masatoshi on 28/07/12.
//  Copyright (c) 2012 Catalystwo. All rights reserved.
//

#import "MyStoreObserver.h"
#import <StoreKit/StoreKit.h>
#import "Tui.h"
#import "GTLBase64.h"
#import "GCNetworkReachability.h"


@implementation MyStoreObserver
@synthesize restoring;


- (id)init
{
	self = [super init];
	if (self) {
		
		
			
	}
	return self;
}


- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
	
	self.restoring = NO;
	
	
	for( SKPaymentTransaction* transaction in queue.transactions )
	{
		
		NSDictionary* purchasedProductIdentifier = [self verifyWithTransaction:transaction];
		
		
		if( !purchasedProductIdentifier )
		{
			NSLog(@"Verify failed %@",purchasedProductIdentifier);
		}
		
		if( purchasedProductIdentifier )
		{
			
			
			NSString* keyName = [NSString stringWithFormat:@"SPAPKeyFor_%@", [purchasedProductIdentifier objectForKey:@"product_id"]];

			
			[[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool:YES] forKey:keyName ];
			[[NSUbiquitousKeyValueStore defaultStore] setObject: [NSNumber numberWithBool:YES] forKey:keyName ];
			[[NSUbiquitousKeyValueStore defaultStore] synchronize];
		}
		
		
		
	}
	
	// Remove the transaction from the payment queue.
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	[self sendNotification];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	self.restoring = NO;
	[self sendNotification];
	
}


- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
	for (SKPaymentTransaction *transaction in transactions)
	{
		switch (transaction.transactionState)
		{
			case SKPaymentTransactionStatePurchased:
				[self completeTransaction:transaction];
				break;
			case SKPaymentTransactionStateFailed:
				[self failedTransaction:transaction];
				break;
			case SKPaymentTransactionStateRestored:
				[self restoreTransaction:transaction];
			default:
				break;
		}
	}
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
	// Your application should implement these two methods.
//	[self recordTransaction:transaction];
//	[self provideContent:transaction.payment.productIdentifier];
//	
//	NSString* productName = transaction.payment.productIdentifier
	
	
	
	///////
	
	
	
	
	NSDictionary* purchasedProductIdentifier = [self verifyWithTransaction:transaction];

	
	if( !purchasedProductIdentifier )
	{
		NSLog(@"Verify failed %@",purchasedProductIdentifier);
	}
	
	if( purchasedProductIdentifier )
	{

		{
			SHOW_ERROR(@"MyStoreObserverPurchased", @"MyStoreObserverPurchasedMessage");
		}
		
		
		NSString* keyName = [NSString stringWithFormat:@"SPAPKeyFor_%@", [purchasedProductIdentifier objectForKey:@"product_id"]];

		
		[[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool:YES] forKey:keyName ];
		[[NSUbiquitousKeyValueStore defaultStore] setObject: [NSNumber numberWithBool:YES] forKey:keyName ];
		[[NSUbiquitousKeyValueStore defaultStore] synchronize];

	}
	
	// Remove the transaction from the payment queue.
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

	[self sendNotification];
}


- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
	SHOW_ERROR(@"MyStoreObserverRestored", @"");

//	[self recordTransaction: transaction];
//	[self provideContent: transaction.originalTransaction.payment.productIdentifier];
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

	[self sendNotification];

}


- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
	if (transaction.error.code != SKErrorPaymentCancelled) {
		// Optionally, display an error here.
		
		NSString* message = [transaction.error localizedDescription];
		SHOW_ERROR(@"Error", message);

	}
	[[SKPaymentQueue defaultQueue] finishTransaction: transaction];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

	[self sendNotification];

}


-(NSDictionary*)verifyWithTransaction:(SKPaymentTransaction *)transaction
{
	GCNetworkReachability *internetReach = [GCNetworkReachability reachabilityForInternetConnection];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	if( netStatus == NotReachable )
	{
		NSLog(@"Offline");
		return nil;
	}
	
	NSData* receiptData = transaction.transactionReceipt;
	
	NSDictionary* dict = [NSDictionary dictionaryWithObject:GTLEncodeBase64(receiptData)
																	 forKey:@"receipt-data"];
	
	NSData *data = [NSJSONSerialization dataWithJSONObject:dict
																  options:0
																	 error:nil];
	
#if DEBUG
	NSURL *postURL = [[[NSURL alloc] initWithString: @"https://sandbox.itunes.apple.com/verifyReceipt"] autorelease];

#else
	NSURL* postURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];

#endif
	
	NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:postURL];
	
	// Set the request's content type to application/x-www-form-urlencoded
	[postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	
	// Designate the request a POST request and specify its body data
	[postRequest setHTTPMethod:@"POST"];
	[postRequest setHTTPBody:data];
	 
	NSURLResponse* response = nil;
	NSError* error = nil;
	NSData* returnData = [NSURLConnection sendSynchronousRequest:postRequest
															 returningResponse:&response
																			 error:&error];
	
	if( error )
	{
		NSLog(@"Error %@", [error localizedDescription]);
		return nil;
	}
	
	dict = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:returnData options:0 error:&error];
	
	
	if( [[dict objectForKey:@"status"] integerValue] == 0 )
		return [dict objectForKey:@"receipt"];
	
	return nil;
}

-(void)sendNotification
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"MNStoreObserverDidChangeNotificationName"
																		 object:nil];
}

-(BOOL)purchased:(NSString*)productID
{
	
	
	NSString* keyName = [NSString stringWithFormat:@"SPAPKeyFor_%@", productID];
	
	id purchased;

	purchased = [[NSUserDefaults standardUserDefaults] objectForKey: keyName];
	if( purchased ) return YES;
	
	[[NSUbiquitousKeyValueStore defaultStore] synchronize];
	purchased = [[NSUbiquitousKeyValueStore defaultStore] objectForKey:keyName];
	if( purchased ) return YES;

	return NO;
}

-(void)clear:(NSString*)productID
{
	NSString* keyName = [NSString stringWithFormat:@"SPAPKeyFor_%@", productID];


	[[NSUserDefaults standardUserDefaults] removeObjectForKey: keyName];
	[[NSUbiquitousKeyValueStore defaultStore] removeObjectForKey:keyName];
	[[NSUbiquitousKeyValueStore defaultStore] synchronize];

}

@end
