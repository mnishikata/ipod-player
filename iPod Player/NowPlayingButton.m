//
//  ArrowButton.m
//  NZSound
//
//  Created by Masatoshi Nishikata on 8/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NowPlayingButton.h"
#import "DrawUtils.h"


@implementation NowPlayingButton


- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

#define kArrowPointWidth 2.8
#define kArrowRadius 2


- (void)drawRect:(CGRect)rect {
    // Drawing code
	
	BOOL isIOS7 = ![[[UIDevice currentDevice] systemVersion] hasPrefix:@"6"];
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();

	CGAffineTransform t = CGAffineTransformMakeTranslation(3, 5);
	CGContextConcatCTM(ctx, t);	
	

	CGRect originalRect = rect;
	
	rect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width-5, rect.size.height-10);
	
	CGFloat fw = rect.size.width;
	CGFloat fh = rect.size.height;
	CGFloat point = floor(fh/kArrowPointWidth);
	CGFloat radius = 5.0f;
	CGFloat radius2 = radius*kArrowRadius;
	
	if( !isIOS7 )
	{
	CGContextMoveToPoint(ctx, fw, floor(fh/2) +1);
	CGContextAddArcToPoint(ctx, fw-point, fh+1, 0, fh+1, radius2);
	CGContextAddArcToPoint(ctx, 0, fh+1, 0, +1, radius);
	CGContextAddArcToPoint(ctx, 0, +1, fw-point, +1, radius);
	CGContextAddArcToPoint(ctx, fw-point, +1, fw, floor(fh/2)+1, radius2);
	CGContextAddLineToPoint(ctx, fw, floor(fh/2)+1);
	
	CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:1 alpha:0.3].CGColor);
	CGContextFillPath(ctx);
	
	

	
	
	CGContextMoveToPoint(ctx, fw, floor(fh/2));
	CGContextAddArcToPoint(ctx, fw-point, fh, 0, fh, radius2);
	CGContextAddArcToPoint(ctx, 0, fh, 0, 0, radius);
	CGContextAddArcToPoint(ctx, 0, 0, fw-point, 0, radius);
	CGContextAddArcToPoint(ctx, fw-point, 0, fw, floor(fh/2), radius2);
	CGContextAddLineToPoint(ctx, fw, floor(fh/2));
	CGContextClip(ctx);
	
	if( self.highlighted )
	{
		[DrawUtils drawGradientInRectNoClip:CGRectMake(0, 0, fw, fh)
								  fromColor:[UIColor colorWithRed:0.5*0.6 green:0.5*0.6 blue:0.5*0.6 alpha:1 ] 
								   midColor:[UIColor colorWithRed:0.25*0.6 green:0.25*0.6 blue:0.25*0.6 alpha:1 ] 
									toColor:[UIColor colorWithRed:0.18*0.6 green:0.18*0.6 blue:0.18*0.6 alpha:1 ]];
	}else {
		[DrawUtils drawGradientInRectNoClip:CGRectMake(0, 0, fw, fh)
								  fromColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1 ] 
								   midColor:[UIColor colorWithRed:0.25 green:0.25 blue:0.25 alpha:1 ] 
									toColor:[UIColor colorWithRed:0.18 green:0.18 blue:0.18 alpha:1 ]];
		
	}

	
	CGContextMoveToPoint(ctx, fw, floor(fh/2));
	CGContextAddArcToPoint(ctx, fw-point, fh, 0, fh, radius2);
	CGContextAddArcToPoint(ctx, 0, fh, 0, 0, radius);
	CGContextAddArcToPoint(ctx, 0, 0, fw-point, 0, radius);
	CGContextAddArcToPoint(ctx, fw-point, 0, fw, floor(fh/2), radius2);
	CGContextAddLineToPoint(ctx, fw, floor(fh/2));
	CGContextSetStrokeColorWithColor(ctx, [UIColor colorWithWhite:0 alpha:0.6].CGColor);
	CGContextSetLineWidth(ctx, 2);
	CGContextStrokePath(ctx);
	}
	
	if( !self.highlighted )
	{

		CGFloat fw = rect.size.width;
		CGFloat fh = rect.size.height;
		
		if( !isIOS7 )
		{
		CGFloat width = 5;
		CGRect shadowRect = CGRectMake(-width, -width, fw+width*2, fh+width*2);
		CGMutablePathRef path = CGPathCreateMutable();
		CGPathAddRect(path, nil, shadowRect);
		CGPathCloseSubpath(path);
		
		CGPathMoveToPoint(path, nil, fw, floor(fh/2));
		CGPathAddArcToPoint(path, nil, fw-point, fh, 0, fh, radius2);
		CGPathAddArcToPoint(path, nil, 0, fh, 0, 0, radius);
		CGPathAddArcToPoint(path, nil, 0, 0, fw-point, 0, radius);
		CGPathAddArcToPoint(path, nil, fw-point, 0, fw, floor(fh/2), radius2);
		CGPathAddLineToPoint(path, nil, fw, floor(fh/2));

		CGPathCloseSubpath(path);
		
		CGContextAddPath(ctx, path);
		
		CGPathRelease(path);
		
		CGContextSetShadowWithColor(ctx, CGSizeMake(0, 1), 2, [UIColor colorWithWhite:0 alpha:0.6].CGColor);
		
		CGContextSetFillColorWithColor(ctx, [UIColor blackColor].CGColor);
		CGContextEOFillPath(ctx);
		}
		
	}else {
		

	}
	

	NSString* line = NSLocalizedString(@"Now Playing",@"");

	CGRect letterRect = CGRectMake(0, 2, fw-6, fh-4);
	UIFont *font = [UIFont boldSystemFontOfSize:10];
	

	
	if( self.highlighted )
	{
		[[UIColor lightGrayColor] set];
	}
	else
	{
		if( isIOS7 )
		{
			[self.tintColor set];
			
			
			
		}else
		{
			[[UIColor whiteColor] set];
		}
	}
	
	if( !isIOS7 )
	{
	CGContextSetShadowWithColor(ctx, CGSizeMake(0, -1), 1, [UIColor colorWithWhite:0 alpha:0.6].CGColor);
	}
	else
	{
		
		CGPoint points[4];
		
		points[0] = CGPointMake(CGRectGetMaxX(letterRect) -3 , letterRect.origin.y + 5);
		points[1] = CGPointMake(CGRectGetMaxX(letterRect) + 4.5, CGRectGetMidY(letterRect));
		points[2] = CGPointMake(CGRectGetMaxX(letterRect) -3, CGRectGetMaxY(letterRect) - 5);
		
		CGContextSetLineJoin(ctx, kCGLineJoinMiter);
		CGContextSetLineWidth(ctx, 3.0);
		CGContextSetMiterLimit(ctx, 1000);
		
		
		CGContextBeginPath (ctx);
		
		CGContextMoveToPoint(ctx, points[0].x, points[0].y);
		
		for (NSUInteger k = 1; k < 3; k ++) {
			CGContextAddLineToPoint(ctx, points[k].x, points[k].y);
		}
		CGContextStrokePath(ctx);

		
	}
	
	
	[line drawInRect:letterRect
			withFont:font
		lineBreakMode:UILineBreakModeWordWrap 
			alignment:UITextAlignmentCenter];
	

	
	
}
- (void)dealloc {
}

-(CGRect)titleRectForContentRect:(CGRect)contentRect
{
	return CGRectZero;	
}

-(void)setHighlighted:(BOOL)flag
{
	[super setHighlighted:flag];
	[self setNeedsDisplay];
}

-(NSString*)accessibilityHint
{
	return NSLocalizedString(@"NowPlaingHint",@"");
}

-(NSString*)accessibilityLabel
{
	return NSLocalizedString(@"NowPlaingLabel",@"");

}

@end
