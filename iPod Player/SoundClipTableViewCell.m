//
//  SoundClipTableViewCell.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 22/04/14.
//
//

#import "SoundClipTableViewCell.h"
#import "CERoundProgressView.h"
@implementation SoundClipTableViewCellContentView

-(void)drawRect:(CGRect)rect
{
	
	[self.cell.attributedString drawInRect:rect];
	
}

@end


@implementation SoundClipTableViewCell

+(NSDictionary* )attributes
{
	static NSMutableParagraphStyle *paragraphStyle_ = nil;
	
	if( paragraphStyle_ == nil )
	{
		paragraphStyle_ = [[NSMutableParagraphStyle alloc] init];
		//		paragraphStyle_.alignment = NSTextAlignmentCenter;
		paragraphStyle_.lineSpacing = 7;
		paragraphStyle_.paragraphSpacing = 7;
		paragraphStyle_.lineBreakMode = NSLineBreakByWordWrapping;
	}
	
	UIFont* font = [UIFont systemFontOfSize: 13];
	font = [font fontWithSize:18];
	
	NSDictionary* attributes = @{ NSFontAttributeName: font ,
											NSParagraphStyleAttributeName : paragraphStyle_,
											NSForegroundColorAttributeName : [UIColor blackColor] };
	
	return attributes;
}

+(NSDictionary* )excerptAttributes
{
	static NSMutableParagraphStyle *paragraphStyle_ = nil;
	
	if( paragraphStyle_ == nil )
	{
		paragraphStyle_ = [[NSMutableParagraphStyle alloc] init];
		//		paragraphStyle_.alignment = NSTextAlignmentCenter;
		paragraphStyle_.lineSpacing = 7;
		paragraphStyle_.paragraphSpacing = 7;
		paragraphStyle_.lineBreakMode = NSLineBreakByWordWrapping;
	}
	
	UIFont* font = [UIFont italicSystemFontOfSize: 13];
	font = [font fontWithSize:18];
	
	NSDictionary* attributes = @{ NSFontAttributeName: font ,
											NSParagraphStyleAttributeName : paragraphStyle_,
											NSForegroundColorAttributeName : [UIColor blackColor] };
	
	return attributes;
}
+(NSDictionary* )disabledAttributes
{
	static NSMutableParagraphStyle *paragraphStyle_ = nil;
	
	if( paragraphStyle_ == nil )
	{
		paragraphStyle_ = [[NSMutableParagraphStyle alloc] init];
		//		paragraphStyle_.alignment = NSTextAlignmentCenter;
		paragraphStyle_.lineSpacing = 7;
		paragraphStyle_.paragraphSpacing = 7;
		paragraphStyle_.lineBreakMode = NSLineBreakByWordWrapping;
	}
	
	UIFont* font = [UIFont systemFontOfSize: 13];
	font = [font fontWithSize:18];
	
	NSDictionary* attributes = @{ NSFontAttributeName: font ,
											NSParagraphStyleAttributeName : paragraphStyle_,
											NSForegroundColorAttributeName : [UIColor lightGrayColor] };
	
	return attributes;
}



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
		
		self.backgroundColor = [UIColor whiteColor];
		
		CGRect frame = self.contentView.bounds;
		frame.origin.x += 35;
		frame.size.width -= 40;
		frame.origin.y += 10;
		frame.size.height -= 10;
		
		{
			SoundClipTableViewCellContentView* view = [[SoundClipTableViewCellContentView alloc] initWithFrame:frame];
			cellContentView_ = view;
			cellContentView_.cell = self;
			cellContentView_.userInteractionEnabled = NO;
			cellContentView_.backgroundColor = [UIColor clearColor];
			cellContentView_.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
			[self.contentView addSubview: cellContentView_];
		}
		
		{
			UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 1, 26, 40)];
			iconView_ = imageView;
			iconView_.contentMode = UIViewContentModeScaleAspectFit;
			iconView_.image = [UIImage imageNamed:@"SoundClipIconFlat"];
			[self.contentView addSubview: iconView_];

			
		}
		
	}
	return self;
}



- (void)prepareForReuse
{
	self.accessoryView = nil;
	self.editing = NO;
	self.copiedClip = NO;
	self.attributedString = nil;
	self.textLabel.text = nil;
	iconView_.hidden = NO;
	[progressView_ removeFromSuperview];
	progressView_ = nil;
	
	[super prepareForReuse];
}

-(void)setProgress:(CGFloat)progress
{
	if( progress < 0 )
	{
		[progressView_ removeFromSuperview];
		progressView_ = nil;
		iconView_.hidden = NO;
		return;
	}
	

	BOOL initial = NO;
	
	if( progress > 0 )
	{
		if( !progressView_ )
		{
			initial = YES;
			CGFloat x = iconView_.frame.origin.x;
			
			CERoundProgressView *progressView = [[CERoundProgressView alloc] initWithFrame:CGRectMake( x, 7, 28, 28)];
			progressView.startAngle = (3.0*M_PI)/2.0;

			progressView_ = progressView;
			[self.contentView addSubview: progressView_];
			iconView_.hidden = YES;

		}
	}
	
	
	progressView_.progress = progress;
	
	if( initial )
		[progressView_.layer removeAllAnimations];
	 
}

-(void)setAttributedString:(NSAttributedString *)attributedString
{
	_attributedString = attributedString;
	[self setNeedsLayout];
}

-(void)layoutSubviews
{
	[super layoutSubviews];
	
	
	CGRect frame = self.contentView.bounds;
	frame.origin.x += 10;
	frame.size.width -= 10;
	frame.origin.y += 10;
	frame.size.height -= 10;
	
	cellContentView_.frame = frame;
	
	
	if( self.copiedClip )
		iconView_.image = [UIImage imageNamed:@"SoundClipIconCopiedFlat"];
	else
		iconView_.image = [UIImage imageNamed:@"SoundClipIconFlat"];
	
	
	
	
	if( [self respondsToSelector:@selector(readableContentGuide)] ) {
		
		CGRect layoutFrame = self.readableContentGuide.layoutFrame;
		layoutFrame.size.width -= 30;
		
		CGRect rect = iconView_.frame;
		rect.origin.x = layoutFrame.origin.x;
		iconView_.frame = rect;
		
		rect = cellContentView_.frame;
		rect.origin.x = CGRectGetMaxX(iconView_.frame) + 5;
		rect.size.width = layoutFrame.size.width - (iconView_.frame.size.width + 5);
		
		cellContentView_.frame = rect;
	}
	
	
	[cellContentView_ setNeedsDisplay];
}

-(void)updateHeightForWidth:(CGFloat)width
{
	CGSize maxSize = CGSizeMake(width, MAXFLOAT);
	
	CGSize size = [_attributedString boundingRectWithSize:maxSize
																 options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
	cellHeight_ = size.height;
	cellHeight_ += 20;
	
	[cellContentView_ setNeedsDisplay];
	
}



-(CGFloat)cellHeight
{
	return cellHeight_ + 2;
}


@end
