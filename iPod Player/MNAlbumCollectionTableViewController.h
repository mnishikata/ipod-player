//
//  MNCollectionTableViewController.h
//  iPod Player
//
//  Created by Nishikata Masatoshi on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>
#import "AB_Player-Swift.h"

@interface MNAlbumCollectionTableViewController : MediaCollectionTableViewController <UIViewControllerRestoration>
{
	BOOL shouldReloadData_;
	
	MPMediaGrouping groupingType_;
	NSSet* filterPredicates_;
		
	NSArray *sections_;
	
	NSArray *collections_;
	
	NSMutableArray* soundClips_;
	
	BOOL hideSoundClips_;
	
	BOOL showArtistName_;

}
- (id)initWithGroupingType:(MPMediaGrouping)groupingType filterPredicates:(NSSet*)predicates;

@property (nonatomic, copy) NSArray *collections; // NSArray of MPMediaEntity
@property (nonatomic, copy) NSArray *sections; // NSArray of MPMediaQuerySection

@property (nonatomic, retain) NSMutableArray* soundClips;

@property (nonatomic, readwrite) BOOL hideSoundClips;

@property (nonatomic, readwrite) BOOL showArtistName;

@end


