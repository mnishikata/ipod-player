//
//  MNBaseNavigationController.m
//  iPod Player
//
//  Created by Masatoshi Nishikata on 5/09/12.
//
//

#import "MNBaseNavigationController.h"



@implementation MNBaseNavigationController

+ (UIViewController *) viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
	MNBaseNavigationController* controller = nil;
	
	
	controller = [[MNBaseNavigationController alloc] init];
//	controller.restorationIdentifier = @"MNBaseNavigationControllerIdentifier";
	controller.restorationClass = [self class];

	
	return controller;
}

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
	[super encodeRestorableStateWithCoder:coder];
	[coder encodeInteger:supportedInterfaceOrientations_ forKey:@"supportedInterfaceOrientations_"];
	
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
	[super decodeRestorableStateWithCoder:coder];

	if( [coder containsValueForKey:@"supportedInterfaceOrientations_"] )
	supportedInterfaceOrientations_ = [coder decodeIntegerForKey:@"supportedInterfaceOrientations_"];
	
}


- (id)init
{
	self = [super init];
	if (self) {
		// Custom initialization
		
		supportedInterfaceOrientations_ = UIInterfaceOrientationMaskAll;
		
		self.restorationIdentifier = @"MNBaseNavigationControllerIdentifier";
		self.restorationClass = [UINavigationController class];
	}
	return self;
}

- (id)initWithRootViewController:(UIViewController*)viewController
{
	self = [super initWithRootViewController:viewController];
    if (self) {
        // Custom initialization
		 
		 supportedInterfaceOrientations_ = UIInterfaceOrientationMaskAll;
		 
		 self.restorationIdentifier = @"MNBaseNavigationControllerIdentifier";
		 self.restorationClass = [UINavigationController class];
    }
    return self;
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	return supportedInterfaceOrientations_;
}

-(void)setSupportedInterfaceOrientations:(NSUInteger)mask;
{
	supportedInterfaceOrientations_ = mask;
}

@end





//@implementation UITabBarController (Extension)
//
//
//- (NSUInteger)supportedInterfaceOrientations
//{
//	BOOL iPad = ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad );
//
//	
//	if( iPad ) return UIInterfaceOrientationMaskAll;
//	
//	return UIInterfaceOrientationMaskPortraitUpsideDown | UIInterfaceOrientationMaskPortrait;
//	
//	//	return UIInterfaceOrientationMaskAll;
//}
//
//@end
