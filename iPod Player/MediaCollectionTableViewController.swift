//
//  MediaCollectionTableViewController.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 26/02/16.
//
//

import UIKit
import CoreData
import GoogleMobileAds

let enumerationQueue:dispatch_queue_t = {
	let enumerationQueue = dispatch_queue_create("Browser Enumeration Queue", DISPATCH_QUEUE_SERIAL);
	dispatch_set_target_queue(enumerationQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
	
	return enumerationQueue
}()

class MediaCollectionTableViewController: UITableViewController, GADBannerViewDelegate {
	
	var bannerView_:GADBannerView? = nil
	var bannerLoadingFailedCount:Int = 0

	init() {
		super.init(style: .Plain)
		
		let center = NSNotificationCenter.defaultCenter()
		center.addObserver(self, selector: "destroyADBannerView", name: "MNStoreObserverDidChangeNotificationName", object: nil)

	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
		let center = NSNotificationCenter.defaultCenter()
		center.removeObserver(self)
	}
	
	func rightNavigationButton() -> UIBarButtonItem? {
		return nil
	}
	
	func setupNowPlayingButton() {
		
		if MNPlayerViewController.canPlay() {
			let nowPlayingButton = NowPlayingButton(type: .Custom)
			nowPlayingButton.addTarget(self, action: "showPlayer", forControlEvents: .TouchUpInside)
			nowPlayingButton.frame = CGRectMake(0, 0, 70, 40)
			let playingButton = UIBarButtonItem(customView: nowPlayingButton)
			
			if let button = rightNavigationButton() {
			navigationItem.rightBarButtonItems = [playingButton, button]
			}else {
				navigationItem.rightBarButtonItems = [playingButton]
			}
		}else {
			navigationItem.rightBarButtonItem = rightNavigationButton()
		}
	}
	
	func showPlayer() {
		let controller = MNPlayerViewController.sharedPlayer
		navigationController!.pushViewController(controller, animated: true)
	}
	
	func info() {
		let controller = FFAboutViewController()
		let nav = MNBaseNavigationController(rootViewController: controller)
		navigationController!.presentViewController(nav, animated: true, completion: { _ in })
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if navigationController?.viewControllers[0] == self && navigationController?.tabBarController?.moreNavigationController != navigationController {
			let button = UIBarButtonItem(image: UIImage(named: "InfoButton"), style: .Plain, target: self, action: "info")
			navigationItem.leftBarButtonItem = button
		}
		
		setupNowPlayingButton()
		
		let center = NSNotificationCenter.defaultCenter()
		center.addObserver(self, selector: "reloadData", name: UIApplicationDidBecomeActiveNotification, object: nil)
		
		
		let purchased_ = APP_DELEGATE().storeObserver.purchased("com.catalystwo.LearningPlayer.paid")
		if( purchased_ == false ) {
			admobView()
		}
	}
	
	func destroyADBannerView() {
		tableView.tableHeaderView = nil
	}
	
	func admobView() {
		let banner = GADBannerView(adSize: kGADAdSizeBanner)
		banner.adUnitID = "ca-app-pub-5527180066718750/8648367025"
		banner.rootViewController = self
		banner.backgroundColor = UIColor.lightGrayColor()
		let request = GADRequest()
		request.testDevices = [kGADSimulatorID]
		banner.delegate = self
		banner.loadRequest(request)
		banner.frame = CGRectMake(0,0,self.view.bounds.size.width,50)
		banner.autoresizingMask = [.FlexibleWidth]

		tableView.tableHeaderView = banner
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		let center = NSNotificationCenter.defaultCenter()
		center.removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
		let player = MNPlayerViewController.sharedPlayer
		player.playingClipCollection = false

		navigationController?.toolbarHidden = true
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		if let selectedRow = tableView.indexPathForSelectedRow {
			tableView.deselectRowAtIndexPath(selectedRow, animated: true)
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	//MARK:- Banner View Delegate
	
	/// Tells the delegate an ad request loaded an ad.
	func adViewDidReceiveAd(bannerView: GADBannerView!) {
		print("adViewDidReceiveAd")

	}
	
	/// Tells the delegate an ad request failed.
	func adView(bannerView: GADBannerView!,
		didFailToReceiveAdWithError error: GADRequestError!) {
			print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
			
			bannerLoadingFailedCount++
			
			// Failed to load... reload in 30 seconds
			
			// Online?
			
			let reachable = GCNetworkReachability.reachabilityForInternetConnection().isReachable()
			
			if bannerLoadingFailedCount > 2 && reachable == true  {
				
				bannerView.backgroundColor = UIColor.lightGrayColor()
			}
			
			if reachable == false {
				bannerLoadingFailedCount = 0
			}
	}
	
	/// Tells the delegate that a full screen view will be presented in response
	/// to the user clicking on an ad.
	func adViewWillPresentScreen(bannerView: GADBannerView!) {
		print("adViewWillPresentScreen")
	}
	
	/// Tells the delegate that the full screen view will be dismissed.
	func adViewWillDismissScreen(bannerView: GADBannerView!) {
		print("adViewWillDismissScreen")
	}
	
	/// Tells the delegate that the full screen view has been dismissed.
	func adViewDidDismissScreen(bannerView: GADBannerView!) {
		print("adViewDidDismissScreen")
	}
	
	/// Tells the delegate that a user click will open another app (such as
	/// the App Store), backgrounding the current app.
	func adViewWillLeaveApplication(bannerView: GADBannerView!) {
		print("adViewWillLeaveApplication")
	}
	
	// MARK: - Subclass should implement these
	
	func reloadData() {
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
	}
	
	
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// #warning Incomplete implementation, return the number of sections
		return 0
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// #warning Incomplete implementation, return the number of rows
		return 0
	}
	
}


class MNAlbumCollectionTableViewController : MediaCollectionTableViewController, UIViewControllerRestoration {
	
	var filterPredicates_:Set<MPMediaPropertyPredicate>? = nil
	var groupingType_:MPMediaGrouping = .Title
	var shouldReloadData_:Bool = false 
	var sections_:[MPMediaQuerySection] = []
	var collections:[MPMediaItemCollection]? = nil
	var soundClips:[MNSoundClip]? = nil
	var hideSoundClips:Bool = false
	var showArtistName:Bool = false
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		 coder: NSCoder) -> UIViewController? {
		var controller:MNAlbumCollectionTableViewController
		var groupingType:MPMediaGrouping = .Title
		var predicates:Set<MPMediaPropertyPredicate>
		groupingType = MPMediaGrouping(rawValue:coder.decodeIntegerForKey("groupingType_")) ?? .Title
		predicates = coder.decodeObjectForKey("filterPredicates_") as! Set<MPMediaPropertyPredicate>? ?? []
		controller = MNAlbumCollectionTableViewController(groupingType: groupingType,filterPredicates:predicates)
		return controller
	}
	
	convenience init(groupingType:MPMediaGrouping, filterPredicates predicates:Set<MPMediaPropertyPredicate>? ) {
		self.init()
		filterPredicates_ = predicates
		groupingType_ = groupingType
		shouldReloadData_ = true
		self.restorationIdentifier = "MNAlbumCollectionTableViewControllerID"
		self.restorationClass = self.dynamicType
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		coder.encodeInteger(groupingType_.rawValue,forKey:"groupingType_")
		coder.encodeObject(filterPredicates_,forKey:"filterPredicates_")
		coder.encodeObject(sections_,forKey:"sections_")
		coder.encodeObject(collections,forKey:"collections_")
		coder.encodeBool(hideSoundClips,forKey:"hideSoundClips_")
		coder.encodeBool(showArtistName,forKey:"showArtistName_")
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		
		shouldReloadData_ = true
		groupingType_ = MPMediaGrouping(rawValue:coder.decodeIntegerForKey("groupingType_")) ?? .Title
		filterPredicates_ = coder.decodeObjectForKey("filterPredicates_") as! Set<MPMediaPropertyPredicate>? ?? []
		sections_ = coder.decodeObjectForKey("sections_") as! [MPMediaQuerySection]? ?? []
		collections = coder.decodeObjectForKey("collections_") as! [MPMediaItemCollection]?
		hideSoundClips = coder.decodeBoolForKey("hideSoundClips_")
		showArtistName = coder.decodeBoolForKey("showArtistName_")
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if( shouldReloadData_ ) {
			reloadData()
		} else {
			fetchSoundClips()
		}
		
		switch groupingType_ {
		case .Album:
			navigationItem.title = WLoc("TabAlbums")
		case .Artist:
			navigationItem.title = WLoc("TabArtists")
		case .Genre:
			navigationItem.title = WLoc("TabGenre")
		case .PodcastTitle:
			navigationItem.title = WLoc("TabPodcasts")
		case .Playlist :
			navigationItem.title = WLoc("TabPlaylists")
		default:
			break
			
		}
	}
	
	final override func reloadData() {
		
		shouldReloadData_ = false
		collections = nil
		
		// Read section data
		dispatch_sync(enumerationQueue) {
			// Grab videos from the iPod Library
			var query:MPMediaQuery? = nil
			
			switch self.groupingType_ {
			case .Album:
				query = MPMediaQuery.albumsQuery()
			case .Artist:
				query = MPMediaQuery.artistsQuery()
			case .Genre:
				query = MPMediaQuery.genresQuery()
			case .PodcastTitle:
				query = MPMediaQuery.podcastsQuery()
			case .Playlist :
				query = MPMediaQuery.playlistsQuery()
			default:
				break
			}
			query?.groupingType = self.groupingType_
			if self.filterPredicates_ != nil {
			query?.filterPredicates = self.filterPredicates_
			}
			
			let sections = query?.collectionSections ?? []
			self.sections_ = sections
		}
		
		// Read album collections
		dispatch_async(enumerationQueue) {
			var query:MPMediaQuery? = nil
			
			switch self.groupingType_ {
			case .Album:
				query = MPMediaQuery.albumsQuery()
			case .Artist:
				query = MPMediaQuery.artistsQuery()
			case .Genre:
				query = MPMediaQuery.genresQuery()
			case .PodcastTitle:
				query = MPMediaQuery.podcastsQuery()
			case .Playlist :
				query = MPMediaQuery.playlistsQuery()
			default:
				break
			}
			
			query?.groupingType = self.groupingType_
			
			if self.filterPredicates_ != nil {
			query?.filterPredicates = self.filterPredicates_
			}
			
			if let collections = query?.collections {
				
				let items = Array(collections)
				dispatch_async(dispatch_get_main_queue()) {
					self.collections = items
					self.tableView.reloadData()
					self.fetchSoundClips()
				}
			}
		}
		
	}

	
	func fetchSoundClips() {
		if hideSoundClips {
			return
		}
		if collections == nil { return }

		soundClips = nil
		var predicate: NSPredicate? = nil
		
		switch groupingType_ {
		case .Album:
			predicate = MNSoundClip.fetchSoundClipsInAlbum(collections!)
		case .Artist:
			predicate = MNSoundClip.fetchSoundClipsInArtist(collections!)
		case .Genre:
			predicate = MNSoundClip.fetchSoundClipsInGenre(collections!)
		case .PodcastTitle:
			predicate = MNSoundClip.fetchSoundClipsInPodcast(collections!)
		case .Playlist :
			predicate = MNSoundClip.fetchSoundClipsForSongs(collections!)
		default:
			break
		}

		MNSoundClip.fetchWithPredicate(predicate!)  {(clips: [MNSoundClip], error: NSError?) -> Void in
			self.soundClips = clips
			self.tableView.reloadSections(NSIndexSet(index:0), withRowAnimation: .Automatic)
		}
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return sections_.count + 1
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
		if hideSoundClips && section == 0 {
			return ""
		}
		if section == 0 {
			return WLoc("Sound Clips")
		}
		if groupingType_ == .Playlist {
			return ""
		}
		let mediaSection: MPMediaQuerySection = sections_[section - 1]
		return mediaSection.title
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// Return the number of rows in the section.
		if section == 0 && hideSoundClips {
			return 0
		}
		if section == 0 {
			return 1
		}
		let mediaSection: MPMediaQuerySection = sections_[section - 1]
		return mediaSection.range.length
	}
	
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		if indexPath.section == 0 {
			
			let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
			cell.selectionStyle = .Blue
			if nil == soundClips {
				cell.textLabel?.text = WLoc("Loading")
				cell.textLabel?.textColor = UIColor.lightGrayColor()
			}
			else {
				if soundClips!.count == 0 {
					cell.textLabel?.text = WLoc("0 clip")
					cell.selectionStyle = .None
					cell.textLabel?.textColor = UIColor.lightGrayColor()
				}
				else if soundClips!.count == 1 {
					cell.textLabel?.text = WLoc("1 clip")
				}
				else {
					cell.textLabel?.text = String(format: WLoc("%d clips"), soundClips!.count)
				}
			}
			cell.imageView?.image = UIImage(named: "SoundClipIconFlat")
			return cell
			
		}
		
		var cell: AlbumTableViewCell? = nil
		if showArtistName {
			cell = tableView.dequeueReusableCellWithIdentifier("CellWithArtist") as? AlbumTableViewCell ??
				AlbumTableViewCell(style: .Subtitle, reuseIdentifier: "CellWithArtist")
			
		}
		else {
			cell = tableView.dequeueReusableCellWithIdentifier("CellWithoutArtist") as? AlbumTableViewCell ??
			AlbumTableViewCell(style: .Default, reuseIdentifier: "CellWithoutArtist")
		}
		if nil == collections {
			cell!.textLabel?.text = WLoc("Loading")
			cell!.textLabel?.textColor = UIColor.lightGrayColor()
			return cell!
		}
		
		
		let mediaSection: MPMediaQuerySection = sections_[indexPath.section - 1]
		var subcollection: [MPMediaItemCollection] = Array(collections![mediaSection.range.toRange()!])
		let item: MPMediaItemCollection = subcollection[indexPath.row]
		var title: String? = nil
		var artistName: String? = nil
		dispatch_sync(enumerationQueue) {
			
			switch self.groupingType_ {
			case .Album:
				title = item.representativeItem?.valueForProperty(MPMediaItemPropertyAlbumTitle) as! String?
			case .Artist:
				title = item.representativeItem?.valueForProperty(MPMediaItemPropertyArtist) as! String?
			case .Genre:
				title = item.representativeItem?.valueForProperty(MPMediaItemPropertyGenre) as! String?
			case .PodcastTitle:
				title = item.representativeItem?.valueForProperty(MPMediaItemPropertyPodcastTitle) as! String?
			case .Playlist :
				title = item.valueForProperty(MPMediaPlaylistPropertyName) as! String?
			default:
				break
			}
			
			artistName = item.representativeItem?.valueForProperty(MPMediaItemPropertyArtist) as! String?
			
		}
		
		cell!.textLabel?.text = title!
		if showArtistName {
			cell!.detailTextLabel?.text = artistName!
		}
		if (item is MPMediaItem) {
			cell!.artworkView?.image = nil
			return cell!
		}
		dispatch_async(enumerationQueue) {
			let artwork: MPMediaItemArtwork? = item.representativeItem?.valueForProperty(MPMediaItemPropertyArtwork) as! MPMediaItemArtwork?
			var imageSize: CGFloat = 44
			if self.showArtistName {
				imageSize = 55
			}
			if let image = artwork?.imageWithSize(CGSizeMake(imageSize, imageSize)) {
				dispatch_async(dispatch_get_main_queue()) {() -> Void in
					cell!.artworkView?.image = image
				}
			}
		}
		
		return cell!
	}
	
	
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		// Return NO if you do not want the specified item to be editable.
		return false
	}
	
	override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
		if sections_.count < 2 {
			return nil
		}
		var array: [String] = []
		
		for var hoge = 0; hoge < sections_.count; hoge++ {
			let mediaSection: MPMediaQuerySection = sections_[hoge]
			array.append(mediaSection.title)
		}
		return array
	}
	
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if let selectedRow = tableView.indexPathForSelectedRow {
			tableView.deselectRowAtIndexPath(selectedRow, animated: true)
		}
		
		guard let collections = collections else { return }
		
		if indexPath.section == 0 {
			if soundClips?.count > 0 {
				let controller = MNClipCollectionTableViewController()
				controller.soundClips = soundClips!
				controller.organizeType = .ByAlbumTitle
				navigationController!.pushViewController(controller, animated: true)
			}
			return
		}
		let mediaSection: MPMediaQuerySection = sections_[indexPath.section - 1]
		var subcollection: [MPMediaItemCollection] = Array(collections[mediaSection.range.toRange()!])
		let item: MPMediaItemCollection = subcollection[indexPath.row]
		
		var predicates:Set<MPMediaPropertyPredicate> = []
		if filterPredicates_ != nil {
			predicates = predicates.union(filterPredicates_!)
		}
		
		switch groupingType_ {
		case .Album:
			let albumID = item[MPMediaItemPropertyPersistentID]
			predicates.insert(MPMediaPropertyPredicate(value: albumID, forProperty: MPMediaItemPropertyAlbumPersistentID))
			let songsController = MNSongCollectionTableViewController(filterPredicates: predicates)
			songsController.title = item.representativeItem?[MPMediaItemPropertyAlbumTitle] as! String?
			navigationController?.pushViewController(songsController, animated: true)
			
		case .Artist:
			let artistID = item[MPMediaItemPropertyPersistentID]
			predicates.insert(MPMediaPropertyPredicate(value: artistID, forProperty: MPMediaItemPropertyArtistPersistentID))
			let songsController = MNAlbumCollectionTableViewController(groupingType: .Album, filterPredicates: predicates)
			songsController.showArtistName = true
			songsController.title = item.representativeItem?[MPMediaItemPropertyArtist] as! String?
			navigationController?.pushViewController(songsController, animated: true)
			
		case .Genre:
			let genreID = item[MPMediaItemPropertyPersistentID]
			predicates.insert(MPMediaPropertyPredicate(value: genreID, forProperty: MPMediaItemPropertyGenrePersistentID))
			let songsController = MNAlbumCollectionTableViewController(groupingType: .Artist, filterPredicates: predicates)
			songsController.showArtistName = false
			songsController.title = item.representativeItem?[MPMediaItemPropertyGenre] as! String?
			navigationController?.pushViewController(songsController, animated: true)
			
		case .PodcastTitle:
			let podcastID = item[MPMediaItemPropertyPersistentID]
			predicates.insert(MPMediaPropertyPredicate(value: podcastID, forProperty: MPMediaItemPropertyPodcastPersistentID))
			let songsController = MNSongCollectionTableViewController(filterPredicates: predicates)
			songsController.title = item.representativeItem?[MPMediaItemPropertyPodcastTitle] as! String?
			navigationController?.pushViewController(songsController, animated: true)
			
		case .Playlist :
			let playlistID = item[MPMediaItemPropertyPersistentID]
			predicates.insert(MPMediaPropertyPredicate(value: playlistID, forProperty: MPMediaPlaylistPropertyPersistentID))
			let songsController = MNSongCollectionTableViewController(filterPredicates: predicates)
			songsController.title = item[MPMediaPlaylistPropertyName] as! String?
			navigationController?.pushViewController(songsController, animated: true)
			
		default:
			break
		}
		
	}
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		if showArtistName && indexPath.section != 0 {
			return 55
		}
		return 44
	}
	

}


class MNRecentAlbumsCollectionTableViewController : MediaCollectionTableViewController, UIViewControllerRestoration {

	var recents_:[NSNumber] = []
	var mediaItems_:[AnyObject/*MPMediaItem or NSNull*/] = []
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		coder: NSCoder) -> UIViewController? {
			var groupingType:MPMediaGrouping = .Title
			var predicates:Set<MPMediaPropertyPredicate>
			groupingType = MPMediaGrouping(rawValue:coder.decodeIntegerForKey("groupingType_")) ?? .Title
			predicates = coder.decodeObjectForKey("filterPredicates_") as! Set<MPMediaPropertyPredicate>? ?? []
			let controller = MNAlbumCollectionTableViewController(groupingType: groupingType,filterPredicates:predicates)
			return controller
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		coder.encodeObject(recents_,forKey:"recents_")
		coder.encodeObject(mediaItems_,forKey:"mediaItems_")
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		
		recents_ = coder.decodeObjectForKey("recents_") as! [NSNumber]? ?? []
		mediaItems_ = coder.decodeObjectForKey("mediaItems_") as! [AnyObject]? ?? []
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		title = WLoc("TabRecents")
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		let ud = NSUserDefaults.standardUserDefaults()
		recents_ = ud.objectForKey("MNPlayerRecentAlbums") as! [NSNumber]? ?? []
		reloadData()
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// Return the number of sections.
		return 1
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		// Return the number of rows in the section.
		if recents_.count != mediaItems_.count {
			return 1
		}
		if recents_.count == 0 {
			return 0
		}
		return recents_.count + 1
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		var cell: AlbumTableViewCell
		// Configure the cell...
		cell = tableView.dequeueReusableCellWithIdentifier("CellWithArtist") as! AlbumTableViewCell? ?? AlbumTableViewCell(style: .Subtitle, reuseIdentifier: "CellWithArtist")
		
		cell.selectionStyle = .Blue
		cell.textLabel?.textColor = UIColor.blackColor()
		cell.userInteractionEnabled = true
		
		if recents_.count != mediaItems_.count {
			cell.textLabel?.text = WLoc("Loading")
			cell.textLabel?.textColor = UIColor.lightGrayColor()
			cell.selectionStyle = .None
			cell.userInteractionEnabled = false
			return cell
		}
		if indexPath.row == recents_.count {
			// Last row
			let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
			cell.textLabel?.textColor = UIColor(red: 0.2, green: 0.5, blue: 0.7, alpha: 1)
			cell.textLabel?.text = WLoc("Clear History")
			cell.textLabel?.font = UIFont.systemFontOfSize(14)
			cell.textLabel?.textAlignment = .Center
			cell.imageView?.image = nil
			cell.detailTextLabel?.text = nil
			return cell
		}

		let item = mediaItems_[indexPath.row]
		if (item is MPMediaItem) == false {
			cell.textLabel?.text = "Not in this device"
			cell.textLabel?.textColor = UIColor.lightGrayColor()
			cell.selectionStyle = .None
			cell.userInteractionEnabled = false
			return cell
		}
		
		let mediaItem = item as! MPMediaItem
		var title: String? = nil
		var artistName: String? = nil
		dispatch_sync(enumerationQueue) {
			title = mediaItem.valueForProperty(MPMediaItemPropertyAlbumTitle) as! String?
			artistName = mediaItem.valueForProperty(MPMediaItemPropertyArtist)  as! String?
		}

		
		cell.textLabel?.text = title!
		cell.detailTextLabel?.text = artistName!
		dispatch_async(enumerationQueue) {
			let artwork: MPMediaItemArtwork? = mediaItem.valueForProperty( MPMediaItemPropertyArtwork) as! MPMediaItemArtwork?
			let imageSize: CGFloat = 60
			let image = artwork?.imageWithSize(CGSizeMake(imageSize, imageSize))
			if image != nil {
				dispatch_async(dispatch_get_main_queue()) {
					cell.artworkView?.image = image
				}
			}
		}
		return cell

	}
	
	final override func reloadData() {
		mediaItems_ = []
		tableView.reloadData()
		// Read album collections
		dispatch_async(enumerationQueue) {
			var albums: [AnyObject] = []
			for albumID: NSNumber in self.recents_ {
				let predicate = MPMediaPropertyPredicate(value: albumID, forProperty: MPMediaItemPropertyAlbumPersistentID)
				let query = MPMediaQuery(filterPredicates: [predicate])
				let obj = query.items
				if obj?.count > 0 {
					albums.append(obj!.last!)
				}
				else {
					albums.append(NSNull())
				}
			}
			dispatch_async(dispatch_get_main_queue()) {
				self.mediaItems_ = albums
				self.tableView.reloadData()
			}
		}
	}
	
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		// Navigation logic may go here. Create and push another view controller.
		if let selectedRow = tableView.indexPathForSelectedRow {
			tableView.deselectRowAtIndexPath(selectedRow, animated: true)
		}
		
		if indexPath.row == mediaItems_.count {
			recents_ = []
			mediaItems_ = []
			NSUserDefaults.standardUserDefaults().removeObjectForKey("MNPlayerRecentAlbums")
			reloadData()
			return
		}
		let item = mediaItems_[indexPath.row]
		if item is MPMediaItem {
			let albumID = (item as! MPMediaItem).valueForProperty(MPMediaItemPropertyAlbumPersistentID)
			let predicate = MPMediaPropertyPredicate(value: albumID, forProperty: MPMediaItemPropertyAlbumPersistentID)
			let songsController = MNSongCollectionTableViewController(filterPredicates: [predicate])
			
			songsController.title = (item as! MPMediaItem)[MPMediaItemPropertyAlbumTitle] as! String?
			navigationController?.pushViewController(songsController, animated: true)
		}
	}
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 60
	}
}


class MNSongCollectionTableViewController : MediaCollectionTableViewController, UIViewControllerRestoration {
	
	var mediaItems:[MPMediaItem] = []
	var soundClips:[MNSoundClip]? = nil
	var filterPredicates_:Set<MPMediaPropertyPredicate> = []
	var shouldReloadData_:Bool = false
	var loadingError_:Bool = false
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		coder: NSCoder) -> UIViewController? {
		let predicates = coder.decodeObjectForKey("filterPredicates_") as! Set<MPMediaPropertyPredicate>? ?? []
		let controller = MNSongCollectionTableViewController(filterPredicates: predicates)
		return controller
	}
	
	convenience init(filterPredicates predicates: Set<MPMediaPropertyPredicate>) {
		self.init()
		soundClips = nil
		filterPredicates_ = predicates

		shouldReloadData_ = true
		self.restorationIdentifier = "MNSongCollectionTableViewControllerID"
		self.restorationClass = self.dynamicType
		
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		coder.encodeObject(mediaItems, forKey: "mediaItems")
		coder.encodeObject(filterPredicates_, forKey: "filterPredicates_")
		coder.encodeObject(title, forKey: "title")
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		shouldReloadData_ = true
		filterPredicates_ = coder.decodeObjectForKey("filterPredicates_") as! Set<MPMediaPropertyPredicate>? ?? []
		mediaItems = coder.decodeObjectForKey("mediaItems") as! [MPMediaItem]? ?? []
		title = coder.decodeObjectForKey("title") as! String?
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		if shouldReloadData_ {
			reloadData()
		}
		else {
			fetchSoundClips()
		}
		// Select currently playing item, if there is no item, deselect
		let mediaItem = MNPlayerViewController.sharedPlayer.mediaItem
		for var row = 0; row < mediaItems.count; row++ {
			let item: MPMediaItem = mediaItems[row]
			if item.isEqual(mediaItem) {
				let indexPath = NSIndexPath(forRow: row, inSection: 1)
				tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .Middle)
			}
		}
	}
	
	override func reloadData() {
		shouldReloadData_ = false
		mediaItems = []
		
		// Read section data first
		dispatch_sync(enumerationQueue) {
			// Grab videos from the iPod Library
			let query = MPMediaQuery.songsQuery()
			query.groupingType = .Album
			query.filterPredicates = self.filterPredicates_
			
			self.mediaItems = query.items ?? []
		}
		fetchSoundClips()
	}
	
	func fetchSoundClips() {
		let collection = MPMediaItemCollection(items:mediaItems)
		var clipPredicate: NSPredicate? = nil
		for predicate: MPMediaPropertyPredicate in filterPredicates_ {
			// アルバムで分類されている場合
			if (predicate.property == MPMediaItemPropertyAlbumPersistentID) {
				clipPredicate = MNSoundClip.fetchSoundClipsInAlbum([collection])
			}
			// Podcastで分類されている場合
			if (predicate.property == MPMediaItemPropertyPodcastPersistentID) {
				clipPredicate = MNSoundClip.fetchSoundClipsInPodcast([collection])
			}
		}
		if nil == clipPredicate {
			clipPredicate = MNSoundClip.fetchSoundClipsForSongs([collection])
		}
		soundClips = nil

		MNSoundClip.fetchWithPredicate(clipPredicate!) {(clips: [MNSoundClip], error: NSError?) -> Void in
			if error != nil {
				self.loadingError_ = true
				self.tableView.reloadSections(NSIndexSet(index:0), withRowAnimation: .None)
				return
			}
			self.soundClips = clips
			self.tableView.reloadSections(NSIndexSet(index:0), withRowAnimation: .Automatic)
		}

	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 2
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
		if section == 0 {
			return WLoc("Sound Clips")
		}
		return WLoc("Tracks")
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return 1
		}
		return mediaItems.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		if indexPath.section == 0 {
			let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
			cell.selectionStyle = .Blue
			if nil == soundClips {
				if loadingError_ {
					cell.textLabel?.text = WLoc("UnableToLoad")
					cell.textLabel?.textColor = UIColor.lightGrayColor()
				}
				else {
					cell.textLabel?.text = WLoc("Loading")
					cell.textLabel?.textColor = UIColor.lightGrayColor()
				}
			}
			else {
				if soundClips!.count == 0 {
					cell.textLabel?.text = WLoc("0 clip")
					cell.selectionStyle = .None
					cell.textLabel?.textColor = UIColor.lightGrayColor()
				}
				else if soundClips!.count == 1 {
					cell.textLabel?.text = WLoc("1 clip")
				}
				else {
					cell.textLabel?.text = String(format: WLoc("%d clips"), soundClips!.count)
				}
			}
			
			cell.imageView?.image = UIImage(named: "SoundClipIconFlat")
			return cell

		}
		
		
		let CellIdentifier: String = "TrackCell"
		let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as! TrackTableViewCell? ??
		  TrackTableViewCell(style: .Default, reuseIdentifier: CellIdentifier)
		
		cell!.selectionStyle = .Blue
		let item = mediaItems[indexPath.row] as MPMediaItem
		var title: String? = nil
		var trackNumberNumber: NSNumber? = nil
		let url: NSURL? = item.valueForProperty(MPMediaItemPropertyAssetURL) as! NSURL?
		dispatch_sync(enumerationQueue) {
			title = item.valueForProperty(MPMediaItemPropertyTitle) as! String?
			trackNumberNumber = item.valueForProperty(MPMediaItemPropertyAlbumTrackNumber) as! NSNumber?
		}
		cell!.trackNumber = trackNumberNumber!
		cell!.titleLabel?.text = title!
		cell!.imageView?.image = nil
		if nil == url {
			cell!.titleLabel?.textColor = UIColor.lightGrayColor()
			cell!.trackLabel?.textColor = UIColor.lightGrayColor()
			cell!.selectionStyle = .None
		}
		return cell!
	}

	// Override to support conditional editing of the table view.
	
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		// Return NO if you do not want the specified item to be editable.
		return false
	}
	
	override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
		if indexPath.section == 1 {
			if indexPath.row != 2 * (indexPath.row / 2) {
				cell.contentView.backgroundColor = RGBA(240, 240, 240, 0.5)
			}
			else {
				cell.contentView.backgroundColor = UIColor.whiteColor()
			}
		}
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		if let selectedRow = tableView.indexPathForSelectedRow {
			tableView.deselectRowAtIndexPath(selectedRow, animated: true)
		}
		
		// Tapping Sound Clip Section
		if indexPath.section == 0 {
			if soundClips?.count > 0 {
				let controller = MNClipCollectionTableViewController()
				controller.organizeType = .ByTrack
				controller.soundClips = soundClips
				navigationController!.pushViewController(controller, animated: true)
			}
			return
		}
		
		let item = mediaItems[indexPath.row]
		let url = item.valueForProperty(MPMediaItemPropertyAssetURL) as! NSURL?
		if nil == url {
			return // Cannot play
		}
		let controller = MNPlayerViewController.sharedPlayer
		controller.mediaItems = mediaItems
		controller.mediaItem = item
		navigationController!.pushViewController(controller, animated: true)
		controller.playFromBeginning(true)
	}
	
	override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
		if mediaItems.count < 5 {
			return nil
		}
		var array: [String] = []
		for var hoge = 0; hoge < mediaItems.count; hoge++ {
			let title: String = "\(hoge + 1)"
			array.append(title)
		}
		return array
	}
	
	override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
		let indexPath = NSIndexPath(forRow: index, inSection: 1)
		tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: false)
		return NSNotFound
	}
	
}

@objc enum ClipCollectionOrganizeType : Int {
	case None = 0
	case ByAlbumTitle
	case ByTrack
	case Arbitrary
}

class SoundClipCollectionTableViewController : MediaCollectionTableViewController {
	var soundClips: [MNSoundClip]? = nil {
		didSet {
			if organizeType != .None {
				organizeSoundClips()
			}
		}
	}
	var playingIndexPath_:NSIndexPath? = nil
	var playNextClip = false
	var enableToolbar = true
	var playlistPredicate:NSPredicate? = nil
	var organizedArray_:[[String : AnyObject]] = []
	var currentProgress_: CGFloat = 0.0
	var repeatMode:SPRepeatType = {
		let ud = NSUserDefaults.standardUserDefaults()
		var repeatMode:SPRepeatType = .None
		if ud.objectForKey("MNClipCollectionTableViewControllerRepeatMode") != nil  {
			repeatMode = SPRepeatType(rawValue: ud.integerForKey("MNClipCollectionTableViewControllerRepeatMode") ) ?? .None
		}
		return repeatMode
	}()
	var organizeType:ClipCollectionOrganizeType = .None {
		didSet {
			if organizeType != .None {
				organizeSoundClips()
			}
		}
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		coder.encodeInteger(organizeType.rawValue, forKey: "organizeType")
		var array: [NSURL] = []
		if soundClips != nil {
			for soundClip in soundClips! {
				array.append(soundClip.objectID.URIRepresentation())
			}
		}
		coder.encodeObject(array, forKey: "soundClipsObjectID")
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		organizeType = ClipCollectionOrganizeType(rawValue:coder.decodeIntegerForKey("organizeType")) ?? .None
		let objectIDs: [NSURL] = coder.decodeObjectForKey("soundClipsObjectID") as! [NSURL]? ?? []
		let context = APP_DELEGATE().managedObjectContext
		let coordinator = APP_DELEGATE().persistentStoreCoordinator
		soundClips = []
		for objectIDURL: NSURL in objectIDs {
			if let objectID = coordinator.managedObjectIDForURIRepresentation(objectIDURL) {
				let object = context.objectWithID(objectID) as! MNSoundClip
				soundClips!.append(object)
			}
		}
	}
	
	func getNextIndexPathToPlay(indexPath:NSIndexPath) -> NSIndexPath? {
		
		if organizeType != .None {
			let dict:[NSObject : AnyObject] = organizedArray_[indexPath.section]
			let nextRow = indexPath.row + 1
			if nextRow >= (dict["soundClips"] as! [MNSoundClip]).count {
				//next section
				let nextSection = indexPath.section + 1
				if nextSection >= organizedArray_.count {
					return nil
				}
				return NSIndexPath(forRow: 0, inSection: nextSection)
			}
			return NSIndexPath(forRow: nextRow, inSection: indexPath.section)
		}
		let nextRow = indexPath.row + 1
		if soundClips == nil || nextRow >= soundClips!.count { return nil }
		return NSIndexPath(forRow: nextRow, inSection: indexPath.section)
	}
	
	func soundClipForIndexPath(indexPath:NSIndexPath) -> MNSoundClip? {
		
		if organizeType != .None {
			var dict: [NSObject : AnyObject] = organizedArray_[indexPath.section]
			let clips = dict["soundClips"] as! [MNSoundClip]?
			
			if clips == nil { return nil }
			if clips!.count <= indexPath.row { return nil }
			let clip: MNSoundClip = clips![indexPath.row]
			return clip
		}
		if soundClips == nil { return nil }
		if soundClips!.count <= indexPath.row { return nil }
		return soundClips![indexPath.row]
	}

	

	//MARK:-

	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	func fetchSoundClips() {
		soundClips = nil
		organizedArray_ = []
		tableView.reloadData()
		MNSoundClip.fetchWithPredicate(playlistPredicate, completion: {(clips: [MNSoundClip], error: NSError?) -> Void in
			self.soundClips = clips
			self.tableView.reloadData()
		})
	}
	
	func indexPathForSoundClip(soundClip:MNSoundClip) -> NSIndexPath? {
		
		if soundClips == nil { return nil }
		
		var indexPath: NSIndexPath? = nil
		
		//  Look for soundClip
		if organizeType != .None {
			for var section = 0; section < organizedArray_.count; section++ {
				var dict: [NSObject : AnyObject] = organizedArray_[section]
				var array = (dict["soundClips"] as! [MNSoundClip])
				for var row = 0; row < array.count; row++ {
					let clip: MNSoundClip = array[row]
					if clip.isEqual(soundClip) {
						indexPath = NSIndexPath(forRow: row, inSection: section)
					}
				}
			}
		}
		else {
			for var row = 0; row < soundClips!.count; row++ {
				let clip: MNSoundClip = soundClips![row]
				if clip.isEqual(soundClip) {
					indexPath = NSIndexPath(forRow: row, inSection: 0)
				}
			}
		}
		
		return indexPath
	}
	
	func organizeSoundClips() {
		
		if soundClips == nil { return }
		
		if organizeType == .ByTrack {
			// array of dictionarys {albumTitle}
			var dict = [String : [MNSoundClip]]()
			for clip in soundClips! {
				var array: [MNSoundClip]? = dict[clip.persistentID!]
				if nil == array {
					array = []
					dict[clip.persistentID!] = [clip]
				}else {
					array!.append(clip)
					dict[clip.persistentID!] = array
				}
			}
			
			//
			organizedArray_ = []
			let allKeys: [String] = Array(dict.keys)
			for key in allKeys {
				if var value = dict[key] {
					value.sortInPlace{ $0.creationDate < $1.creationDate }
					let clip: MNSoundClip = value.last!
					var headerTitle: String = ""
					if clip.trackNumber == 0 {
						headerTitle = clip.title ?? "Unknown Title"
					}else {
						let title = clip.title ?? ""
						headerTitle = "\(String(clip.trackNumber)). \(title)"
					}
					var dict2: [String : AnyObject] = [:]
					dict2["headerTitle"] = headerTitle
					dict2["soundClips"] = value
					dict2["trackNumber"] = Int(clip.trackNumber)
					organizedArray_.append(dict2)
				}
			}
			
			organizedArray_.sortInPlace{ ($0["trackNumber"] as! Int) < ($1["trackNumber"] as! Int) }
		}
		
		
		if organizeType == .ByAlbumTitle {
			
			if soundClips == nil { return }
			
			// array of dictionarys {albumTitle}
			var dict: [String : [MNSoundClip]] = [String : [MNSoundClip]]()
			for clip in soundClips! {
				if clip.albumPersistentID == nil { continue }
				
				var array: [MNSoundClip]? = dict[clip.albumPersistentID!]
				if nil == array {
					array = []
					dict[clip.albumPersistentID!] = [clip]
				}else {
					array!.append(clip)
					dict[clip.albumPersistentID!] = array!
				}
			}
			//
			organizedArray_ = []
			let allKeys: [String] = Array(dict.keys)
			for key in allKeys {
				if var value = dict[key] {
					value.sortInPlace{ $0.creationDate < $1.creationDate }
					let clip: MNSoundClip = value.last!
					let headerTitle = clip.albumTitle ?? "Unknown Album"
					
					var dict2: [String : AnyObject] = [:]
					dict2["headerTitle"] = headerTitle
					dict2["soundClips"] = value
					organizedArray_.append(dict2)
				}
			}
			organizedArray_.sortInPlace{ ($0["headerTitle"] as! String) < ($1["headerTitle"] as! String) }
		}
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		tableView.reloadData()
		
		if enableToolbar {
			updateToolbar()
			navigationController?.toolbarHidden = false
			MNPlayerViewController.sharedPlayer.playingClipCollection = true
		}
	}
	
	func unselect() {
		if playingIndexPath_ != nil {
			stop()
		}
	}
	
	func canPlay() -> Bool {
		if nil == soundClips {
			// Loading
			return false
		}
		// Return the number of rows in the section.
		if organizeType != .None {
			return organizedArray_.count > 0
		}
		return soundClips!.count > 0
	}
	
	func play() {
		if canPlay() == false { return }
		let indexPath = NSIndexPath(forRow: 0, inSection: 0)
		tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
		
		playNextClip = true
		playClipAt( indexPath )
	}
	
	func stop() {
		playNextClip = false
		let player = MNPlayerViewController.sharedPlayer
		player.stop()
	}
	
	
	func updateToolbar() {
		if enableToolbar == false { return }
		
		let playintItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Play, target: self, action: "play")
		let stopItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Pause, target: self, action: "stop")
		
		var image: UIImage!
		if repeatMode == .None {
			image = UIImage(named: "RepeatNoneFlat")!
			image = UIImage.colorizedImage(image, withTintColor: UIColor.lightGrayColor(), alpha: 1, glow: false)
		}
		if repeatMode == .All {
			image = UIImage(named: "RepeatNoneFlat")!
			image = UIImage.colorizedImage(image, withTintColor: view.tintColor, alpha: 1, glow: false)		}
		if repeatMode == .Track {
			image = UIImage(named: "RepeatTrackFlat")!
			image = UIImage.colorizedImage(image, withTintColor: view.tintColor, alpha: 1, glow: false)
		}
		let button = UIButton(type: .Custom)
		button.addTarget(self, action: "repeatButton:", forControlEvents: .TouchUpInside)
		button.setImage(image, forState: .Normal)
		button.frame = CGRectMake(0, 0, 40, 40)
		let repeatButton = UIBarButtonItem(customView: button)
		
		let spacing = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
		
		if playingIndexPath_ != nil {
			toolbarItems = [stopItem, spacing, repeatButton]
		}else {
			toolbarItems = [playintItem, spacing, repeatButton]
			
		}
	}
	
	func repeatButton(sender: AnyObject) {
		repeatMode = repeatMode.next()
		updateToolbar()
		saveToUserDefaults()
	}
	
	func saveToUserDefaults() {
		let ud = NSUserDefaults.standardUserDefaults()
		ud.setInteger(repeatMode.rawValue, forKey: "MNClipCollectionTableViewControllerRepeatMode")
		ud.synchronize()
	}

	func flashSoundClip(soundClip: MNSoundClip) {
		
		if let indexPath = indexPathForSoundClip(soundClip) {
			tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
			tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Middle, animated: true)
			let delayInSeconds: Double = 1.0
			let popTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)))
			dispatch_after(popTime, dispatch_get_main_queue(), {
				self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
			})
		}
	}
	
	func playClipAt(indexPath: NSIndexPath) {
		currentProgress_ = 0
		let center = NSNotificationCenter.defaultCenter()
		let player = MNPlayerViewController.sharedPlayer
		if playingIndexPath_ == indexPath {
			stop()
			updateToolbar()
			return
		}
		
		// Another clip is playing
		if playingIndexPath_ != nil {
			stop()
		}
		
		playingIndexPath_ = indexPath
		guard let clip = soundClipForIndexPath(indexPath) else {
			stop()
			updateToolbar()
			return
		}
	
		center.addObserver(self, selector: "playing:", name: "MNPlayerViewControllerPlaying", object: nil)
		center.addObserver(self, selector: "playingEnded:", name: "MNPlayerViewControllerDidFinishPlaying", object: nil)
		
		if enableToolbar {
			player.playingClipCollection = true
		}
		
		let ullvalue: UInt64 = strtoull((clip.persistentID! as NSString).UTF8String, nil, 0)
		if (player.mediaItem?.valueForProperty(MPMediaItemPropertyPersistentID) as! NSNumber?)?.unsignedLongLongValue != ullvalue {
			
			let mediaItem = MNSoundClip.lookForMedia(clip)
			
			if mediaItem != nil {
				player.mediaItem = mediaItem
				player.playFromBeginning(true, withRange: clip.timeRange)
			}
			else {
				// when clip is not found, look for embedded data
				if clip.binaryData != nil {
					let fm = NSFileManager.defaultManager()
					var tempURL = NSURL.fileURLWithPath(NSTemporaryDirectory())
					tempURL = tempURL.URLByAppendingPathComponent("com.catalystwo")
					if !fm.fileExistsAtPath(tempURL.path!) {
						do {
							try fm.createDirectoryAtURL(tempURL, withIntermediateDirectories: true, attributes: nil)
						}catch {
							
						}
					}
					let filename = ("\(clip.title!).m4a" as NSString).safeFilename()
					tempURL = tempURL.URLByAppendingPathComponent(filename)
					
					if !fm.fileExistsAtPath(tempURL.path!) {
						if false == clip.binaryData!.writeToURL(tempURL, atomically: false) {
							tempURL = tempURL.URLByAppendingPathComponent("\(clip.uuid).m4a")
							if false == clip.binaryData!.writeToURL(tempURL, atomically: false)  {
								return
							}
						}
					}
					player.mediaItem = nil
					player.setAssetAt(tempURL)
					player.playFromBeginning(true)
					
				}else {
					// Original item does not exists
					tableView.deselectRowAtIndexPath(indexPath, animated: true)
					playingIndexPath_ = nil
					NSNotificationCenter.defaultCenter().removeObserver(self)
					
					ShowAlertMessage("", self)
				}
			}
		}else {
			player.playFromBeginning(true, withRange: clip.timeRange)
		}
		setupNowPlayingButton()
		updateToolbar()
	}
	
	


	func playing(notification: NSNotification) {
		if playingIndexPath_ == nil { return }
		let cell = tableView.cellForRowAtIndexPath(playingIndexPath_!) as! SoundClipTableViewCell?
		
		var userInfo: [NSObject : AnyObject] = notification.userInfo!
		let timeDictionary = userInfo["time"] as! [NSObject : AnyObject]
		let time = CMTimeMakeFromDictionary(timeDictionary as CFDictionaryRef)
		let selectedRange = MNPlayerViewController.sharedPlayer.selectedRange
		let elapsed: Float64 = (CMTimeGetSeconds(time) - CMTimeGetSeconds(selectedRange.start)) / CMTimeGetSeconds(selectedRange.duration)
		currentProgress_ = CGFloat(elapsed)
		cell?.progress = currentProgress_
	}
	
	func playingEnded(notification: NSNotification?) {
		
		NSNotificationCenter.defaultCenter().removeObserver(self)
		
		if playingIndexPath_ == nil { return }
		
		let cell = tableView.cellForRowAtIndexPath(playingIndexPath_!) as! SoundClipTableViewCell?
		cell?.progress = -1
		
		if let selectedRow = tableView.indexPathForSelectedRow {
			tableView.deselectRowAtIndexPath(selectedRow, animated: true)
		}
		
		if playNextClip == false || repeatMode == .None || MNPlayerViewController.sharedPlayer.playingClipCollection == false {
			playingIndexPath_ = nil
			
			updateToolbar()
			return
		}
		
		if repeatMode == .All {
			if let indexPath = getNextIndexPathToPlay(playingIndexPath_!) {
				playingIndexPath_ = nil
				playClipAt(indexPath)
				tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
				
				return
			}else {
				// Start from beginning
				let indexPath = NSIndexPath(forRow: 0, inSection: 0)
				playingIndexPath_ = nil
				playClipAt(indexPath)
				tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
				return
			}
		}
		
		if repeatMode == .Track {
			let indexPath = playingIndexPath_
			playingIndexPath_ = nil
			playClipAt(indexPath!)
			tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
			
			return
		}
		
		playingIndexPath_ = nil
		updateToolbar()
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		// Return the number of sections.
		if nil == soundClips {
			return 1
		}
		if organizeType != .None {
			return organizedArray_.count
		}
		
		return 1
	}
	
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
	
		let cell = self.tableView(tableView, cellForRowAtIndexPath: indexPath) as! SoundClipTableViewCell
		// top row : 290
		// second row : 245
		var width = view.bounds.size.width
		if cell.accessoryView == nil {
			width -= 20
		}
		else {
			width -= 55
		}
		cell.updateHeightForWidth(width)
		return max(cell.cellHeight, 44)
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if nil == soundClips {
			// Loading
			return 1
		}
		// Return the number of rows in the section.
		if organizeType != .None {
			var dict: [String : AnyObject] = organizedArray_[section]
			return ((dict["soundClips"] as! [MNSoundClip]).count ?? 0)
		}
		return soundClips!.count
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		
		if nil == soundClips { return nil }
		if organizeType == .None {
			return nil
		}
		var dict: [String : AnyObject] = organizedArray_[section]
		let title = dict["headerTitle"] ?? ""
		return title as? String
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let CellIdentifier = "ClipCell"
		let cell: SoundClipTableViewCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as! SoundClipTableViewCell? ??
			SoundClipTableViewCell(style: .Default, reuseIdentifier: CellIdentifier)
		
		if nil == soundClips {
			// Loading
			let attr = NSAttributedString(string: WLoc("Loading"), attributes: SoundClipTableViewCell.disabledAttributes() as? [String:AnyObject])
			cell.attributedString = attr
			cell.selectionStyle = .None
			return cell
		}
		
		
		cell.accessoryType = .DetailButton
		cell.selectionStyle = .Blue
		
		if organizeType != .None {
			var dict: [NSObject : AnyObject] = organizedArray_[indexPath.section]
			let clip: MNSoundClip = (dict["soundClips"] as! [MNSoundClip])[indexPath.row]
			if clip.transcription?.characters.count > 0 {
				let attr = NSAttributedString(string: clip.transcription!, attributes: SoundClipTableViewCell.excerptAttributes() as? [String:AnyObject])
				cell.attributedString = attr
			}
			else {
				let attr = NSAttributedString(string: clip.title ?? "", attributes: SoundClipTableViewCell.attributes() as? [String:AnyObject])
				cell.attributedString = attr
			}
			
			if clip.binaryData != nil {
				cell.copiedClip = true
			}
			cell.indentationLevel = 2
			return cell
			
		}
		
		let clip = soundClips![indexPath.row]
		
		let attr = NSAttributedString(string: clip.title ?? "", attributes: SoundClipTableViewCell.attributes() as? [String:AnyObject])
		cell.attributedString = attr
		if indexPath.isEqual(playingIndexPath_) {
			cell.progress = currentProgress_
			cell.selected = true
		}
		
		return cell
		
	}
	
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		if nil == soundClips { return false }
		return true
	}
	
	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		if soundClips == nil { return }
		
		if .Delete == editingStyle {
			if organizeType != .None {
				var dict: [NSObject : AnyObject] = organizedArray_[indexPath.section]
				var soundClips = dict["soundClips"] as! [MNSoundClip]
				let clip = soundClips[indexPath.row]
				APP_DELEGATE().managedObjectContext.deleteObject(clip)
				APP_DELEGATE().saveContext()
				soundClips.removeAtIndex(indexPath.row)
				dict["soundClips"] = soundClips
				tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			}
			else {
				let clip = soundClips![indexPath.row]
				APP_DELEGATE().managedObjectContext.deleteObject(clip)
				APP_DELEGATE().saveContext()
				soundClips!.removeAtIndex(indexPath.row)
				tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			}
		}
	}
	
	func accessoryButtonTapped(button: ClipInfoButton) {
		let indexPath: NSIndexPath = button.representedObject as! NSIndexPath
		tableView(tableView, accessoryButtonTappedForRowWithIndexPath: indexPath)
	}
	
	override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
		if soundClips == nil { return }
		
		var clip: MNSoundClip? = nil
		if organizeType != .None {
			var dict: [NSObject : AnyObject] = organizedArray_[indexPath.section]
			clip = (dict["soundClips"] as! [MNSoundClip])[indexPath.row]
		}
		else {
			clip = soundClips![indexPath.row]
		}
		let editor = MNSoundClipEditor()
		editor.clip = clip!
		let nav = MNBaseNavigationController(rootViewController: editor)
		if navigationController != nil {
			navigationController!.pushViewController(editor, animated: true)
		}
		else {
			MNPlayerViewController.sharedPlayer.presentViewController(nav, animated: true, completion: nil)
		}
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		if nil == soundClips {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
			return
		}
		
		playNextClip = true
		playClipAt(indexPath)
	}
}

//class SoundClipPickerTableViewController : SoundClipCollectionTableViewController {
//	
//	override func viewWillAppear(animated: Bool) {
//		super.viewWillAppear(animated)
//		
//		fetchSoundClips()
//	}
//	
//	func fetchSoundClips() {
//		soundClips = nil
//		tableView.reloadData()
//		MNSoundClip.fetchWithPredicate(nil, completion: {(clips: [MNSoundClip], error: NSError?) -> Void in
//			self.soundClips = clips
//			self.tableView.reloadData()
//		})
//	}
//	
//	
//}

class MNClipCollectionTableViewController : SoundClipCollectionTableViewController, UIViewControllerRestoration {
	
	var displayAllClips = false
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		coder: NSCoder) -> UIViewController? {
			let controller = MNClipCollectionTableViewController()
			return controller
	}
	
	override init() {
		super.init()
		// Custom initialization
		self.title = WLoc("Sound Clips")
		self.restorationIdentifier = "MNClipCollectionViewControllerID"
		self.restorationClass = self.dynamicType
		
	}
	
	convenience init(playlistPredicate predicate: NSPredicate) {
		self.init()
		soundClips = nil
		playlistPredicate = predicate
		self.restorationIdentifier = "MNClipCollectionViewControllerID"
		self.restorationClass = self.dynamicType
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		coder.encodeBool(displayAllClips, forKey: "displayAllClips_")
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		displayAllClips = coder.decodeBoolForKey("displayAllClips_")
	}
	
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if displayAllClips || nil != playlistPredicate {
			fetchSoundClips()
		}
	}
}

class ClipsInPlaylistTableViewController : SoundClipCollectionTableViewController, UIViewControllerRestoration {
	
	var displayAllClips = true
	var playlist:ClipPlaylist!
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		coder: NSCoder) -> UIViewController? {
			let controller = MNClipCollectionTableViewController()
			return controller
	}
	
	override init() {
		super.init()
		// Custom initialization
		self.title = WLoc("Sound Clips")
		self.restorationIdentifier = "ClipsInPlaylistTableViewControllerID"
		self.restorationClass = self.dynamicType
	}
	
	convenience init(playlist list:ClipPlaylist) {
		self.init()
		soundClips = nil
		let uuids = list.clipsUUIDs
		playlistPredicate = NSPredicate(format: "uuid IN %@", uuids)
		playlist = list
		organizeType = .None
		self.restorationIdentifier = "MNClipCollectionViewControllerID"
		self.restorationClass = self.dynamicType
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		coder.encodeObject(playlist.objectID.URIRepresentation(), forKey: "playlistID")
		coder.encodeBool(displayAllClips, forKey: "displayAllClips_")
	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		let context = APP_DELEGATE().managedObjectContext
		let coordinator = APP_DELEGATE().persistentStoreCoordinator
		if let objectIDURL = coder.decodeObjectForKey("playlistID") as! NSURL? {
			
			if let objectID = coordinator.managedObjectIDForURIRepresentation(objectIDURL) {
				playlist = context.objectWithID(objectID) as! ClipPlaylist
			}
		}
			
		displayAllClips = coder.decodeBoolForKey("displayAllClips_")
	}
	
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		fetchSoundClips()
	}
	
	override func fetchSoundClips() {
		soundClips = nil
		organizedArray_ = []
		tableView.reloadData()
		let array = playlist.clipsUUIDs

		playlistPredicate = NSPredicate(format: "uuid IN %@", array)

		MNSoundClip.fetchWithPredicate(playlistPredicate, completion: {(clips: [MNSoundClip], error: NSError?) -> Void in
			self.soundClips = clips.sort{ array.indexOf( $0.uuid! ) < array.indexOf( $1.uuid! ) }
			self.tableView.reloadData()
		})
	}
	
	override func rightNavigationButton() -> UIBarButtonItem? {
		return editButtonItem()
	}
	
	override func setEditing(editing: Bool, animated: Bool) {
		stop()
		super.setEditing(editing, animated: animated)
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		if nil == soundClips { return 1 }

		return 2
	}
	
	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 44
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if nil == soundClips {
			// Loading
			return 1
		}
		
		if section == 0 {
			return soundClips!.count
		}
		
		return 1 //section 1
	}
	
	override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return nil
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		if indexPath.section == 0 {
			return super.tableView(tableView, cellForRowAtIndexPath: indexPath)
		}
		
		let cell = UITableViewCell(style: .Default, reuseIdentifier: nil)

		// Add
		cell.textLabel?.text = WLoc("Add Sound Clip")
		cell.textLabel?.textAlignment = .Center
		cell.textLabel?.textColor = self.view.tintColor
		
		return cell
		
	}
	
	override func tableView(tableView: UITableView,
		targetIndexPathForMoveFromRowAtIndexPath sourceIndexPath: NSIndexPath,
		toProposedIndexPath proposedDestinationIndexPath: NSIndexPath) -> NSIndexPath {
			if proposedDestinationIndexPath.section == 1 {
				return NSIndexPath(forRow: soundClips!.count - 1, inSection: 0)
			}else {
				return proposedDestinationIndexPath
			}
	}
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		if nil == soundClips { return false }
		if indexPath.section == 0 { return true }
		return false
	}
	
	override func tableView( tableView: UITableView,
  canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
	if nil == soundClips { return false }
	if indexPath.section == 0 { return true }
	return false
	}
	
	override func tableView( tableView: UITableView,
		titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
			return WLoc("Un-list")
	}
	
	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		if soundClips == nil { return }
		
		if editingStyle == .Delete {
			let clip = soundClips![indexPath.row]
			var uuids = playlist.clipsUUIDs
			if uuids.contains(clip.uuid!) {
				uuids.removeAtIndex(uuids.indexOf(clip.uuid!)!)
			}
			playlist.clipsUUIDs = uuids
			APP_DELEGATE().saveContext()
			soundClips!.removeAtIndex(indexPath.row)
			tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			return
		}
		
	}
	
	override func tableView( tableView: UITableView,
		moveRowAtIndexPath sourceIndexPath: NSIndexPath,
		toIndexPath destinationIndexPath: NSIndexPath) {
			
		var uuids = playlist.clipsUUIDs
		let uuid = uuids[sourceIndexPath.row]
		uuids.removeAtIndex(sourceIndexPath.row)
		uuids.insert(uuid, atIndex: destinationIndexPath.row)
		
		playlist.clipsUUIDs = uuids
		APP_DELEGATE().saveContext()
			
		let soundClip = soundClips![sourceIndexPath.row]
		soundClips!.removeAtIndex(sourceIndexPath.row)
		soundClips!.insert(soundClip, atIndex: destinationIndexPath.row)
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		if nil == soundClips {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
			return
		}
		if indexPath.section == 0 {
			super.tableView(tableView, didSelectRowAtIndexPath: indexPath)
			return
		}

		// Picker
		
		let controller = SoundClipPickerViewController()
		controller.completion = { clips in
		
			var uuids:[String] = []
			for clip in clips {
				if clip.uuid != nil {
					uuids.append(clip.uuid!)
				}
			}
			self.playlist.clipsUUIDs = uuids
			APP_DELEGATE().saveContext()

			self.playlistPredicate = NSPredicate(format: "uuid IN %@", uuids)
			self.fetchSoundClips()

		}
		
		controller.selectedUUIDs = playlist.clipsUUIDs
		let nav = UINavigationController(rootViewController: controller)
		presentViewController(nav, animated: true, completion: nil)
	}

}

class SoundClipPickerViewController : SoundClipCollectionTableViewController {
	
	var displayAllClips = true
	var selectedUUIDs:[String] = []
	var completion:([MNSoundClip]->Void)? = nil
	
	override init() {
		super.init()
		// Custom initialization
		self.title = WLoc("Sound Clips")
		organizeType = .ByAlbumTitle
		enableToolbar = false

	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		fetchSoundClips()
	}
	
	override func setupNowPlayingButton() {
		let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "done:")
		navigationItem.rightBarButtonItem = button
		
		let cancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel:")
		navigationItem.leftBarButtonItem = cancel
	}
	
	func done(sender:AnyObject) {
		
		var clips:[MNSoundClip] = []
		for uuid in selectedUUIDs {
			if let clip = soundClipForUUID(uuid) {
				clips.append(clip)
			}
		}
		completion?(clips)
		dismissViewControllerAnimated(true, completion: nil)
	}
	
	func soundClipForUUID(uuid:String) -> MNSoundClip? {
		
		if organizeType != .None {
			for  dict: [NSObject : AnyObject] in organizedArray_ {
				let clips = dict["soundClips"] as! [MNSoundClip]?
				
				if clips == nil { continue }
				
				for clip in clips! {
					if clip.uuid == uuid { return clip }
				}
				continue
			}
		}
		
		if soundClips == nil { return nil }
		for clip in soundClips! {
			if clip.uuid == uuid { return clip }
		}
		
		return nil
	}
	
	func cancel(sender:AnyObject) {
		dismissViewControllerAnimated(true, completion: nil)
	}
	
	func addClip(sender:SelectButton) {
		
		if selectedUUIDs.contains((sender.representedObject as? String)!) {
			selectedUUIDs.removeAtIndex(selectedUUIDs.indexOf((sender.representedObject as? String)!)!)
			sender.selected = false
		}else {
			selectedUUIDs.append((sender.representedObject as? String)!)
			sender.selected = true
		}
//		tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let CellIdentifier = "ClipCell"
		let cell: SoundClipTableViewCell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as! SoundClipTableViewCell? ??
			SoundClipTableViewCell(style: .Default, reuseIdentifier: CellIdentifier)
		cell.selectionStyle = .Blue
		cell.accessoryView = nil

		if nil == soundClips {
			// Loading
			let attr = NSAttributedString(string: WLoc("Loading"), attributes: SoundClipTableViewCell.disabledAttributes() as? [String:AnyObject])
			cell.attributedString = attr
			cell.selectionStyle = .None
			return cell
		}
		


		if organizeType != .None {
			var dict: [NSObject : AnyObject] = organizedArray_[indexPath.section]
			let clip: MNSoundClip = (dict["soundClips"] as! [MNSoundClip])[indexPath.row]
			if clip.transcription?.characters.count > 0 {
				let attr = NSAttributedString(string: clip.transcription!, attributes: SoundClipTableViewCell.excerptAttributes() as? [String:AnyObject])
				cell.attributedString = attr
			}
			else {
				let attr = NSAttributedString(string: clip.title ?? "", attributes: SoundClipTableViewCell.attributes() as? [String:AnyObject])
				cell.attributedString = attr
			}
			
			if clip.binaryData != nil {
				cell.copiedClip = true
			}
			cell.indentationLevel = 2
			
			let button = SelectButton()
			button.sizeToFit()
			button.representedObject = clip.uuid
			button.addTarget(self, action: "addClip:", forControlEvents: .TouchUpInside)
			cell.accessoryView = button
			
			if clip.uuid != nil && selectedUUIDs.contains(clip.uuid!) {
				
				button.selected = true
			}else {

			}
			return cell
			
		}
		
		let clip = soundClips![indexPath.row]
		
		let attr = NSAttributedString(string: clip.title ?? "", attributes: SoundClipTableViewCell.attributes() as? [String:AnyObject])
		cell.attributedString = attr
		if indexPath.isEqual(playingIndexPath_) {
			cell.progress = currentProgress_
			cell.selected = true
		}
		
		let button = SelectButton()
		button.sizeToFit()
		button.representedObject = clip.uuid
		button.addTarget(self, action: "addClip:", forControlEvents: .TouchUpInside)
		cell.accessoryView = button
		
		if clip.uuid != nil && selectedUUIDs.contains(clip.uuid!) {
			button.selected = true

		}else {

		}
		
		return cell
		
	}
	
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return false
	}
	
}

class ClipPlaylistsPickerViewController : ClipPlaylistsTableViewController {

	var clip:MNSoundClip!
	var selectedPlaylists:[ClipPlaylist] = []

	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		title = WLoc("Folders")
		navigationItem.prompt = WLoc("Choose Playlist to add this clip")
	}
	
	override func setupNowPlayingButton() {
		let button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Save, target: self, action: "done:")
		navigationItem.rightBarButtonItems = [button, editButtonItem()]
		
		let cancel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: "cancel:")
		navigationItem.leftBarButtonItem = cancel
	}
	
	func done(sender:AnyObject) {
		guard playlists != nil else { return }
		defer{ dismissViewControllerAnimated(true, completion: nil) }
		
		for pl in playlists! {
			
			var uuids = pl.clipsUUIDs
			
			if uuids.contains(clip.uuid!) == true {
				if selectedPlaylists.contains(pl) {
					// nothing
				}else {
					// remove from uuids
					uuids.removeAtIndex(uuids.indexOf(clip.uuid!)!)
					pl.clipsUUIDs = uuids
				}

			}else {
				if selectedPlaylists.contains(pl) == true {
					uuids.append(clip.uuid!)
					pl.clipsUUIDs = uuids
				}else {
					// nothing

				}
			}
		}
		
		APP_DELEGATE().saveContext()
		
	}
	
	func cancel(sender:AnyObject) {
		dismissViewControllerAnimated(true, completion: nil)
	}
	
	func playlistForUUID(uuid:String) -> ClipPlaylist? {
		
		if playlists == nil { return nil }
		for pl in playlists! {
			if pl.uuid == uuid { return pl }
		}
		
		return nil
	}
	
	override func fetchAllPlaylist() {
		playlists = nil
		tableView.reloadData()
		ClipPlaylist.fetchAllPlaylist {(playlists: [ClipPlaylist], error: NSError?) -> Void in
			
			self.playlists = playlists.sort{ $0.index < $1.index }
			self.selectedPlaylists = []
			for pl in playlists {
				let uuids = pl.clipsUUIDs
				if uuids.contains(self.clip.uuid!) {
					self.selectedPlaylists.append(pl)
				}
			}
			self.tableView.reloadData()
		}
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if playlists == nil { return 1 }
		
		if section == 0 { return 0 } // Disabled
		if section == 1 { return playlists!.count }
		if section == 2 { return 1 }
		return 0
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let CellIdentifier = "Playlist"
		var cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) ??
			UITableViewCell(style: .Default, reuseIdentifier: CellIdentifier)
		cell.selectionStyle = .Blue
		cell.accessoryView = nil

		if nil == playlists {
			// Loading
			cell.textLabel?.text = WLoc("Loading")
			cell.selectionStyle = .None
			return cell
		}
		
		// Add
		if indexPath.section == 2 {
			cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
			cell.textLabel?.text = WLoc("Add Folder")
			cell.textLabel?.textAlignment = .Center
			cell.textLabel?.textColor = self.view.tintColor
			return cell
		}
		
		let playlist = playlists![indexPath.row]
		cell.accessoryView = nil
		cell.selectionStyle = .Blue
			let title = "📂 " + (playlists![indexPath.row].title ?? "")
			cell.textLabel?.text = title
		
		let button = SelectButton()
		button.sizeToFit()
		button.representedObject = playlist
		button.addTarget(self, action: "addToPlaylist:", forControlEvents: .TouchUpInside)
		button.selected = selectedPlaylists.contains(playlist)
		cell.accessoryView = button

		return cell
	}
	
	func addToPlaylist(sender:SelectButton) {
		
		if selectedPlaylists.contains((sender.representedObject as? ClipPlaylist)!) {
			selectedPlaylists.removeAtIndex(selectedPlaylists.indexOf((sender.representedObject as? ClipPlaylist)!)!)
			sender.selected = false
		}else {
			selectedPlaylists.append((sender.representedObject as? ClipPlaylist)!)
			sender.selected = true
		}
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		tableView.deselectRowAtIndexPath(indexPath, animated: true)
		
		if nil == playlists {
			return
		}
		
		if self.editing {
			if indexPath.section == 1  {
				let alertController = UIAlertController(title: WLoc("Rename"), message: "", preferredStyle: .Alert)
				
				let cancel = UIAlertAction(title: WLoc("Cancel"), style: .Cancel) { (UIAlertAction action) -> Void in }
				
				let action1 = UIAlertAction(title: WLoc("Rename"), style: .Default) { [unowned self] (UIAlertAction action) -> Void in
					
					let fields = alertController.textFields
					if let field = fields?.first as  UITextField? {
						
						let string = field.text
						if string!.isEmpty == true  { return }
						
						self.playlists![indexPath.row].title = string
						APP_DELEGATE().saveContext()
						
						self.playlists?.sortInPlace{ $0.index < $1.index }
						self.tableView?.reloadData()
					}
				}
				alertController.addAction(action1)
				alertController.addAction(cancel)
				alertController.addTextFieldWithConfigurationHandler { (field:UITextField) -> Void in
					
					field.text = self.playlists![indexPath.row].title ?? ""
					field.clearButtonMode = .WhileEditing
					field.keyboardType = .Default
				}
				presentViewController(alertController, animated: true, completion: nil)
				return
			}
		}
		
		if indexPath.section == 0  {
			let controller = MNClipCollectionTableViewController()
			controller.organizeType = .ByAlbumTitle
			controller.displayAllClips = true
			navigationController!.pushViewController(controller, animated: true)
		}
			
		else if indexPath.section == 2 {
			// Add
			addPlaylist()
			return
			
		}else {
			let managedObject = playlists![indexPath.row]
			
			if selectedPlaylists.contains(managedObject) {
				selectedPlaylists.removeAtIndex(selectedPlaylists.indexOf(managedObject)!)
			}else {
				selectedPlaylists.append(managedObject)
			}
			tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
			return
		}
		
	}

	
}



class ClipPlaylistsTableViewController : MediaCollectionTableViewController {
	var playlists:[ClipPlaylist]? = nil
	
	static func viewControllerWithRestorationIdentifierPath( identifierComponents: [AnyObject],
		coder: NSCoder) -> UIViewController? {
			let controller = MNClipCollectionTableViewController()
			return controller
	}
	
	override init() {
		super.init()
		// Custom initialization
		self.title = WLoc("Sound Clips")
		self.restorationIdentifier = "ClipPlaylistsTableViewControllerID"
		self.restorationClass = self.dynamicType
		
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func encodeRestorableStateWithCoder(coder: NSCoder) {
		super.encodeRestorableStateWithCoder(coder)
		var array: [NSURL] = []
		if playlists != nil {
			for plist in playlists! {
				array.append(plist.objectID.URIRepresentation())
			}
		}
		coder.encodeObject(array, forKey: "playlistsObjectID")

	}
	
	override func decodeRestorableStateWithCoder(coder: NSCoder) {
		super.decodeRestorableStateWithCoder(coder)
		let objectIDs: [NSURL] = coder.decodeObjectForKey("playlistsObjectID") as! [NSURL]? ?? []
		let context = APP_DELEGATE().managedObjectContext
		let coordinator = APP_DELEGATE().persistentStoreCoordinator
		playlists = []
		for objectIDURL: NSURL in objectIDs {
			if let objectID = coordinator.managedObjectIDForURIRepresentation(objectIDURL) {
				let object = context.objectWithID(objectID) as! ClipPlaylist
				playlists!.append(object)
			}
		}
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		fetchAllPlaylist()
		tableView.allowsSelectionDuringEditing = true
	}
	
	func fetchAllPlaylist() {
		playlists = nil
		tableView.reloadData()
		ClipPlaylist.fetchAllPlaylist {(playlists: [ClipPlaylist], error: NSError?) -> Void in
			
			self.playlists = playlists.sort{ $0.index < $1.index }
			self.tableView.reloadData()
		}
	}
	
	override func rightNavigationButton() -> UIBarButtonItem? {
		return editButtonItem()
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		if playlists == nil { return 1 }
		return 3
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		if playlists == nil { return 1 }
		
		if section == 0 { return 1 }
		if section == 1 { return playlists!.count }
		if section == 2 { return 1 }
		return 0
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let CellIdentifier = "Playlist"
		var cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) ??
			UITableViewCell(style: .Default, reuseIdentifier: CellIdentifier)
		
		if nil == playlists {
			// Loading
			cell.textLabel?.text = WLoc("Loading")
			cell.selectionStyle = .None
			return cell
		}
		
		cell.accessoryType = .DisclosureIndicator
		cell.selectionStyle = .Blue
		
		if indexPath.section == 0 {
			cell.textLabel?.text = WLoc("All Sound Clips")
			return cell
		}
		
		if indexPath.section == 1 {
			let title = "📂 " + (playlists![indexPath.row].title ?? "")
			
			cell.textLabel?.text = title
			return cell
		}
		
		// Add
		if indexPath.section == 2 {
			cell = UITableViewCell(style: .Default, reuseIdentifier: nil)
			cell.textLabel?.text = WLoc("Add Folder")
			cell.textLabel?.textAlignment = .Center
			cell.textLabel?.textColor = self.view.tintColor
			return cell
		}
	
		return cell // dummy
	}
	
	override func tableView(tableView: UITableView,
		targetIndexPathForMoveFromRowAtIndexPath sourceIndexPath: NSIndexPath,
		toProposedIndexPath proposedDestinationIndexPath: NSIndexPath) -> NSIndexPath {
		if proposedDestinationIndexPath.section == 2 {
			return NSIndexPath(forRow: playlists!.count - 1, inSection: 1)
		}else if proposedDestinationIndexPath.section == 0 {
			return NSIndexPath(forRow: 0, inSection: 1)
			
		}else {
			return proposedDestinationIndexPath
		}
	}
	
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		if indexPath.section == 1 {
			return true
		}
		return false
	}
	
	override func tableView( tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		if nil == playlists { return false }
		if indexPath.section == 1 { return true }
		return false
	}
	
	override func tableView( tableView: UITableView,
		moveRowAtIndexPath sourceIndexPath: NSIndexPath,
		toIndexPath destinationIndexPath: NSIndexPath) {
		
			let plist = playlists![sourceIndexPath.row]
			playlists!.removeAtIndex(sourceIndexPath.row)
			playlists!.insert(plist, atIndex: destinationIndexPath.row)
			
		for playlist in playlists! {
			if let idx = playlists!.indexOf(playlist) {
				playlist.index = Int16(idx)
			}
		}
			
		APP_DELEGATE().saveContext()
	}

	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		if playlists == nil { return }
		
		if .Delete == editingStyle {
			
			let plist = playlists![indexPath.row]
			APP_DELEGATE().managedObjectContext.deleteObject(plist)
			APP_DELEGATE().saveContext()
			playlists!.removeAtIndex(indexPath.row)
			tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
		}
	}
	
	func addPlaylist() {
		
		if playlists == nil { return }
		
		let alertController = UIAlertController(title: WLoc("New Folder"), message: "", preferredStyle: .Alert)
		let cancel = UIAlertAction(title: WLoc("Cancel"), style: .Cancel) { (UIAlertAction action) -> Void in }
		let action1 = UIAlertAction(title: WLoc("Create Folder"), style: .Default) { [unowned self] (UIAlertAction action) -> Void in
			
			let fields = alertController.textFields
			if let field = fields?.first as  UITextField? {
				
				var string = field.text
				if string!.isEmpty == true  { string = "Untitled Folder" }
				
				let context = APP_DELEGATE().managedObjectContext
				let entity = NSEntityDescription.entityForName("ClipPlaylist", inManagedObjectContext: context)
				let playlist = ClipPlaylist(entity: entity!, insertIntoManagedObjectContext: context)
				playlist.creationDate = NSDate().timeIntervalSinceReferenceDate
				playlist.clips = ""
				playlist.index = Int16(self.playlists!.count)
				playlist.title = string
				let uuid = CFUUIDCreate(nil)
				let uuidString = CFUUIDCreateString(nil, uuid)
				playlist.uuid = uuidString as String
				
				APP_DELEGATE().saveContext()
				self.playlists!.append(playlist)
				self.tableView.reloadData()
			}
		}
		alertController.addAction(action1)
		alertController.addAction(cancel)
		alertController.addTextFieldWithConfigurationHandler { (field:UITextField) -> Void in
			field.keyboardType = .Default
		}
		
		presentViewController(alertController, animated: true, completion: nil)
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		tableView.deselectRowAtIndexPath(indexPath, animated: true)

		if nil == playlists {
			return
		}
		
		if self.editing {
			if indexPath.section == 1  {
				let alertController = UIAlertController(title: WLoc("Rename"), message: "", preferredStyle: .Alert)
				
				let cancel = UIAlertAction(title: WLoc("Cancel"), style: .Cancel) { (UIAlertAction action) -> Void in }
				
				let action1 = UIAlertAction(title: WLoc("Rename"), style: .Default) { [unowned self] (UIAlertAction action) -> Void in
					
					let fields = alertController.textFields
					if let field = fields?.first as  UITextField? {
						
						let string = field.text
						if string!.isEmpty == true  { return }
						
						self.playlists![indexPath.row].title = string
						APP_DELEGATE().saveContext()
						
						self.playlists?.sortInPlace{ $0.index < $1.index }
						self.tableView?.reloadData()
					}
				}
				alertController.addAction(action1)
				alertController.addAction(cancel)
				alertController.addTextFieldWithConfigurationHandler { (field:UITextField) -> Void in
					
					field.text = self.playlists![indexPath.row].title ?? ""
					field.clearButtonMode = .WhileEditing
					field.keyboardType = .Default
				}
				presentViewController(alertController, animated: true, completion: nil)
				return
			}
		}
		
		if indexPath.section == 0  {
			let controller = MNClipCollectionTableViewController()
			controller.organizeType = .ByAlbumTitle
			controller.displayAllClips = true
			navigationController!.pushViewController(controller, animated: true)
		}
			
		else if indexPath.section == 2 {
			// Add
			addPlaylist()
			return
			
		}else {
			let managedObject = playlists![indexPath.row]
			
			let controller = ClipsInPlaylistTableViewController(playlist:managedObject)
			navigationController!.pushViewController(controller, animated: true)
			
			return
		}
		
	}

}

class SelectButton : UIButton {
	var representedObject:AnyObject!
	override func sizeThatFits(size: CGSize) -> CGSize {
		return CGSizeMake(40,40)
	}
	override var selected:Bool {
		didSet{
			setNeedsDisplay()
		}
	}
	override func drawRect(rect: CGRect) {
		if selected {
			let image = UIImage(named:"AddClipSelected")!
			image.drawAtPoint(CGPointMake(5,5))
		}else {
			let image = UIImage(named:"AddClip")!
			image.drawAtPoint(CGPointMake(5,5))
		}
	}
	
}

func ShowAlertMessage(message:String, _ inViewController:UIViewController!) {
	
	let alertController = UIAlertController(title: message, message: "", preferredStyle: .Alert)
	
	let cancel = UIAlertAction(title: WLoc("OK"), style: .Cancel) { (UIAlertAction action) -> Void in
	}
	
	alertController.addAction(cancel)
	inViewController.presentViewController(alertController, animated: true, completion: nil)
	
}