//
//  MNFontLabelCell.m
//  MNCTTextView
//
//  Created by Masatoshi Nishikata on 8/01/11.
//  Copyright 2011 Catalystwo Ltd. All rights reserved.
//

#import "AlbumTableViewCell.h"
#import "MNCTFontUtils.h"
#import "DrawUtils.h"
#import <QuartzCore/QuartzCore.h>
#import "MNMenuView.h"
#import "NSString+MNPath.h"
#import "Tui.h"


#define SUB_TEXT_FONT [UIFont systemFontOfSize:14]


#define STANDARD_RIGHT_INDENTATION 10
#define DISCLOSURE_RIGHT_INDENTATION 30
#define DETAILED_DISCLOSURE_RIGHT_INDENTATION 40
#define DELETING_RIGHT_INDENTATION 60
#define REORDER_RIGHT_INDENTATION 40

#define EDITING_LEFT_INDENTATIO 35
#define  gIsRunningOnIPad (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
#define   ICON_WIDTH  (gIsRunningOnIPad?60:44)

@implementation AlbumTableViewCell

@synthesize artworkView = artworkView_;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code.
		self.selectionStyle = UITableViewCellSelectionStyleBlue;
		self.contentView.clipsToBounds = YES;

		self.textLabel.textColor = [UIColor blackColor];
		self.textLabel.minimumFontSize = 10;
		self.textLabel.adjustsFontSizeToFitWidth = YES;

		
		CGSize size = self.bounds.size;
		
		

		UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
		artworkView_ = imageView;
		artworkView_.contentMode = UIViewContentModeScaleAspectFill;

		[self.imageView addSubview: artworkView_];
		self.imageView.clipsToBounds = YES;
		self.imageView.image = [UIImage imageNamed:@"BlankSong.png"];
		
		
	}
	return self;
}



- (void)prepareForReuse
{
	[super prepareForReuse];
	
	[self setUserInteractionEnabled: YES];

	artworkView_.image = nil;
	self.backgroundColor = [UIColor whiteColor];
	
	self.textLabel.text = nil;
	self.detailTextLabel.text = nil;
	self.textLabel.textColor = [UIColor blackColor];
	self.indentationLevel = 0;
	


}


- (void)dealloc {

	artworkView_.image = nil;

}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	artworkView_.frame = self.imageView.bounds;
	
}

@end
