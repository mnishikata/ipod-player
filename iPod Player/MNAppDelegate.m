//
//  MNAppDelegate.m
//  iPod Player
//
//  Created by Nishikata Masatoshi on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MNAppDelegate.h"

#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "MNBaseNavigationController.h"
#import "JishoViewController.h"
#import "AB_Player-Swift.h"

#import "C2Manager.h"
#import "MyStoreObserver.h"
#import "Tui.h"

@implementation MNAppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize fetchManagedObjectContext = _fetchManagedObjectContext;

@synthesize storeObserver = storeObserver_;

static dispatch_queue_t fetchQueue_;

BOOL gIsIOS6 = NO;


-(dispatch_queue_t)fetchQueue
{
	if( !fetchQueue_ )
	{
		fetchQueue_ = dispatch_queue_create("Fetch Queue", DISPATCH_QUEUE_SERIAL);
		dispatch_set_target_queue(fetchQueue_, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
	}
	return fetchQueue_;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	gIsIOS6 = [[[UIDevice currentDevice] systemVersion] hasPrefix:@"6"];
	
	storeObserver_ = [[MyStoreObserver alloc] init];
	[[SKPaymentQueue defaultQueue] addTransactionObserver:storeObserver_];
	MNPlayerViewController * player = [MNPlayerViewController sharedPlayer]; //INstantiate

	NSLog(@"player status %ld",(long)player.player.status);
	
	
	// まず最初にinstantiateする。フェッチでdeadlockを防ぐため
	[self managedObjectContext];

	[[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];


#if TARGET_IPHONE_SIMULATOR
	
	
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];


	MNPlayerViewController* controller = [MNPlayerViewController sharedPlayer];
	
	NSURL* fileURL = [[NSBundle mainBundle] URLForResource:@"amazing-grace-10s" withExtension:@"mp3"];
	
	[controller setAssetAt: fileURL];
	
	
	MNBaseNavigationController* navigationController = [[MNBaseNavigationController alloc] initWithRootViewController:controller];


	
	self.window.rootViewController = navigationController;
	[self.window makeKeyAndVisible];
	
	
	
	return YES;
	
#else
	

	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	
	
	// Tab Bar Controller
	
	NSArray* tagArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"MNAppDelegateTabBarOrder"];

	
	// Check validity ... 0, 1, 2, 3, 4, 5, 6
	
	if( tagArray.count != 7 ) tagArray = nil;
	
	for( NSUInteger tag = 0; tag < 7; tag++ )
	{
		NSNumber *num = [NSNumber numberWithInteger:tag];
		
		if( ![tagArray containsObject: num] )
		{
			tagArray = nil;
			break;
		}
	}
	
	
	if( !tagArray )
		tagArray = @[@0, @1, @2, @3, @4, @5, @6];
	
	
	
	NSMutableArray* viewControllers = [[NSMutableArray alloc] init];
	
	
	for( NSUInteger hoge = 0; hoge < tagArray.count ; hoge++)
	{
		NSNumber* num = [tagArray objectAtIndex: hoge];
		UIViewController* controller = [self tabViewControllerForTag:num.integerValue];
		
		[viewControllers addObject: controller];

	}
	

	///
	
	UITabBarController* tabBarController = [[UITabBarController alloc] init];
	tabBarController.restorationIdentifier = @"MNBaseTabBarControllerIdentifier";
	tabBarController.restorationClass = [UITabBarController class];
	tabBarController.delegate = self;
	tabBarController.viewControllers = viewControllers;
	

	
	self.window.rootViewController = tabBarController;
	self.window.restorationIdentifier = @"UIWindowIdentifier";
	
	[self.window makeKeyAndVisible];
	
	
	
	return YES;
	
#endif
}

-(UIViewController*)tabViewControllerForTag:(NSInteger)tag
{
	MNAlbumCollectionTableViewController * controller = nil;
	
	// Playlist
	
	if( tag == 0 )
	{
		controller =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType: MPMediaGroupingPlaylist
																		  filterPredicates:nil];
		
		controller.hideSoundClips = YES;
		
		MNBaseNavigationController* playlistNav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
		
		UIImage * image = nil;
		
		if( FLAT_UI )
			image = [UIImage imageNamed:@"TabPlaylistFlat"];
		else
			image = [UIImage imageNamed:@"TabPlaylist"];

		playlistNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabPlaylists", @"")
																				 image:image
																					tag:0];
		
		if( FLAT_UI )
			playlistNav.tabBarItem.selectedImage = [UIImage imageNamed:@"TabPlaylistFlatSelected"];
			
		playlistNav.restorationIdentifier = @"Nav0";
		
		
		return playlistNav;
	}
	
	// Genre
	if( tag == 1 )
	{
		controller =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType: MPMediaGroupingGenre
																		  filterPredicates:nil];
		
		controller.hideSoundClips = YES;
		
		MNBaseNavigationController* genreNav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
		
		UIImage * image = nil;
		
		if( FLAT_UI )
			image = [UIImage imageNamed:@"TabGenreFlat"];
		else
			image = [UIImage imageNamed:@"TabGenre"];
		
		genreNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabGenre", @"")
																			 image:image
																				tag:1];
		
		if( FLAT_UI )
			genreNav.tabBarItem.selectedImage = [UIImage imageNamed:@"TabGenreFlatSelected"];

		
		genreNav.restorationIdentifier = @"Nav1";
		
		return genreNav;
	}
	
	//Artists
	
	if( tag == 2 )
	{
		controller =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType: MPMediaGroupingArtist
																		  filterPredicates:nil];
		
		controller.hideSoundClips = YES;
		
		MNBaseNavigationController* artistsNav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
		
		
		
		artistsNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabArtists", @"")
																				image:[UIImage imageNamed:@"TabArtist"]
																				  tag:2];
		
		artistsNav.restorationIdentifier = @"Nav2";
		
		return artistsNav;
		
	}
	//Albums
	
	if( tag == 3 )
	{
		controller =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType: MPMediaGroupingAlbum
																		  filterPredicates:nil];
		
		controller.hideSoundClips = YES;
		controller.showArtistName = YES;

		MNBaseNavigationController* albumNav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
		
		UIImage * image = nil;

		if( FLAT_UI )
			image = [UIImage imageNamed:@"TabAlbumFlat"];
		else
			image = [UIImage imageNamed:@"TabAlbum"];
		
		albumNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabAlbums", @"")
																			 image:image
																				tag:3];
		
		if( FLAT_UI )
			albumNav.tabBarItem.selectedImage = [UIImage imageNamed:@"TabAlbumFlatSelected"];
		
		albumNav.restorationIdentifier = @"Nav3";
		
		return albumNav;
	}
	
	//Podcasts
	
	if( tag == 4 )
	{
		controller =
		[[MNAlbumCollectionTableViewController alloc] initWithGroupingType: MPMediaGroupingPodcastTitle
																		  filterPredicates:nil];
		
		controller.hideSoundClips = YES;
		controller.showArtistName = YES;

		MNBaseNavigationController* podcastsNav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
		
		UIImage * image = nil;

		if( FLAT_UI )
			image = [UIImage imageNamed:@"TabPodcastFlat"];
		else
			image = [UIImage imageNamed:@"TabPodcast"];
		
		podcastsNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabPodcasts", @"")
																				 image:image
																					tag:4];
		
		podcastsNav.restorationIdentifier = @"Nav4";
		
		return podcastsNav;
	}
	
	//All Clips
	
	if( tag == 5 )
	{
		ClipPlaylistsTableViewController* clipController = [[ClipPlaylistsTableViewController alloc] init];
		
		
		MNBaseNavigationController* clipNav = [[MNBaseNavigationController alloc] initWithRootViewController:clipController];
		
		UIImage * image = nil;
		
		if( FLAT_UI )
			image = [UIImage imageNamed:@"TabSoundClipIconFlat"];
		else
			image = [UIImage imageNamed:@"TabSoundClipIcon"];
		
		clipNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabSoundClips", @"")
																			image:image
																			  tag:5];
		
		if( FLAT_UI )
			clipNav.tabBarItem.selectedImage = [UIImage imageNamed:@"TabSoundClipIconFlatSelected"];

		
		clipNav.restorationIdentifier = @"Nav5";
		
		return clipNav;
	}
	
	//Recent Album
	
	if( tag == 6 )
	{
		controller =
		[[MNRecentAlbumsCollectionTableViewController alloc] init];
		
		
		MNBaseNavigationController* recentNav = [[MNBaseNavigationController alloc] initWithRootViewController:controller];
		
		UIImage * image = nil;
		
		if( FLAT_UI )
			image = [UIImage imageNamed:@"TabRecentsFlat"];
		else
			image = [UIImage imageNamed:@"TabRecents"];
		
		recentNav.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"TabRecents", @"")
																			  image:image
																				 tag:6];
		
		if( FLAT_UI )
			recentNav.tabBarItem.selectedImage = [UIImage imageNamed:@"TabRecentsFlatSelected"];
		
		recentNav.restorationIdentifier = @"Nav6";
		
		return recentNav;
	}
	
	
	return nil;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	if( self.shouldShowJisho )
	{
		MNBaseNavigationController* nav = [[MNBaseNavigationController alloc] initWithRootViewController:self.shouldShowJisho];
		nav.restorationIdentifier = @"ModalNav";
		nav.supportedInterfaceOrientations = UIInterfaceOrientationMaskAll;
		self.shouldShowJisho = nil;
		
		[self.window.rootViewController presentViewController:nav
																	animated:NO
																 completion:nil];
	}
		

	if( [[NSUserDefaults standardUserDefaults] boolForKey:@"UpgradeToVersion2"] != YES )
	{
		
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UpgradeToVersion2"];
		[[NSUserDefaults standardUserDefaults] synchronize];
		
		if( ![storeObserver_ purchased:@"com.catalystwo.LearningPlayer.paid"] ) {
		
		MNBlockAlertView * alert = [[MNBlockAlertView alloc] initWithTitle:NSLocalizedString(@"UpgradeMessageTitle", @"") message:NSLocalizedString(@"UpgradeMessage", @"")];
		
		[alert addTitle:NSLocalizedString(@"Restore", @"") actionBlock:^{
			
			//			SKProduct *selectedProduct = [self.products objectAtIndex: indexPath.section];
			//			SKPayment *payment = [SKPayment paymentWithProduct:selectedProduct];
			//			[[SKPaymentQueue defaultQueue] addPayment:payment];
			
			
			[APP_DELEGATE storeObserver].restoring = YES;
			[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
			
		} cancel:NO];
		
		[alert addTitle:NSLocalizedString(@"Cancel", @"") actionBlock:^{} cancel:YES];
		
		[alert show];
		}
	}
	
	return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	

	
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AppSafelyInactive"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	
//	UIBackgroundTaskIdentifier)beginBackgroundTaskWithExpirationHandler:
	
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	
	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"AppSafelyInactive"];
	[[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Saves changes in the application's managed object context before the application terminates.
	[self saveContext];
}

- (void)saveContext
{
	NSError *error = nil;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
	
	
	[self.fetchManagedObjectContext reset];
}



- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
	// Pop other to root
	NSArray* viewControllers = tabBarController.viewControllers;
	
	for( UINavigationController* nav in viewControllers )
	{
		if( nav != viewController )
		[nav popToRootViewControllerAnimated:NO];
	}
	
	
	if( viewController != tabBarController.moreNavigationController )
	{
		[tabBarController.moreNavigationController popToRootViewControllerAnimated:NO];
	}
}

- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
	// Changed order
	
	NSMutableArray* order = [[NSMutableArray alloc] init];
	
	for( NSUInteger hoge = 0; hoge < viewControllers.count; hoge++ )
	{
		UIViewController* controller = [viewControllers objectAtIndex: hoge];
		
		NSInteger tag = controller.tabBarItem.tag;
		[order addObject:[NSNumber numberWithInteger:tag]];
		 
	}
		
	
	[[NSUserDefaults standardUserDefaults] setObject:order forKey:@"MNAppDelegateTabBarOrder"];
	
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
		_managedObjectContext = [[NSManagedObjectContext alloc] init];
		[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	return _managedObjectContext;
}


- (NSManagedObjectContext *)fetchManagedObjectContext
{
	
	if (_fetchManagedObjectContext != nil) {
		return _fetchManagedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
		_fetchManagedObjectContext = [[NSManagedObjectContext alloc] init];
		[_fetchManagedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	return _fetchManagedObjectContext;
}


// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataModel" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Database.sqlite"];
	
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		 
		 Typical reasons for an error here include:
		 * The persistent store is not accessible;
		 * The schema for the persistent store is incompatible with current managed object model.
		 Check the error message to determine what the actual problem was.
		 
		 
		 If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
		 
		 If you encounter schema incompatibility errors during development, you can reduce their frequency by:
		 * Simply deleting the existing store:
		 [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
		 
		 * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
		 @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
		 
		 Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
		 
		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	
	return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - State Preservation


- (UIViewController *)application:(UIApplication *)application viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents coder:(NSCoder *)coder
{
//	NSLog(@"identifierComponents %@",identifierComponents);
//	
//	NSString* theID = [identifierComponents lastObject];

	
	// Build navigation controller child view controllers
//	
//	if( [theID isEqualToString: @"MNAlbumCollectionTableViewControllerID"] )
//	{
//		UIViewController *controller = [MNAlbumCollectionTableViewController viewControllerWithRestorationIdentifierPath:identifierComponents coder:coder];
//		
//		
//		return controller;
//	}
//	
//	
//	if( [theID isEqualToString: @"MNSongCollectionTableViewControllerID"] )
//	{
//		UIViewController *controller = [MNSongCollectionTableViewController viewControllerWithRestorationIdentifierPath:identifierComponents coder:coder];
//		
//		
//		return controller;
//	}
//	
//	if( [theID isEqualToString: @"MNClipCollectionViewControllerID"] )
//	{
//		UIViewController *controller = [MNClipCollectionViewController viewControllerWithRestorationIdentifierPath:identifierComponents coder:coder];
//		
//		
//		return controller;
//	}
//	
//
//
//	if( [theID isEqualToString: @"MNPlayerViewControllerID"] )
//	{
//		UIViewController *controller = [MNPlayerViewController viewControllerWithRestorationIdentifierPath:identifierComponents coder:coder];
//		
//		
//		return controller;
//	}
//
//	
//	
//	if( [theID isEqualToString: @"JishoViewControllerID"] )
//	{
//		UIViewController *controller = [JishoViewController viewControllerWithRestorationIdentifierPath:identifierComponents coder:coder];
//		
//		
//		[self.window.rootViewController presentViewController:controller animated:NO completion:nil];
//		
//		return controller;
//	}
//	

	return nil;
	
}


- (void)application:(UIApplication *)application willEncodeRestorableStateWithCoder:(NSCoder *)coder
{
	C2Manager *manager = [[C2Manager alloc] init];
	
	[coder encodeObject:[manager versionString] forKey:@"ApplicationVersion"];
	
}

- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
{
	return NO;
}

- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
{
	if( [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown ) return NO;
	if( ![[NSUserDefaults standardUserDefaults] boolForKey:@"AppSafelyInactive"] ) return NO;

	C2Manager *manager = [[C2Manager alloc] init];
	
	NSString* savedVersion = [coder decodeObjectForKey:@"ApplicationVersion"];

	if( [savedVersion isEqualToString:manager.versionString] )
		return YES;
	
	
	
	return NO;
}
@end
