//
//  MNBaseNavigationController.h
//  iPod Player
//
//  Created by Masatoshi Nishikata on 5/09/12.
//
//

#import <UIKit/UIKit.h>

@interface MNBaseNavigationController : UINavigationController <UIViewControllerRestoration>
{
	NSUInteger supportedInterfaceOrientations_;
}

-(void)setSupportedInterfaceOrientations:(NSUInteger)mask;
@end




@interface UITabBarController (Extension)
@end
