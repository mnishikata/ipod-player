//
//  MNCollectionTableViewController.h
//  iPod Player
//
//  Created by Nishikata Masatoshi on 20/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>
#import "AB_Player-Swift.h"

@interface MNSongCollectionTableViewController : MediaCollectionTableViewController <UIViewControllerRestoration>
{
	BOOL shouldReloadData_;
	
	NSSet* filterPredicates_;
			
	NSArray *mediaItems_;
	
	NSMutableArray* soundClips_;
	
	BOOL loadingError_;
}
- (id)initWithFilterPredicates:(NSSet*)predicates;

@property (atomic, copy) NSArray *mediaItems; // NSArray of MPMediaItem
@property (nonatomic, retain) NSMutableArray* soundClips;

@end


