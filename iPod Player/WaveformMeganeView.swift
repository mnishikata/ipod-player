//
//  WaveformMeganeView.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 20/02/16.
//
//

import Foundation
import UIKit
import CoreMedia

let TIME_SLICE:Float64 = Float64((UI_USER_INTERFACE_IDIOM() == .Pad) ? 10.0:5.0)
let SECOND_FONT:CGFloat = 14


@objc protocol WaveformSource : NSObjectProtocol {
	
	func waveformAtIndex(index: Int, size: CGSize, forSectionRange range: CMTimeRange, completion completionHandler: (image: UIImage?) -> Void)
	
}


let WINDOW_HEIGHT:CGFloat = 64

class WaveformMeganeView : UIView {
	
	var waveformSouce:WaveformSource!
	var offset:CGFloat = 0.0 // Middle point in the screen
	var selectedRange = CMTimeRangeMake(CMTimeMake(0, 1), CMTimeMake(0, 1))
	var time = CMTimeMake(0, 0) {
		didSet {
			
			func retrieveWaveImageAt( sliceIndex:Int ) {
				
				guard sliceIndex >= 0 else { return }
				guard waveformSouce != nil else { return }

				if cache[sliceIndex] != nil { return } // Already retrieved or being retrieved
				
				let start = CMTimeMakeWithSeconds( TIME_SLICE * Float64(sliceIndex), time.timescale)
				let duration = CMTimeMakeWithSeconds( TIME_SLICE, time.timescale)
				let timeRange = CMTimeRangeMake(start, duration)
				
				cache[sliceIndex] =  NSNull()

				waveformSouce.waveformAtIndex(sliceIndex, size: bounds.size, forSectionRange: timeRange)  {
					[unowned self] (image: UIImage?) -> Void in
					
					if image != nil {
						self.cache[sliceIndex] = image!
						self.setNeedsDisplay()
					}
					else if self.cache[sliceIndex] is NSNull {
						self.cache.removeValueForKey(sliceIndex)
						
					}
				}
			}
			
			
			let sliceIndex = Int( floor( CMTimeGetSeconds(time) / TIME_SLICE ) )
			
			retrieveWaveImageAt( sliceIndex )
			retrieveWaveImageAt( sliceIndex - 1 )
			retrieveWaveImageAt( sliceIndex + 1 )

			setNeedsDisplay()
		}
	}
	
	var wholeImage:UIImage? = nil {
		didSet {
			cache = [:]
		}
	}
	
	private var cache:[Int:AnyObject] = [:]
	
	override var hidden:Bool {
		set {
			if hidden == newValue { return }
			if newValue == false {
				super.hidden = newValue
				alpha = 1
				return
			}
			
			UIView.animateWithDuration(0.1, delay: 0, options: .CurveEaseIn, animations: {
				self.alpha = 0
				}) { finished in
				super.hidden = newValue
			}
		}
		get {
			return super.hidden
		}
	}
	
	// Creating a singleton
	static let sharedWaveformMeganeView:WaveformMeganeView = {
		
		var obj = WaveformMeganeView(frame: CGRectMake(0, 0, 320.0, WINDOW_HEIGHT))
		obj.contentScaleFactor = 1.0
		obj.userInteractionEnabled = false
		obj.opaque = false
		obj.backgroundColor = UIColor.darkGrayColor()
		return obj
	}()
	
	func clearCache() {
		cache = [:]
	}
	
	override func drawRect(rect: CGRect) {
		
		if hidden { return }

		let context = UIGraphicsGetCurrentContext()

		var drawRect = rect
		drawRect.origin.x = offset
		var nextDrawRect:CGRect? = nil
		var previousDrawRect:CGRect? = nil

		CGContextSaveGState(context)
		CGContextClearRect(context, rect)
		
		let sliceIndex = Int( floor( CMTimeGetSeconds(time) / TIME_SLICE ) )
		var pixelsPerSecond = Float64(bounds.size.width) / TIME_SLICE
		
		var mod = CMTimeGetSeconds(time) - Float64(sliceIndex) * TIME_SLICE
		var modX = mod * pixelsPerSecond
		
		drawRect.origin.x -= CGFloat(modX)
		
		// Draw This image
		
		let color = UIColor.grayColor()
		CGContextSetFillColorWithColor(context, color.CGColor)
		CGContextSetBlendMode(context, .DestinationOver)
		CGContextFillRect(context, rect)
		
		if cache[sliceIndex] is UIImage {
			(cache[sliceIndex] as! UIImage).drawInRect(drawRect)
		}
		
		// Draw Next image
		if cache[sliceIndex + 1] is UIImage {
			
			nextDrawRect = CGRectMake(  CGRectGetMaxX(drawRect), drawRect.origin.y, drawRect.size.width, drawRect.size.height)

			(cache[sliceIndex + 1] as! UIImage).drawInRect(nextDrawRect!)
		}
		
		// Draw previous image
		if cache[sliceIndex - 1] is UIImage {
			previousDrawRect = CGRectMake(  drawRect.origin.x - drawRect.size.width, drawRect.origin.y, drawRect.size.width, drawRect.size.height)

			(cache[sliceIndex - 1] as! UIImage).drawInRect(previousDrawRect!)
		}
		
		// Draw selection
		var toColor: UIColor = RGBA(1, 95, 231, 1)
		if respondsToSelector("tintColor") {
			toColor = tintColor
		}
		var selectionTopX = CMTimeGetSeconds(selectedRange.start) - Float64(sliceIndex) * TIME_SLICE
		var selectionBottomX = CMTimeGetSeconds(selectedRange.start) + CMTimeGetSeconds(selectedRange.duration) - Float64(sliceIndex) * TIME_SLICE
		selectionBottomX *= pixelsPerSecond
		selectionTopX *= pixelsPerSecond
		selectionBottomX += Float64(offset)
		selectionTopX += Float64(offset)
		selectionBottomX -= modX
		selectionTopX -= modX
		var clipFrame = CGRectMake( CGFloat(selectionTopX), rect.origin.y, CGFloat(selectionBottomX - selectionTopX ), rect.size.height)
		clipFrame.origin.y += 1
		clipFrame.size.height -= 2
		CGContextClipToRect(context, clipFrame)
		CGContextSetFillColorWithColor(context, toColor.CGColor)
		CGContextSetBlendMode(context, .Color)
		CGContextFillRect(context, clipFrame);
		
		
		// Draw current bar
		CGContextRestoreGState(context)
		CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
		var points: [CGPoint] = [CGPointZero, CGPointZero]
		points[0] = CGPointMake(offset + 0.5, 0)
		points[1] = CGPointMake(offset + 0.5, drawRect.size.height)
		CGContextSetLineWidth(context, 1)
		CGContextStrokeLineSegments(context, points, 2)
		
		
		// Display Time
		
		let attributes = [NSFontAttributeName : UIFont.systemFontOfSize(SECOND_FONT), NSForegroundColorAttributeName : UIColor.whiteColor()]
		
		UIGraphicsPushContext(context!)
		UIColor.whiteColor().set()
		// Current time
		var str = secondStringAt(CMTimeGetSeconds(time))
		var x = offset + 5
		let size = str.sizeWithAttributes(attributes)
		if x > bounds.size.width - size.width - 5 {
			x = offset - size.width - 5
		}
		str.drawAtPoint(CGPointMake(x, bounds.size.height - 20), withAttributes: attributes)
		
		func drawForSliceIndex( sliceIndex:Int, inRect drawRect:CGRect ) {
			
			let start = Float64(sliceIndex) * TIME_SLICE
			str = secondStringAt(start)
			str.drawAtPoint(CGPointMake(drawRect.origin.x + 5, 0), withAttributes: attributes)
			
			points[0] = CGPointMake(drawRect.origin.x + 0.25, 0)
			points[1] = CGPointMake(drawRect.origin.x + 0.25, drawRect.size.height)
			CGContextSetLineWidth(context, 0.5)
			CGContextStrokeLineSegments(context, points, 2)
		}
		
		drawForSliceIndex( sliceIndex, inRect: drawRect )
		if previousDrawRect != nil {
			drawForSliceIndex( sliceIndex-1, inRect: previousDrawRect! )
		}
		if nextDrawRect != nil {
			drawForSliceIndex( sliceIndex+1, inRect: nextDrawRect! )
		}
		
		// Finishing
		UIGraphicsPopContext()
		
	}
	
	//MARK:- Private functions
	
	private func secondStringAt(time: Float64) -> String {
		let elapsedTimeMin: Double = floor(time / 60)
		let elapsedTimeSec: Double = time - elapsedTimeMin * 60
		let str: String = String(format: "%.0f:%02.0f", elapsedTimeMin, elapsedTimeSec)
		return str
	}
	
}