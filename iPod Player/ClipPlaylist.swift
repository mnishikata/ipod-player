//
//  ClipPlaylist.swift
//  iPod Player
//
//  Created by Masatoshi Nishikata on 28/02/16.
//
//

import Foundation
import CoreData

extension ClipPlaylist {
	
	@NSManaged var clips: String?
	@NSManaged var creationDate: NSTimeInterval
	@NSManaged var index: Int16
	@NSManaged var title: String?
	@NSManaged var uuid: String?
	
}


class ClipPlaylist: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
	class func fetchAllPlaylist(completion handler: (playlists: [ClipPlaylist], error:NSError?) -> Void) {
		let fetchQueue = APP_DELEGATE().fetchQueue()
		let fetchMOC = APP_DELEGATE().fetchManagedObjectContext
		if #available(iOS 8.3, *) {
			fetchMOC.refreshAllObjects()
		} else {
			// Fallback on earlier versions
		}
		dispatch_async(fetchQueue) {
			let request = NSFetchRequest(entityName:"ClipPlaylist")
			request.resultType = .ManagedObjectIDResultType
			
			let descriptor = NSSortDescriptor(key: "title", ascending: true)
			request.sortDescriptors = [descriptor]
			var results: [NSManagedObjectID]
			var error:NSError? = nil
			do {
				results = try fetchMOC.executeFetchRequest(request) as! [NSManagedObjectID]
			}catch let error1 as NSError  {
				results = []
				error = error1
			}
			
			// Convert object id to object
			dispatch_async(dispatch_get_main_queue()) {
				var array: [ClipPlaylist] = []
				let context = APP_DELEGATE().managedObjectContext
				for objectID: NSManagedObjectID in results {
					do {
						let clip: ClipPlaylist = try context.objectWithID(objectID) as! ClipPlaylist
						array.append(clip)
					}catch {
					}
				}
				// DONE
				handler(playlists: array, error:error)
			}
		}
	}
	
	var clipsUUIDs:[String] {
		get {
			guard let clips = self.clips else { return [] }
			return clips.componentsSeparatedByString(",") ?? []
		}
		set {
			//TODO: check atomicity
			self.clips = newValue.joinWithSeparator(",")
		}
	}
}
