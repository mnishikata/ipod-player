//
// $Id: PTHAudioLameOutput.h,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioOutput.h"
#import <semaphore.h>
#import <libmp3lame/lame.h>

@class PTHAudioDevice;

@interface PTHAudioLameOutput : PTHAudioOutput {
    unsigned int lameBufferCounter;
    sem_t *lame_semaphore;
    NSString *filename;
    BOOL lameWrite;
    lame_global_flags *lgf;
}

- initWithFilename:(NSString *)aFilename;
- (void)startLame;
- (void)stopLame;

@end
