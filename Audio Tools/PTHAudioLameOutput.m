//
// $Id: PTHAudioLameOutput.m,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioLameOutput.h"
//#import "PTHAudioDevice.h"
#import "PTHAudioInput.h"
#import <AudioToolbox/AudioConverter.h>
#import <unistd.h>
#import "TypeSelectorViewController.h"

@implementation PTHAudioLameOutput

// High
//#define SAMPLE_RATE 44100
//#define OUT_SAMPLE_RATE 0 
//#define BIT_RATE 128
//#define CONSTANT 4

// Low
//#define SAMPLE_RATE 44100 
//#define OUT_SAMPLE_RATE 22050 
//
//#define BIT_RATE 64
//#define CONSTANT 4

#define SAMPLE_RATE 44100
#define BIT_RATE ( [TypeSelectorViewController typeForName: MNAudioQualityTypeName] == MNAudioQualityTypeHigh ? 128:64)
#define OUT_SAMPLE_RATE ( [TypeSelectorViewController typeForName: MNAudioQualityTypeName] == MNAudioQualityTypeHigh ? 0:22050) 
#define CONSTANT 4

- initWithFilename:(NSString *)aFilename
{
    if ((self = [self init])) {
        filename = [aFilename retain];
        lgf = lame_init();
		
		lame_set_num_channels(lgf,1);
		lame_set_in_samplerate(lgf,SAMPLE_RATE);
		lame_set_out_samplerate(lgf,OUT_SAMPLE_RATE);
		lame_set_brate(lgf, BIT_RATE);
		lame_set_scale(lgf, 200.0); /// volume

		lame_init_params(lgf);
		
		id3tag_init(lgf);
		id3tag_set_comment(lgf, "Created by FastFinga");

    }
    return self;
}

- (void)startLame
{
	lame_set_num_channels(lgf,1);
	lame_set_in_samplerate(lgf,SAMPLE_RATE);
	lame_set_out_samplerate(lgf,OUT_SAMPLE_RATE);
	lame_set_brate(lgf, BIT_RATE);
	lame_set_scale(lgf, 200.0); /// volume
	
	lame_init_params(lgf);
	
    lameBufferCounter = audioInput->recordBufferCounter;
    lameWrite = YES;
    [NSThread detachNewThreadSelector:@selector(writeFile) toTarget:self withObject:nil];
}

- (void)stopLame
{
    lameWrite = NO;
    usleep(100000);
}

#import <mach/mach.h>
#import <mach/mach_error.h>

- (void)setThreadPolicy
{
    kern_return_t error;
    thread_extended_policy_data_t extendedPolicy;
    thread_precedence_policy_data_t precedencePolicy;

    extendedPolicy.timeshare = 0;
    error = thread_policy_set(mach_thread_self(), THREAD_EXTENDED_POLICY,  (thread_policy_t)&extendedPolicy, THREAD_EXTENDED_POLICY_COUNT);
    if (error != KERN_SUCCESS)
        mach_error("Couldn't set feeder thread's extended policy", error);

    precedencePolicy.importance = 5;
    error = thread_policy_set(mach_thread_self(), THREAD_PRECEDENCE_POLICY, (thread_policy_t)&precedencePolicy, THREAD_PRECEDENCE_POLICY_COUNT);
    if (error != KERN_SUCCESS)
        mach_error("Couldn't set feeder thread's precedence policy", error);
}

- (void)writeFile
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    SInt8 *inputData;
    unsigned char mp3Data[32768];
    int mp3DataSize;
	
	
	if( ![[NSFileManager defaultManager] fileExistsAtPath:filename] )
	[[NSData data] writeToFile:filename atomically:NO];
	
	NSFileHandle *fh = [NSFileHandle fileHandleForWritingAtPath: filename];
    AudioConverterRef audioConverter;
    AudioStreamBasicDescription sourceFormat, destinationFormat;
    OSStatus status;

    [self setThreadPolicy];
    
    //lame_init_params(lgf);


    sourceFormat = [audioInput streamDescription];
    destinationFormat = [self streamDescription];

//	NSLog(@"thruFormat.mSampleRate %f", sourceFormat.mSampleRate);
//	NSLog(@"thruFormat.mFormatID %d (kAudioFormatLinearPCM=%d)", sourceFormat.mFormatID,kAudioFormatLinearPCM);
//	NSLog(@"thruFormat.mFormatFlags %d", sourceFormat.mFormatFlags);
//	NSLog(@"thruFormat.mBytesPerPacket %d", sourceFormat.mBytesPerPacket);
//	NSLog(@"thruFormat.mFramesPerPacket %d", sourceFormat.mFramesPerPacket);
//	NSLog(@"thruFormat.mBytesPerFrame %d", sourceFormat.mBytesPerFrame);
//	NSLog(@"thruFormat.mChannelsPerFrame %d", sourceFormat.mChannelsPerFrame);
//	NSLog(@"thruFormat.mBitsPerChannel %d", sourceFormat.mBitsPerChannel);

	
	
    if ((status = AudioConverterNew(&sourceFormat, &destinationFormat, &audioConverter)))
        [NSException raise:@"PTHAudioLameOutputError" format:@"AudioConverterNew failed!:%d", status];

    while (lameWrite) {
//		NSLog(@"lameBufferCounter %d",lameBufferCounter);
//		NSLog(@"self->audioInput->recordBufferCounter %d",self->audioInput->recordBufferCounter);
//		
        if (lameBufferCounter + PTH_AUDIO_BUFFER_COUNT < self->audioInput->recordBufferCounter) {
            printf("Lame Buffer UnderRun lb=%d rb=%d\n", lameBufferCounter, self->audioInput->recordBufferCounter);
            lameBufferCounter += PTH_AUDIO_BUFFER_COUNT;
        }
        else if (lameBufferCounter < self->audioInput->recordBufferCounter) {
			
			AudioBuffer buffer = audioInput->recordBuffers[(lameBufferCounter)%PTH_AUDIO_BUFFER_COUNT].mBuffers[0];
		
			
            inputData = (SInt8 *)(buffer.mData);
            lameBufferCounter++;
			
//			UInt32 outputDataSize;
//            outputDataSize = 4096; //2048
//			Byte outputData[ outputDataSize ];

//			if ((status = AudioConverterConvertBuffer(audioConverter, buffer.mDataByteSize, buffer.mData, &outputDataSize, outputData)))
//			{
//				
//				
//				NSLog(@"buffer.mDataByteSize %d",buffer.mDataByteSize);
//				NSLog(@"outputDataSize %d",outputDataSize);
//				
//				NSError *error = [NSError errorWithDomain:NSOSStatusErrorDomain
//													 code:status
//												 userInfo:nil];
//				NSLog(@"Error: %@", [error description]);
//				
//				[NSException raise:@"PTHAudioLameOutputError" format:@"AudioConverterConvertBuffer failed!:%d", status];
//			}
			
			
			//NSLog(@"Lame %d : Input %d",lameBufferCounter, self->audioInput->recordBufferCounter);

			
			mp3DataSize = lame_encode_buffer_int(lgf, buffer.mData, nil, buffer.mDataByteSize/CONSTANT, mp3Data, sizeof(mp3Data));

			
			
			
			//mp3DataSize = lame_encode_buffer_interleaved(lgf, (short int*)outputData, 512, mp3Data, sizeof(mp3Data));
			//fwrite(mp3Data, mp3DataSize, 1, lameFile);
			[fh writeData: [NSData dataWithBytesNoCopy:mp3Data length:mp3DataSize freeWhenDone:NO]];
			
        } else {
            usleep(50000); // around 4 or so buffers
        }
    }

    mp3DataSize = lame_encode_flush(lgf, mp3Data, sizeof(mp3Data));
   // fwrite(mp3Data, mp3DataSize, 1, lameFile);
	[fh writeData: [NSData dataWithBytesNoCopy:mp3Data length:mp3DataSize freeWhenDone:NO]];

    AudioConverterDispose(audioConverter);
//    fclose(lameFile);
	
	[fh closeFile];
	
    [pool release];
}

- (AudioStreamBasicDescription)streamDescription
{
    AudioStreamBasicDescription streamDescription;

    streamDescription.mSampleRate = (float)lame_get_in_samplerate(lgf);
    streamDescription.mFormatID = kAudioFormatLinearPCM;
    streamDescription.mFormatFlags =  kLinearPCMFormatFlagIsBigEndian | kLinearPCMFormatFlagIsSignedInteger;
   
 
	/// Original input PCM
//	streamDescription.mBytesPerPacket = 4;
//    streamDescription.mFramesPerPacket = 1;
//    streamDescription.mBytesPerFrame = 4;
//    streamDescription.mChannelsPerFrame = 2;
//    streamDescription.mBitsPerChannel = 32;
	
	/// Basic Output
//	streamDescription.mBytesPerPacket = 4;
//    streamDescription.mFramesPerPacket = 1;
//    streamDescription.mBytesPerFrame = 4;
//    streamDescription.mChannelsPerFrame = 2;
//    streamDescription.mBitsPerChannel = 16;
 
	/// Trimmed Output
	streamDescription.mBytesPerPacket = 2;
    streamDescription.mFramesPerPacket = 1;
    streamDescription.mBytesPerFrame = 2;
    streamDescription.mChannelsPerFrame = 1;
    streamDescription.mBitsPerChannel = 16;

	
	/// This result is unused
    return streamDescription;
}


- (void)setAudioInput:(PTHAudioInput *)anInput
{
    [super setAudioInput:anInput];

    if (anInput)
        [self startLame];
    else
        [self stopLame];
}

- (void)dealloc
{
	MNLOG(@"%@ %@ %d",NSStringFromClass([self class]), NSStringFromSelector( _cmd ), __LINE__);

    [self stopLame];
    [filename release];
	
	lame_close(lgf);

	
    [super dealloc];
}

@end
