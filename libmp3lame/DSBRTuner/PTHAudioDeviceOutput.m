//
// $Id: PTHAudioDeviceOutput.m,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioDeviceOutput.h"
#import "PTHAudioDevice.h"
#import "PTHAudioInput.h"
#import <unistd.h>

#define EXTRA_PLAY_BUFFER 8

@implementation PTHAudioDeviceOutput

- initWithAudioDevice:(PTHAudioDevice *)aDevice
{
    if (self = [self init]) {
        audioDevice = [aDevice retain];
        sem_unlink("pth_play_semaphore");
        play_semaphore = sem_open("pth_play_semaphore", O_CREAT, 666, 0);
        sem_unlink("pth_play_semaphore");
    }

    return self;
}

static OSStatus playStream(AudioDeviceID inDevice, const AudioTimeStamp *inNow, const AudioBufferList *inInputData, const AudioTimeStamp *inInputTime,  AudioBufferList *outOutputData, const AudioTimeStamp *inOutputTime, PTHAudioDeviceOutput *self)
{
    AudioBufferList recordBuffer;

    if (self->audioInput->recordBufferCounter < self->playBufferCounter) {
        printf("Audio Device Buffer Underrun pb=%d rb=%d\n", self->playBufferCounter, self->audioInput->recordBufferCounter);
        usleep(50000);
    }
    else if (self->playBufferCounter + PTH_AUDIO_BUFFER_COUNT < self->audioInput->recordBufferCounter) {
        printf("Audio Device Buffer Overrun pb=%d rb=%d\n", self->playBufferCounter, self->audioInput->recordBufferCounter);
        self->playBufferCounter = self->audioInput->recordBufferCounter - EXTRA_PLAY_BUFFER;
    }

    if (!self->mute) {
        recordBuffer = self->audioInput->recordBuffers[(self->playBufferCounter)%PTH_AUDIO_BUFFER_COUNT];

        outOutputData->mNumberBuffers = recordBuffer.mNumberBuffers;
        outOutputData->mBuffers[0].mNumberChannels = recordBuffer.mBuffers[0].mNumberChannels;
        outOutputData->mBuffers[0].mDataByteSize = recordBuffer.mBuffers[0].mDataByteSize;
        memcpy(outOutputData->mBuffers[0].mData, recordBuffer.mBuffers[0].mData, outOutputData->mBuffers[0].mDataByteSize);
    }
    
    (self->playBufferCounter)++;
    return 0;
}

- (void)mute
{
    mute = YES;
}

- (void)unmute
{
    mute = NO;
}

- (AudioStreamBasicDescription)streamDescription
{
    AudioStreamBasicDescription streamDescription;
    UInt32 dataSize = sizeof(streamDescription);

    AudioDeviceGetProperty([audioDevice deviceID], 0, NO, kAudioDevicePropertyStreamFormat, &dataSize, &streamDescription);

    return streamDescription;
}

- (void)setAudioInput:(PTHAudioInput *)anInput
{
    AudioDeviceID deviceID = [audioDevice deviceID];

    [super setAudioInput:anInput];

    if (anInput) {
        while (anInput->recordBufferCounter < EXTRA_PLAY_BUFFER)
            usleep(50000);
        playBufferCounter = anInput->recordBufferCounter - EXTRA_PLAY_BUFFER;
        AudioDeviceAddIOProc(deviceID, (AudioDeviceIOProc)playStream, self);
        AudioDeviceStart(deviceID, (AudioDeviceIOProc)playStream);
    } else {
        AudioDeviceStop(deviceID, (AudioDeviceIOProc)playStream);
        AudioDeviceRemoveIOProc(deviceID, (AudioDeviceIOProc)playStream);
    }
}

- (void)dealloc
{
    [audioDevice release];
    sem_close(play_semaphore);
    [super dealloc];
}

@end
