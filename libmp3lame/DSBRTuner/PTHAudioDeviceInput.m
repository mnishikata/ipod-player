//
// $Id: PTHAudioDeviceInput.m,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioDeviceInput.h"
#import "PTHAudioDevice.h"
#import "PTHAudioOutput.h"

@implementation PTHAudioDeviceInput

- initWithAudioDevice:(PTHAudioDevice *)aDevice
{
    if (self = [self init]) {
        audioDevice = [aDevice retain];
    }

    return self;
}

static OSStatus recordStream(AudioDeviceID inDevice, const AudioTimeStamp *inNow, const AudioBufferList *inInputData, const AudioTimeStamp *inInputTime,  AudioBufferList *outOutputData, const AudioTimeStamp *inOutputTime, PTHAudioInput *self)
{
    AudioBuffer *recordBuffer = &(self->recordBuffers[(self->recordBufferCounter)%PTH_AUDIO_BUFFER_COUNT].mBuffers[0]);
    //unsigned int index, count;

    recordBuffer->mNumberChannels = inInputData->mBuffers[0].mNumberChannels;
    recordBuffer->mDataByteSize = inInputData->mBuffers[0].mDataByteSize;
    memcpy(recordBuffer->mData, inInputData->mBuffers[0].mData, inInputData->mBuffers[0].mDataByteSize);

    (self->recordBufferCounter)++;

   // for (index = 0, count = [self->audioOutputs count]; index < count; index++)
     //   [[self->audioOutputs objectAtIndex:index] inputDidRecord];

    return 0;
}

- (void)_stopRecording
{
    AudioDeviceID deviceID = [audioDevice deviceID];

    AudioDeviceStop(deviceID, (AudioDeviceIOProc)recordStream);
    AudioDeviceRemoveIOProc(deviceID, (AudioDeviceIOProc)recordStream);
}

- (void)_startRecording
{
    AudioDeviceID deviceID = [audioDevice deviceID];

    AudioDeviceAddIOProc(deviceID, (AudioDeviceIOProc)recordStream, self);
    AudioDeviceStart(deviceID, (AudioDeviceIOProc)recordStream);
}

- (void)addAudioOutput:(PTHAudioOutput *)anOutput
{
    if ([audioOutputs count] == 0)
        [self _startRecording];
    [super addAudioOutput:anOutput];
}

- (void)removeAudioOutput:(PTHAudioOutput *)anOutput
{
    if ([audioOutputs count] == 1)
        [self _stopRecording];
    [super removeAudioOutput:anOutput];
}

- (AudioStreamBasicDescription)streamDescription
{
    AudioStreamBasicDescription streamDescription;
    UInt32 dataSize = sizeof(streamDescription);

    AudioDeviceGetProperty([audioDevice deviceID], 0, YES, kAudioDevicePropertyStreamFormat, &dataSize, &streamDescription);

    return streamDescription;
}

- (void)dealloc
{
    [self _stopRecording];
    [audioDevice release];
    [super dealloc];
}

@end
