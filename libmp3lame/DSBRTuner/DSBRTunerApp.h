//
// $Id: DSBRTunerApp.h,v 1.3 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <CoreAudio/AudioHardware.h>
@class PTHAudioDevice;
@class DSBRRadio;
@class PTHAudioDeviceInput;
@class PTHAudioDeviceOutput;
@class PTHAudioLameOutput;

@interface DSBRTunerApp : NSApplication
{
    IBOutlet NSWindow *window;
    IBOutlet NSMatrix *presetMatrix;
    IBOutlet NSSlider *frequencySlider;
    IBOutlet NSSlider *inputVolumeSlider;
    IBOutlet NSTextField *frequencyTextField;
    IBOutlet NSTextField *stereoTextField;
    IBOutlet NSTableView *inputSourceTableView;
    IBOutlet NSTableView *outputSourceTableView;
    IBOutlet NSButton *audioPassthroughEnabledCheckBox;
    IBOutlet NSButton *recordButton;
    IBOutlet NSButton *powerButton;
    
    PTHAudioDevice *outputDevice;
    PTHAudioDevice *inputDevice;
    
    PTHAudioDeviceInput *audioInput;
    PTHAudioDeviceOutput *audioOutput;
    PTHAudioLameOutput *lameOutput;
    
    DSBRRadio *radio;
    
    NSArray *inputDevices, *outputDevices;
    
    NSMutableArray *presets;
    BOOL _power;
    unsigned int _frequency;
}

- (void)setFrequency:(unsigned int)frequency;

- (void)power:(id)sender;
- (void)seekDown:(id)sender;
- (void)seekUp:(id)sender;
- (void)stepDown:(id)sender;
- (void)stepUp:(id)sender;
- (void)setInputVolumeBySlider:(NSSlider *)sender;
- (void)setFrequencyBySlider:(id)sender;
- (void)setPreset:(NSCell *)presetCell;
- (void)clearPreset:(NSCell *)presetCell;
- (void)selectPreset:(id)sender;

- (void)sendFeedback:(id)sender;
- (void)showPTHHomePage:(id)sender;
- (void)showAppHome:(id)sender;

- (void)setAudioInput:(id)sender;
- (void)setAudioOutput:(id)sender;
- (void)togglePassthrough:(id)sender;

- (unsigned int)frequency;
- (void)setFrequency:(unsigned int)aFrequency;
- (NSString *)frequencyString;

- (BOOL)passthrough;
- (void)setPassthrough:(BOOL)aFlag;

- (void)recordToFilename:(NSString *)aFile;
- (void)stopRecording:(id)sender;
- (void)toggleRecording:(id)sender;

- (void)updatePresetsMatrix;
- (void)drawAppIcon;

@end
