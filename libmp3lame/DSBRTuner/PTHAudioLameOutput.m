//
// $Id: PTHAudioLameOutput.m,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioLameOutput.h"
#import "PTHAudioDevice.h"
#import "PTHAudioInput.h"
#import <AudioToolbox/AudioConverter.h>
#import <unistd.h>

@implementation PTHAudioLameOutput

- initWithFilename:(NSString *)aFilename
{
    if (self = [self init]) {
        filename = [aFilename retain];
        lgf = lame_init();
    }
    return self;
}

- (void)startLame
{
    lameBufferCounter = audioInput->recordBufferCounter;
    lameWrite = YES;
    [NSThread detachNewThreadSelector:@selector(writeFile) toTarget:self withObject:nil];
}

- (void)stopLame
{
    lameWrite = NO;
    usleep(100000);
}

#import <mach/mach.h>
#import <mach/mach_error.h>

- (void)setThreadPolicy
{
    kern_return_t error;
    thread_extended_policy_data_t extendedPolicy;
    thread_precedence_policy_data_t precedencePolicy;

    extendedPolicy.timeshare = 0;
    error = thread_policy_set(mach_thread_self(), THREAD_EXTENDED_POLICY,  (thread_policy_t)&extendedPolicy, THREAD_EXTENDED_POLICY_COUNT);
    if (error != KERN_SUCCESS)
        mach_error("Couldn't set feeder thread's extended policy", error);

    precedencePolicy.importance = 5;
    error = thread_policy_set(mach_thread_self(), THREAD_PRECEDENCE_POLICY, (thread_policy_t)&precedencePolicy, THREAD_PRECEDENCE_POLICY_COUNT);
    if (error != KERN_SUCCESS)
        mach_error("Couldn't set feeder thread's precedence policy", error);
}

- (void)writeFile
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    float *inputData;
    short outputData[1024];
    unsigned char mp3Data[32768];
    int mp3DataSize;
    FILE *lameFile = fopen([filename cString], "w");
    AudioConverterRef audioConverter;
    AudioStreamBasicDescription sourceFormat, destinationFormat;
    UInt32 outputDataSize;
    OSStatus status;

    [self setThreadPolicy];
    
    lame_init_params(lgf);

    id3tag_init(lgf);
    id3tag_set_comment(lgf, "Created by DSBRTuner");

    sourceFormat = [audioInput streamDescription];
    destinationFormat = [self streamDescription];

    if (status = AudioConverterNew(&sourceFormat, &destinationFormat, &audioConverter))
        [NSException raise:@"PTHAudioLameOutputError" format:@"AudioConverterNew failed!:%d", status];

    while (lameWrite) {
        if (lameBufferCounter + PTH_AUDIO_BUFFER_COUNT < self->audioInput->recordBufferCounter) {
            printf("Lame Buffer UnderRun lb=%d rb=%d\n", lameBufferCounter, self->audioInput->recordBufferCounter);
            lameBufferCounter += PTH_AUDIO_BUFFER_COUNT;
        }
        else if (lameBufferCounter < self->audioInput->recordBufferCounter) {
            inputData = (float *)(audioInput->recordBuffers[(lameBufferCounter)%PTH_AUDIO_BUFFER_COUNT].mBuffers[0].mData);
            lameBufferCounter++;
            outputDataSize = 2048;
            if (status = AudioConverterConvertBuffer(audioConverter, 4096, inputData, &outputDataSize, outputData))
                [NSException raise:@"PTHAudioLameOutputError" format:@"AudioConverterConvertBuffer failed!:%d", status];

            mp3DataSize = lame_encode_buffer_interleaved(lgf, outputData, 512, mp3Data, sizeof(mp3Data));
            fwrite(mp3Data, mp3DataSize, 1, lameFile);
        } else {
            usleep(50000); // around 4 or so buffers
        }
    }

    mp3DataSize = lame_encode_flush(lgf, mp3Data, sizeof(mp3Data));
    fwrite(mp3Data, mp3DataSize, 1, lameFile);
    AudioConverterDispose(audioConverter);
    fclose(lameFile);
    lame_close(lgf);
    [pool release];
}

- (AudioStreamBasicDescription)streamDescription
{
    AudioStreamBasicDescription streamDescription;

    streamDescription.mSampleRate = (float)lame_get_in_samplerate(lgf);
    streamDescription.mFormatID = kAudioFormatLinearPCM;
    streamDescription.mFormatFlags =  kLinearPCMFormatFlagIsBigEndian | kLinearPCMFormatFlagIsSignedInteger;
    streamDescription.mBytesPerPacket = 4;
    streamDescription.mFramesPerPacket = 1;
    streamDescription.mBytesPerFrame = 4;
    streamDescription.mChannelsPerFrame = 2;
    streamDescription.mBitsPerChannel = 16;

    return streamDescription;
}


- (void)setAudioInput:(PTHAudioInput *)anInput
{
    [super setAudioInput:anInput];

    if (anInput)
        [self startLame];
    else
        [self stopLame];
}

- (void)dealloc
{
    [self stopLame];
    [filename release];
    [super dealloc];
}

@end
