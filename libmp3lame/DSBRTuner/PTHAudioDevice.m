//
// $Id: PTHAudioDevice.m,v 1.2 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioDevice.h"
#import <Foundation/Foundation.h>

static NSMapTable *deviceTable = NULL;

@implementation PTHAudioDevice

+ (void)initialize
{
    if (!deviceTable) {
        deviceTable = NSCreateMapTable(NSNonOwnedPointerMapKeyCallBacks, NSNonRetainedObjectMapValueCallBacks, 0);
    }
}

+ (NSArray *)allDevices
{
    AudioDeviceID *devices;
    unsigned long dataSize;
    unsigned int index, count;
    NSMutableArray *array = [NSMutableArray array];
    OSStatus status;

    if (status = AudioHardwareGetPropertyInfo(kAudioHardwarePropertyDevices, &dataSize, NULL))
        [NSException raise:@"PTHAudioDevice" format:@"AudioHardwareGetPropertyInfo = %d", status];
    devices = malloc(dataSize);
    if (status = AudioHardwareGetProperty(kAudioHardwarePropertyDevices, &dataSize, devices))
        [NSException raise:@"PTHAudioDevice" format:@"AudioHardwareGetProperty = %d", status];
    for (index = 0, count = dataSize / sizeof(AudioDeviceID); index < count; index++) {
        [array addObject:[self deviceWithID:devices[index]]];
    }

    return array;
}

+ (NSArray *)inputDevices
{
    NSEnumerator *deviceEnumerator = [[self allDevices] objectEnumerator];
    PTHAudioDevice *aDevice;
    NSMutableArray *inputDevices = [NSMutableArray array];

    while (aDevice = [deviceEnumerator nextObject])
        if ([aDevice isInput])
            [inputDevices addObject:aDevice];

    return inputDevices;
}

+ (NSArray *)outputDevices
{
    NSEnumerator *deviceEnumerator = [[self allDevices] objectEnumerator];
    PTHAudioDevice *aDevice;
    NSMutableArray *outputDevices = [NSMutableArray array];

    while (aDevice = [deviceEnumerator nextObject])
        if ([aDevice isOutput])
            [outputDevices addObject:aDevice];

    return outputDevices;
}

+ (PTHAudioDevice *)defaultOutputDevice
{
    AudioDeviceID aDeviceID;
    OSStatus status;
    unsigned long dataSize;

    if (status = AudioHardwareGetProperty(kAudioHardwarePropertyDefaultOutputDevice, &dataSize, &aDeviceID))
        [NSException raise:@"PTHAudioDevice" format:@"defaultOutputDevice = %d", status];

    return [self deviceWithID:aDeviceID];
}

+ (PTHAudioDevice *)defaultInputDevice
{
    AudioDeviceID aDeviceID;
    OSStatus status;
    unsigned long dataSize;

    if (status = AudioHardwareGetProperty(kAudioHardwarePropertyDefaultInputDevice, &dataSize, &aDeviceID))
        [NSException raise:@"PTHAudioDevice" format:@"defaultInputDevice = %d", status];

    return [self deviceWithID:aDeviceID];
}

+ (PTHAudioDevice *)deviceWithName:(NSString *)name
{
    NSEnumerator *deviceEnumerator = [[self allDevices] objectEnumerator];
    PTHAudioDevice *device;

    while (device = [deviceEnumerator nextObject]) {
        if ([[device name] isEqualToString:name])
            return device;
    }

    return device;
}

- initWithID:(AudioDeviceID)aDeviceID
{
    if (self = [self init]) {

        deviceID = aDeviceID;
        NSMapInsert(deviceTable, (void *)deviceID, self);
    }

    return self;
}

- (AudioDeviceID)deviceID
{
    return deviceID;
}

+ (PTHAudioDevice *)deviceWithID:(AudioDeviceID)aDeviceID
{
    PTHAudioDevice *device;
    if (!(device = NSMapGet(deviceTable, (void *)aDeviceID)))
        device = [[[[self class] alloc] initWithID:aDeviceID] autorelease];
    return device;
}

- (NSString *)name
{
    OSStatus status;
    unsigned long dataSize;
    NSString *devName;
    NSString *manName;

    dataSize = sizeof(devName);
    if (status = AudioDeviceGetProperty(deviceID, 0, NO, kAudioDevicePropertyDeviceNameCFString, &dataSize, &devName))
        [NSException raise:@"PTHAudioDevice" format:@"name = %d", status];
    [devName autorelease];

    dataSize = sizeof(manName);
    if (status = AudioDeviceGetProperty(deviceID, 0, NO, kAudioDevicePropertyDeviceManufacturerCFString, &dataSize, &manName))
        [NSException raise:@"PTHAudioDevice" format:@"name = %d", status];
    [manName autorelease];

    return [manName stringByAppendingFormat:@" %@", devName];
}

- (BOOL)isInput
{
    OSStatus status;
    unsigned long dataSize;
    AudioBufferList *buffer;

    if (status =  AudioDeviceGetPropertyInfo(deviceID, 0, YES, kAudioDevicePropertyStreamConfiguration, &dataSize, (void *)&buffer))
        [NSException raise:@"PTHAudioDevice" format:@"isInput = %d", status];

    return dataSize > 4;
}

- (BOOL)isOutput
{
    OSStatus status;
    unsigned long dataSize;
    AudioBufferList *buffer;

    if (status =  AudioDeviceGetPropertyInfo(deviceID, 0, NO, kAudioDevicePropertyStreamConfiguration, &dataSize, (void *)&buffer))
        [NSException raise:@"PTHAudioDevice" format:@"isOutput = %d", status];

    return dataSize > 4;
}

- (float)volume:(BOOL)isInput
{
    float aVolume;
    OSStatus status;
    unsigned long dataSize = sizeof(aVolume);

    if (status = AudioDeviceGetProperty(deviceID, 1, isInput, kAudioDevicePropertyVolumeScalar, &dataSize, &aVolume))
        [NSException raise:@"PTHAudioDevice" format:@"volume = %d", status];

    return aVolume;
}

- (void)mute:(BOOL)isInput
{
    OSStatus status;
    unsigned long mute = YES;
    unsigned long dataSize = sizeof(mute);

    if (status = AudioDeviceSetProperty(deviceID, NULL, 0, isInput, kAudioDevicePropertyMute, dataSize, &mute))
        [NSException raise:@"PTHAudioDevice" format:@"mute = %d", status];
}

- (void)unmute:(BOOL)isInput
{
    OSStatus status;
    unsigned long unmute = NO;
    unsigned long dataSize  = sizeof(unmute);

    if (status = AudioDeviceSetProperty(deviceID, NULL, 0, isInput, kAudioDevicePropertyMute, dataSize, &unmute))
        [NSException raise:@"PTHAudioDevice" format:@"unmute = %d", status];
}

- (BOOL)canSetVolumeIsInput:(BOOL)isInput;
{
    Boolean isWritable = false;
    OSStatus status = AudioDeviceGetPropertyInfo(deviceID, 0, isInput, kAudioDevicePropertyVolumeScalar, NULL, &isWritable);
    if (status == noErr) return isWritable;
    return NO;
}

- (void)setVolume:(float)aVolume isInput:(BOOL)isInput
{
    OSStatus status;
    unsigned long dataSize;

    dataSize = sizeof(aVolume);

    /// XXX should set volume for channel 0 = "master" instead of individual channels?
    if (status = AudioDeviceSetProperty(deviceID, NULL, 1, isInput, kAudioDevicePropertyVolumeScalar, dataSize, &aVolume))
        ; //[NSException raise:@"PTHAudioDevice" format:@"setVolume 0 = %d", status];
    if (status = AudioDeviceSetProperty(deviceID, NULL, 2, isInput, kAudioDevicePropertyVolumeScalar, dataSize, &aVolume))
        ;//[NSException raise:@"PTHAudioDevice" format:@"setVolume = %d", status];
}
- (void)dealloc
{
    NSMapRemove(deviceTable, (void *)deviceID);

    [super dealloc];
}

@end
