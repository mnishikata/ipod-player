//
// $Id: PTHAudioDeviceOutput.h,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioOutput.h"
#import <semaphore.h>

@class PTHAudioDevice;

@interface PTHAudioDeviceOutput : PTHAudioOutput {
    @public
    PTHAudioDevice *audioDevice;
    unsigned int playBufferCounter;
    sem_t *play_semaphore;
    BOOL mute;
}

- initWithAudioDevice:(PTHAudioDevice *)aDevice;
- (void)mute;
- (void)unmute;

@end
