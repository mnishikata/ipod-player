//
// $Id: PTHAudioOutput.m,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioOutput.h"


@implementation PTHAudioOutput

- (void)setAudioInput:(PTHAudioInput *)anInput
{
    audioInput = anInput; // Not retained since AudioInput should do so
}

- (AudioStreamBasicDescription)streamDescription
{
    AudioStreamBasicDescription streamDescription;

    return streamDescription;
}

@end
