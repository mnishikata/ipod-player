//
// $Id: DSBRRadio.h,v 1.3 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/usb/IOUSBLib.h>

#define MINFREQUENCY 871
#define MAXFREQUENCY 1079
#define FREQUENCYSTEP 1
#define TUNERPAUSE .1

@interface DSBRRadio : NSObject {
        IOUSBDeviceInterface  **dev;
}

- (void)setPower:(BOOL)on; // on == 1, off == 0
- (void)setFrequency:(unsigned int)frequency;  // This is in KiloHertz
- (BOOL)isStereo;

@end
