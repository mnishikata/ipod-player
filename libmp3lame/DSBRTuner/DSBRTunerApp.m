//
// $Id: DSBRTunerApp.m,v 1.3 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "DSBRTunerApp.h"
#import "DSBRRadio.h"
#import "PTHAudioDevice.h"

#import "PTHAudioDeviceOutput.h"
#import "PTHAudioDeviceInput.h"
#import "PTHAudioLameOutput.h"

#define PRESETS 		@"Presets"
#define FREQUENCY		@"Frequency"
#define PASSTHROUGHENABLED	@"PassThroughEnabled"
#define INPUTVOLUME		@"InputVolume"
#define POWER			@"Power"
#define INPUTDEVICE		@"InputDevice"
#define OUTPUTDEVICE		@"OutputDevice"

#define FREQUENCY_TO_STRING(frequency) (frequency == 0 ? @"��" : [NSString stringWithFormat:@"%3.1f",  frequency / 10.0])

@implementation DSBRTunerApp

- (NSArray *)presets
{
    NSMutableArray *array = [[[[NSUserDefaults standardUserDefaults] arrayForKey: PRESETS] mutableCopy] autorelease];
    if (!array) {
        unsigned int index, count;
        array = [NSMutableArray array];
        for (index = 0, count = 8; index < count; index++)
            [array addObject:[NSNumber numberWithInt:MINFREQUENCY+(index*FREQUENCYSTEP)]];
    }
    return array;
}

- (void)awakeFromNib
{
    NSString *inputDeviceName;
    NSString *outputDeviceName;
    unsigned int index, count;

    [[NSUserDefaults standardUserDefaults] registerDefaults: [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithFloat: 1.0], INPUTVOLUME, nil]];

    BOOL powerOn = [[NSUserDefaults standardUserDefaults] boolForKey:POWER];

    [self setDelegate:self];

    NS_DURING
        radio = [[DSBRRadio alloc] init];
    NS_HANDLER
        [radio release];
        radio = nil;
        NSRunCriticalAlertPanel(@"Error", @"Couldn't connect to Radio.  %@", @"OK", NULL, NULL, localException);
    NS_ENDHANDLER

    inputDevices = [[PTHAudioDevice inputDevices] retain];
    outputDevices = [[PTHAudioDevice outputDevices] retain];

    [frequencySlider setMaxValue:(double)MAXFREQUENCY];
    [frequencySlider setMinValue:(double)MINFREQUENCY];

    presets = [[self presets] mutableCopy];
    [self updatePresetsMatrix];

    [audioPassthroughEnabledCheckBox setState:[[NSUserDefaults standardUserDefaults] boolForKey:PASSTHROUGHENABLED]];
    
    [self setFrequency:[[NSUserDefaults standardUserDefaults] integerForKey:FREQUENCY]];

    if (!powerOn) [self power: nil];
    
    [inputSourceTableView reloadData];
    inputDeviceName = [[NSUserDefaults standardUserDefaults] stringForKey:INPUTDEVICE];
    for (index = 0, count = [inputDevices count]; index < count; index++)
        if ([inputDeviceName isEqualToString:[[inputDevices objectAtIndex:index] name]])
            [inputSourceTableView selectRow:index byExtendingSelection:NO];

    if ([inputSourceTableView selectedRow] == NSNotFound)
        [inputSourceTableView selectRow:0 byExtendingSelection:NO];

    [outputSourceTableView reloadData];
    outputDeviceName = [[NSUserDefaults standardUserDefaults] stringForKey:OUTPUTDEVICE];
    for (index = 0, count = [outputDevices count]; index < count; index++)
        if ([outputDeviceName isEqualToString:[[outputDevices objectAtIndex:index] name]])
            [outputSourceTableView selectRow:index byExtendingSelection:NO];

    if ([outputSourceTableView selectedRow] == NSNotFound)
        [outputSourceTableView selectRow:0 byExtendingSelection:NO];

    [inputVolumeSlider setFloatValue: [[NSUserDefaults standardUserDefaults] floatForKey: INPUTVOLUME]];

    [self setAudioInput:nil];

    [window makeKeyAndOrderFront:nil];
}

- (NSMenu *)applicationDockMenu:(NSApplication *)sender
{
    NSMenu *docMenu = [[[NSMenu alloc] init] autorelease];
    NSEnumerator *e = [presets objectEnumerator];
    NSNumber *presetFrequency;
    int frequency;
    NSMenuItem *item;

    [docMenu setAutoenablesItems: NO]; // so we can disable the frequency
    
    [[docMenu addItemWithTitle: [@"Power " stringByAppendingString: _power ? @"Off" : @"On"] action:@selector(power:) keyEquivalent:@""] setTarget:self];
    [docMenu addItem: [NSMenuItem separatorItem]];
    [[docMenu addItemWithTitle:@"Seek Up" action:@selector(seekUp:) keyEquivalent:@""] setTarget:self];
    [[docMenu addItemWithTitle:@"Seek Down" action:@selector(seekDown:) keyEquivalent:@""] setTarget:self];
    [docMenu addItem: [NSMenuItem separatorItem]];

    [[docMenu addItemWithTitle: @"Presets" action: nil keyEquivalent: @""] setEnabled: NO];
    
    while ( (presetFrequency = [e nextObject]) != nil) {
        frequency = [presetFrequency intValue];
        if (frequency == 0) continue;
        if (_frequency != frequency) {
            item = [docMenu addItemWithTitle: [@"���" stringByAppendingString: FREQUENCY_TO_STRING(frequency)] action: @selector(setFrequencyByRepresentedObject:) keyEquivalent: @""];
        } else {
            item = [docMenu addItemWithTitle: [@"� " stringByAppendingString: FREQUENCY_TO_STRING(frequency)] action: @selector(setFrequencyByRepresentedObject:) keyEquivalent: @""];
        }
        [item setTarget: self];
        [item setRepresentedObject: presetFrequency];
    }
    // [[docMenu addItemWithTitle:@"Next Preset" action:@selector(nextPreset:) keyEquivalent:@""] setTarget:self];
    // [[docMenu addItemWithTitle:@"Previous Preset" action:@selector(previousPreset:) keyEquivalent:@""] setTarget:self];

    return docMenu;
}

- (void)setAudioInput:(id)sender
{
    int index = [inputSourceTableView selectedRow];
    if (index != -1) {
        inputDevice = [inputDevices objectAtIndex: index];
        BOOL canSetVolume = [inputDevice canSetVolumeIsInput: YES];
        [inputVolumeSlider setEnabled: canSetVolume];
        if (canSetVolume)
            [inputDevice setVolume:[inputVolumeSlider floatValue] isInput:YES];
        [[NSUserDefaults standardUserDefaults] setObject:[inputDevice name] forKey:INPUTDEVICE];
        [audioInput release];
        audioInput = [[PTHAudioDeviceInput alloc] initWithAudioDevice:inputDevice];
    } else {
        [inputVolumeSlider setEnabled: NO];
    }
    [self setAudioOutput:nil];
    [self stopRecording:nil];
}

- (void)setInputVolumeBySlider:(NSSlider *)sender;
{
    float volume = [sender floatValue];
    [inputDevice setVolume: volume isInput: YES];
    [[NSUserDefaults standardUserDefaults] setFloat: volume forKey: INPUTVOLUME];
}

- (void)setAudioOutput:(id)sender
{
    int index = [outputSourceTableView selectedRow];
    [audioInput removeAudioOutput:audioOutput];
    if (index != -1) {
        outputDevice = [outputDevices objectAtIndex: index];
        [[NSUserDefaults standardUserDefaults] setObject:[outputDevice name] forKey:OUTPUTDEVICE];
        [audioOutput release];
        audioOutput = [[PTHAudioDeviceOutput alloc] initWithAudioDevice:outputDevice];
    }
    [self togglePassthrough:nil];
    [self stopRecording:nil];
}

- (void)togglePassthrough:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[audioPassthroughEnabledCheckBox state] forKey:PASSTHROUGHENABLED];
    if (_power && [audioPassthroughEnabledCheckBox state])
        [audioInput addAudioOutput:audioOutput];
    else
        [audioInput removeAudioOutput:audioOutput];
}

- (BOOL)passthrough
{
    return [audioPassthroughEnabledCheckBox state];   
}

- (void)setPassthrough:(BOOL)aFlag
{
    [audioPassthroughEnabledCheckBox setState:aFlag];
    [self togglePassthrough:nil];
}

- (void)power:(id)sender
{
    _power = !_power;
    [self togglePassthrough: nil];
    [powerButton setState: _power];
    [radio setPower:_power];
    [[NSUserDefaults standardUserDefaults] setBool: _power forKey: POWER];
}

- (void)recordToFilename:(NSString *)aFile
{
    [self stopRecording:nil];
    lameOutput = [[PTHAudioLameOutput alloc] initWithFilename:aFile];
    [audioInput addAudioOutput:lameOutput];
    [recordButton setState:1];
}

- (void)_recordASK:(NSScriptCommand *)aCommand
{
    [self recordToFilename:[[aCommand arguments] objectForKey:@"file"]];
}

- (void)stopRecording:(id)sender
{
    if (lameOutput) {
        [audioInput removeAudioOutput:lameOutput];
        [lameOutput release];
        lameOutput = nil;
    }
    [recordButton setState:0];
}

- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    if (returnCode == NSOKButton)
        [self recordToFilename:[sheet filename]];
    else
        [self stopRecording:nil];
}

- (void)toggleRecording:(id)sender
{
    if ([recordButton state]) {
        [[NSSavePanel savePanel] beginSheetForDirectory:nil file:nil modalForWindow:window modalDelegate:self didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:) contextInfo:NULL];
    } else {
        [self stopRecording:nil];
    }
}
// Preset Methods

- (void)updatePresetsMatrix
{
    unsigned int index, count;

    count = [presetMatrix numberOfRows]*[presetMatrix numberOfColumns];

    for (index = 0; index < count; index++) {
        NSCell *cell = [presetMatrix cellAtRow:index%[presetMatrix numberOfRows] column:index/[presetMatrix numberOfRows]];
        unsigned int frequency = [[presets objectAtIndex:index] intValue];

        [cell setTitle:FREQUENCY_TO_STRING(frequency)];
        [cell setTag:frequency];
        [cell setEnabled:(frequency != 0)];
    }
}

- (int)selectedPresetIndex
{
    if ([presetMatrix selectedCell])
        return [presetMatrix selectedRow] + [presetMatrix numberOfRows] * [presetMatrix selectedColumn];
    return -1;
}

- (int)indexOfPreset:(NSCell *)presetCell;
{
    int row, col;
    if ([presetMatrix getRow: &row column: &col ofCell: presetCell]) {
        return row + [presetMatrix numberOfRows] * col;
    }
    return -1;
}

- (void)previousPreset:(id)sender
{
    int currentPresetIndex = [self selectedPresetIndex] - 1;

    if (currentPresetIndex < 0)
        currentPresetIndex = [presets count] - 1;
    [self setFrequency:[[presets objectAtIndex:currentPresetIndex] intValue]];
}

- (void)nextPreset:(id)sender
{
    int currentPresetIndex = [self selectedPresetIndex] + 1;

    if (currentPresetIndex > [presets count] - 1)
        currentPresetIndex = 0;
    [self setFrequency:[[presets objectAtIndex:currentPresetIndex] intValue]];
}

- (void)_setFrequency:(unsigned int)frequency forPreset:(NSCell *)presetCell;
{
    [presets replaceObjectAtIndex: [self indexOfPreset: presetCell] withObject: [NSNumber numberWithInt: frequency]];
    [self updatePresetsMatrix];
    [[NSUserDefaults standardUserDefaults] setObject:presets forKey:PRESETS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setPreset:(NSCell *)presetCell;
{
    int row, col;
    [self _setFrequency: _frequency forPreset: presetCell];
    if ([presetMatrix getRow: &row column: &col ofCell: presetCell]) {
        [presetMatrix selectCellAtRow: row column: col];
    }
}

- (void)clearPreset:(NSCell *)presetCell;
{
    [self _setFrequency: 0 forPreset: presetCell];
    [presetMatrix deselectAllCells];
    [presetMatrix selectCellWithTag: _frequency];
}


- (void)selectPreset:(id)sender;
{
    if ([presetMatrix selectedCell]) {
        [self setFrequency: [[presetMatrix selectedCell] tag]];
    }
}

// Seek Methods

- (void)_seekStep:(int)step
{
    unsigned int index = (MAXFREQUENCY-MINFREQUENCY)/FREQUENCYSTEP;
    [audioOutput mute];
    do {
        if (!(index--))
            break;
        [self setFrequency:_frequency+step];
        [NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:TUNERPAUSE]];
    } while (![radio isStereo]);
    [audioOutput unmute];
}

- (void)seekDown:(id)sender
{
    [self _seekStep:-FREQUENCYSTEP];
}

- (void)seekUp:(id)sender
{
    [self _seekStep:FREQUENCYSTEP];
}

// Step Methods

- (void)stepDown:(id)sender
{
    [self setFrequency:_frequency-FREQUENCYSTEP];
}

- (void)stepUp:(id)sender
{
    [self setFrequency:_frequency+FREQUENCYSTEP];
}

// Frequency Methods

- (void)setFrequencyBySlider:(id)sender
{
    [self setFrequency:[frequencySlider intValue]];
}

- (void)setFrequencyByRepresentedObject:(NSMenuItem *)sender
{
    [self setFrequency:[[sender representedObject] intValue]];
}

- (unsigned int)frequency
{
    return _frequency;
}

- (NSString *)frequencyString;
{
    return FREQUENCY_TO_STRING(_frequency);
}

- (void)showStereo;
{
    [stereoTextField setStringValue: [radio isStereo] ? @"stereo" : @""];
}

- (void)setFrequency:(unsigned int)frequency
{

    if (frequency < MINFREQUENCY)
        _frequency = MAXFREQUENCY;
    else if (frequency > MAXFREQUENCY)
        _frequency = MINFREQUENCY;
    else
        _frequency = ((frequency - MINFREQUENCY)/FREQUENCYSTEP)*FREQUENCYSTEP + MINFREQUENCY;

    [frequencySlider setIntValue:_frequency];
    [[NSUserDefaults standardUserDefaults] setInteger:_frequency forKey:FREQUENCY];
    [frequencyTextField setStringValue:FREQUENCY_TO_STRING(_frequency)];
    [radio setFrequency:_frequency];

    [presetMatrix deselectAllCells];
    [presetMatrix selectCellWithTag: _frequency];

    [stereoTextField setStringValue: @""];

    [self performSelector: @selector(showStereo) withObject: nil afterDelay: TUNERPAUSE];

    if (!_power) [self power: nil];
    
    [window display];
    [self drawAppIcon];

}

- (void)drawAppIcon
{
    NSImage *newAppIcon;
    NSAttributedString *iconString;
    static NSImage *applicationImage = nil;

    if (!applicationImage)
        applicationImage = [[self applicationIconImage] copy];

    newAppIcon = [applicationImage copy];
    iconString = [[NSAttributedString alloc] initWithString:[frequencyTextField stringValue] attributes:[NSDictionary dictionaryWithObject:[NSFont boldSystemFontOfSize:40] forKey:NSFontAttributeName]];

    [newAppIcon lockFocus];
    [iconString drawAtPoint:NSMakePoint(0,0)];
    [iconString release];
    [newAppIcon unlockFocus];
    [self setApplicationIconImage:newAppIcon];
    [newAppIcon release];
}

- (void)sendFeedback:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"mailto:dsbrtuner@pth.com?subject=DSBRTuner%20Feedback&body=DSBRTuner%20is%20great%20but..."]];
}

- (void)showPTHHomePage:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.pth.com/"]];
}

- (void)showAppHome:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.pth.com/DSBRTuner"]];
}

- (int)numberOfRowsInTableView:(NSTableView *)tableView
{
    if (tableView == inputSourceTableView)
        return [inputDevices count];
    return [outputDevices count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(int)row
{
    if (tableView == inputSourceTableView)
        return [[inputDevices objectAtIndex:row] name];
    return [[outputDevices objectAtIndex:row] name];
}

- (void)terminate:(id)sender
{
    [audioInput removeAudioOutput:audioOutput];
    [audioOutput release];
    audioOutput = nil;
    [audioInput release];
    audioInput = nil;
    [lameOutput release];
    lameOutput = nil;

    [self setApplicationIconImage: [NSImage imageNamed: @"NSApplicationIcon"]];
    
    [super terminate:sender];
}

- (void)dealloc
{
    [window release];
    [presetMatrix release];
    [frequencySlider release];
    [frequencyTextField release];
    [inputDevices release];
    [outputDevices release];
    [inputSourceTableView release];
    [outputSourceTableView release];

    [presets release];
    [radio release];

    [super dealloc];
}

@end
