//
// $Id: PTHAudioInput.h,v 1.1 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/AudioHardware.h>

@class PTHAudioOutput;

#define PTH_AUDIO_BUFFER_COUNT 256

@interface PTHAudioInput : NSObject {
    @public
    NSMutableArray *audioOutputs;
    AudioBufferList recordBuffers[PTH_AUDIO_BUFFER_COUNT];
    unsigned int recordBufferCounter;
}

- (void)addAudioOutput:(PTHAudioOutput *)anOutput;
- (void)removeAudioOutput:(PTHAudioOutput *)anOutput;
- (NSArray *)audioOutputs;
- (AudioStreamBasicDescription)streamDescription;

@end
