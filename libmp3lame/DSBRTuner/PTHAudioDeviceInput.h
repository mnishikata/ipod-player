//
// $Id: PTHAudioDeviceInput.h,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioInput.h"
#import <CoreAudio/AudioHardware.h>

@class PTHAudioDevice;

@interface PTHAudioDeviceInput : PTHAudioInput {
    PTHAudioDevice *audioDevice;
}

- initWithAudioDevice:(PTHAudioDevice *)aDevice;

@end
