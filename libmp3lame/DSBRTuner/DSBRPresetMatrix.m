//
//  DSBRPresetMatrix.m
//  DSBRTuner
//
//  Created by Nicholas Riley on Sat Jan 11 2003.
//  Copyright (c) 2003 Nicholas Riley. All rights reserved.
//

#import "DSBRPresetMatrix.h"
#import "DSBRTunerApp.h"


@implementation DSBRPresetMatrix

- (void)_setFrequency:(NSMenuItem *)sender;
{
    [(DSBRTunerApp *)NSApp setPreset: [sender representedObject]];
}

- (void)_clearFrequency:(NSMenuItem *)sender;
{
    [(DSBRTunerApp *)NSApp clearPreset: [sender representedObject]];
}

- (NSMenu *)menu;
{
    NSMenu *contextMenu = [[[NSMenu alloc] init] autorelease];
    NSCell *cell;
    int row, col;
    NSMenuItem *item;

    if (![self getRow: &row column: &col forPoint: [self convertPoint: [[NSApp currentEvent] locationInWindow] fromView: nil]]) return nil;
    cell = [self cellAtRow: row column: col];
    
    item = [contextMenu addItemWithTitle: [@"Set to " stringByAppendingString: [(DSBRTunerApp *)NSApp frequencyString]] action: @selector(_setFrequency:) keyEquivalent: @""];
    [item setTarget: self];
    [item setRepresentedObject: cell];
    
    if ([cell tag] != 0) {
        item = [contextMenu addItemWithTitle: @"Clear" action: @selector(_clearFrequency:) keyEquivalent: @""];
        [item setTarget: self];
        [item setRepresentedObject: cell];
    }

    return contextMenu;
}

@end
