//
// $Id: PTHAudioDevice.h,v 1.2 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/AudioHardware.h>

#define VOLUMESTEP .05

@class NSArray;
@class NSMutableData;

@interface PTHAudioDevice : NSObject {
    AudioDeviceID deviceID;
}

+ (NSArray *)allDevices;
+ (NSArray *)inputDevices;
+ (NSArray *)outputDevices;
+ (PTHAudioDevice *)defaultOutputDevice;
+ (PTHAudioDevice *)defaultInputDevice;
+ (PTHAudioDevice *)deviceWithID:(AudioDeviceID)aDeviceID;
+ (PTHAudioDevice *)deviceWithName:(NSString *)name;

- (NSString *)name;
- (BOOL)canSetVolumeIsInput:(BOOL)isInput;
- (float)volume:(BOOL)isInput;
- (void)setVolume:(float)aVolume isInput:(BOOL)isInput;

- (BOOL)isInput;
- (BOOL)isOutput;

- (void)mute:(BOOL)isInput;
- (void)unmute:(BOOL)isInput;

- (AudioDeviceID)deviceID;

@end
