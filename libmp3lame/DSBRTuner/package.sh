APPNAME=DSBRTuner
DISTDIR=/tmp/$APPNAME.dst
PRODUCT=$DISTDIR/$APPNAME.app
rm -rf $PRODUCT
pbxbuild clean
pbxbuild -buildstyle Deployment install
cd ..
gnutar zcvf $DISTDIR/$APPNAME.src.gnutar.gz $APPNAME
cd $DISTDIR
gnutar zcvf $APPNAME.app.gnutar.gz $APPNAME.app
gnutartodmg.sh $APPNAME.app.gnutar.gz
