//
// $Id: PTHAudioOutput.h,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/AudioHardware.h>

@class PTHAudioInput;

@interface PTHAudioOutput : NSObject {
    @public
    PTHAudioInput *audioInput;
}

- (void)setAudioInput:(PTHAudioInput *)anInput;
- (AudioStreamBasicDescription)streamDescription;

@end
