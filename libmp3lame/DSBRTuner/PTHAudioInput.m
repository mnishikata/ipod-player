//
// $Id: PTHAudioInput.m,v 1.1 2002/07/09 19:38:24 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//

#import "PTHAudioInput.h"
#import "PTHAudioOutput.h"
#import "PTHAudioDevice.h"

@implementation PTHAudioInput

- init
{
    if (self = [super init]) {
        unsigned int x;

        for (x = 0; x < PTH_AUDIO_BUFFER_COUNT; x++) {
            recordBuffers[x].mNumberBuffers = 1;
            recordBuffers[x].mBuffers[0].mData = malloc(4096);
        }
        
        audioOutputs = [[NSMutableArray alloc] init];
    }

    return self;
}

- (void)addAudioOutput:(PTHAudioOutput *)anOutput
{
    [audioOutputs addObject:anOutput];
    [anOutput setAudioInput:self];
}

- (void)removeAudioOutput:(PTHAudioOutput *)anOutput
{
    [anOutput setAudioInput:nil];
    [audioOutputs removeObject:anOutput];
}

- (NSArray *)audioOutputs
{
    return audioOutputs;
}

- (AudioStreamBasicDescription)streamDescription
{
    AudioStreamBasicDescription streamDescription;

    return streamDescription;
}

- (void)dealloc
{
     int index;

    for (index = [audioOutputs count] - 1; index >= 0; index--)
        [self removeAudioOutput:[audioOutputs objectAtIndex:index]];

    [audioOutputs release];

    for (index = 0; index < PTH_AUDIO_BUFFER_COUNT; index++)
        free(recordBuffers[index].mBuffers[0].mData);

    [super dealloc];
}

@end
