//
// $Id: DSBRRadio.m,v 1.3 2002/07/09 19:38:23 phadd Exp $
//  Copyright (c) 2000-2002 PTH Consulting. All rights reserved.
//


/*
 This is a pretty simple USB device, as far as I can tell, it only responds to 3 vendor specific commands.

	0x02 Set Power
	0x01 Set Frequency
	0x00 (Possibly any other command) Is Stereo.

 The value for the frequency is pretty odd, I got this from looking through the linux driver and calculating from the windows values.  A gold star for anyone who can explain why the weird formula.
 */

#import "DSBRRadio.h"

#import <ctype.h>
#import <stdlib.h>
#import <stdio.h>
#import <mach/mach.h>
#import <mach/mach_error.h>
#import <unistd.h>

#import <IOKit/IOKitLib.h>
#import <IOKit/usb/IOUSBLib.h>
#import <IOKit/IOCFPlugIn.h>

#define REQUEST_TYPE USBmakebmRequestType(kUSBIn, kUSBVendor, kUSBDevice)

@implementation DSBRRadio

- init
{
    if (self = [super init]) {
        kern_return_t  kr;
        mach_port_t    masterPort;
        io_iterator_t  iterator;
        io_service_t usbDevice;
        CFMutableDictionaryRef dict;
        int vendorID = 1204;
        int deviceID =  4098;
        CFNumberRef ID;
        
        kr = IOMasterPort(MACH_PORT_NULL, &masterPort);
        if(KERN_SUCCESS != kr)
            [NSException raise:@"DSBRRadioError" format:@"Failed to get master device port!:%s", mach_error_string(kr)];

        dict = IOServiceMatching("IOUSBDevice");
        
        ID = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &vendorID);
        CFDictionarySetValue(dict, CFSTR(kUSBVendorID), ID);
        CFRelease(ID);

        ID = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &deviceID);
        CFDictionarySetValue(dict, CFSTR(kUSBProductID), ID);
        CFRelease(ID);

        kr = IOServiceGetMatchingServices(masterPort, dict, &iterator);
        if(KERN_SUCCESS != kr)
            [NSException raise:@"DSBRRadioError" format:@"Failed to get service!:%s", mach_error_string(kr)];

        if (usbDevice = IOIteratorNext(iterator)) {
            IOCFPlugInInterface  **plugInInterface = NULL;
            SInt32  score;

            kr = IOCreatePlugInInterfaceForService(usbDevice, kIOUSBDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &plugInInterface, &score);
            if(KERN_SUCCESS != kr)
                [NSException raise:@"DSBRRadioError" format:@"Failed to create plugin!:%s", mach_error_string(kr)];
            IOObjectRelease(usbDevice);

            (*plugInInterface)->QueryInterface(plugInInterface, CFUUIDGetUUIDBytes(kIOUSBDeviceInterfaceID), (LPVOID)&dev);
            (*plugInInterface)->Release(plugInInterface);

            (*dev)->USBDeviceOpen(dev);
        } else
            [NSException raise:@"DSBRRadioError" format:@"Failed to find device!"];
        IOObjectRelease(iterator);
        mach_port_deallocate(mach_task_self(), masterPort);
    }

    return self;
}

- (void)setPower:(BOOL)on
{
    UInt8 data;
    kern_return_t       kr;
    IOUSBDevRequest devRequest;

    devRequest.bmRequestType = REQUEST_TYPE;
    devRequest.bRequest = 0x02;
    devRequest.wValue = on;
    devRequest.wIndex = 0x00;
    devRequest.wLength = sizeof(data);
    devRequest.pData = (void *)&data;

    kr = (*dev)->DeviceRequest(dev,&devRequest);

    if(kIOReturnSuccess != kr) {
        [NSException raise:@"DSBRRadioError" format:@"Error setting power!:%s", mach_error_string(kr)];
    }
}

- (BOOL)isStereo
{
    UInt8 data;
    kern_return_t       kr;
    IOUSBDevRequest devRequest;

    devRequest.bmRequestType = REQUEST_TYPE;
    devRequest.bRequest = 0x00;
    devRequest.wValue = 0x00;
    devRequest.wIndex = 0x00;
    devRequest.wLength = sizeof(data);
    devRequest.pData = (void *)&data;

    kr = (*dev)->DeviceRequest(dev, &devRequest);

    if(kIOReturnSuccess != kr) {
        [NSException raise:@"DSBRRadioError" format:@"Error checking stereo!:%s", mach_error_string(kr)];
    }
    return data == 254;
}

- (void)setFrequency:(unsigned int)frequency
{
    UInt8 data;
    kern_return_t       kr;
    int freq = frequency*8 + 855;
    IOUSBDevRequest devRequest;

    devRequest.bmRequestType = REQUEST_TYPE;
    devRequest.bRequest = 0x01;
    devRequest.wValue = (freq&0xff00)>>8;
    devRequest.wIndex = freq&0xff;
    devRequest.wLength = sizeof(data);
    devRequest.pData = (void *)&data;

    kr = (*dev)->DeviceRequest(dev, &devRequest);

    if(kIOReturnSuccess != kr) {
        [NSException raise:@"DSBRRadioError" format:@"Error setting frequency!:%s", mach_error_string(kr)];
    }
}

- (void)dealloc
{
    (*dev)->USBDeviceClose(dev);
    (*dev)->Release(dev);
    [super dealloc];
}

@end


